/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Box;
import org.zkoss.zul.Button;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.impl.InputElement;

public class GridUtils {

	public static void setReadOnly(Grid grid, boolean readOnly) {
		if (grid.getRows() == null) {
			return;
		}
		
		for (int i = 0; i < grid.getRows().getVisibleItemCount(); i++) {
			for (int j = 0; j < grid.getColumns().getChildren().size(); j++) {
				setReadOnly(grid.getCell(i, j), readOnly);
			}
		}
	}
	
	private static void setReadOnly(Component widget, boolean readOnly) {
		if (widget == null) {
			return;
		}
		if (widget instanceof InputElement) {
			InputElement impl = (InputElement) widget;
			impl.setReadonly(readOnly);
		} else if (widget instanceof Checkbox) {
			Checkbox impl = (Checkbox) widget;
			impl.setDisabled(readOnly);
		} else if (widget instanceof Button) {
			Button impl = (Button) widget;
			impl.setDisabled(readOnly);
		} else if (widget instanceof Box) {
			for (int i = 0; i < widget.getChildren().size(); i++) {
				Component childWidget = (Component) widget.getChildren().get(i);
				setReadOnly(childWidget, readOnly);
			}
		}
	}

	/**
	 * Pre-condition: first cell must contain checkbox in the first cell
	 * 
	 * @param grid
	 * @return
	 */
	public static List<? extends Object> getCheckedDataInGrid(Grid grid) {
		List<Object> result = new ArrayList<Object>();

		for (int i = 0; i < grid.getRows().getChildren().size(); i++) {
			Row row = (Row) grid.getRows().getChildren().get(i);
			Checkbox checkbox = (Checkbox) grid.getCell(i, 0);
			if (checkbox.isChecked()) {
				result.add(row.getValue());
			}
		}

		return result;
	}

	/**
	 * Pre-condition: first cell must contain checkbox in the first cell
	 * 
	 * @param grid
	 * @param valueList
	 */
	public static void setCheckedDataInGrid(Grid grid, List<? extends Object> valueList) {
		for (int i = 0; i < grid.getRows().getChildren().size(); i++) {
			Checkbox checkbox = (Checkbox) grid.getCell(i, 0);
			Row row = (Row) grid.getRows().getChildren().get(i);
			boolean found = valueList.contains(row.getValue());
			
			checkbox.setChecked(found);
		}
	}

	public static void renderNonModelData(Grid grid, List<? extends Object> dataList, RowRenderer<Object> renderer) throws Exception {
		Rows rows = (Rows) grid.getRows();
		if (rows == null) {
			rows = new Rows();
			rows.setParent(grid);
		} else {
			while (rows.getFirstChild() != null) {
				rows.removeChild(rows.getFirstChild());
			}
		}
		for (Object data : dataList) {
			Row row = new Row();
			row.setParent(rows);
			renderer.render(row, data, rows.getChildren().indexOf(row));
		}
	}
}
