/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.sidebar;

import org.jumin.zkx.components.table.AbstractDataTable;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.ListitemRenderer;

/** 
 * The class <code>ValidationResultTable</code> is used for showing error in an editor.<br>
 *
 */
public class ValidationResultSidebarBlock extends SidebarBlock {

	private static final long serialVersionUID = 4045743714924181936L;

    public static final String SCLASS = "zkx-validation-result-sidebar-block";
	
	public static final String DEFAULT_EXPANDED_HEIGHT = "250px";
	
	public static final String DEFAULT_COLLAPSED_HEIGHT = "30px";
	
	private Listbox errorTable;
	
	@Override
	protected void createContents() {
		super.createContents();
		
		errorTable = new Listbox();
		appendContent(errorTable);
	}
	
	@Override
	protected void initProperties() {
	    super.initProperties();
	    setSclass(SCLASS);
	    
	    setTitle(ZkLabels.getLabel("errorSidebarBlock.title", "Validation Results"));
	    setHeight(DEFAULT_EXPANDED_HEIGHT);

	    errorTable.setVflex("true");
	    setClosable(true);
	}
	
	@Override
	protected void initEvents() {
	    super.initEvents();
	    
	    errorTable.addForward(AbstractDataTable.ON_ITEM_OPEN, this, AbstractDataTable.ON_ITEM_OPEN);
	}

	public ListitemRenderer<?> getErrorItemRenderer() {
		return errorTable.getItemRenderer();
	}

	public void setErrorItemRenderer(ListitemRenderer<?> renderer) {
		errorTable.setItemRenderer(renderer);
	}

	public ListModel<?> getModel() {
		return errorTable.getModel();
	}
	
	public void setModel(ListModel<?> listModel) {
		errorTable.setModel(listModel);
	}
	
	public boolean isValidationSuccess() {
		return ValidationResultSidebarBlock.SCLASS.equals(getSclass());
	}
	
	public void setValidationSuccess(boolean success) {
		if (success) {
			setSclass(ValidationResultSidebarBlock.SCLASS.replace("-result-", "-success-"));
			errorTable.setVisible(errorTable.getModel() != null && errorTable.getModel().getSize() > 0);
			setTitle(ZkLabels.getLabel("general.validation.success", "Validation Succeeded"));
		} else {
			setSclass(ValidationResultSidebarBlock.SCLASS);
			errorTable.setVisible(true);
			setTitle(ZkLabels.getLabel("general.validation.fail", "Validation Failed"));
		}
		setOpen(!success);
		invalidate();
	}
	
}
