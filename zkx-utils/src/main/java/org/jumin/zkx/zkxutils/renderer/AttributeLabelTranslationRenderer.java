/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.renderer;

import java.io.Serializable;

import org.jumin.common.utils.translation.ITranslatable;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

public class AttributeLabelTranslationRenderer implements ComboitemRenderer<Object>, Serializable {

	private static final long serialVersionUID = -178646402664791857L;
	
	private static final String ITEM_HEIGHT = "18px";

    /* (non-Javadoc)
	 * @see org.zkoss.zul.ComboitemRenderer#render(org.zkoss.zul.Comboitem, java.lang.Object)
	 */
	@Override
	public void render(Comboitem item, Object data, int index) throws Exception {
		item.setValue(data);
		
		if (data == null) {
			item.setLabel(ZkLabels.getLabel("general.null", ""));
			item.setHeight(ITEM_HEIGHT);
			return;
		}
		
		if (data instanceof ITranslatable) {
			ITranslatable t = (ITranslatable) data;
			item.setLabel(ZkLabels.getLabel(t.getLabelProperty(), t.getLabel()));

			if(item.getLabel().equals(""))
				item.setHeight(ITEM_HEIGHT);
			return;
		}
		
		// Last resort
		item.setLabel("" + data);
	}

}
