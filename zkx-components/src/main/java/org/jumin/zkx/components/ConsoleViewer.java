/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class ConsoleViewer extends Window {

    private static final long serialVersionUID = 5460519064729088619L;
    
    private Textbox console;

    public ConsoleViewer() {
        super();

        createContents();
        initProperties();
        initEvents();
    }

    private void initProperties() {
        setTitle("Console Viewer");
        setSizable(true);
        setClosable(true);

        console.setMultiline(true);
        console.setReadonly(true);
        console.setHflex("true");
        console.setVflex("true");
        console.setStyle("resize:none;");
        console.setWidgetAttribute("wrap", "off");
    }

    private void initEvents() {
        addForward(Events.ON_OK, this, Events.ON_CLOSE);
        addForward(Events.ON_CANCEL, this, Events.ON_CLOSE);
    }

    private void createContents() {
        console = new Textbox();
        console.setParent(this);
    }

    public void setText(String text) {
        console.setText(text);
    }
}
