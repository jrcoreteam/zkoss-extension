/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

public class ApplicationConstants {
    public static final String COMPONENT_LOOKUP_DESCRIPTION = "comp-lookup-desc";

    public static final String QUEUE_SESSION_INTERNAL_ERROR = "SessionInternalError";

    public static final String SYSTEM_DEBUG_MODE_VAR = "system.debugModeVar";

    public static final String SESSION_USER_FORMAT_LOCALE = "app-user-format-locale";

    public static final String SESSION_USER_PREFERENCE = "app-user-preference";

    public static final String SESSION_USER_PRINCIPAL = "app-active-user";

    public static final String SESSION_USER_PRINCIPAL_EXISTANCE = "app-user-existence";

    public static final String WIDGET_VAR_ATTRIBUTE_NAME = "objectAttributeName";

    public static final String WIDGET_VAR_ATTRIBUTE_LABEL = "objectAttributeLabel";

    public static final String STYLE_UPPERCASE_TRANSFORM = "text-transform:uppercase;";

    public static final String SYSTEM_RUNTIME_ACTIVATE_STATIC_LABEL_VAR = "system.runforselenium";

    // needed for selenium
    public static final String WIDGET_STATIC_LABEL = "static_label";

    // needed to identify the widget type
    public static final String WIDGET_TYPE = "widgetType";
}
