/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Page;

public class ZulUtils {

	@SuppressWarnings("unchecked")
    public static <T> T loadComponentFromZulInJar(Execution execution, Class<? extends Object> clazzReference, Component parent,
			String zulFileName) {
		return (T) loadComponentFromZulInJar(execution, clazzReference, parent, zulFileName, null);
	}

	@SuppressWarnings("unchecked")
    public static <T> T loadComponentFromZulInJar(Execution execution, Class<? extends Object> clazzReference, Component parent,
			String zulFileName, Map<Object, Object> attributes) {
		Component comp = null;
		InputStream is = clazzReference.getResourceAsStream(zulFileName);
		if (is != null) {
			String content;
			try {
				content = IOUtils.toString(is);
				comp = execution.createComponentsDirectly(content, null, parent, attributes);
			} catch (IOException e) {
				MessageDialogUtils.captureInternalError(e);
			}
		}
		return (T) comp;
	}
	
	@SuppressWarnings("unchecked")
    public static <T> T loadComponentFromZulInJar(Execution execution, Class<? extends Object> clazzReference, Page page,
			String zulFileName) {
		return (T) loadComponentFromZulInJar(execution, clazzReference, page, zulFileName, null);
	}

	@SuppressWarnings("unchecked")
    public static <T> T loadComponentFromZulInJar(Execution execution, Class<? extends Object> clazzReference, Page page,
			String zulFileName, Map<Object, Object> attributes) {
		Component comp = null;
		InputStream is = clazzReference.getResourceAsStream(zulFileName);
		if (is != null) {
			String content;
			try {
				content = IOUtils.toString(is);
				comp = execution.createComponentsDirectly(content, null, null, attributes);
				comp.setPage(page);
			} catch (IOException e) {
				MessageDialogUtils.captureInternalError(e);
			}
		}
		return (T) comp;
	}
	
	@SuppressWarnings("unchecked")
    public static <T> T loadComponentFromZul(Execution execution, String url, Component parent, Map<Object, Object> attributes) {
		Component comp = execution.createComponents(url,parent, attributes);

		return (T) comp;
	}
}
