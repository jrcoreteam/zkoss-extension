/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zhtml.impl.AbstractTag;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.HtmlNativeComponent;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

/**
 * The class <code>LayoutUtils</code> is a helper class to layout org.zkoss.zul.Grid.
 * 
 * @version 23.11.2010 / jru : initial version <br>
 *          24.05.2012 / buch : set LayoutItemModel static, to be usable from outside <br>
 */
public class LayoutUtils implements Serializable {

	private static final long serialVersionUID = -7463744891014369935L;

	private static Logger log = LoggerFactory.getLogger(LayoutUtils.class);
	
	public static final String CELL_STYLE_TOP_ALIGN = "vertical-align:top;padding-top:6px;";

	private String itemAlignment = null;

	private String itemVerticalAlignment = "top";

	public static LayoutUtils getInstance() {
		return new LayoutUtils();
	}

	public HtmlNativeComponent gridLayoutComponentsNativeByModel(List<LayoutItemModel> layoutItemList, int columnCount,
	        boolean fullWidth) {
		return gridLayoutComponentsNativeByModel(layoutItemList, columnCount, null, null);
	}

	public HtmlNativeComponent gridLayoutComponentsNativeByModel(List<LayoutItemModel> layoutItemList, int columnCount,
	        int[] spaceColumnIndexList, String spaceColumnWidth) {
		String[] columnWidthList = extractColumnWidthList(layoutItemList);
		int[] columnSpanList = extractColumnSpanList(layoutItemList);
		int[] rowSpanList = extractRowSpanList(layoutItemList);
		List<? extends Component> componentList = extractComponentList(layoutItemList);
		String[] cellStyleList = extractCellStyleList(layoutItemList);
		String[] rowStyleList = extractRowStyleList(layoutItemList);

		return gridLayoutComponentsNative(componentList, columnCount, columnWidthList, columnSpanList, rowSpanList,
		        spaceColumnIndexList, spaceColumnWidth, cellStyleList, rowStyleList);
	}

	public HtmlNativeComponent gridLayoutComponentsNative(List<? extends Component> componentList, int columnCount,
	        String[] columnWidthList, int[] columnSpanList, int[] rowSpanList, int[] spaceColumnIndexList,
	        String spaceColumnWidth) {
		return gridLayoutComponentsNative(componentList, columnCount, columnWidthList, columnSpanList, rowSpanList,
		        spaceColumnIndexList, spaceColumnWidth, null, null);
	}

	public HtmlNativeComponent gridLayoutComponentsNative(List<? extends Component> componentList, int columnCount,
	        String[] columnWidthList, int[] columnSpanList, int[] rowSpanList, int[] spaceColumnIndexList,
	        String spaceColumnWidth, String[] cellStyleList, String[] rowStyleList) {
		HtmlNativeComponent layoutTable = new HtmlNativeComponent("table");
		layoutTable.setPrologContent("<tbody>");
		layoutTable.setEpilogContent("</tbody>");

		if (componentList == null) {
			return layoutTable;
		}

		if (columnCount < 1) {
			columnCount = 1;
		}

		HtmlNativeComponent row = null;
		int columnIndex = columnCount;

		int[] rowSpanArray = new int[columnCount];
		for (int i = 0; i < columnCount; i++) {
			rowSpanArray[i] = 1;
		}

		// get effective space column index
		int[] effectiveSpaceColumnIndexList = null;
		if (spaceColumnIndexList != null) {
			effectiveSpaceColumnIndexList = new int[spaceColumnIndexList.length];
			for (int i = 0; i < spaceColumnIndexList.length; i++) {
				effectiveSpaceColumnIndexList[i] = spaceColumnIndexList[i] + i;
			}
		}

		for (int i = 0; i < componentList.size(); i++) {
			if (columnCount < 2) {
				row = addNativeRow(layoutTable);
				columnIndex = 0;
			} else if (columnIndex >= columnCount) {
				row = addNativeRow(layoutTable);
				columnIndex = 0;
			}

			if (effectiveSpaceColumnIndexList != null && isSpaceColumnIndex(effectiveSpaceColumnIndexList, columnIndex)) {
				HtmlNativeComponent cell = new HtmlNativeComponent("td");
				cell.setDynamicProperty("style", "width: " + spaceColumnWidth + ";");
				cell.setParent(row);
				columnIndex++;
			}

			HtmlNativeComponent cell = new HtmlNativeComponent("td");

			// Set Row Spanning
			if (rowSpanList != null) {
				int rowSpan = 1;
				if (i < rowSpanList.length) {
					rowSpan = rowSpanList[i];
				}
				if (rowSpan > 1 && rowSpanArray[columnIndex] <= 1) {
					cell.setDynamicProperty("rowspan", "" + rowSpan);
					rowSpanArray[columnIndex] = rowSpan;
				} else if (rowSpanArray[columnIndex] > 1) {
					rowSpanArray[columnIndex]--;
					columnIndex++;
				}
			}

			// Set Column Spanning
			if (columnSpanList != null) {
				int columnSpan = 1;
				if (i < columnSpanList.length) {
					columnSpan = columnSpanList[i];
				}
				if ((columnIndex + columnSpan) > columnCount) {
					columnSpan = columnCount - columnIndex;
				}
				if (columnSpan > 1) {
					cell.setDynamicProperty("colspan", "" + columnSpan);
					columnIndex += (columnSpan - 1);
				}
			}

			// Set cell style or sclass
			applyStyleToComponent(cell, cellStyleList, i);
			// Set cell style or sclass
			applyStyleToComponent(row, rowStyleList, i);

			cell.setParent(row);

			componentList.get(i).setParent(cell);
			columnIndex++;
		}

		return layoutTable;
	}

	private HtmlNativeComponent addNativeRow(HtmlNativeComponent layoutTable) {
		HtmlNativeComponent row = new HtmlNativeComponent("tr");
		if (itemVerticalAlignment != null && !itemVerticalAlignment.equals("")) {
			row.setDynamicProperty("valign", itemVerticalAlignment);
		}
		if (itemAlignment != null && !itemAlignment.equals("")) {
			row.setDynamicProperty("align", itemAlignment);
		}
		row.setParent(layoutTable);

		return row;
	}

	public Table gridLayoutComponentsByModel(List<LayoutItemModel> layoutItemList, int columnCount) {
		return gridLayoutComponentsByModel(layoutItemList, columnCount, null, null);
	}

	public Table gridLayoutComponentsByModel(List<LayoutItemModel> layoutItemList, int columnCount,
	        String[] columnWidthList) {
		return gridLayoutComponentsByModel(layoutItemList, columnCount, columnWidthList, null, null);
	}

	public Table gridLayoutComponentsByModel(List<LayoutItemModel> layoutItemList, int columnCount,
	        int[] spaceColumnIndexList, String spaceColumnWidth) {
		return gridLayoutComponentsByModel(layoutItemList, columnCount, extractColumnWidthList(layoutItemList), spaceColumnIndexList, spaceColumnWidth);
	}
	
	public Table gridLayoutComponentsByModel(List<LayoutItemModel> layoutItemList, int columnCount,
	        String[] columnWidthList, int[] spaceColumnIndexList, String spaceColumnWidth) {
		int[] columnSpanList = extractColumnSpanList(layoutItemList);
		int[] rowSpanList = extractRowSpanList(layoutItemList);
		List<? extends Component> componentList = extractComponentList(layoutItemList);
		String[] cellStyleList = extractCellStyleList(layoutItemList);
		String[] rowStyleList = extractRowStyleList(layoutItemList);

		return gridLayoutComponents(componentList, columnCount, columnWidthList, columnSpanList, rowSpanList,
		        spaceColumnIndexList, spaceColumnWidth, cellStyleList, rowStyleList);
	}

	public Table gridLayoutComponents(List<? extends Component> componentList, int columnCount,
	        String[] columnWidthList, int[] columnSpanList, int[] rowSpanList, int[] spaceColumnIndexList,
	        String spaceColumnWidth) {
		return gridLayoutComponents(componentList, columnCount, columnWidthList, columnSpanList, rowSpanList,
		        spaceColumnIndexList, spaceColumnWidth, null, null);
	}

	public Table gridLayoutComponents(List<? extends Component> componentList, int columnCount,
	        String[] columnWidthList, int[] columnSpanList, int[] rowSpanList, int[] spaceColumnIndexList,
	        String spaceColumnWidth, String[] cellStyleList, String[] rowStyleList) {
		Table layoutTable = new Table();

		if (componentList == null) {
			return layoutTable;
		}

		if (columnCount < 1) {
			columnCount = 1;
		}

		Tr row = null;

		int[] rowSpanArray = new int[columnCount];
		for (int i = 0; i < columnCount; i++) {
			rowSpanArray[i] = 1;
		}

		// get effective space column index
		int[] effectiveSpaceColumnIndexList = new int[] {};
		if (spaceColumnIndexList != null) {
			effectiveSpaceColumnIndexList = new int[spaceColumnIndexList.length];
			for (int i = 0; i < spaceColumnIndexList.length; i++) {
				effectiveSpaceColumnIndexList[i] = spaceColumnIndexList[i] + i;
			}
		}

		int effectiveColumnCount = columnCount + effectiveSpaceColumnIndexList.length;
		int columnIndex = effectiveColumnCount;
		for (int i = 0; i < componentList.size(); i++) {
			if (effectiveColumnCount < 2) {
				row = addRow(layoutTable);
				columnIndex = 0;
			} else if (columnIndex >= effectiveColumnCount) {
				row = addRow(layoutTable);
				columnIndex = 0;
			}

			if (effectiveSpaceColumnIndexList != null && isSpaceColumnIndex(effectiveSpaceColumnIndexList, columnIndex)) {
				Td cell = new Td();
				cell.setStyle("width: " + spaceColumnWidth + ";");
				cell.setParent(row);
				columnIndex++;
			}

			Td cell = new Td();
			// cell.setStubonly("true");

			// Set Row Spanning
			if (rowSpanList != null) {
				int rowSpan = 1;
				if (i < rowSpanList.length) {
					rowSpan = rowSpanList[i];
				}
				if (rowSpan > 1 && rowSpanArray[columnIndex] <= 1) {
					cell.setDynamicProperty("rowspan", "" + rowSpan);
					rowSpanArray[columnIndex] = rowSpan;
				} else if (rowSpanArray[columnIndex] > 1) {
					rowSpanArray[columnIndex]--;
					columnIndex++;
				}
			}

			// Set Column Spanning
			if (columnSpanList != null) {
				int colSpan = 1;
				if (i < columnSpanList.length) {
					colSpan = columnSpanList[i];
				}
				if ((columnIndex + colSpan) > effectiveColumnCount) {
					colSpan = effectiveColumnCount - columnIndex;
				}
				if (colSpan > 1) {
					cell.setDynamicProperty("colspan", "" + colSpan);
					columnIndex += (colSpan - 1);
				}
			}

			// Set cell style or sclass
			applyStyleToComponent(cell, cellStyleList, i);
			// Set row style or sclass
			applyStyleToComponent(row, rowStyleList, i);

			cell.setParent(row);

			componentList.get(i).setParent(cell);
			columnIndex++;
		}

		return layoutTable;
	}

	private Tr addRow(Table layoutTable) {
		Tr row = new Tr();
		// row.setStubonly("true");

		if (itemVerticalAlignment != null && !itemVerticalAlignment.equals("")) {
			row.setDynamicProperty("valign", itemVerticalAlignment);
		}
		if (itemAlignment != null && !itemAlignment.equals("")) {
			row.setDynamicProperty("align", itemAlignment);
		}
		row.setParent(layoutTable);
		return row;
	}

	public Grid gridLayoutComponentsGridByModel(List<LayoutItemModel> layoutItemList, int columnCount,
	        String[] columnWidthList, int[] spaceColumnIndexList, String spaceColumnWidth) {
		int[] columnSpanList = extractColumnSpanList(layoutItemList);
		int[] rowSpanList = extractRowSpanList(layoutItemList);
		List<? extends Component> componentList = extractComponentList(layoutItemList);
		String[] cellStyleList = extractCellStyleList(layoutItemList);
		String[] rowStyleList = extractRowStyleList(layoutItemList);
		
		return gridLayoutComponentsGrid(componentList, columnCount, columnWidthList, columnSpanList, rowSpanList,
		        spaceColumnIndexList, spaceColumnWidth, cellStyleList, rowStyleList);
	}

	public Grid gridLayoutComponentsGrid(List<? extends Component> componentList, int columnCount,
	        String[] columnWidthList, int[] columnSpanList, int[] rowSpanList, int[] spaceColumnIndexList,
	        String spaceColumnWidth) {
		return gridLayoutComponentsGrid(componentList, columnCount, columnWidthList, columnSpanList, rowSpanList,
		        spaceColumnIndexList, spaceColumnWidth, null);
	}

	public Grid gridLayoutComponentsGrid(List<? extends Component> componentList, int columnCount,
	        String[] columnWidthList, int[] columnSpanList, int[] rowSpanList, int[] spaceColumnIndexList,
	        String spaceColumnWidth, String[] cellStyleList) {
		return gridLayoutComponentsGrid(componentList, columnCount, columnWidthList, columnSpanList, rowSpanList,
		        spaceColumnIndexList, spaceColumnWidth, cellStyleList, null);
	}
	
	public Grid gridLayoutComponentsGrid(List<? extends Component> componentList, int columnCount,
	        String[] columnWidthList, int[] columnSpanList, int[] rowSpanList, int[] spaceColumnIndexList,
	        String spaceColumnWidth, String[] cellStyleList, String[] rowStyleList) {
		log.debug("Start...");
		Grid layoutTable = new Grid();
		Columns columns = new Columns();
		columns.setParent(layoutTable);
		Rows rows = new Rows();
		rows.setParent(layoutTable);

		if (componentList == null) {
			return layoutTable;
		}
		int effectiveColumnCount = columnCount;
		if (effectiveColumnCount < 1) {
			effectiveColumnCount = 1;
		}

		// creating columns
		for (int i = 0; i < effectiveColumnCount; i++) {
			Column column = new Column();
			String width = "";
			if (columnWidthList != null && i < columnWidthList.length) {
				width = columnWidthList[i];
			}
			if (width != null) {
				applyWidth(column, width);
			}
			column.setParent(columns);
		}

		List<Column> spaceColumnList = new ArrayList<Column>();
		if (spaceColumnIndexList != null) {
			// Inserting space column(s) from the end
			for (int i = spaceColumnIndexList.length - 1; i >= 0; i--) {
				int spaceColumnIndex = spaceColumnIndexList[i] - 1;
				if (spaceColumnIndex >= 0) {
					Component referenceColumn = null;
					if (spaceColumnIndex + 1 < columns.getChildren().size()) {
						referenceColumn = (Component) columns.getChildren().get(spaceColumnIndex + 1);
					}
					Column spaceColumn = new Column();
					if (referenceColumn == null) {
						columns.appendChild(spaceColumn);
					} else {
						columns.insertBefore(spaceColumn, referenceColumn);
					}
					// insert always from the front
					spaceColumnList.add(0, spaceColumn);
					effectiveColumnCount++;
				}
			}
		}

		// get effective space column index
		int[] effectiveSpaceColumnIndexList = new int[spaceColumnList.size()];
		for (int i = 0; i < spaceColumnList.size(); i++) {
			Column spaceColumn = spaceColumnList.get(i);
			effectiveSpaceColumnIndexList[i] = columns.getChildren().indexOf(spaceColumn);
			spaceColumn.setWidth(spaceColumnWidth);
		}

		Row row = null;
		int columnIndex = effectiveColumnCount;

		int[] rowSpanArray = new int[effectiveColumnCount];
		for (int i = 0; i < effectiveColumnCount; i++) {
			rowSpanArray[i] = 1;
		}

		boolean scheduleNextRowSpanDecrement = false;
		boolean rowHasCellWithRowSpan = false;
		for (int i = 0; i < componentList.size(); i++) {
			if (effectiveSpaceColumnIndexList != null && isSpaceColumnIndex(effectiveSpaceColumnIndexList, columnIndex)) {
				Cell cell = new Cell();
				cell.setParent(row);
				columnIndex++;
			}

			if (effectiveColumnCount < 2) {
				// single column table
				if (scheduleNextRowSpanDecrement) {
					decreaseRowSpan(rowSpanArray, 1);
					scheduleNextRowSpanDecrement = false;
				}
				row = addGridRow(layoutTable);
				columnIndex = 0;
				rowHasCellWithRowSpan = hasRowSpan(rowSpanArray);
				if (rowHasCellWithRowSpan)
					scheduleNextRowSpanDecrement = true;
				columnIndex = 0;
			} else if (columnIndex >= effectiveColumnCount) {
				if (scheduleNextRowSpanDecrement) {
					decreaseRowSpan(rowSpanArray, 1);
					scheduleNextRowSpanDecrement = false;
				}
				row = addGridRow(layoutTable);
				columnIndex = 0;
				rowHasCellWithRowSpan = hasRowSpan(rowSpanArray);
				if (rowHasCellWithRowSpan) {
					scheduleNextRowSpanDecrement = true;
					for (int j = columnIndex; j < rowSpanArray.length; j++) {
						if (rowSpanArray[j] > 1)
							columnIndex++;
					}

					if (columnIndex >= effectiveColumnCount) {
						row = addGridRow(layoutTable);
						columnIndex = 0;
					}
				}
			}

			Cell cell = new Cell();

			// Set Row Spanning
			if (rowSpanList != null) {
				int rowSpan = 1;
				if (i < rowSpanList.length) {
					rowSpan = rowSpanList[i];
				}
				if (rowSpan > 1 && rowSpanArray[columnIndex] < 2) {
					cell.setRowspan(rowSpan);
					rowSpanArray[columnIndex] = rowSpan;
				} else if (rowSpanArray[columnIndex] > 1) {
					// rowSpanArray[columnIndex]--;
					columnIndex++;
				}
			}

			// Set Column Spanning
			if (columnSpanList != null) {
				int colSpan = 1;
				if (i < columnSpanList.length) {
					colSpan = columnSpanList[i];
					// offset colspan with number of spaceColumn
					if (colSpan > 1) {
						colSpan = setSpaceColumnOffset(columnIndex, colSpan, effectiveSpaceColumnIndexList);
					}
				}
				if ((columnIndex + colSpan) > effectiveColumnCount) {
					colSpan = effectiveColumnCount - columnIndex;
				}
				if (colSpan > 1) {
					cell.setColspan(colSpan);
					columnIndex += (colSpan - 1);
				}
			}

			// Set cell style or sclass
			applyStyleToComponent(cell, cellStyleList, i);

			// Set row style or sclass
			applyStyleToComponent(row, rowStyleList, i);

			cell.setParent(row);

			componentList.get(i).setParent(cell);
			columnIndex++;
		}

		return layoutTable;
	}

	private void decreaseRowSpan(int[] rowSpanArray, int decrementValue) {
		for (int i = 0; i < rowSpanArray.length; i++) {
			if (rowSpanArray[i] > 1)
				rowSpanArray[i] = rowSpanArray[i] - Math.abs(decrementValue);
		}
	}

	private boolean hasRowSpan(int[] rowSpanArray) {
		for (int i = 0; i < rowSpanArray.length; i++) {
			if (rowSpanArray[i] > 1)
				return true;
		}

		return false;
	}
	
	private int setSpaceColumnOffset(int columnIndex, int columnSpan, int[] spaceColumnIndexList) {
		int newColumnSpan = columnSpan;
		for (int i = 0; i < spaceColumnIndexList.length; i++) {
			if (spaceColumnIndexList[i] > columnIndex && spaceColumnIndexList[i] < (columnIndex + newColumnSpan)) {
				newColumnSpan++;
			}
		}
		return newColumnSpan;
	}

	private boolean isSpaceColumnIndex(int[] spaceColumnIndexList, int columnIndex) {
		for (int i = 0; i < spaceColumnIndexList.length; i++) {
			if (spaceColumnIndexList[i] == columnIndex) {
				return true;
			}
		}
		return false;
	}

	private Row addGridRow(Grid layoutTable) {
		Row row = new Row();

		if (itemVerticalAlignment != null && !itemVerticalAlignment.equals("")) {
			row.setValign(itemVerticalAlignment);
		}
		if (itemAlignment != null && !itemAlignment.equals("")) {
			row.setAlign(itemAlignment);
		}
		row.setParent(layoutTable.getRows());
		return row;
	}

	public void setItemAlignment(String itemAlignment) {
		this.itemAlignment = itemAlignment;
	}

	public String getItemAlignment() {
		return itemAlignment;
	}

	public void setItemVerticalAlignment(String itemVerticalAlignment) {
		this.itemVerticalAlignment = itemVerticalAlignment;
	}

	public String getItemVerticalAlignment() {
		return itemVerticalAlignment;
	}

	private List<? extends Component> extractComponentList(List<LayoutItemModel> layoutItemList) {
		List<Component> componentList = new ArrayList<Component>();

		for (LayoutItemModel itemModel : layoutItemList) {
			componentList.add(itemModel.getWidget());
		}

		return componentList;
	}

	private int[] extractRowSpanList(List<LayoutItemModel> layoutItemList) {
		int[] intArray = new int[layoutItemList.size()];
		for (int i = 0; i < layoutItemList.size(); i++) {
			intArray[i] = layoutItemList.get(i).getRowSpan();
		}
		return intArray;
	}

	private int[] extractColumnSpanList(List<LayoutItemModel> layoutItemList) {
		int[] intArray = new int[layoutItemList.size()];
		for (int i = 0; i < layoutItemList.size(); i++) {
			intArray[i] = layoutItemList.get(i).getColumnSpan();
		}
		return intArray;
	}

	private String[] extractCellStyleList(List<LayoutItemModel> layoutItemList) {
		String[] styleArray = new String[layoutItemList.size()];
		for (int i = 0; i < layoutItemList.size(); i++) {
			styleArray[i] = layoutItemList.get(i).getCellStyle();
		}
		return styleArray;
	}

	private String[] extractColumnWidthList(List<LayoutItemModel> layoutItemList) {
		String[] widthArray = new String[layoutItemList.size()];
		for (int i = 0; i < layoutItemList.size(); i++) {
			widthArray[i] = layoutItemList.get(i).getColumnWidth();
		}
		return widthArray;
	}

	private String[] extractRowStyleList(List<LayoutItemModel> layoutItemList) {
		String[] styleArray = new String[layoutItemList.size()];
		for (int i = 0; i < layoutItemList.size(); i++) {
			styleArray[i] = layoutItemList.get(i).getRowStyle();
		}
		return styleArray;
	}

	private void applyStyleToComponent(HtmlBasedComponent comp, String[] styleList, int index) {
		if (styleList != null) {
			String style = null;
			if (index < styleList.length) {
				style = styleList[index];
			}
			if (style != null && !style.equals("")) {
				if (style.indexOf(":") > 0) {
					// the style value contains ":" which is commonly used for defining style
					String existingStyle = comp.getStyle();
					if (existingStyle == null || existingStyle.equals(""))
						comp.setStyle(style);
					else
						comp.setStyle(existingStyle + style);
				} else
					// the style value is an sclass name
					comp.setSclass(style);
			}
		}
	}

	private void applyStyleToComponent(HtmlNativeComponent comp, String[] styleList, int index) {
		if (styleList != null) {
			String style = null;
			if (index < styleList.length) {
				style = styleList[index];
			}
			if (style != null && !style.equals("")) {
				if (style.indexOf(":") > 0) {
					// the style value contains ":" which is commonly used for defining style
					String existingStyle = (String) comp.getDynamicProperty("style"); 
					if (existingStyle == null || existingStyle.equals(""))  
						comp.setDynamicProperty("style", style);
					else
						comp.setDynamicProperty("style", existingStyle + style);
				} else
					// the style value is an sclass name
					comp.setDynamicProperty("class", style);
			}
		}
	}

	private void applyStyleToComponent(AbstractTag comp, String[] styleList, int index) {
		if (styleList != null) {
			String style = null;
			if (index < styleList.length) {
				style = styleList[index];
			}
			if (style != null && !style.equals("")) {
				if (style.indexOf(":") > 0) {
					// the style value contains ":" which is commonly used for defining style
					String existingStyle = comp.getStyle();
					if (existingStyle == null || existingStyle.equals(""))
						comp.setStyle(style);
					else
						comp.setStyle(existingStyle + style);
				} else
					// the style value is an sclass name
					comp.setSclass(style);
			}
		}
	}
	
	public static final void applyWidth(HtmlBasedComponent component, String width) {
		if (width == null || width.equals("") || component == null)
			return;

		if (width.matches("[1-9][0-9]*[Pp][Xx]")) {
			component.setWidth(width);
		} else if (width.matches("([1-9][0-9]*|[Tt][Rr][Uu][Ee]|[Ff][Aa][Ll][Ss][Ee]|[Mm][Ii][Nn])")
		        && component instanceof HtmlBasedComponent) {
			component.setHflex(width);
		}
	}

	public static class LayoutItemModel implements Serializable {
		private static final long serialVersionUID = 7666322711897287432L;

		private Component widget;
		private String columnWidth;
		private int columnSpan = 1;
		private int rowSpan = 1;
		private String cellStyle = null;
		private String rowStyle = null;

		public LayoutItemModel(Component widget) {
			this(widget, 1, 1, null, null);
		}

		public LayoutItemModel(Component widget, int columnSpan) {
			this(widget, columnSpan, 1, null, null);
		}

		public LayoutItemModel(Component widget, int columnSpan, int rowSpan) {
			this(widget, columnSpan, rowSpan, null, null);
		}

		public LayoutItemModel(Component widget, int columnSpan, int rowSpan, String cellStyle) {
			this(widget, columnSpan, rowSpan, cellStyle, null);
		}

		public LayoutItemModel(Component widget, int columnSpan, int rowSpan, String cellStyle, String rowStyle) {
			super();
			this.widget = widget;
			this.columnSpan = columnSpan;
			this.rowSpan = rowSpan;
			this.cellStyle = cellStyle;
			this.rowStyle = rowStyle;
		}

		public Component getWidget() {
			return widget;
		}

		public int getColumnSpan() {
			return columnSpan;
		}

		public void setColumnSpan(int columnSpan) {
			this.columnSpan = columnSpan;
		}

		public int getRowSpan() {
			return rowSpan;
		}

		public void setRowSpan(int rowSpan) {
			this.rowSpan = rowSpan;
		}

		public String getCellStyle() {
			return cellStyle;
		}

		public void setCellStyle(String cellStyle) {
			this.cellStyle = cellStyle;
		}

		public String getRowStyle() {
			return rowStyle;
		}

		public void setRowStyle(String rowStyle) {
			this.rowStyle = rowStyle;
		}

		public String getColumnWidth() {
			return columnWidth;
		}

		public void setColumnWidth(String columnWidth) {
			this.columnWidth = columnWidth;
		}
	}
}
