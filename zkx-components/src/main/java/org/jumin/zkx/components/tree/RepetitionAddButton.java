/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;

public class RepetitionAddButton extends Button {

	private static final long serialVersionUID = -2289605555413523359L;

    private boolean disabled = false;
	
	private XmlTreerow row;
	
	private XmlChoiceOptionCombobox combobox;
	
	public RepetitionAddButton(String label, XmlChoiceOptionCombobox combobox, XmlTreerow row) {
		super(label);
		this.row = row;
		this.combobox = combobox;
		initEvents();
		initProperties();
		initListeners();
	}
	
	private void initProperties() {
		setSclass("jrx-msg-tree-button jrx-msg-tree-add-button");
	}
	
	private void initEvents() {
		if (combobox != null) {
			combobox.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					Combobox widget = (Combobox) event.getTarget();
					if (widget.getSelectedIndex() < 1) {
						setDisabled(true);
					} else {
						setDisabled(false);
					}
				}
			});
		}
	}
	
	private void initListeners() {
		this.addEventListener(Events.ON_CLICK, new RepetitionAddButtonListener(this.row));
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		if (this.disabled) {
			setSclass("jrx-msg-tree-button jrx-msg-tree-add-button-disabled");
		} else {
			setSclass("jrx-msg-tree-button jrx-msg-tree-add-button");
		}
	}
	
	public XmlTreerow getRow() {
		return row;
	}
}
