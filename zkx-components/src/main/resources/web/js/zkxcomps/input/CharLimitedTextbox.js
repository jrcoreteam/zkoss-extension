zkxcomps.input.CharLimitedTextbox = zk.$extends(zul.inp.Textbox, {
	_allowedChars : '', // default value
	getAllowedChars : function() {
		return this._allowedChars;
	},
	setAllowedChars : function(allowedChars) {
//		if (this._allowedChars != allowedChars) {
			this._allowedChars = allowedChars;
//		}
	},
	getZclass: function () {
		var zcs = this._zclass;
		return zcs != null ? zcs: "z-textbox";
	},
    doKeyPress_: function(evt) {
        if (zk.ie && evt.keyCode == 13) {
        	// Skip checking allowance of <ENTER> on IE. Safari, Firefox, and Chrome can handle this well.
        	this.$supers('doKeyPress_', arguments);
        } else if (this.getAllowedChars().length > 0 && this._shallIgnore(evt, this.getAllowedChars())) {
        	// skip this key
    	} else {
    		this.$supers('doKeyPress_', arguments);
    	}
    }
});