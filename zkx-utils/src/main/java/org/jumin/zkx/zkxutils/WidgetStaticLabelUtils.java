/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;

/**
 * The class <code>WidgetStaticLabelUtil</code> is used for providing static
 * label to the UI widgets in a test environment.<br>
 * 
 */
public class WidgetStaticLabelUtils implements Serializable {

    private static final long serialVersionUID = -6408116466652591348L;
    
    private boolean testingMode = false;

    private WidgetStaticLabelUtils() {
        String sysPropValue = System.getProperty(ApplicationConstants.SYSTEM_RUNTIME_ACTIVATE_STATIC_LABEL_VAR);
        if (sysPropValue != null && Boolean.parseBoolean(sysPropValue)) {
            testingMode = true;
        }
    }

    /**
     * 
     * @return an instance of <code>SeleniumWidgetLabel</code>
     */
    public static WidgetStaticLabelUtils getInstance() {
        return new WidgetStaticLabelUtils();
    }

    public boolean isTestingMode() {
        return testingMode;
    }

    /**
     * This method sets the provided label as static label to a widget.
     * <p>
     * 
     * @param widget
     *            is the widget to process
     * @param label
     *            is the label to set when running on testing runtime
     *            environment
     */
    public void setLabel(Component widget, String label) {
        if (widget == null || label == null)
            return;

        if (testingMode) {
            label = StringUtils.remove(label, '\r');
            label = StringUtils.remove(label, '\n');
            label = StringUtils.remove(label, '\t');
            String existingStaticLabel = (String) widget.getAttribute(ApplicationConstants.WIDGET_STATIC_LABEL);
            if (existingStaticLabel == null || existingStaticLabel.equals("")) {
                widget.setAttribute(ApplicationConstants.WIDGET_STATIC_LABEL, label);
            }
            widget.setClientAttribute(ApplicationConstants.WIDGET_STATIC_LABEL, label);
        }
    }
}
