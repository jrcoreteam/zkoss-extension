/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.dialog;

import java.util.List;

import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Window;

public abstract class SelectionDialog<T> extends Window {

    private static final long serialVersionUID = -8437146194227329008L;

    protected WidgetStaticLabelUtils widgetStaticLabelUtil = null;

    private ListModel<T> model;

    public SelectionDialog() {
        super();
        widgetStaticLabelUtil = WidgetStaticLabelUtils.getInstance();
    }

    protected void createContents() {
        createHeader(this);

        createBody(this);

        createFooter(this);
    }

    protected void createBody(Component parent) {
    }

    protected void createFooter(Component parent) {
    }

    protected void initEvents() {
    }

    protected void initProperties() {
        setBorder(true);
        setClosable(true);
        setPosition("center");
    }

    protected void createHeader(Component parent) {
        Separator sep = new Separator();
        sep.setParent(parent);
    }

    public ListModel<T> getModel() {
        return model;
    }

    public void setModel(ListModel<T> model) {
        this.model = model;
    }

    public void beforeCancel() {
        setVisible(false);
    }

    public void beforeOK() {
        setVisible(false);
    }

    public abstract Listitem getSelection();

    public abstract List<Listitem> getSelections();

    public abstract void selectItems(Object[] keyArray);

    public void selectItemsById(Object[] keyArray) {

    }

    public abstract void clearSelections();
}
