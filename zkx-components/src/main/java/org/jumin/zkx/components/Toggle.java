/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.Button;

public class Toggle extends Button {

    private static final long serialVersionUID = -6135719102252667341L;

    private static final String ICON_SCLASS_COLLAPSED = "z-icon-cevron-right";
    
    private static final String ICON_SCLASS_EXPAND = "z-icon-cevron-down";
    
    private boolean disabled;

    public Toggle() {
        super();

        initProperties();
        initEvents();
    }

    protected void initEvents() {
        addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                toggle();
            }
        });
    }

    private void toggle() {
        if (disabled)
            return; // Cannot toggle when it is disabled.

        if (isOpen()) {
            setOpen(false);
        } else {
            setOpen(true);
        }
    }

    protected void initProperties() {
        setSclass(WidgetStyleConstants.BUTTON_NAKED_ICON);
        setIconSclass(ICON_SCLASS_COLLAPSED);
    }

    public void setOpen(boolean open) {
        if (open != isOpen()) {
            if (open) {
                setIconSclass(ICON_SCLASS_EXPAND);
            } else {
                setIconSclass(ICON_SCLASS_COLLAPSED);
            }
            Events.postEvent(new OpenEvent(Events.ON_OPEN, this, isOpen()));
        }
    }

    public boolean isOpen() {
        return getIconSclass().equals(ICON_SCLASS_EXPAND);
    }

    public void setDisabled(boolean disabled) {
        if (this.disabled != disabled) {
            this.disabled = disabled;
            smartUpdate("disabled", disabled);
        }
    }

    public boolean isDisabled() {
        return disabled;
    }
}
