/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.dialog;

import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Constraint;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class InputDialog extends Window {

	private static final long serialVersionUID = 763536721738350567L;

    private Label fieldLabel;
	
	private Textbox fieldValue;
	
	private DialogButtonBar buttonBar;
	
	private String state;

	public InputDialog() {
		super();
		createContents();
		initProperties();
		initEvents();
	}

	private void initEvents() {
		buttonBar.addEventListener(Events.ON_OK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (isInputValid() && !fieldValue.getText().trim().equals("")) {
					Events.postEvent(Events.ON_OK, buttonBar.getParent(), fieldValue.getText());
				} else {
					fieldValue.setFocus(true);
					fieldValue.select();
				}
			}
		});
		buttonBar.addForward(Events.ON_CANCEL, this, Events.ON_CANCEL);
	}

	private void initProperties() {
		setBorder("normal");
		setTitle("Input");
		setWidth("480px");
	}

	private void createContents() {
		Hbox layout = new Hbox();
		layout.setAlign("center");
		layout.setWidth("100%");
		layout.setSpacing("5px");
		layout.setParent(this);
		
		fieldLabel = new Label();
		fieldLabel.setParent(layout);
		
		fieldValue = new Textbox();
		fieldValue.setHflex("true");
		fieldValue.setParent(layout);
		
		buttonBar = new DialogButtonBar(ZkLabels.getLabel("general.ok"), ZkLabels.getLabel("general.cancel"));
		buttonBar.setParent(this);
	}

	public void setFieldLabel(String label) {
		fieldLabel.setValue(label);
	}

	public void setFieldValue(String value) {
		fieldValue.setText(value);
	}
	
	public String getFieldValue() {
		return fieldValue.getText();
	}
	
	public void setInputConstraint(Constraint constraint) {
		fieldValue.setConstraint(constraint);
	}
	
	public boolean isInputValid() {
		return fieldValue.isValid();
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}
	
	public void selectValue() {
		fieldValue.select();
		fieldValue.setFocus(true);
	}
}
