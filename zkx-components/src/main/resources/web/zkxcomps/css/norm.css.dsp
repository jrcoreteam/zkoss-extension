<%@ page contentType="text/css;charset=UTF-8" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>

textarea.z-textbox {
    resize: none;
}
.z-window {
	background-color: #d9dcdf;
}

/* Toolbar and StackedIcon */
.z-toolbarbutton .z-toolbarbutton-content, a.z-toolbarbutton .fa-stack {
	border: 1px solid rgba(0,0,0,0.4);
}
a.z-toolbarbutton .fa-stack {
	width: 32px;
	height: 32px;
	font-size: 18px;
	font-weight: normal;
	font-style: normal;
	color: rgba(0,0,0,0.57);
	background-color: transparent;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	-o-border-radius: 4px;
	-ms-border-radius: 4px;
	border-radius: 4px;
	padding: 6px 6px;
	line-height: 18px;
}
a.z-toolbarbutton:hover .fa-stack {
	color:#fff;border-color:transparent;background-color:#4fb7ff;
}
a.z-toolbarbutton:active .fa-stack {
	color:#fff;border-color:transparent;background-color:#0064ed;
}
a.z-toolbarbutton:active .fa-stack {
	color:#fff;border-color:#ffa516;background-color:#0093f9;
}
a.z-toolbarbutton[disabled] .fa-stack {
	color: rgba(0,0,0,0.34) !important;
	border-color:#d9d9d9;
    background-color: #d9d9d9;
    cursor: default !important;
}
a.z-toolbarbutton .fa-stack > i {
	margin-right: 4px;
	max-height: 20px;
	max-width: 20px;
	vertical-align: middle;
	left: unset;
}

