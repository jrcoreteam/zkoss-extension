/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class ButtonBarModel implements Serializable {
    
    private static final long serialVersionUID = 1390575146066690902L;

    private SortedMap<String, StateFunctionAssignment> stateFunctionCatalog;
    
    private Map<String, SortedMap<String, StateFunctionAssignment>> accessIndexedCatalog;
    
    private Map<String, UserFunction> functionCatalog;
    
    private String context;

    public ButtonBarModel() {
	    functionCatalog = new TreeMap<String, UserFunction>();
	    stateFunctionCatalog = new TreeMap<String, StateFunctionAssignment>();
	    accessIndexedCatalog = new Hashtable<String, SortedMap<String, StateFunctionAssignment>>();
    }
    
	public SortedMap<String, StateFunctionAssignment> getStateFunctionCatalog() {
	    return stateFunctionCatalog;
    }

	public void setStateFunctionCatalog(SortedMap<String, StateFunctionAssignment> stateFunctionCatalog) {
	    this.stateFunctionCatalog = stateFunctionCatalog;
    }

	public Map<String, UserFunction> getFunctionCatalog() {
	    return functionCatalog;
    }

	public void setFunctionCatalog(Map<String, UserFunction> functionCatalog) {
	    this.functionCatalog = functionCatalog;
    }

	public String getContext() {
	    return context;
    }

	public void setContext(String context) {
	    this.context = context;
    }

	public Map<String, SortedMap<String, StateFunctionAssignment>> getAccessIndexedCatalog() {
	    return accessIndexedCatalog;
    }
	
}
