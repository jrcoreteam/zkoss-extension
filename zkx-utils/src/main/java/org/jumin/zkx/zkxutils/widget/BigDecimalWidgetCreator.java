/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.math.BigDecimal;

import org.zkoss.zul.Doublebox;

public class BigDecimalWidgetCreator extends AbstractDoubleboxWidgetCreator<BigDecimal> {

	private static final long serialVersionUID = 6723207332568741446L;

    @Override
    public BigDecimal getWidgetValue(Doublebox widget) {
	    return BigDecimal.valueOf(widget.getValue());
    }

	@Override
    public void setWidgetValue(Doublebox widget, BigDecimal value) {
	    widget.setValue(value == null ? null : value.doubleValue());
    }

}
