/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.utils.DatetimeTextParser;
import org.jumin.common.utils.InvalidDateTimeValueException;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Constraint;

public class DatetimeConstraint implements Constraint, Serializable {
	
	private static final long serialVersionUID = -8660908584863440096L;

    private TimeZone timezone = null;
	
	private Locale locale = null;
	
	protected DatetimeTextParser parser = new DatetimeTextParser();
	
	private boolean skip = false;
	
	public DatetimeConstraint() {
		this(TimeZone.getDefault(), Locale.getDefault());
	}
	
	public DatetimeConstraint(TimeZone timezone, Locale locale) {
		super();
		this.timezone = timezone;
		this.locale = locale;
	}

	@Override
	public void validate(Component comp, Object value) throws WrongValueException {
		if (isSkip()) {
			// skip validation
			return;
		}
		try {
			Date[] dates = parser.getDatesFromString((String) value, getLocale(), getTimezone());
			if (dates.length > 1 && dates[0] != null && dates[1] != null) {
				if (dates[0].after(dates[1])) {
					throw new WrongValueException(comp, "Invalid date range specified. End date/time must be after start date/time.");
				}
			}
		} catch (InvalidDateTimeValueException e) {
			throw new WrongValueException(comp, e.getMessage());
		} catch (ParseException e) {
			throw new WrongValueException(comp, e.getMessage());
		} catch (NumberFormatException e) {
			throw new WrongValueException(comp, "Invalid keyword provided! A valid keyword is one of : "  + parser.getUsedKeywordMap().keySet().toString());
		}
	}

	public TimeZone getTimezone() {
		return timezone;
	}

	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void setSkip(boolean skip) {
		this.skip = skip;
	}

	public boolean isSkip() {
		return skip;
	}

}
