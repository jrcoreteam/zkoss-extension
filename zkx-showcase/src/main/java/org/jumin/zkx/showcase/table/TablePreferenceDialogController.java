/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.showcase.table;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.preferences.Preference;
import org.jumin.common.preferences.table.TableConfiguration;
import org.jumin.common.preferences.table.TableConfigurationLoader;
import org.jumin.zkx.components.table.TablePreferenceDialog;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Window;

public class TablePreferenceDialogController extends SelectorComposer<Window> {

    private static final long serialVersionUID = -5618503010243266681L;

    private static Logger logger = LoggerFactory.getLogger(TablePreferenceDialogController.class);

    private TablePreferenceDialog dialog;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        initProperties();

        dialog = (TablePreferenceDialog) comp;
        initDataTableConfig();
    }

    private void initDataTableConfig() {
        logger.debug("Start...");

        InputStream configStream = getPage().getDesktop().getWebApp()
                .getResourceAsStream("/table/system-table-column-config.xml");
        TableConfigurationLoader loader = new TableConfigurationLoader();
        try {
            List<Preference> prefList = loader.load(configStream);
            if (prefList.isEmpty())
                return;

            TableConfiguration tableConfig = loader.convert(prefList.get(0));
            dialog.setModel(tableConfig);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                configStream.close();
            } catch (IOException e) {
            }
        }

        logger.debug("End...");
    }

    private void initProperties() {
        if (UserContextUtil.getUserFormatLocale(getPage()) == null)
            UserContextUtil.setUserFormatLocale(getPage(), Locale.getDefault());
        if (UserContextUtil.getUserTimeZone(getPage()) == null)
            UserContextUtil.setUserTimeZone(getPage(), TimeZone.getDefault());
    }

}
