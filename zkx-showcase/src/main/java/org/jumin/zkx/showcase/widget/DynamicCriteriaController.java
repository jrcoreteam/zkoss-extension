/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.showcase.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.jumin.common.utils.model.LabelDefinition;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.widget.AbstractWidgetCreator;
import org.jumin.zkx.zkxutils.widget.ComboboxWidgetCreator;
import org.jumin.zkx.zkxutils.widget.DateWidgetCreator;
import org.jumin.zkx.zkxutils.widget.DoubleWidgetCreator;
import org.jumin.zkx.zkxutils.widget.SearchAttributeDefinitionUtil;
import org.jumin.zkx.zkxutils.widget.SimpleStringWidgetCreator;
import org.jumin.zkx.zkxutils.widget.WidgetAttributeDefinition;
import org.jumin.zkx.zkxutils.widget.WidgetAttributeDefinitionComboitemRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Space;
import org.zkoss.zul.Spinner;
import org.zkoss.zul.Vlayout;

public class DynamicCriteriaController extends SelectorComposer<Vlayout> {

    private static final long serialVersionUID = 1940246095017665402L;

    private static Logger log = LoggerFactory.getLogger(DynamicCriteriaController.class);

    private List<WidgetAttributeDefinition<?>> wadList = new ArrayList<>();

    public static final String SCLASS_FIELD_WIDGET = "zkx-field-widget";
    public static final String SCLASS_OPERATOR_WIDGET = "zkx-operator-widget";
    public static final String SCLASS_VALUE_WIDGET = "zkx-value-widget";

    public static final String VAR_PREVIOUS_VALUE = "zkx-previous-value";

    @Wire
    private Grid criteriaGrid;

    @Wire
    private Spinner columnCount;

    @Override
    public void doAfterCompose(Vlayout comp) throws Exception {
        super.doAfterCompose(comp);
        initModel();
        initCriteriaGrid();
    }

    private void initModel() {
        wadList.add(new WidgetAttributeDefinition<String>("ID", new LabelDefinition("general.id", "ID"), String.class,
                new SimpleStringWidgetCreator()));
        wadList.add(new WidgetAttributeDefinition<String>("FIRST_NAME",
                new LabelDefinition("general.person.firstName", "First Name"), String.class,
                new SimpleStringWidgetCreator()));
        wadList.add(new WidgetAttributeDefinition<String>("LAST_NAME",
                new LabelDefinition("general.person.lastName", "Last Name"), String.class,
                new SimpleStringWidgetCreator()));
        wadList.add(new WidgetAttributeDefinition<String>("GENDER", new LabelDefinition("general.gender", "Gender"),
                String.class, new ComboboxWidgetCreator(Arrays.asList(new String[] { "Male", "Female" })), "120px"));
        wadList.add(new WidgetAttributeDefinition<String>("PLACE_OF_BIRTH",
                new LabelDefinition("general.person.placeOfBirth", "Place of Birth"), String.class,
                new SimpleStringWidgetCreator()));
        wadList.add(new WidgetAttributeDefinition<Date>("DATE_OF_BIRTH",
                new LabelDefinition("general.person.dateOfBirth", "Date of Birth"), Date.class, new DateWidgetCreator(),
                "120px"));
        wadList.add(
                new WidgetAttributeDefinition<Double>("SALARY", new LabelDefinition("general.person.salary", "Salary"),
                        Double.class, new DoubleWidgetCreator(), "120px"));
        wadList.add(new WidgetAttributeDefinition<String>("REMARKS",
                new LabelDefinition("general.person.remarks", "Remarks"), String.class,
                new SimpleStringWidgetCreator(5)));
    }

    private void initCriteriaGrid() {
        renderColumns();
        clearRows();
        addEmptyRow();
    }

    private void renderColumns() {
        clearColumns();
        Column column = null;

        for (int i = 0; i < columnCount.getValue(); i++) {
            if (i > 0) {
                // Space column
                column = new Column();
                column.setWidth("20px");
                column.setParent(criteriaGrid.getColumns());
            }

            column = new Column("Field");
            column.setHflex("1");
            column.setParent(criteriaGrid.getColumns());

            column = new Column("Operator");
            column.setWidth("80px");
            column.setParent(criteriaGrid.getColumns());

            column = new Column("Criteria Value");
            column.setHflex("2");
            column.setParent(criteriaGrid.getColumns());
        }
    }

    private void addEmptyRow() {
        Row row = (Row) criteriaGrid.getRows().getLastChild();
        if (row != null && row.getChildren().size() < 2)
            return; // Skip because the last row is an empty row

        row = new Row();
        row.setParent(criteriaGrid.getRows());

        Combobox fieldCombobox = new Combobox();
        fieldCombobox.setParent(row);
        fieldCombobox.setSclass(SCLASS_FIELD_WIDGET);
        fieldCombobox.setHflex("true");
        fieldCombobox.setModel(new ListModelList<WidgetAttributeDefinition<?>>(wadList));
        fieldCombobox.setItemRenderer(new WidgetAttributeDefinitionComboitemRenderer());
        fieldCombobox.addForward(Events.ON_SELECT, fieldCombobox, Events.ON_OK);
        fieldCombobox.addEventListener(Events.ON_OK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                renderOperatorAndValueWidget(event.getTarget());
            }
        });
    }

    protected void renderOperatorAndValueWidget(Component refWidget) {
        if (!(refWidget instanceof HtmlBasedComponent))
            return;

        Combobox fieldCombobox = (Combobox) getPreviousSiblingBySclass(refWidget, SCLASS_FIELD_WIDGET,
                SCLASS_VALUE_WIDGET);

        if (fieldCombobox == null)
            return;

        HtmlBasedComponent operatorWidget = getNextSiblingBySclass(fieldCombobox, SCLASS_OPERATOR_WIDGET,
                SCLASS_FIELD_WIDGET);

        HtmlBasedComponent valueWidget = getNextSiblingBySclass(fieldCombobox, SCLASS_VALUE_WIDGET,
                SCLASS_FIELD_WIDGET);

        Comboitem selectedItem = fieldCombobox.getSelectedItem();
        if (selectedItem == null || selectedItem.getValue() == null) {
            if (operatorWidget != null)
                operatorWidget.detach();
            if (valueWidget != null)
                valueWidget.detach();

            // Check the row
            Row row = ComponentUtils.getParentByClass(refWidget, Row.class);
            if (row.getChildren().size() == 1) {
                removeThisRow(refWidget);
            }

            return;
        }

        WidgetAttributeDefinition<?> wad = selectedItem.getValue();
        if (wad.equals(fieldCombobox.getAttribute(VAR_PREVIOUS_VALUE))) {
            // Same selection as before
            log.info("Same field selection as before!");
            return;
        }

        if (operatorWidget != null)
            operatorWidget.detach();
        if (valueWidget != null)
            valueWidget.detach();

        String existingSclass = "";
        if (SearchAttributeDefinitionUtil.requireOperator(wad.getDataType())) {
            operatorWidget = SearchAttributeDefinitionUtil.getOperatorWidgetByDataType(wad.getDataType());
            operatorWidget.setParent(refWidget.getParent());
            operatorWidget.setHflex("true");
            existingSclass = operatorWidget.getSclass();
            operatorWidget.setSclass(SCLASS_OPERATOR_WIDGET
                    + (existingSclass == null || existingSclass.equals("") ? "" : " " + existingSclass));
        } else {
            Space space = new Space();
            space.setParent(refWidget.getParent());
            space.setSclass(SCLASS_OPERATOR_WIDGET);
        }

        @SuppressWarnings("unchecked")
        AbstractWidgetCreator<HtmlBasedComponent, Object> awc = (AbstractWidgetCreator<HtmlBasedComponent, Object>) wad
                .getWidgetCreator();
        valueWidget = awc.createWidget(refWidget.getParent());
        if (wad.getWidth() != null && wad.getWidth().trim().length() > 0)
            valueWidget.setWidth(wad.getWidth());
        else
            valueWidget.setHflex("true");
        existingSclass = valueWidget.getSclass();
        valueWidget.setSclass(SCLASS_VALUE_WIDGET
                + (existingSclass == null || existingSclass.equals("") ? "" : " " + existingSclass));
        awc.setWidgetValue(valueWidget, wad.getValue());

        fieldCombobox.setAttribute(VAR_PREVIOUS_VALUE, selectedItem.getValue());

        addEmptyRow();
    }

    private void removeThisRow(Component refWidget) {
        if (refWidget == null)
            return;

        Row row = ComponentUtils.getParentByClass(refWidget, Row.class);
        if (row != null) {
            criteriaGrid.getRows().removeChild(row);
        }
    }

    private HtmlBasedComponent getPreviousSiblingBySclass(Component widget, String sclass, String stopSclass) {
        Component temp = widget;
        while (temp != null) {
            if (temp instanceof HtmlBasedComponent && ((HtmlBasedComponent) temp).getSclass().indexOf(sclass) >= 0) {
                return (HtmlBasedComponent) temp;
            }

            if (temp.getPreviousSibling() != null) {
                temp = temp.getPreviousSibling();

                if (temp instanceof HtmlBasedComponent
                        && ((HtmlBasedComponent) temp).getSclass().indexOf(stopSclass) >= 0) {
                    return null; // Stop!
                }
            } else {
                return null;
            }
        }

        return null;
    }

    private HtmlBasedComponent getNextSiblingBySclass(Component widget, String sclass, String stopSclass) {
        Component temp = widget;
        while (temp != null) {
            if (temp instanceof HtmlBasedComponent && ((HtmlBasedComponent) temp).getSclass().indexOf(sclass) >= 0) {
                return (HtmlBasedComponent) temp;
            }

            if (temp.getNextSibling() != null) {
                temp = temp.getNextSibling();

                if (temp instanceof HtmlBasedComponent
                        && ((HtmlBasedComponent) temp).getSclass().indexOf(stopSclass) >= 0) {
                    return null; // Stop!
                }
            } else {
                temp = null;
            }
        }

        return null;
    }

    private void clearColumns() {
        while (criteriaGrid.getColumns().getFirstChild() != null) {
            criteriaGrid.getColumns().removeChild(criteriaGrid.getColumns().getFirstChild());
        }
    }

    private void clearRows() {
        while (criteriaGrid.getRows().getFirstChild() != null) {
            criteriaGrid.getRows().removeChild(criteriaGrid.getRows().getFirstChild());
        }
    }

    @Listen("onClick=button")
    public void rearrangeColumns() {
        int currentColumnCount = criteriaGrid.getColumns().getChildren().size();
        int newColumnCount = columnCount.intValue();

        if (newColumnCount == currentColumnCount) {
            // ignore
            return;
        } else if (newColumnCount > currentColumnCount) {
            increaseColumns(newColumnCount - currentColumnCount);
        } else {
            decreaseColumns(currentColumnCount - newColumnCount);
        }

        System.out.println(String.format("Set Column from %d to %d.", currentColumnCount, newColumnCount));
    }

    private void decreaseColumns(int newColumnCount) {

    }

    private void increaseColumns(int noOfColumns) {
        Columns cols = criteriaGrid.getColumns();

        for (int i = 0; i < noOfColumns; i++) {
            // Space column
            Column column = new Column();
            column.setWidth("20px");
            column.setParent(cols);

            column = new Column("Field");
            column.setHflex("1");
            column.setParent(cols);

            column = new Column("Operator");
            column.setWidth("80px");
            column.setParent(cols);

            column = new Column("Criteria Value");
            column.setHflex("2");
            column.setParent(cols);
        }

        int newColumnSize = cols.getChildren().size();

        // Transpose the existing input widgets
        Rows rows = criteriaGrid.getRows();
        int i = 0;
        while (i < rows.getChildren().size()) {
            Row targetRow = (Row) rows.getChildren().get(i);
            int j = i + 1;
            boolean fillingComplete = false;
            while (j < rows.getChildren().size()) {
                Row sourceRow = (Row) rows.getChildren().get(j);
                List<Component[]> widgetSetList = collectWidgetsInRow(sourceRow);
                for (Component[] widgetSet : widgetSetList) {
                    Space space = new Space();
                    space.setHflex("true");
                    space.setParent(targetRow);

                    for (Component widget : widgetSet) {
                        widget.setParent(targetRow);
                    }

                    // Cleanup distance space widget in source row
                    Component firstChild = sourceRow.getFirstChild();
                    while (firstChild != null && firstChild instanceof Space) {
                        sourceRow.removeChild(firstChild);
                        firstChild = sourceRow.getFirstChild();
                    }

                    if (targetRow.getChildren().size() >= newColumnSize) {
                        // Filling of target row is completed!
                        fillingComplete = true;
                        break;
                    }
                }
                if (fillingComplete) {
                    break;
                }
                j++;
            }
        }
    }

    private List<Component[]> collectWidgetsInRow(Row row) {
        List<Component[]> widgetSetList = new ArrayList<>();

        Component[] widgetSet = new Component[3];
        int i = 4;
        for (Object obj : row.getChildren()) {
            if (!(obj instanceof HtmlBasedComponent)) {
                continue;
            }
            if (i > widgetSet.length) {
                widgetSet = new Component[3];
                widgetSetList.add(widgetSet);
                i = 0;
            }
            HtmlBasedComponent comp = (HtmlBasedComponent) obj;
            if (comp instanceof Space) {
                if (SCLASS_OPERATOR_WIDGET.equals(comp.getSclass())) {
                    widgetSet[i] = comp;
                    i++;
                }
            } else {
                widgetSet[i] = comp;
                i++;
            }
        }

        return widgetSetList;
    }

}
