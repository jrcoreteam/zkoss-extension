/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import java.util.Set;

import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxTerm;

public class XmlTreeRootDefinition extends AbstractXmlTreeNodeDefinition {

    private static final long serialVersionUID = -6559083433302143348L;
    
    private String labelKey;
    private boolean optional = false;

    public XmlTreeRootDefinition() {
        super();
    }
	
    public String getLabelKey() {
        return labelKey;
    }
	
    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }
	
    public boolean isOptional() {
        return optional;
    }
	
    public void setOptional(boolean optional) {
        this.optional = optional;
    }
	
    @Override
    public int hashCode() {
        final int prime = 31; 
        int totalHashCode = 1;

        totalHashCode = super.hashCode();
        totalHashCode = prime * totalHashCode + ((labelKey == null) ? 0 : labelKey.hashCode());
        totalHashCode += prime * totalHashCode + (optional ? 1231 : 1237);
		
        return totalHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!super.equals(obj)) 
            return false;

		if (!(obj instanceof XmlTreeRootDefinition))
			return false;

		final XmlTreeRootDefinition other = (XmlTreeRootDefinition) obj;
		if (labelKey == null) {
			if (other.labelKey != null)
				return false;
		} else if (!labelKey.equals(other.labelKey)) {
			return false;
		}
		if (optional != other.optional)
			return false;

		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append(super.toString());
		sb.append("labelKey: ").append(labelKey).append("\n");
		sb.append("optional: ").append(optional).append("\n");

		return sb.toString();
	}
	
	public JrxElement getJrxElement() {
		Set<JrxTerm<?>> termSet = super.getJrxTermSet();
		if (termSet.isEmpty()) return null;
	    return (JrxElement) termSet.iterator().next();
	}
	
	public void setJrxElement(JrxElement jrxElement) {
		Set<JrxTerm<?>> termSet = super.getJrxTermSet();
		if (!termSet.isEmpty()) {
			termSet.clear();
		}
		termSet.add(jrxElement);
		super.setJrxTermSet(termSet);
	}
	
	public XmlTreeRootDefinition clone() {
		XmlTreeRootDefinition clone = new XmlTreeRootDefinition();
		
		clone.labelKey = this.labelKey;
		clone.optional = this.optional;
		clone.setNamespacePrefixes(this.getNamespacePrefixes());
		clone.addXpath(this.getXpaths());
		return clone;
	}
	
}
