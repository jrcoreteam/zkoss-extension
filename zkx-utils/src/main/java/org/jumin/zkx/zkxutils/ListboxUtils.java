/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;
import org.zkoss.image.Image;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelMap;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ext.Sortable;
import org.zkoss.zul.impl.InputElement;
import org.zkoss.zul.impl.XulElement;

public class ListboxUtils {

	public static List<String> getListitems(Listbox listbox) {
		List<String> items = new ArrayList<String>();

		if (listbox.getChildren() != null) {
			for (int i = 0; i < listbox.getChildren().size(); i++) {
				String item = String.valueOf(((Listitem) listbox.getChildren().get(i)).getLabel());
				items.add(item);
			}
		}

		return items;
	}

	public static boolean containsListitemLabel(Listbox listbox, String label) {
		if (listbox.getChildren() != null) {
			for (int i = 0; i < listbox.getChildren().size(); i++) {
				if (label.equals((((Listitem) listbox.getChildren().get(i)).getLabel()))) {
					return true;
				}
			}
		}

		return false;
	}

	public static int indexOfListitemLabel(Listbox listbox, String label) {
		if (listbox.getChildren() != null) {
			for (int i = 0; i < listbox.getChildren().size(); i++) {
				if (label.equals((((Listitem) listbox.getChildren().get(i)).getLabel()))) {
					return i;
				}
			}
		}

		return -1;
	}

	public static int indexOfListitemValue(Listbox listbox, Object value) {
		if (listbox.getChildren() != null) {
			for (int i = 0; i < listbox.getChildren().size(); i++) {
				if (value.equals((((Listitem) listbox.getChildren().get(i)).getValue()))) {
					return i;
				}
			}
		}

		return -1;
	}

	public static String getStringFromListitemSet(List<Listitem> selections) {
		StringBuffer result = new StringBuffer();

		for (int i = 0; i < selections.size(); i++) {
			if (result.length() > 0) {
				result.append(", ");
			}
			result.append(selections.get(i).getLabel());
		}

		return result.toString();
	}

	public static List<? extends Object> getSelectedValueListFromListitem(Listbox listbox) {
		List<Object> result = new ArrayList<Object>();

		for (Iterator<Listitem> it = listbox.getSelectedItems().iterator(); it.hasNext();) {
			result.add(it.next().getValue());
		}

		return result;
	}

	public static void setReadOnly(Listbox listbox, boolean readOnly) {
		for (int i = 0; i < listbox.getItemCount(); i++) {
			Listitem item = listbox.getItemAtIndex(i);
			for (int j = 0; j < item.getChildren().size(); j++) {
				Listcell cell = (Listcell) item.getChildren().get(j);
				if (cell.getChildren().size() > 0) {
					XulElement widget = (XulElement) cell.getChildren().get(0);
					if (widget instanceof InputElement) {
						InputElement impl = (InputElement) widget;
						impl.setReadonly(readOnly);
					} else if (widget instanceof Checkbox) {
						Checkbox impl = (Checkbox) widget;
						impl.setDisabled(readOnly);
					}
				}
			}
		}
	}

	public static void setSelectionFromValueList(Listbox listbox, List<? extends Object> selectedValueList) {
		listbox.clearSelection();
		for (Object value : selectedValueList) {
			int index = ListboxUtils.indexOfListitemValue(listbox, value);
			listbox.getItemAtIndex(index).setSelected(true);
		}
	}

	public static Listcell createCell(Listitem listitem, String label, Object value) {
		Listcell cell = new Listcell(label);
		cell.setValue(value);
		cell.setParent(listitem);
		cell.addForward(Events.ON_DOUBLE_CLICK, cell.getListbox(), Events.ON_DOUBLE_CLICK);

		return cell;
	}

	public static Listcell createCell(Listitem listitem, Image image) {
		Listcell cell = new Listcell();
		cell.setImageContent(image);
		cell.setParent(listitem);
		cell.addForward(Events.ON_DOUBLE_CLICK, cell.getListbox(), Events.ON_DOUBLE_CLICK);

		return cell;
	}

	public static Listcell createCell(Listitem listitem, String label) {
		return createCell(listitem, label, label);
	}

	public static Listhead getListheadByEntity(Object object) {
		Listhead head = new Listhead();

		List<Method> methodList = getGetterMethod(object.getClass());
		for (Method method : methodList) {
			Listheader header = new Listheader();
			String effectiveLabel = ZkLabels.getLabel(method.getClass().getName() + "." + method.getName(), "");
			if (effectiveLabel.equals("")) {
				effectiveLabel = ZkLabels.getLabel(method.getClass().getSimpleName() + "." + method.getName(), "");
			}
			if (effectiveLabel.equals("")) {
				effectiveLabel = ZkLabels.getLabel(method.getName());
			}

			header.setLabel(effectiveLabel);
			header.setParent(head);
		}
		return head;
	}

	public static void getListitemByEntity(Listitem item, Object object) {
		List<Method> methodList = getGetterMethod(object.getClass());
		for (Method method : methodList) {
			Listcell cell = new Listcell();
			String value = "";
			try {
				value = "" + method.invoke(object, (Object[]) null);
			} catch (IllegalArgumentException e) {
				value = "";
			} catch (IllegalAccessException e) {
				value = "";
			} catch (InvocationTargetException e) {
				value = "";
			}
			cell.setLabel(value);
			cell.setParent(item);
		}
	}

	public static List<Method> getGetterMethod(Class<?> clazz) {
		List<Method> methodList = new ArrayList<Method>();

		Method[] methods = clazz.getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			if (method.getName().startsWith("get") || method.getName().startsWith("is")) {
				if (method.getParameterTypes() == null || method.getParameterTypes().length == 0) {
					methodList.add(method);
				}
			}
		}

		return methodList;
	}

	public static void sortFirstColumn(Listbox listbox) {
		if (listbox.getListModel() == null || listbox.getListModel().getSize() < 1)
			return;

		Listhead head = listbox.getListhead();
		if (head != null) {

			Listheader header = (Listheader) head.getFirstChild();
			if (header != null) {
				header.sort(!"descending".equalsIgnoreCase(header.getSortDirection()), true);
			} else {
				blindSort(listbox, true);
			}
		} else {
			blindSort(listbox, true);
		}
	}

	public static void blindSort(Listbox listbox, boolean ascending) {
		ListModel<?> lm = listbox.getModel();
		if (lm instanceof List<?>) {
			Collections.sort((List<?>) lm, new SimpleObjectComparator<Object>(ascending));
			return;
		}

		if (lm instanceof Sortable) {
			@SuppressWarnings("unchecked")
			Sortable<Object> sortable = (Sortable<Object>) lm;
			sortable.sort(new SimpleObjectComparator<Object>(ascending), true);
			return;
		}

		if (lm instanceof ListModelMap) {
			ListModelMap<?, ?> lmm = (ListModelMap<?, ?>) lm;
			SortedMap<Object, Object> sortedObjectMap = new TreeMap<Object, Object>();

			for (int i = 0; i < lmm.getSize(); i++) {
				@SuppressWarnings("unchecked")
				Entry<Object, Object> entry = (Entry<Object, Object>) lmm.getElementAt(i);
				sortedObjectMap.put(entry.getKey(), entry.getValue());
			}

			lmm.clear();
			listbox.setModel(new ListModelMap<Object, Object>(sortedObjectMap));
		}

	}

	public static class SimpleObjectComparator<T> implements Comparator<T> {

		private boolean ascending;

		public SimpleObjectComparator(boolean ascending) {
			this.ascending = ascending;
		}

		@SuppressWarnings("unchecked")
		@Override
		public int compare(T obj1, T obj2) {
			int result = 0;
			if (obj1 instanceof Comparator && obj2 instanceof Comparator) {
				result = ((Comparator<Object>) obj1).compare(obj1, obj2);
			} else if (obj1 instanceof Map.Entry && obj2 instanceof Map.Entry) {
				SimpleObjectComparator<Object> keyComparator = new SimpleObjectComparator<Object>(ascending);
				Map.Entry<?, ?> entry1 = (Entry<?, ?>) obj1;
				Map.Entry<?, ?> entry2 = (Entry<?, ?>) obj2;
				result = keyComparator.compare(entry1.getKey(), entry2.getKey());
			} else if (obj1.getClass().isArray() && obj1.getClass().isInstance(obj2)) {
				String obj1String = ArrayUtils.toString(obj1, "");
				String obj2String = ArrayUtils.toString(obj2, "");
				result = obj1String.compareTo(obj2String);
			} else {
				String obj1String = "" + obj1;
				String obj2String = "" + obj2;
				result = obj1String.compareTo(obj2String);
			}
			return ascending ? result : (0 - result);
		}
	}
}
