/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.translator;

import java.util.Locale;
import java.util.TimeZone;

import org.zkoss.zul.impl.XulElement;

public abstract class ObjectValueListcellTranslator<T> extends ObjectValueListcellLocaleAwareTranslator<T> {

    private static final long serialVersionUID = 3936291359216620087L;

    public abstract void translate(XulElement cell, T object, String extra);

    @Override
    public void translate(XulElement cell, Locale locale, TimeZone timeZone, T object, String extra) {
        this.translate(cell, object, extra);
    }
}
