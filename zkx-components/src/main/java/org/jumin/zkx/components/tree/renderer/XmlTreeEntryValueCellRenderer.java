/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.renderer;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.jumin.common.base.services.ServiceConnector;
import org.jumin.common.xsutils.JrxXmlModelUtil;
import org.jumin.common.xsutils.XmlSchemaConstants;
import org.jumin.common.xsutils.XsdToXmlUtil;
import org.jumin.common.xsutils.model.JrxAttribute;
import org.jumin.common.xsutils.model.JrxChoiceGroup;
import org.jumin.common.xsutils.model.JrxDeclaration;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxElementGroup;
import org.jumin.common.xsutils.model.JrxGroup;
import org.jumin.common.xsutils.model.JrxNode;
import org.jumin.common.xsutils.model.JrxTerm;
import org.jumin.zkx.components.lookup.DefaultXmlLookupFactory;
import org.jumin.zkx.components.lookup.XmlLookupFactory;
import org.jumin.zkx.components.lookup.XmlTreeAbstractLookupWidgetCreator;
import org.jumin.zkx.components.tree.XmlChoiceOptionCombobox;
import org.jumin.zkx.components.tree.XmlTreeInputWidgetEventHandler;
import org.jumin.zkx.components.tree.XmlTreeSubNodeChangeListener;
import org.jumin.zkx.components.tree.XmlTreeUtil;
import org.jumin.zkx.components.tree.constraint.TreeSyntaxPatternConstraint;
import org.jumin.zkx.components.tree.constraint.TreeSyntaxTextLengthConstraint;
import org.jumin.zkx.zkxutils.ComboboxUtils;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.renderer.KeyValueStringArrayComboitemRenderer;
import org.jumin.zkx.zkxutils.renderer.LabelValueDescriptionComboitemRenderer;
import org.jumin.zkx.zkxutils.tree.XmlTreeModel;
import org.jumin.zkx.zkxutils.tree.XmlTreeNodeDefault;
import org.jumin.zkx.zkxutils.tree.XmlTreeTagLabelUtil;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.impl.InputElement;

import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSFacet;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSRestrictionSimpleType;
import com.sun.xml.xsom.XSSimpleType;
import com.sun.xml.xsom.XSType;

public class XmlTreeEntryValueCellRenderer extends AbstractXmlTreeValueCellRenderer {

    private static final long serialVersionUID = -8900492754432433280L;

    private static Logger log = LoggerFactory.getLogger(XmlTreeEntryValueCellRenderer.class);

    private XsdToXmlUtil xsdUtil = new XsdToXmlUtil();

    private ServiceConnector connector = null;

    public static final String ATTR_CURRENCY = "Ccy";

    public static final String BASE_TYPE_BOOLEAN = "boolean";

    public static final String BASE_TYPE_DATE = "date";

    public static final String BASE_TYPE_DATETIME = "datetime";

    public static final String BASE_TYPE_DECIMAL = "decimal";

    public static final String BASE_TYPE_STRING = "string";

    protected XmlLookupFactory lookupFactory = null;

    protected XmlTreeTagLabelUtil tagLabelUtil = null;

    public XmlTreeEntryValueCellRenderer() {
        super();

        lookupFactory = new DefaultXmlLookupFactory();
        tagLabelUtil = new XmlTreeTagLabelUtil();
    }

    @Override
    public Treecell createSecondCell(XmlTreerow row, XmlTreeNodeDefault[] treeNodeDefaults) {
        Treecell secondCell = null;
        if (row.getData() instanceof JrxElementGroup) {
            secondCell = createSecondCell(row, (JrxElementGroup) row.getData());
        } else if (row.getData() instanceof JrxGroup) {
            secondCell = createSecondCell(row, (JrxGroup) row.getData());
        } else {
            secondCell = createSecondCell(row, (JrxElement) row.getData(), treeNodeDefaults);
        }
        
        if (XmlTreeNodeDefault.isReadonly(treeNodeDefaults)) {
            List<Combobox> comboboxList = ComponentUtils.getChildrenByClass(secondCell, Combobox.class);
            if (comboboxList != null && comboboxList.size() > 0) {
                for (Combobox combobox : comboboxList) {
                    combobox.setDisabled(true);
                }
            }
        }

        return secondCell;
    }

    private Treecell createSecondCell(XmlTreerow row, JrxElement jrxElement,
            XmlTreeNodeDefault[] treeNodeDefaults) {
        Treecell cell = new Treecell();
        cell.setSclass("jrx-msg-tree-mid-cell");

        XmlTreeNodeDefault[] elementNodeDefaults = null;
        if (treeNodeDefaults != null) {
            List<XmlTreeNodeDefault> elementNodeDefaultList = new ArrayList<>();
            for (XmlTreeNodeDefault nodeDefault : treeNodeDefaults) {
                if (nodeDefault.getXpath().indexOf("/@") < 0) {
                    // Collect real element node default
                    elementNodeDefaultList.add(nodeDefault);
                }
            }
            elementNodeDefaults = elementNodeDefaultList.toArray(new XmlTreeNodeDefault[] {});
        }

        if (elementNodeDefaults != null && !XmlTreeNodeDefault.isVisible(elementNodeDefaults)) {
            return cell;
        }

        JrxElement jrxEffectiveElement = jrxElement;
        if (jrxElement.getChildrenBlock() != null && jrxElement.getChildrenBlock().getElements().size() == 1) {
            // Node with single child

            // Check the child count from schema
            if (jrxElement.getChildrenBlock().getXsdDeclaration() != null) {
                XSModelGroup xsModelGroup = jrxElement.getChildrenBlock().getXsdDeclaration();
                if (xsModelGroup.getChildren().length == 1) {
                    JrxTerm<?> jrxTerm = jrxElement.getChildrenBlock().getElements().get(0);
                    if (jrxTerm instanceof JrxElement && ((JrxElement) jrxTerm).isLeaf() && !jrxTerm.isRepetitive()) {
                        jrxEffectiveElement = (JrxElement) jrxTerm;
                    }
                }
            }

        }

        if (jrxEffectiveElement.isLeaf()) {
            // Simple type
            if (EditorMode.VIEW.equals(getEditorMode()) && jrxEffectiveElement.getXmlElement().hasAttributes()) {
                String value = null;
                if (jrxEffectiveElement.getXmlElement() != null) {
                    value = jrxEffectiveElement.getXmlElement().getAttribute(ATTR_CURRENCY);
                }
                if (value != null && !value.equals("")) {
                    Label subWidget = new Label(value + " ");
                    subWidget.setSclass("jrx-labelvalue");
                    subWidget.setParent(cell);
                }
            } else if (jrxEffectiveElement.getAttributeList().size() == 1) {
                JrxAttribute jrxAttribute = jrxEffectiveElement.getAttributeList().get(0);
                if (ATTR_CURRENCY.equals(jrxAttribute.getSimpleName())) {
                    HtmlBasedComponent subWidget = null;
                    List<XmlTreeNodeDefault> attrNodeDefaults = new ArrayList<>();
                    String xpathSuffix = "/@" + jrxAttribute.getSimpleName();
                    if (treeNodeDefaults != null) {
                        for (XmlTreeNodeDefault nodeDefault : treeNodeDefaults) {
                            if (nodeDefault.getXpath().endsWith(xpathSuffix)) {
                                attrNodeDefaults.add(nodeDefault);
                            }
                        }
                    }

                    String value = null;
                    if (jrxEffectiveElement.getXmlElement() != null) {
                        value = jrxEffectiveElement.getXmlElement().getAttribute(ATTR_CURRENCY);
                    }
                    if (attrNodeDefaults.size() > 0
                            && XmlTreeNodeDefault.isReadonly(attrNodeDefaults
                                    .toArray(new XmlTreeNodeDefault[] {}))) {
                        subWidget = new Label();
                        subWidget.setSclass("jrx-labelvalue");
                        if (value != null && !value.equals("")) {
                            ((Label) subWidget).setValue(value + " ");
                        }
                    } else {
                        subWidget = getEntryWidget(jrxAttribute, row,
                                attrNodeDefaults.toArray(new XmlTreeNodeDefault[] {}));
                    }
                    if (subWidget != null) {
                        subWidget.setParent(cell);
                    }
                } else {
                    log.warn(jrxAttribute.getName() + " is not handled!");
                }
            }

            HtmlBasedComponent widget = null;
            if (XmlTreeNodeDefault.isReadonly(elementNodeDefaults)) {
                String value = null;
                if (jrxEffectiveElement.getXmlElement() != null) {
                    value = jrxEffectiveElement.getXmlElement().getTextContent();
                    value = clearValueFromApplicationSpecificEscape(value);
                }

                if (value != null) {
                    if (jrxEffectiveElement.getXsdDeclaration() != null) {
                        // This block shall be retrieved from configuration
                        XmlTreeAbstractLookupWidgetCreator widgetCreator = getWidgetCreator(jrxEffectiveElement, connector, value, row);
                        if (widgetCreator != null) {
                            // If there is any creator registered
                            widget = widgetCreator.createViewModeWidget();
                            widget.setParent(cell);
                        }
                    }

                    if (widget == null) {
                        // Readonly per definition and shall be displayed as label.
                        widget = new Label();
                        widget.setParent(cell);
                        widget.setSclass("jrx-labelvalue");
                        widget.setHflex("true");
                        ((Label) widget).setValue(value);
                    }
                }
            } else {
                widget = getEntryWidget(jrxEffectiveElement, row, elementNodeDefaults);
                if (widget != null) {
                    widget.setParent(cell);
                    if (elementNodeDefaults != null) {
                        if (XmlTreeNodeDefault.isReadonly(elementNodeDefaults) && widget instanceof InputElement) {
                            ((InputElement) widget).setReadonly(true);
                        }
                    }
                    widget.setHflex("true");
                }
            }

            if (widget != null && elementNodeDefaults != null && elementNodeDefaults.length > 0) {
                for (XmlTreeNodeDefault treeNodeDefault : elementNodeDefaults) {
                    if (treeNodeDefault.getSubscribeEvent() != null && !treeNodeDefault.getSubscribeEvent().equals("")) {
                        // Subscribe event from message tree view
// @formatter:off
//                        MessageObjectDetailTreeView<?> msgTreeView = ComponentUtils.getParentByClass(row,
//                                MessageObjectDetailTreeView.class);
//                        if (msgTreeView != null) {
//                            String eventName = treeNodeDefault.getSubscribeEvent();
//                            msgTreeView.subscribeWidgetUpdateEvent(eventName, widget);
//                        }
// @formatter:on
                    }
                }
            }
        } else if (jrxEffectiveElement.getChildrenBlock() != null
                && jrxEffectiveElement.getChildrenBlock().getElements() != null
                && jrxEffectiveElement.getChildrenBlock().getElements().size() == 1) {
            // Complex type with single child
            JrxTerm<?> jrxChildTerm = jrxEffectiveElement.getChildrenBlock().getElements().get(0);

            if (jrxChildTerm instanceof JrxChoiceGroup) {
                // In-direct choice
                JrxChoiceGroup jrxChildChoiceGroup = (JrxChoiceGroup) jrxChildTerm;

                createChoiceEntry(row, cell, jrxChildChoiceGroup, jrxEffectiveElement.isRepetitive());
            } else {
                if (XmlTreeModel.DEFAULT_DOCUMENT_TAG.equals(jrxEffectiveElement.getSimpleName())) {
                    JrxElement jrxDocumentRootElement = (JrxElement) jrxEffectiveElement.getChildrenBlock()
                            .getElements().get(0);
                    Label documentLabel = new Label(jrxDocumentRootElement.getSimpleName());
                    documentLabel.setSclass("jrx-labelvalue");
                    documentLabel.setParent(cell);
                }
            }
        } else if (jrxEffectiveElement.getChildrenBlock() != null
                && jrxEffectiveElement.getChildrenBlock() instanceof JrxChoiceGroup) {
            // In-direct choice group with complex content definition
            createChoiceEntry(row, cell, (JrxChoiceGroup) jrxEffectiveElement.getChildrenBlock(),
                    jrxEffectiveElement.isRepetitive());
        } else if (jrxEffectiveElement.getChildrenBlock() == null) {
            Label widget = new Label();
            widget.setSclass("jrx-labelvalue");
            widget.setHflex("true");
            String value = null;
            if (jrxEffectiveElement.getXmlElement() != null) {
                value = jrxEffectiveElement.getXmlElement().getTextContent();
                value = clearValueFromApplicationSpecificEscape(value);
            }

            if (value != null) {
                widget.setValue(value);
            }
            widget.setParent(cell);
        }

        cell.setParent(row);

        InputElement inputElement = ComponentUtils.getChildByClass(cell, InputElement.class);
        if (inputElement != null) {
            XmlTreeUtil.registerControlKeys(inputElement);

            if (row.getTree() != null) {
// @formatter:off
//                Map<Long, InputElement> table = XmlTree.getXmlToInputElementTable(row.getTree());
//                table.put(jrxEffectiveElement.getScopedNameHashCode(), inputElement);
//                XmlTree.setXmlToInputElementTable(row.getTree(), table);
// @formatter:on
            }
        }

        return cell;
    }

    protected void createChoiceEntry(XmlTreerow row, Treecell cell, JrxChoiceGroup jrxChoiceGroup,
            boolean repetitive) {
        if (jrxChoiceGroup.getSelection() == null || !repetitive) {
            // Case: Non-Repetitive + Chosen
            if (JrxXmlModelUtil.isChoiceGroupOfElement(jrxChoiceGroup)) {
                if (EditorMode.VIEW.equals(getEditorMode()) && jrxChoiceGroup.getSelection() != null) {
                    if (jrxChoiceGroup.getSelection() instanceof JrxDeclaration) {
                        JrxDeclaration<?> jrxDecl = (JrxDeclaration<?>) jrxChoiceGroup.getSelection();
                        String selectionLabel = tagLabelUtil.getChoiceSelectionTagLabel(jrxChoiceGroup,
                                (XSElementDecl) jrxDecl.getXsdDeclaration());
                        if (!XmlSchemaConstants.ELEMENT_EMPTY_FIELD.equals(selectionLabel)) {
                            Label label = new Label(selectionLabel);
                            label.setParent(cell);
                        }
                    }
                } else {
                    // Present options for choice by showing Combobox
                    XmlChoiceOptionCombobox combobox = createChoiceSelectionbox(row, repetitive, cell, jrxChoiceGroup);

                    if (!repetitive && jrxChoiceGroup.getSelection() != null) {
                        // Case: Not Repetitive + Chosen -> Show selection in combobox
                        ComboboxUtils.setSelectionByValueOrEmpty(combobox, jrxChoiceGroup.getSelection().getXsdDeclaration());
                    }
                }
            } else {
                // Choice of xs:all, xs:sequence, xs:group -> not yet supported!!!
            }
        } else {
            // Case: Repetitive + Chosen -> Show chosen option from choice
            if (jrxChoiceGroup.getSelection() instanceof JrxElement) {
                JrxElement jrxSelectedOptionElement = (JrxElement) jrxChoiceGroup.getSelection();
                String selectionLabel = tagLabelUtil.getChoiceSelectionTagLabel(jrxChoiceGroup,
                        jrxSelectedOptionElement.getXsdDeclaration());
                if (!XmlSchemaConstants.ELEMENT_EMPTY_FIELD.equals(selectionLabel)) {
                    Label label = new Label();
                    label.setParent(cell);
                }
            } else {
                // Choice of xs:all, xs:sequence, xs:group -> not yet supported!!!
            }
        }
    }

    private XmlChoiceOptionCombobox createChoiceSelectionbox(XmlTreerow row, boolean isRepetitive, Treecell cell,
            JrxChoiceGroup jrxChoiceGroup) {
        XSModelGroup xsChildChoiceGroup = jrxChoiceGroup.getXsdDeclaration();
        XSElementDecl[] choiceElementArray = xsdUtil.getElementDeclarationList(xsChildChoiceGroup, true, true).toArray(
                new XSElementDecl[] {});

        Object[] choiceArray = ArrayUtils.insert(0, choiceElementArray, (XSElementDecl) null);
        XmlChoiceOptionCombobox combobox = new XmlChoiceOptionCombobox(row);
        combobox.setAttribute(JrxChoiceGroup.class.getSimpleName(), jrxChoiceGroup);
        combobox.setHflex("true");
        combobox.setReadonly(true);
        combobox.setItemRenderer(new ComboitemRenderer<XSElementDecl>() {
            @Override
            public void render(Comboitem item, XSElementDecl data, int index) throws Exception {
                item.setValue(data);
                if (data == null) {
                    item.setLabel(ZkLabels.getLabel("general.pleaseSelect", "Please select..."));
                    return;
                }

                XSElementDecl xsElement = (XSElementDecl) data;
                JrxChoiceGroup jrxChoiceGroup = (JrxChoiceGroup) item.getParent().getAttribute(
                        JrxChoiceGroup.class.getSimpleName());
                item.setLabel(tagLabelUtil.getChoiceSelectionTagLabel(jrxChoiceGroup, xsElement));
            }
        });
        combobox.setModel(new ListModelArray<Object>(choiceArray));
        ComboboxUtils.initializeValue(combobox);
        // if (!isRepetitive) {
        EventListener<Event> el = new XmlTreeSubNodeChangeListener();
        combobox.addEventListener(Events.ON_CHANGE, el);
        combobox.addEventListener(Events.ON_OK, el);
        // }
        combobox.setParent(cell);
        combobox.setDisabled(EditorMode.VIEW.equals(lookupFactory.getEditorMode()));

        return combobox;
    }

    private Treecell createSecondCell(XmlTreerow row, JrxElementGroup jrxElementGroup) {
        Treecell cell = new Treecell();
        cell.setSclass("jrx-msg-tree-mid-cell");

        if (jrxElementGroup.getXsdDeclaration() == null) {
            return cell;
        }

        cell.setParent(row);

        if (jrxElementGroup instanceof JrxChoiceGroup) {
            JrxChoiceGroup jrxChoiceGroup = (JrxChoiceGroup) jrxElementGroup;
            createChoiceEntry(row, cell, jrxChoiceGroup, jrxChoiceGroup.isRepetitive());
        }

        InputElement inputElement = ComponentUtils.getChildByClass(cell, InputElement.class);
        if (inputElement != null) {
            XmlTreeUtil.registerControlKeys(inputElement);
        }

        return cell;
    }

    private Treecell createSecondCell(XmlTreerow row, JrxGroup jrxGroup) {
        Treecell cell = new Treecell();
        cell.setSclass("jrx-msg-tree-mid-cell");

        return cell;
    }

    @Override
    public void setEditorMode(EditorMode editorMode) {
        lookupFactory.setEditorMode(editorMode);
    }

    @Override
    public EditorMode getEditorMode() {
        return lookupFactory.getEditorMode();
    }

    @Override
    public void setConnector(ServiceConnector connector) {
        this.connector = connector;
    }

    @Override
    public ServiceConnector getConnector() {
        return connector;
    }

    protected HtmlBasedComponent getEntryWidget(JrxAttribute jrxAttribute, XmlTreerow row,
            XmlTreeNodeDefault[] treeNodeDefaults) {
        if (jrxAttribute == null) {
            return null;
        }

        String value = null;
        JrxElement jrxElement = jrxAttribute.getOwner();
        if (jrxElement.getXmlElement() != null) {
            value = jrxElement.getXmlElement().getAttribute(jrxAttribute.getSimpleName());
        }

        // Get entry widget according to configuration for special entry fields
        // e.g. BIC lookup, ISIN lookup, etc.
        HtmlBasedComponent widget = getEntryWidgetFromConfiguration(jrxAttribute, row);
        if (widget == null) {
            XSType xsType = null;
            if (jrxAttribute.getXsdDeclaration() != null) {
                xsType = jrxAttribute.getXsdDeclaration().getType();
            }

            widget = getEntryWidget(xsType, value, treeNodeDefaults);
        }

        if (widget != null)
            widget.addEventListener(Events.ON_CHANGE, createInputWidgetEventHandler(jrxAttribute));

        return widget;
    }

    protected HtmlBasedComponent getEntryWidget(JrxElement jrxElement, XmlTreerow row,
            XmlTreeNodeDefault[] treeNodeDefaults) {
        if (jrxElement == null) {
            return null;
        }

        String value = null;
        if (jrxElement.getXmlElement() != null) {
            value = jrxElement.getXmlElement().getTextContent();
        }

        // Get entry widget according to configuration for special entry fields
        // e.g. BIC lookup, ISIN lookup, etc.
        HtmlBasedComponent widget = getEntryWidgetFromConfiguration(jrxElement, row);
        if (widget == null) {
            XSType xsType = null;
            if (jrxElement.getXsdDeclaration() != null) {
                xsType = jrxElement.getXsdDeclaration().getType();
            }

            widget = getEntryWidget(xsType, value, treeNodeDefaults);
        }

        if (widget != null && widget instanceof InputElement)
            widget.addEventListener(Events.ON_CHANGE, createInputWidgetEventHandler(jrxElement));

        return widget;
    }

    protected HtmlBasedComponent getEntryWidget(XSType xsType, String value, XmlTreeNodeDefault[] treeNodeDefaults) {
        if (EditorMode.VIEW.equals(lookupFactory.getEditorMode())
                || (treeNodeDefaults != null && XmlTreeNodeDefault.isReadonly(treeNodeDefaults))) {
            Label widget = new Label();
            widget.setSclass("jrx-labelvalue");
            widget.setHflex("true");
            if (value != null) {
                value = clearValueFromApplicationSpecificEscape(value);
                widget.setValue(value);
            }

            return widget;
        }

        String regexPattern = null;
        int maxLength = -1;
        int minLength = -1;

        if (xsType != null) {
            XSSimpleType xsSimpleType = null;
            if (xsType instanceof XSSimpleType) {
                xsSimpleType = xsType.asSimpleType();
            } else {
                xsSimpleType = xsType.getBaseType().asSimpleType();
            }
            String baseTypeName = xsSimpleType.getPrimitiveType().getName();
            if (BASE_TYPE_BOOLEAN.equals(baseTypeName)) {
                Combobox widget = new Combobox();
                widget.setItemRenderer(new KeyValueStringArrayComboitemRenderer());
                widget.setReadonly(true);
                String[][] booleanData = new String[][] { { ZkLabels.getLabel("general.null"), "" },
                        { "true", "true" }, { "false", "false" } };
                widget.setModel(new ListModelList<String[]>(booleanData));
                if (value != null && !value.equals("")) {
                    String label = value;
                    for (int i = 0; i < booleanData.length; i++) {
                        if (value.equalsIgnoreCase(booleanData[i][1])) {
                            label = booleanData[i][0];
                        }
                    }
                    ComboboxUtils.setSelectionByValue(widget, new String[] { label, value });
                } else {
                    ComboboxUtils.initializeValue(widget);
                }
                return widget;
            }
            if (BASE_TYPE_DATE.equals(baseTypeName)) {
                Textbox widget = createSimpleInputWidget();
                widget.setHflex("true");
                if (value != null && !value.equals("")) {
                    widget.setValue(value);
                }
                return widget;
            }
            if (BASE_TYPE_DATETIME.equals(baseTypeName)) {
                Textbox widget = createSimpleInputWidget();
                widget.setHflex("true");
                if (value != null && !value.equals("")) {
                    widget.setValue(value);
                }
                return widget;
            }
            if (BASE_TYPE_DECIMAL.equals(baseTypeName)) {
                Decimalbox widget = new Decimalbox();
                widget.setHflex("true");
                if (value != null && !value.equals("")) {
                    widget.setValue(value);
                }
                return widget;
            }
            if (xsSimpleType.isRestriction()) {
                XSRestrictionSimpleType xsRestrictionSimpleType = xsSimpleType.asRestriction();

                // handle other restrictions such as xs:pattern, xs:minLength, xs:maxLength and set them as
                // constraints to the widget

                List<? extends XSFacet> facetPattern = xsRestrictionSimpleType.getFacets(XSFacet.FACET_PATTERN);
                XSFacet maxLengthFacet = xsRestrictionSimpleType.getFacet(XSFacet.FACET_MAXLENGTH);
                XSFacet minLengthFacet = xsRestrictionSimpleType.getFacet(XSFacet.FACET_MINLENGTH);

                if (facetPattern != null && facetPattern.size() == 1) {
                    // if pattern found
                    String valuePattern = facetPattern.get(0).getValue().value;
                    valuePattern = clearPatternFromApplicationSpecificEscape(valuePattern);

                    regexPattern = valuePattern;
                }

                if (minLengthFacet != null || maxLengthFacet != null) {
                    // if minLength/maxLength provided
                    if (maxLengthFacet != null) {
                        try {
                            maxLength = Integer.parseInt(maxLengthFacet.getValue().value);
                        } catch (NumberFormatException e) {
                            maxLength = -1;
                        }
                    }
                    if (minLengthFacet != null) {
                        try {
                            minLength = Integer.parseInt(minLengthFacet.getValue().value);
                        } catch (NumberFormatException e) {
                            minLength = -1;
                        }
                    }
                }

                // retrieve enumeration
                List<? extends XSFacet> facetList = xsRestrictionSimpleType.getFacets(XSFacet.FACET_ENUMERATION);
                if (facetList != null && facetList.size() > 0) {
                    // Enumeration
                    Combobox widget = new Combobox();
                    widget.setItemRenderer(new LabelValueDescriptionComboitemRenderer());
                    widget.setModel(createComboboxModelFromFacets(facetList));
                    if (value != null && !value.equals("")) {
                        widget.setValue(value);
                    } else {
                        // commented by buch, on request by kto/dsu: Default of optional fields 19,20,21 should not be set
                        // widget.initializeFirstModelObject();
                    }

                    if (maxLength > 0) {
                        widget.setMaxlength(maxLength);
                    }

                    return widget;
                }
            }
        }
        Textbox widget = createSimpleInputWidget();
        widget.setReadonly(EditorMode.VIEW.equals(lookupFactory.getEditorMode()));
        widget.setHflex("true");
        if (value != null && !value.equals("")) {
            value = clearValueFromApplicationSpecificEscape(value);
            widget.setValue(value);
        }

        if (regexPattern != null && !regexPattern.equals("")) {
            widget.setConstraint(new TreeSyntaxPatternConstraint(regexPattern));
        } else if (minLength > 0 || maxLength > 0) {
            widget.setConstraint(new TreeSyntaxTextLengthConstraint(minLength, maxLength));
        }

        if (maxLength > 0) {
            widget.setMaxlength(maxLength);
        }

        return widget;
    }

    protected String clearPatternFromApplicationSpecificEscape(String valuePattern) {
        return valuePattern;
    }

    protected String clearValueFromApplicationSpecificEscape(String value) {
        return value;
    }

    private ListModel<String[]> createComboboxModelFromFacets(List<? extends XSFacet> facets) {
        List<String[]> list = new ArrayList<String[]>();
        for (XSFacet facet : facets) {
            String value = facet.getValue().value;
            list.add(new String[] { value, value, "" });
        }

        return new ListModelList<String[]>(list);
    }

    protected HtmlBasedComponent getEntryWidgetFromConfiguration(JrxAttribute jrxAttribute, XmlTreerow row) {
        if (jrxAttribute == null || jrxAttribute.getXsdDeclaration() == null) {
            return null;
        }

        String value = null;
        JrxElement jrxElement = jrxAttribute.getOwner();
        if (jrxElement.getXmlElement() != null) {
            value = jrxElement.getXmlElement().getAttribute(jrxAttribute.getSimpleName());
        }

        HtmlBasedComponent subWidget = null;

        if (ATTR_CURRENCY.equals(jrxAttribute.getSimpleName())) {
            XSSimpleType xsAttrType = jrxAttribute.getXsdDeclaration().getType();

            // This block shall be retrieved from configuration
            subWidget = lookupFactory.getWidget(xsAttrType, connector, value, row);
        } else {
            subWidget = getEntryWidgetFromConfiguration(jrxAttribute.getXsdDeclaration().getType(), value, row);
        }
        if (subWidget != null)
            subWidget.setWidth("80px");

        return subWidget;
    }

    protected HtmlBasedComponent getEntryWidgetFromConfiguration(JrxElement jrxElement, XmlTreerow row) {
        if (jrxElement == null || jrxElement.getXsdDeclaration() == null) {
            return null;
        }

        String value = null;
        boolean hasValue = false;
        if (jrxElement.getXmlElement() != null) {
            value = jrxElement.getXmlElement().getTextContent();
            if (value != null && value.length() > 0) {
                hasValue = true;
            }
        }

        HtmlBasedComponent widget = getEntryWidgetFromConfiguration(jrxElement.getXsdDeclaration().getType(), value,
                row);

        if (hasValue && widget instanceof Datebox) {
            // Handle keyword resolving for Datetimebox
            String widgetTextValue = ((Datebox) widget).getText();
            if (!value.equals(widgetTextValue)) {
                // tbd.
            }
        }
        return widget;
    }

    protected HtmlBasedComponent getEntryWidgetFromConfiguration(XSType xsType, String value, XmlTreerow row) {
        if (xsType == null) {
            return null;
        }

        // This block shall be retrieved from configuration
        return lookupFactory.getWidget(xsType, connector, value, row);
    }

    protected XmlTreeInputWidgetEventHandler createInputWidgetEventHandler(JrxNode jrxNode) {
        return new XmlTreeInputWidgetEventHandler(jrxNode);
    }

    protected Textbox createSimpleInputWidget() {
        return new Textbox();
    }

    protected XmlTreeAbstractLookupWidgetCreator getWidgetCreator(JrxElement jrxEffectiveElement,
            ServiceConnector connector2, String value, XmlTreerow row) {
        return lookupFactory.getWidgetCreator(
                jrxEffectiveElement.getXsdDeclaration().getType(), connector, value, row);
    }
}
