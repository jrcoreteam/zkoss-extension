/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.lookup;

public enum TypeNameForLookup implements ITypeNameForLookup {
    // @formatter:off
    ISO_DATE ("ISODate", TypeNameMatchingMechanism.EQUALS, XmlTreeIsoDateLookupWidgetCreator.class.getName()),
    ISO_DATETIME ("ISODateTime", TypeNameMatchingMechanism.EQUALS, XmlTreeIsoDatetimeLookupWidgetCreator.class.getName())
    // @formatter:on
    ;

    private String typeName;
    
    private String lookupClass;

    private TypeNameMatchingMechanism matchingMechanism;
    
    private Class<? extends XmlTreeAbstractLookupWidgetCreator> widgetCreatorClass = null;

    private TypeNameForLookup(String typeName, TypeNameMatchingMechanism matchingMechanism, String lookupClass) {
        this.typeName = typeName;
        this.lookupClass = lookupClass;
        this.matchingMechanism = matchingMechanism;
    }

    public String getLookupClass() {
        return lookupClass;
    }

    public TypeNameMatchingMechanism getMatchingMechanism() {
        return matchingMechanism;
    }
    
    public String toString() {
        return name() + " - " + matchingMechanism + " - " + lookupClass;
    }

    @Override
    public String getTypeName() {
        return typeName;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Class<? extends XmlTreeAbstractLookupWidgetCreator> getWidgetCreatorClass() {
        if (lookupClass == null || lookupClass.equals("")) return null;
        
        if (widgetCreatorClass == null) {
            try {
                widgetCreatorClass = (Class<? extends XmlTreeAbstractLookupWidgetCreator>) Class.forName(lookupClass);
            } catch (ClassNotFoundException e) {
                widgetCreatorClass = null;
            }
        }
        
        return widgetCreatorClass;
    }

}
