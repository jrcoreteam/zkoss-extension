/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jumin.zkx.zkxutils.LayoutUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Space;

public class WidgetGridLayoutCreator implements Serializable {

	private static final long serialVersionUID = -2748804091316022758L;
	
	public static final String SCLASS_GRID_LAYOUT = "zkx-grid-layout";
	
	private static final Map<Class<?>, AbstractWidgetCreator<?, ?>> DEFAULT_WIDGET_CREATOR_MAP = new ConcurrentHashMap<>();
	
	private LayoutUtils layoutUtil = LayoutUtils.getInstance();

	public Grid createLayout(Component parentContext, List<WidgetAttributeDefinition<?>> widgetDefinitionList, int columnCount) {
		List<Component> widgetList = new ArrayList<>();
		
		for (WidgetAttributeDefinition<?> widgetDefinition : widgetDefinitionList) {
			if (widgetDefinition.isShowLabel()) {
	 			Label widgetLabel = new Label(widgetDefinition.getDisplayLabel() + ":");
	 			widgetDefinition.setLabelWidgetObject(widgetLabel);
				widgetList.add(widgetLabel);
			} else {
				widgetList.add(new Space());
			}
			
			Component widgetObject = null;
			if (widgetDefinition.getWidgetObject() != null) {
				widgetObject = widgetDefinition.getWidgetObject();
			} else if (widgetDefinition.getWidgetClass() != null) {
                try {
	                widgetObject = (Component) widgetDefinition.getWidgetClass().newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
	                e.printStackTrace();
                }
			} else {
				AbstractWidgetCreator<?, ?> widgetCreator = widgetDefinition.getWidgetCreator();
				if (widgetCreator == null && widgetDefinition.getDataType() != null) {
					widgetCreator = getDefaultWidgetCreator(widgetDefinition.getDataType());
				}
				
				widgetObject = widgetCreator.createWidget(parentContext);
			}
			if (widgetObject != null) {
				widgetList.add(widgetObject);
			}
			if (widgetObject instanceof HtmlBasedComponent) {
				LayoutUtils.applyWidth((HtmlBasedComponent) widgetObject, widgetDefinition.getWidth());
			}
			widgetDefinition.setWidgetObject(widgetObject);
		}
		
		return layout(parentContext, widgetList, columnCount);
	}
	
	public Grid arrangeLayout(Component parentContext, List<WidgetAttributeDefinition<?>> widgetDefinitionList, int columnCount) {
		List<Component> widgetList = new ArrayList<>();
		
		for (WidgetAttributeDefinition<?> widgetDefinition : widgetDefinitionList) {
			if (widgetDefinition.isShowLabel()) {
				widgetList.add(widgetDefinition.getLabelWidgetObject());
			} else {
				widgetList.add(new Space());
			}
			
			widgetList.add(widgetDefinition.getWidgetObject());
		}
		
		return layout(parentContext, widgetList, columnCount);
	}
	
	public Grid layout(Component parentContext, List<Component> widgetList, int columnCount) {
		int effectiveColumnCount = columnCount * 2;
		
		String[] columnWidthList = new String[effectiveColumnCount];
		for (int i = 0; i < columnWidthList.length; i++) {
			if (i % 2 == 0) {
				columnWidthList[i] = "min";
			} else {
				columnWidthList[i] = "true";
			}
        }
		
		return layout(parentContext, widgetList, columnCount, columnWidthList);
	}
	
	public Grid layout(Component parentContext, List<Component> widgetList, int columnCount, String[] columnWidthList) {
		// Effectively label will be also calculated
		int effectiveColumnCount = columnCount * 2;
		
		// space column calculation
		int[] spaceColumnIndexList = null;
		if (columnCount > 1) {
			spaceColumnIndexList = new int[columnCount - 1];
			for (int i = 0; i < spaceColumnIndexList.length; i++) {
	            spaceColumnIndexList[i] = 2 * (i + 1);
            }
		}
		
		Grid grid = layoutUtil.gridLayoutComponentsGrid(widgetList, effectiveColumnCount, columnWidthList, null, null, spaceColumnIndexList, "20px", null, null);
		grid.setSclass(SCLASS_GRID_LAYOUT);
		return grid;
	}
	
	public static final AbstractWidgetCreator<?, ?> getDefaultWidgetCreator(Class<?> type) {
		AbstractWidgetCreator<?, ?> widgetCreator = DEFAULT_WIDGET_CREATOR_MAP.get(type);
		
		if (widgetCreator == null) {
			if (String.class.equals(type)) {
				widgetCreator = new SimpleStringWidgetCreator();
			} else if (Date.class.equals(type)) {
				widgetCreator = new DatetimeWidgetCreator();
			} else if (Integer.class.equals(type)) {
				widgetCreator = new IntegerWidgetCreator();
			} else if (Double.class.equals(type)) {
				widgetCreator = new DoubleWidgetCreator();
			} else if (BigDecimal.class.equals(type)) {
				widgetCreator = new DoubleWidgetCreator();
			}
			if (widgetCreator != null)
				DEFAULT_WIDGET_CREATOR_MAP.put(type, widgetCreator);
		}
		
		return widgetCreator;
	}
	
}
