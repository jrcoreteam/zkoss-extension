/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.translator;

import org.jumin.zkx.components.table.AbstractDataTable;
import org.zkoss.zk.ui.Component;

abstract public class ListItemActionTranslator<T> extends ListItemEventTranslator<T> {

    private static final long serialVersionUID = 5632009615411925486L;

    protected void addForwardActionEventToListItem(Component originalComp, String originalEvent, Object eventData) {
        addForwardEventToListItem(originalComp, originalEvent, AbstractDataTable.ON_ITEM_ACTION, eventData);
    }

    protected void sendListItemActionEvent(Object eventData) {
        sendListItemEvent(AbstractDataTable.ON_ITEM_ACTION, eventData);
    }
}
