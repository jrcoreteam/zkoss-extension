/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.utils.DatetimeTextParser;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Datebox;

public abstract class AbstractJavaUtilDateWidgetCreator extends AbstractWidgetCreator<Datebox, Date> {

	private static final long serialVersionUID = -1767007569488700095L;
    
	private DatetimeTextParser dtp = null;
	
	protected abstract Datebox newDatetimeboxWidget();

	@Override
    public Datebox createWidget(Component parent) {
	    Locale formatLocale = Locale.getDefault();
	    TimeZone timezone = TimeZone.getDefault();
	    
	    if (parent.getDesktop() != null) {
	    	formatLocale = UserContextUtil.getUserFormatLocale(parent.getDesktop());
	    	timezone = UserContextUtil.getUserTimeZone(parent.getDesktop());
	    }
		
	    Datebox dtbox = newDatetimeboxWidget();
	    dtbox.setParent(parent);
	    dtbox.setLocale(formatLocale);
	    dtbox.setTimeZone(timezone);
	    
	    return dtbox;
    }

	@Override
    public Date getWidgetValue(Datebox widget) {
		return widget.getValue();
    }
	
	@Override
    public void setWidgetValue(Datebox widget, Date value) {
		widget.setValue(value);
    }

	@Override
    public void setWidgetValueFromString(Datebox widget, String stringValue) {
		Date objectValue = null;
		
		if (dtp == null) dtp = new DatetimeTextParser();
		
		try {
			objectValue = dtp.getDateTimeFromString(stringValue, widget.getLocale(), widget.getTimeZone());
		} catch (Exception e) {}
	    
		widget.setValue(objectValue);
    }

}