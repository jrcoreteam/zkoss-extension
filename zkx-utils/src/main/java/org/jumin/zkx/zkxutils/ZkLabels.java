/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.util.Properties;

import org.zkoss.text.MessageFormats;
import org.zkoss.util.resource.Labels;

public class ZkLabels {

    public static final Properties MISSING_LABELS_PROPS = new Properties();

    public static String getLabel(String key) {
        return getLabel(key, key);
    }

    public static String getLabel(String key, Object[] args) {
        return getLabel(key, key, args);
    }

    public static String getLabel(String key, String defaultValue) {
        String label = Labels.getLabel(key);
        if (label == null) {
            registerMissingLabel(key, defaultValue);
            return defaultValue;
        }

        return label;
    }

    public static String getLabel(String key, String defaultValue, Object[] args) {
        String label = Labels.getLabel(key, args);
        if (label == null) {
            registerMissingLabel(key, defaultValue);
            return MessageFormats.format(defaultValue, args, null);
        }

        return label;
    }

    private static void registerMissingLabel(String key, String defaultValue) {
        if (key == null || key.equals(""))
            return; // skip

        MISSING_LABELS_PROPS.put(key, defaultValue);
    }

    public static String createLabelKey(String prefix, String logicalLabelValue) {
        if (prefix != null && prefix.length() > 0 && !prefix.endsWith("."))
            prefix += ".";

        String labelKey = prefix == null ? "" : prefix;
        labelKey += convertToKeySegmentNaming(logicalLabelValue.replaceAll("[:,.\\-/\\s\\(\\)\\[\\]\\{\\}]", ""));
        return labelKey;
    }

    public static String convertToKeySegmentNaming(String name) {
        if (name != null && name.trim().length() > 1) {
            return name.substring(0, 1).toLowerCase() + name.substring(1);
        }

        return name;
    }

}
