/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

public class FunctionAssignment implements AssignmentItem, Cloneable {

    private static final long serialVersionUID = 5643781690270034007L;

    private UserFunction function = null;

    private boolean visible = true;

    private boolean disabled = false;

    private CheckLevel checkLevel = CheckLevel.NONE;

    private String image;

    private String labelKey;

    private String defaultLabel;

    public FunctionAssignment(UserFunction function) {
        this(function, true);
    }

    public FunctionAssignment(UserFunction function, boolean visible) {
        this(function, visible, false);
    }

    public FunctionAssignment(UserFunction function, boolean visible, boolean disabled) {
        this(function, visible, disabled, CheckLevel.NONE);
    }

    public FunctionAssignment(UserFunction function, boolean visible, boolean disabled, CheckLevel checkLevel) {
        this(function, visible, disabled, checkLevel, "", "");
    }

    public FunctionAssignment(UserFunction function, boolean visible, boolean disabled, CheckLevel checkLevel,
            String labelKey, String defaultLabel) {
        super();
        this.function = function;
        this.visible = visible;
        this.disabled = disabled;
        this.checkLevel = checkLevel;
        this.labelKey = labelKey;
        this.defaultLabel = defaultLabel;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public UserFunction getFunction() {
        return function;
    }

    public void setFunction(UserFunction function) {
        this.function = function;
    }

    @Override
    public String toString() {
        return function + " Visible: " + visible + " Disabled: " + disabled;
    }

    public FunctionAssignment copy() {
        FunctionAssignment copy = new FunctionAssignment(function, visible, disabled, checkLevel, labelKey,
                defaultLabel);
        return copy;
    }

    public CheckLevel getCheckLevel() {
        return checkLevel;
    }

    public void setCheckLevel(CheckLevel checkLevel) {
        this.checkLevel = checkLevel;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }

    public String getDefaultLabel() {
        return defaultLabel;
    }

    public void setDefaultLabel(String defaultLabel) {
        this.defaultLabel = defaultLabel;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((checkLevel == null) ? 0 : checkLevel.hashCode());
        result = prime * result + ((defaultLabel == null) ? 0 : defaultLabel.hashCode());
        result = prime * result + (disabled ? 1231 : 1237);
        result = prime * result + ((function == null) ? 0 : function.hashCode());
        result = prime * result + ((image == null) ? 0 : image.hashCode());
        result = prime * result + ((labelKey == null) ? 0 : labelKey.hashCode());
        result = prime * result + (visible ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FunctionAssignment other = (FunctionAssignment) obj;
        if (checkLevel != other.checkLevel)
            return false;
        if (defaultLabel == null) {
            if (other.defaultLabel != null)
                return false;
        } else if (!defaultLabel.equals(other.defaultLabel))
            return false;
        if (function == null) {
            if (other.function != null)
                return false;
        } else if (!function.equals(other.function))
            return false;
        if (image == null) {
            if (other.image != null)
                return false;
        } else if (!image.equals(other.image))
            return false;
        if (labelKey == null) {
            if (other.labelKey != null)
                return false;
        } else if (!labelKey.equals(other.labelKey))
            return false;
        return true;
    }
}
