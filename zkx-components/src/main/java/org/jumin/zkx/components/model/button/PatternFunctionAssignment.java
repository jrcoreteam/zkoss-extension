/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

public class PatternFunctionAssignment extends FunctionAssignment {

	private static final long serialVersionUID = -2985927619972961773L;
    
	private String[] patterns = new String[0];

	public PatternFunctionAssignment(UserFunction function) {
		super(function);
	}

	public PatternFunctionAssignment(UserFunction function, boolean visible, boolean disabled, CheckLevel checkLevel,
	        String[] patterns) {
		super(function, visible, disabled, checkLevel);
		this.patterns = patterns;
	}

	public String[] getPatterns() {
		return patterns;
	}

	public void setPatterns(String[] patterns) {
		this.patterns = patterns;
	}

	public FunctionAssignment copy() {
		FunctionAssignment copy = new PatternFunctionAssignment(getFunction(), isVisible(), isDisabled(), getCheckLevel(), getPatterns());
		copy.setLabelKey(getLabelKey());
		copy.setDefaultLabel(getDefaultLabel());
		
		return copy;
	}
}
