/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.jumin.common.ehcache.CacheManagerFactory;
import org.jumin.common.ehcache.CacheOperationHelper;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.metainfo.PageDefinition;

import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;

public class WidgetComponentHelper {

    private static Logger log = LoggerFactory.getLogger(WidgetComponentHelper.class);

    private static final String COMPONENT_DEFINITION_CACHE_NAME = "zkxComponentDefinitionCache";

    private static Cache initCache() {
        CacheConfiguration cacheConfig = CacheManagerFactory.getDefaultCacheConfiguration();
        cacheConfig.setName(COMPONENT_DEFINITION_CACHE_NAME);
        cacheConfig.setEternal(false);
        cacheConfig.setTimeToIdleSeconds(300); // 5 minutes idle -> clear
        Cache cache = CacheManagerFactory.createCache(cacheConfig);
        CacheManagerFactory.getFmpDefaultCacheManager().addCache(cache);
        return cache;
    }

    public static final <T extends Component> T loadComponentFromZul(T component) {
        if (component == null)
            return null;

        Cache cache = CacheManagerFactory.getFmpDefaultCacheManager().getCache(COMPONENT_DEFINITION_CACHE_NAME);
        if (cache == null) {
            cache = initCache();
        }

        @SuppressWarnings("unchecked")
        Class<T> clazz = (Class<T>) component.getClass();
        PageDefinition pageDef = CacheOperationHelper.retrieveCacheValue(cache, clazz.getName());
        if (pageDef == null) {
            pageDef = WidgetComponentHelper.retrieveComponentDefinition(clazz);
            CacheOperationHelper.storeToCache(cache, clazz.getName(), pageDef);
        }
        if (pageDef != null) {
            Component tempComp = Executions.createComponents(pageDef, null, null);
            
            Map<String, Component> decendentCompCatalog = new HashMap<>();
            // Moving over the direct children

            Component childComp = tempComp.getFirstChild();
            while (childComp != null) {
                childComp.setParent(component);

                // Register direct children that have id
                if (childComp.getId() != null && childComp.getId().trim().length() > 0) {
                    decendentCompCatalog.put(childComp.getId(), childComp);
                }

                List<Component> secondDecendentComponentList = ComponentUtils.getChildrenByClass(childComp,
                        Component.class, true);
                for (Component secondDecendentComponent : secondDecendentComponentList) {
                    if (secondDecendentComponent.getId() != null
                            && secondDecendentComponent.getId().trim().length() > 0) {
                        decendentCompCatalog.put(secondDecendentComponent.getId(), secondDecendentComponent);
                    }
                }

                childComp = tempComp.getFirstChild();
            }

            // Copy over local variable
            for (Method getterMethod : tempComp.getClass().getMethods()) {
                if (Modifier.isStatic(getterMethod.getModifiers()) || Modifier.isAbstract(getterMethod.getModifiers())
                        || getterMethod.getParameterTypes().length > 0) {
                    continue;
                }

                String methodName = getterMethod.getName();
                if (!methodName.startsWith("get") || !methodName.matches("get[A-Z][a-zA-Z0-9]*")) {
                    continue;
                }

                methodName = "set" + methodName.substring(3);
                Method setterMethod = null;
                try {
                    setterMethod = tempComp.getClass().getMethod(methodName, getterMethod.getReturnType());
                } catch (Exception e) {
                    continue; // skip
                }

                try {
                    setterMethod.invoke(component, getterMethod.invoke(tempComp, (Object[]) null));
                } catch (Exception e) {
                    log.error("Failure in copying value from getter method: " + getterMethod.getName(), e);
                }
            }
            
            // Wire inner component
            for (Entry<String, Component> entry : decendentCompCatalog.entrySet()) {
                Field f = FieldUtils.getField(clazz, entry.getKey(), true);
                if (f != null) {
                    if (f.getType().isAssignableFrom(entry.getValue().getClass())) {
                        try {
                            f.set(component, entry.getValue());
                        } catch (Exception e) {
                            log.debug("Failure setting attribute: " + entry.getKey(), e);
                        }
                    }
                }
            }
            
            tempComp.detach();
        }
        
        return component;
    }

    public static final PageDefinition retrieveComponentDefinition(Class<? extends Component> componentClass) {
        InputStream is = null;
        try {
            is = componentClass.getResourceAsStream("/" + componentClass.getName().replace('.', '/') + ".zul");
            Reader reader = new InputStreamReader(is);
            Execution exec = Executions.getCurrent();
            if (exec == null)
                return null;
            return exec.getPageDefinitionDirectly(reader, "zul");
        } catch (Exception e) {
            log.debug("Failure in loading component definition for class: " + componentClass.getName(), e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }

        return null;
    }

}
