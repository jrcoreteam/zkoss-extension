/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.objectlocking;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import org.jumin.common.base.services.UserObjectLockingService;
import org.jumin.common.utils.DatetimeTextParser;
import org.jumin.common.utils.UserPrincipalUtil;
import org.jumin.common.utils.locking.InvalidObjectLockOwnershipException;
import org.jumin.common.utils.locking.LockItem;
import org.jumin.common.utils.locking.ObjectLockingService;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Auxhead;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timer;
import org.zkoss.zul.Window;

public class LockManagementController extends SelectorComposer<Window> {

    private static final long serialVersionUID = -8738085547444893684L;

    public static final int TIMER_DELAY = 3000;

	private static Logger log = LoggerFactory.getLogger(LockManagementController.class);

	private static final String[][] REGEX_ESCAPE_TABLE = new String[][] {
// @formatter:off
			{ "[" , "\\[" },
			{ "]" , "\\]" },
			{ "." , "[.]" },
			{ "?" , "[?]" },
			{ "+" , "[+]" },
			{ "*" , ".*" },
			{ "\\" , "\\\\" },
			{ "+" , "[+]" },
			{ "+" , "[+]" },
			// @formatter:on
	};

	@Wire("listbox")
	private Listbox lockTable;

	@Wire("timer")
	private Timer timer;

	@Wire("label#timestamp")
	private Label timestamp;

	@Wire("label#entriesFoundNumber")
	private Label entriesFoundNumber;

	@Wire("button#removeButton")
	private Button removeButton;

	@Wire("auxhead#filterHeader")
	private Auxhead filterHeader;

	private String currentUserPrincipal = null;

	private String currentUserOrg = null;

	private List<String> filterCriterias = null;

	private Locale userFormatLocale = Locale.getDefault();

	private TimeZone userTimeZone = TimeZone.getDefault();

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);

		if (getPage().getDesktop().isServerPushEnabled()) {
			getPage().getDesktop().enableServerPush(true);
		}

		currentUserPrincipal = UserContextUtil.getSessionUserPrincipal(getPage());
		currentUserOrg = UserPrincipalUtil.getOrganizationNameFromUserPrincipal(currentUserPrincipal);
		
		userFormatLocale = UserContextUtil.getUserFormatLocale(getPage());
		userTimeZone = UserContextUtil.getUserTimeZone(getPage());

		initProperties();
		initEvents();
	}

	private void initEvents() {
		timer.addEventListener(Events.ON_TIMER, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				log.debug("Refresh lock table");
				timer.stop();
				refreshLockTable();
				if (timer.getDelay() < TIMER_DELAY)
					timer.setDelay(TIMER_DELAY);
				timer.start();
			}
		});
	}

	private void initProperties() {
		ListitemRenderer<LockItem> renderer = new ListitemRenderer<LockItem>() {
			private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");

			@Override
			public void render(Listitem item, LockItem data, int index) throws Exception {
				Listcell cell = new Listcell(data.getEntityName());
				cell.setParent(item);
				cell = new Listcell(data.getEntityId());
				cell.setParent(item);
				cell = new Listcell(data.getLockedBy());
				cell.setParent(item);
				cell = new Listcell(sdf.format(new Date(data.getLockedUtcTimestamp())));
				cell.setParent(item);
				cell = new Listcell(data.getSource());
				cell.setParent(item);

				item.setValue(data);
			}
		};

		lockTable.setItemRenderer(renderer);
	}

	@Listen("onClick = button#refreshButton")
	public void refreshLockTable() {
		log.info("Refresh Lock Table");
		ObjectLockingService lockService = new UserObjectLockingService(currentUserPrincipal);

		// sort lock info
		List<LockItem> lockItemList = lockService.retrieveLockInfo();

		SortedMap<String, LockItem> sortedMap = new TreeMap<String, LockItem>();
		for (LockItem lockItem : lockItemList) {
			if (!lockItem.getLockedBy().endsWith(UserPrincipalUtil.USER_ORG_SEPARATOR + currentUserOrg)) {
				continue; // skip lock
			}

			if (filterHeader.isVisible() && filterCriterias != null && filterCriterias.size() > 0) {
				// apply filter criterias
				if (filterCriterias.get(0) != null && filterCriterias.get(0).length() > 0
				        && !match(lockItem.getEntityName(), filterCriterias.get(0))) {
					continue;
				}
				if (filterCriterias.get(1) != null && filterCriterias.get(1).length() > 0
				        && !match(lockItem.getEntityId(), filterCriterias.get(1))) {
					continue;
				}
				if (filterCriterias.get(2) != null && filterCriterias.get(2).length() > 0
				        && !match(lockItem.getLockedBy(), filterCriterias.get(2))) {
					continue;
				}
				if (filterCriterias.get(3) != null && filterCriterias.get(3).length() > 0
				        && !match(lockItem.getSource(), filterCriterias.get(3))) {
					continue;
				}
			}

			sortedMap.put(lockItem.getLockedUtcTimestamp() + lockItem.getEntityName() + lockItem.getEntityId(),
			        lockItem);
		}

		lockTable.setModel(new ListModelList<LockItem>(sortedMap.values()));
		entriesFoundNumber.setValue(NumberFormat.getIntegerInstance(userFormatLocale).format(sortedMap.size()));
		timestamp.setValue(new DatetimeTextParser().getStringFromDateTime(new Date(System.currentTimeMillis()),
		        userFormatLocale, userTimeZone));
	}

	@Listen("onClick = button#removeButton")
	public void doRemove() {
		if (lockTable.getItemCount() < 1) {
			// Nothing to remove
			return;
		}

		Set<Listitem> selectedItems = lockTable.getSelectedItems();
		if (selectedItems.isEmpty()) {
			// Nothing selected to be removed -> Remove all
			selectedItems = new HashSet<Listitem>(lockTable.getItems());
		}

		if (log.isDebugEnabled()) {
			log.debug("Removing " + selectedItems.size() + " lock" + (selectedItems.size() > 1 ? "s" : "") + ".");
		}
		ObjectLockingService lockService = new UserObjectLockingService(currentUserPrincipal);

		for (Listitem item : selectedItems) {
			if (!item.isVisible())
				continue;

			LockItem lockItem = item.getValue();
			if (lockItem == null)
				continue;

			lockItem = lockService.retrieveLockInfo(lockItem);
			try {
				lockService.unlockObject(lockItem);

				EventQueues.lookup(ObjectLockingUtil.APPLICATION_LOCK_QUEUE_NAME, EventQueues.APPLICATION, true)
				        .publish(new TakeOverLockEvent(lockItem, null));
			} catch (InvalidObjectLockOwnershipException e) {
				log.warn("Lock ownership has been changed");
			}
		}

		refreshLockTable();
	}

	@Listen("onClick = button#clearFilterButton")
	public void clearFilter() {
		if (!filterHeader.isVisible())
			return;

		filterCriterias = null;
		for (Component comp : filterHeader.getChildren()) {
			Textbox filterValueWidget = ComponentUtils.getChildByClass(comp, Textbox.class);
			if (filterValueWidget == null)
				continue;

			filterValueWidget.setText("");
		}

		refreshLockTable();
	}

	@Listen("onClick=button#applyFilterButton; onOK=textbox")
	public void applyFilter() {
		if (!filterHeader.isVisible())
			return;

		filterCriterias = new ArrayList<String>();
		for (Component comp : filterHeader.getChildren()) {
			Textbox filterValueWidget = ComponentUtils.getChildByClass(comp, Textbox.class);
			if (filterValueWidget == null)
				continue;

			filterCriterias.add(filterValueWidget.getText());
		}

		refreshLockTable();
	}

	private boolean match(String value, String pattern) {
		if (pattern == null || pattern.length() < 1)
			return true;

		String escapedPattern = escapeStandardPatternForRegEx(pattern);
		if (escapedPattern.equals(pattern)) {
			return value.startsWith(escapedPattern);
		}
		return value.matches(escapedPattern);
	}

	private String escapeStandardPatternForRegEx(String pattern) {
		String temp = pattern;

		for (String[] regexEscape : REGEX_ESCAPE_TABLE) {
			if (temp.indexOf( regexEscape[0]) >= 0) {
				temp = temp.replace( regexEscape[0],  regexEscape[1]);
			}
		}

		return temp;
	}
}
