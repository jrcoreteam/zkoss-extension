/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.lookup;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.DatatypeConverter;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.common.utils.DatetimeTextParser;
import org.jumin.zkx.components.IsoDatebox;
import org.jumin.zkx.components.IsoDatetimebox;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;

public class XmlTreeIsoDateLookupWidgetCreator extends XmlTreeAbstractLookupWidgetCreator {

	private static final long serialVersionUID = -7122103931547483295L;

	private final DatetimeTextParser dateTimeParser = new DatetimeTextParser();

	public XmlTreeIsoDateLookupWidgetCreator(ServiceConnector connector, XmlTreerow row,
	        EditorMode editorMode, String initialValue) {
		super(connector, row, editorMode, initialValue);
	}

	@Override
	public HtmlBasedComponent createWidget() {
		if (EditorMode.VIEW.equals(editorMode)) {
			return createViewModeWidget();
		}

		IsoDatebox dtbox = new IsoDatebox();
		if (value != null && !value.equals("")) {
			Date date = dateTimeParser.getDateFromISODate(value);
			dtbox.setValue(date);
			registerValueDescription(value, false);
		}

		dtbox.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				String isoDateValue = "";
				IsoDatetimebox dtbox = (IsoDatetimebox) event.getTarget();
				if (dtbox.getValue() != null ) {
				    Calendar cal = Calendar.getInstance();
				    cal.setTime(dtbox.getValue());
					isoDateValue = DatatypeConverter.printDate(cal);
				}

				registerValueDescription(isoDateValue, true);
			}
		});

		row.setLookupWidget(dtbox);
		return dtbox;
	}

	@Override
	protected void registerValueDescription(String value, boolean triggerEvent) {
		if (row == null || row.getDesktop() == null)
			return;

		String desc = "";

		if (value != null && !value.equals("")) {
			Locale currentLocale = UserContextUtil.getUserFormatLocale(row.getDesktop());
			desc = dateTimeParser.getStringFromISODate(value, currentLocale);
		}

		if (desc == null)
			desc = "";
		// Set description via row's attribute because the description cell may not be created yet.
		registerValueDescription(row, desc, triggerEvent);
	}

}
