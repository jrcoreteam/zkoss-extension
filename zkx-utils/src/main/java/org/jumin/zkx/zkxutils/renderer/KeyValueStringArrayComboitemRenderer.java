/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.renderer;

import java.io.Serializable;

import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

public class KeyValueStringArrayComboitemRenderer implements ComboitemRenderer<String[]>, Serializable {

	private static final long serialVersionUID = 8703761498544134483L;

	@Override
	public void render(Comboitem item, String[] data, int index) throws Exception {
		if (data == null) return;
		String[] nv = data;
		item.setValue(nv.length > 1 ? nv[1] : null);
		item.setLabel(nv.length > 0 ? nv[0] : "");
		
		if (nv[0] == null || nv[0].equals("")) {
			item.setLabel(" ");
			item.setHeight("18px");
		}
	}

}
