/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.dialog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.Map;

import org.jumin.zkx.components.ConsoleViewer;
import org.jumin.zkx.zkxutils.ApplicationConstants;
import org.jumin.zkx.zkxutils.MessageDialog;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Button;
import org.zkoss.zul.Messagebox;

public class SystemErrorDialog extends MessageDialog {

    private static final long serialVersionUID = 6798816151855301949L;
    
    public static Logger log = LoggerFactory.getLogger(SystemErrorDialog.class);
    
    public static final String ON_SHOW_TECHNICAL_ERROR = "onShowTechnicalError"; 
    
    public static final ButtonModel SHOW_TECHNICAL_ERROR = new ButtonModel("general.showTechnicalErrorMessage", "Show Technical Message", ON_SHOW_TECHNICAL_ERROR, false);

    private boolean debugMode = System.getProperty(ApplicationConstants.SYSTEM_DEBUG_MODE_VAR, "false").equalsIgnoreCase("true");
    
    private Button technicalMessageButton;
    
    private Throwable throwable = null;
    
    protected void initProperties() {
    	super.initProperties();
    	
    	setIcon(Messagebox.ERROR);
	    setTitle(ZkLabels.getLabel("zkx.system.error.title", "System Error"));
	    setBorder(true);
	    
	    Map<ButtonModel, EventListener<Event>> elMap = new Hashtable<>();
	    elMap.put(SHOW_TECHNICAL_ERROR, new EventListener<Event>() {
	    	@Override
	    	public void onEvent(Event event) throws Exception {
	    		showTechnicalErrorMessage();
	    	}
	    });
	    
	    setButtons(new ButtonModel[] {SHOW_TECHNICAL_ERROR}, null, new ButtonModel[] {MessageDialog.OK}, elMap);
	    if (getTechnicalMessageButton() != null) {
	    	getTechnicalMessageButton().setVisible(throwable != null && debugMode);
	    }
    }

	public static void showError(Page context, String message, Throwable throwable) {
		SystemErrorDialog dialog = new SystemErrorDialog();
		dialog.setPage(context);
		
        dialog.doModal();
    }

	protected void showTechnicalErrorMessage() {
	    ConsoleViewer viewer = new ConsoleViewer();
	    viewer.setTitle(ZkLabels.getLabel("zkx.technical.error.message", "Technical Error Message"));
	    viewer.setHeight("600px");
	    viewer.setWidth("800px");
		if (throwable != null) {
			StringWriter sw = new StringWriter();
			throwable.printStackTrace(new PrintWriter(sw));
			viewer.setText(sw.toString());
		} else {
			viewer.setText("");
		}
		
	    viewer.setPage(getPage());
	    viewer.doModal();
    }
	
	public Throwable getException() {
		return throwable;
	}
	
	public void setException(Throwable throwable) {
		this.throwable = throwable;
	    if (getTechnicalMessageButton() != null) {
	    	getTechnicalMessageButton().setVisible(throwable != null && debugMode);
	    }
	}
	
	public void setErrorMessage(String message) {
		setMessage(message);
	}
	
	private Button getTechnicalMessageButton() {
		if (technicalMessageButton == null) {
			technicalMessageButton = getButton(SHOW_TECHNICAL_ERROR);
		}
	    return technicalMessageButton;
    }
}
