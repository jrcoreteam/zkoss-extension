/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.sidebar;

import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.preferences.object.ObjectAttributeConfiguration;
import org.jumin.common.utils.InvalidMethodPathExpression;
import org.jumin.common.utils.ObjectPropertyUtil;
import org.jumin.common.utils.translator.FormatLocaleAndTimeZoneAwareStringTranslator;
import org.jumin.common.utils.translator.FormatLocaleAwareStringTranslator;
import org.jumin.common.utils.translator.StringTranslator;
import org.jumin.zkx.components.exception.ExceptionLogger;
import org.jumin.zkx.zkxutils.LabelUtils;
import org.jumin.zkx.zkxutils.exception.InvalidSystemRuntimeSettingException;

public class ObjectAttributeUtils {

	private static final ObjectPropertyUtil propUtil = new ObjectPropertyUtil();

	@SuppressWarnings("unchecked")
	public static final String getObjectValue(Object object, ObjectAttributeConfiguration attrConfig, Locale locale,
	        TimeZone timeZone) throws InvalidMethodPathExpression {
		String valueText = null;

		Object o = propUtil.getPropertyValue(object, attrConfig.getName(), attrConfig.getRetrievingMethod());

		if (null != o) {
			if (attrConfig.getTranslator() != null && attrConfig.getTranslator().length() > 0) {
				try {
					Class<?> translatorClazz = Class.forName(attrConfig.getTranslator());
					Object translatorInstance = translatorClazz.newInstance();
					if (translatorInstance instanceof StringTranslator<?>) {
						StringTranslator<Object> concreteTranslatorInstance = (StringTranslator<Object>) translatorInstance;
						valueText = concreteTranslatorInstance.translate(o);
					} else if (translatorInstance instanceof FormatLocaleAndTimeZoneAwareStringTranslator<?>) {
						FormatLocaleAndTimeZoneAwareStringTranslator<Object> concreteTranslatorInstance = (FormatLocaleAndTimeZoneAwareStringTranslator<Object>) translatorInstance;
						valueText = concreteTranslatorInstance.translate(o, locale, timeZone);
					} else if (translatorInstance instanceof FormatLocaleAwareStringTranslator<?>) {
						FormatLocaleAwareStringTranslator<Object> concreteTranslatorInstance = (FormatLocaleAwareStringTranslator<Object>) translatorInstance;
						valueText = concreteTranslatorInstance.translate(o, locale);
					} else {
						// register error
						String errorMsg = "Translator: '" + attrConfig.getTranslator()
						        + "' is not a valid registered translator. Fallback to default data formatting.";
						ExceptionLogger.registerInternalError(errorMsg, new InvalidSystemRuntimeSettingException(
						        errorMsg));

						// Fallback to standard formatter
						valueText = "" + o;
					}
				} catch (Exception e) {
					// register error
					String errorMsg = "Error using translator: '" + attrConfig.getTranslator() + "' with data: " + o
					        + "\nFallback to default data formatting.";
					ExceptionLogger.registerInternalError(errorMsg, e);

					// Fallback to standard formatter
					valueText = "" + o;
				}
			}
			if (valueText == null)
				valueText = "" + o;

			valueText = LabelUtils.formatNonBreakingSpace(valueText);
		}
		return valueText;
	}
	
	public static final String formulateScopedObjectAttributeName(Class<?> entityClass, String attributeName) {
		return formulateScopedObjectAttributeName(entityClass.getSimpleName(), attributeName);
	}

	public static final String formulateScopedObjectAttributeName(String entityName, String attributeName) {
		return entityName + "." + attributeName;
	}
}
