/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;

public class ComboboxWidgetCreator extends AbstractWidgetCreator<Combobox, String> {

	private static final long serialVersionUID = 8573570830402191683L;
	
    private List<String> model = null;
	
	public ComboboxWidgetCreator() {
	    this(new ArrayList<String>());
    }

	public ComboboxWidgetCreator(List<String> model) {
	    super();
	    this.model = model;
    }

	@Override
    public Combobox createWidget(Component parent) {
		Combobox widget = new Combobox();
		widget.setParent(parent);
		
		if (model != null) {
			widget.setModel(new ListModelList<>(model));
		}
		
	    return widget;
    }

	@Override
    public String getWidgetValue(Combobox widget) {
	    return widget.getValue();
    }

	@Override
    public void setWidgetValue(Combobox widget, String value) {
	    widget.setText(value);   
    }

	@Override
    public void setWidgetValueFromString(Combobox widget, String stringValue) {
	    setWidgetValue(widget, stringValue);
    }

}
