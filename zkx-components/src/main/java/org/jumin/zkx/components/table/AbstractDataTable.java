/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.table;

import java.util.List;

import org.jumin.common.preferences.table.TableColumnConfiguration;
import org.jumin.common.preferences.table.TableConfiguration;
import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Vlayout;

public abstract class AbstractDataTable<T> extends Vlayout {

    private static final long serialVersionUID = 2524157032370148656L;

    public static final String ON_DATA_REFRESH = "onDataRefresh";

    public static final String ON_ITEM_OPEN = "onItemOpen";
    
    public static final String ON_ITEM_ACTION = "onItemAction";
    
    public static final String ON_SORT = "onSort";

    protected String entity;
    
    protected String module;

    protected WidgetStaticLabelUtils widgetDebugUtil = null;

    public AbstractDataTable() {
        super();
        widgetDebugUtil = WidgetStaticLabelUtils.getInstance();
        createContents();
        initEvents();
        initProperties();
    }

    protected void createContents() {
    }

    protected void initEvents() {
    }

    protected void initProperties() {
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public abstract String getOrgUnitResultConfig();

    public abstract void setOrgUnitResultConfig(String orgUnitConfigXmlString);

    public abstract String getSystemResultConfig();

    public abstract void setSystemResultConfig(String systemConfigXmlString);

    public abstract String getUserResultConfig();

    public abstract void setUserResultConfig(String resultConfiguration);

    public abstract int getIndexOf(T obj);

    public abstract List<TableColumnConfiguration> getOrderedAttributes();

    public abstract void setOrderedAttributes(TableConfiguration config);

    public abstract ListModel<T> getResultModel();

    public abstract void setDataTableModel(ListModel<T> model);

    public abstract Listitem getSelectedItem();

    public abstract List<Listitem> getSelectedItems();

    protected abstract TablePreferenceDialog getConfigDialog();

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public abstract int getDataTablePageSize();

    public abstract int getDataTableActivePage();

    public abstract void setDataTableActivePage(int activePageIndex);

    public abstract void setResultTotal(int totalSize);

    public abstract void clearDataTable();

    public abstract void clearAttributeSortOrder();

    public abstract void clearPagingInfo();
}
