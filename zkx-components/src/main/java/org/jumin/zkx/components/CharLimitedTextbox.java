/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.jumin.zkx.zkxutils.ApplicationConstants;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.sys.ContentRenderer;
import org.zkoss.zul.Textbox;

public class CharLimitedTextbox extends Textbox {

    private static final long serialVersionUID = -436347648619104047L;

    private String _allowedChars = "";

    private boolean uppercaseOnly = false;

    public String getAllowedChars() {
        return _allowedChars;
    }

    public void setAllowedChars(String allowedChars) {
        _allowedChars = allowedChars;
        smartUpdate("allowedChars", uppercaseOnly ? enrichWithLowerCase(_allowedChars) : _allowedChars);
    }

    public void setUppercaseOnly(boolean uppercaseOnly) {
        this.uppercaseOnly = uppercaseOnly;
        String currentStyle = getStyle();
        if (uppercaseOnly) {
            if (currentStyle == null || currentStyle.length() < 1) {
                this.setStyle(ApplicationConstants.STYLE_UPPERCASE_TRANSFORM);
            } else if (currentStyle.indexOf(ApplicationConstants.STYLE_UPPERCASE_TRANSFORM) < 0) {
                if (currentStyle.trim().length() > 0) {
                    currentStyle = currentStyle.trim() + ApplicationConstants.STYLE_UPPERCASE_TRANSFORM;
                }
                this.setStyle(currentStyle);
            }
            setAllowedChars(enrichWithLowerCase(_allowedChars));
        } else {
            if (currentStyle != null && currentStyle.indexOf(ApplicationConstants.STYLE_UPPERCASE_TRANSFORM) >= 0) {
                currentStyle = StringUtils.remove(currentStyle, ApplicationConstants.STYLE_UPPERCASE_TRANSFORM);
                currentStyle = currentStyle.trim();
                if (currentStyle.equals(""))
                    currentStyle = null; // Set Style NULL

                this.setStyle(currentStyle);
            }
            setAllowedChars(_allowedChars);
        }
    }

    public boolean isUppercaseOnly() {
        return uppercaseOnly;
    }

    @Override
    public void setText(String value) throws WrongValueException {
        String txtValue = value;
        if (uppercaseOnly && txtValue != null) {
            txtValue = txtValue.toUpperCase();
        }
        super.setText(txtValue);
    }

    @Override
    public String getText() throws WrongValueException {
        if (uppercaseOnly) {
            return super.getText().toUpperCase();
        }
        return super.getText();
    }

    @Override
    public void setValue(String value) throws WrongValueException {
        String txtValue = value;
        if (uppercaseOnly && txtValue != null) {
            txtValue = txtValue.toUpperCase();
        }
        super.setValue(txtValue);
    }

    @Override
    public String getValue() throws WrongValueException {
        if (uppercaseOnly) {
            return super.getValue().toUpperCase();
        }
        return super.getValue();
    }

    protected void renderProperties(ContentRenderer renderer) throws IOException {
        super.renderProperties(renderer);
        render(renderer, "allowedChars", _allowedChars);
    }

    @Override
    protected void validate(Object value) throws WrongValueException {
        if (uppercaseOnly && value != null && value instanceof String) {
            super.validate(("" + value).toUpperCase());
        } else {
            super.validate(value);
        }
    }

    public String enrichWithLowerCase(String s) {
        String newString = s;
        String lowerCaseVersion = s.toLowerCase();
        for (int i = 0; i < lowerCaseVersion.length(); i++) {
            char c = lowerCaseVersion.charAt(i);
            if (newString.indexOf(c) < 0) {
                newString += c;
            }
        }
        return newString;
    }

}
