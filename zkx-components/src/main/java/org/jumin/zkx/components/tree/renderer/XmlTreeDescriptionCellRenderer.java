/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.renderer;

import java.io.Serializable;

import org.jumin.common.xsutils.JrxXmlModelUtil;
import org.jumin.common.xsutils.model.JrxChoiceGroup;
import org.jumin.common.xsutils.model.JrxDeclaration;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxElementGroup;
import org.jumin.common.xsutils.model.JrxGroup;
import org.jumin.common.xsutils.model.JrxTerm;
import org.jumin.zkx.components.tree.RepetitionAddButton;
import org.jumin.zkx.components.tree.RepetitionRemoveButton;
import org.jumin.zkx.components.tree.RepetitionRemoveButtonListener;
import org.jumin.zkx.components.tree.XmlChoiceOptionCombobox;
import org.jumin.zkx.components.tree.XmlTreeUtil;
import org.jumin.zkx.zkxutils.ApplicationConstants;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treerow;

import com.sun.xml.xsom.XSDeclaration;
import com.sun.xml.xsom.XSModelGroup;

/**
 * The class <code>XmlTreeDescriptionCellRenderer</code> is used for rendering widgets in the description cell of a
 * message tree.
 * 
 * @version Apr 25, 2014 / jru : initial version <br>
 */
public class XmlTreeDescriptionCellRenderer implements Serializable {
    private static final long serialVersionUID = 3821929391806096064L;

    private static Logger log = LoggerFactory.getLogger(XmlTreeDescriptionCellRenderer.class);

    protected JrxXmlModelUtil jrxXmlUtil = null;

    private EditorMode editorMode = EditorMode.VIEW;

    public XmlTreeDescriptionCellRenderer() {
        super();
        jrxXmlUtil = new JrxXmlModelUtil();
    }

    protected RepetitionRemoveButton addRemoveButton(Treecell cell, XmlTreerow row) {
        return addRemoveButton(cell, row, null);
    }

    protected RepetitionRemoveButton addRemoveButton(Treecell cell, XmlTreerow row,
            RepetitionRemoveButtonListener listener) {
        RepetitionRemoveButton button = new RepetitionRemoveButton(ZkLabels.getLabel("general.remove"), row, listener);
        button.setParent(cell);

        XmlTreeUtil.registerControlKeys(button);

        return button;
    }

    protected RepetitionAddButton addAddButton(Treecell cell, XmlChoiceOptionCombobox combobox, XmlTreerow row) {
        RepetitionAddButton button = new RepetitionAddButton(ZkLabels.getLabel("general.add"), combobox, row);
        button.setParent(cell);

        XmlTreeUtil.registerControlKeys(button);

        return button;
    }

    public Treecell createThirdCell(XmlTreerow row) {
        if (row.getData() instanceof JrxElementGroup) {
            return createThirdCell(row, (JrxElementGroup) row.getData());
        } else if (row.getData() instanceof JrxGroup) {
            return createThirdCell(row, (JrxGroup) row.getData());
        }

        return createThirdCell(row, (JrxElement) row.getData());
    }

    /**
     * Create empty 3rd tree cell
     * <p>
     * 
     * @param row
     *            is the tree row that hosts the new created tree cell
     * @return new tree cell for 3rd column of a tree row
     */
    public Treecell createThirdCell(XmlTreerow row, JrxElement jrxElement) {
        final Treecell cell = new Treecell();
        cell.setParent(row);

        if (jrxElement.getXsdDeclaration() == null) {
            return cell;
        }

        JrxElement jrxEffectiveElement = jrxElement;
        if (jrxElement.getChildrenBlock() != null && jrxElement.getChildrenBlock().getElements().size() == 1) {
            // Node with single child

            JrxTerm<?> jrxTerm = jrxElement.getChildrenBlock().getElements().get(0);
            if (jrxTerm instanceof JrxElement && ((JrxElement) jrxTerm).isLeaf()) {
                jrxEffectiveElement = (JrxElement) jrxTerm;
            }
        }

		// Fill description at creation time if there is any provided
		String desc = (String) row.getAttribute(ApplicationConstants.COMPONENT_LOOKUP_DESCRIPTION);
		if (desc != null) {
		    cell.setLabel(desc);
		}
		
		if (row.getLookupWidget() != null) {
		    // register event listener to the row if there is any lookup widget registered!
		    row.addEventListener(XmlTreerow.ON_DESCRIPTION, createRowListenerForDescriptionCell(row, cell));
		}
		
		if (jrxEffectiveElement.isLeaf()) {
			if (!EditorMode.VIEW.equals(editorMode) && (jrxEffectiveElement.isRepetitive() || jrxElement.isRepetitive())) {
				// Show add button
				boolean showAddButton = checkShowAddButton(jrxEffectiveElement.isRepetitive() ? jrxEffectiveElement : jrxElement);
				if (showAddButton) {
					addAddButton(cell, null, row);
				} else if (checkShowRemoveButton(jrxEffectiveElement.isRepetitive() ? jrxEffectiveElement : jrxElement)) {
					addRemoveButton(cell, row);
				}
			}
		} else if (!EditorMode.VIEW.equals(editorMode) && jrxEffectiveElement.getChildrenBlock() != null
		        && jrxEffectiveElement.getChildrenBlock().getElements() != null) {

            if (jrxEffectiveElement.getChildrenBlock().getElements().size() == 1) {
                // Complex type with single child
                JrxTerm<?> jrxChildTerm = jrxEffectiveElement.getChildrenBlock().getElements().get(0);

                if (jrxChildTerm instanceof JrxChoiceGroup) {
                    JrxChoiceGroup jrxChildChoiceGroup = (JrxChoiceGroup) jrxChildTerm;
                    if (jrxChildChoiceGroup.getSelection() == null) {
                        // Show add button
                        boolean showAddButton = checkShowAddButton(jrxEffectiveElement);
                        if (showAddButton && row.getChildren().size() > 1) {
                            XmlChoiceOptionCombobox combobox = (XmlChoiceOptionCombobox) ComponentUtils
                                    .getChildByClass((Component) row.getChildren().get(1),
                                            XmlChoiceOptionCombobox.class);
                            // Disable the button because there is no selection yet
                            addAddButton(cell, combobox, row).setDisabled(true);
                        }
                    } else {
                        // Option is selected, show delete button when the multiplicity > 1
                        if (jrxEffectiveElement.isRepetitive() && !jrxXmlUtil.isElementMandatory(jrxEffectiveElement)) {
                            addRemoveButton(cell, row);
                        }
                    }
                    // } else if (jrxChildTerm instanceof JrxElement) {
                    // Single sub-element
                }
            } else {
                // Complex type
                if (!EditorMode.VIEW.equals(editorMode) && jrxEffectiveElement.isRepetitive()) {
                    // Show add button
                    boolean showAddButton = checkShowAddButton(jrxEffectiveElement);
                    if (showAddButton) {
                        addAddButton(cell, null, row);
                    } else if (checkShowRemoveButton(jrxEffectiveElement)) {
                        addRemoveButton(cell, row);
                    }
                }
            }
        }

        return cell;
    }

    public Treecell createThirdCell(Treerow row, JrxElementGroup jrxElementGroup) {
        Treecell cell = new Treecell();

        if (jrxElementGroup.getXsdDeclaration() == null) {
            return cell;
        }

        if (jrxElementGroup.getCompositor().equals(XSModelGroup.CHOICE)) {
            // Create selection
            log.debug("TODO: Create selection!");
        }

        if (jrxElementGroup.isRepetitive()) {
            // repetitive row
            log.debug("Repetitive row");
        }

        return cell;
    }

    public Treecell createThirdCell(XmlTreerow row, JrxGroup jrxGroup) {
        Treecell cell = new Treecell();
        cell.setParent(row);

        if (jrxGroup.getXsdDeclaration() == null) {
            return cell;
        }

        if (!EditorMode.VIEW.equals(editorMode) && jrxGroup.getChildrenBlock() != null
                && jrxGroup.getChildrenBlock().getElements() != null) {

            if (jrxGroup.getChildrenBlock().getElements().size() == 1) {
                // Complex type with single child
                JrxTerm<?> jrxChildTerm = jrxGroup.getChildrenBlock().getElements().get(0);

                if (jrxChildTerm instanceof JrxChoiceGroup) {
                    JrxChoiceGroup jrxChildChoiceGroup = (JrxChoiceGroup) jrxChildTerm;
                    if (jrxChildChoiceGroup.getSelection() == null) {
                        // Show add button
                        boolean showAddButton = checkShowAddButton(jrxGroup);
                        if (showAddButton && row.getChildren().size() > 1) {
                            XmlChoiceOptionCombobox combobox = (XmlChoiceOptionCombobox) ComponentUtils
                                    .getChildByClass((Component) row.getChildren().get(1),
                                            XmlChoiceOptionCombobox.class);
                            // Disable the button because there is no selection yet
                            addAddButton(cell, combobox, row).setDisabled(true);
                        }
                    } else if (checkShowRemoveButton(jrxGroup)) {
                        // Option is selected, show delete button when the multiplicity > 1
                        addRemoveButton(cell, row);
                    }
                    // } else if (jrxChildTerm instanceof JrxElement) {
                    // Single sub-element
                }
            } else {
                // Complex type
                if (!EditorMode.VIEW.equals(editorMode) && jrxGroup.isRepetitive()) {
                    // Show add button
                    boolean showAddButton = checkShowAddButton(jrxGroup);
                    if (showAddButton) {
                        addAddButton(cell, null, row);
                    } else if (checkShowRemoveButton(jrxGroup)) {
                        addRemoveButton(cell, row);
                    }
                }
            }
        }

        return cell;
    }

    public void setEditorMode(EditorMode editorMode) {
        this.editorMode = editorMode;
    }

    public EditorMode getEditorMode() {
        return editorMode;
    }

    private EventListener<Event> createRowListenerForDescriptionCell(final XmlTreerow row, final Treecell cell) {
        return new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                String desc = (String) row.getAttribute(ApplicationConstants.COMPONENT_LOOKUP_DESCRIPTION);
                if (desc == null) {
                    cell.setLabel("");
                } else {
                    cell.setLabel(desc);
                }
            }
        };
    }

    public boolean checkShowAddButton(JrxDeclaration<?> jrxDeclaration) {
        boolean showAddButton = false;
        int elementIndex = getElementIndexWithinSameName(jrxDeclaration);
        int sameNameCount = countSiblingElementsWithSameName(jrxDeclaration);
        if ((elementIndex + 1) >= sameNameCount) {
            if (jrxDeclaration.getMaxOccurs() == -1) {
                // when the max occurs = -1 (*)
                showAddButton = true;
            } else {
                if (jrxDeclaration.getMaxOccurs() > 1 && jrxDeclaration.getMaxOccurs() > sameNameCount) {
                    // when the number of object from the same entity has not reached max occurs yet
                    showAddButton = true;
                }
            }
        }
        return showAddButton;
    }

    public boolean checkShowRemoveButton(JrxDeclaration<?> jrxDeclaration) {
        if (jrxDeclaration.getMaxOccurs() == 0 || jrxDeclaration.getMaxOccurs() == 1) {
            return false;
        }

        boolean isMandatory = jrxDeclaration instanceof JrxElement ? jrxXmlUtil
                .isElementMandatory((JrxElement) jrxDeclaration) : jrxDeclaration.isMandatory();

        if (!isMandatory || getElementIndexWithinSameName(jrxDeclaration) > 0) {
            return true;
        }

        return false;
    }

    public static int countSiblingElementsWithSameName(JrxDeclaration<?> jrxDeclaration) {
        JrxElement jrxParentElement = jrxDeclaration.getParentElement();
        if (jrxParentElement == null) {
            if (jrxDeclaration.getParentBlock() instanceof JrxChoiceGroup) {
                // The element is a selection from a direct choice group
                if (jrxDeclaration.getParentBlock().getParentBlock().getOwner() instanceof JrxElement)
                    jrxParentElement = (JrxElement) jrxDeclaration.getParentBlock().getParentBlock().getOwner();
                else
                    jrxParentElement = ((JrxGroup) jrxDeclaration.getParentBlock().getParentBlock().getOwner())
                            .getParentElement();
            } else if (jrxDeclaration.getParentBlock() instanceof JrxElementGroup) {
                jrxParentElement = JrxXmlModelUtil.getParentElement(jrxDeclaration.getParentBlock());
            }
        }
        return JrxXmlModelUtil.countElementWithSameSchemaDefinition(jrxParentElement.getChildrenBlock(),
                (XSDeclaration) jrxDeclaration.getXsdDeclaration());
    }

    public static int getElementIndexWithinSameName(JrxDeclaration<?> jrxDeclaration) {
        JrxElement jrxParentElement = jrxDeclaration.getParentElement();
        if (jrxParentElement == null) {
            if (jrxDeclaration.getParentBlock() instanceof JrxChoiceGroup) {
                // The element is a selection from a direct choice group
                if (jrxDeclaration.getParentBlock().getParentBlock().getOwner() instanceof JrxElement)
                    jrxParentElement = (JrxElement) jrxDeclaration.getParentBlock().getParentBlock().getOwner();
                else
                    jrxParentElement = ((JrxGroup) jrxDeclaration.getParentBlock().getParentBlock().getOwner())
                            .getParentElement();
            } else if (jrxDeclaration.getParentBlock() instanceof JrxElementGroup) {
                jrxParentElement = JrxXmlModelUtil.getParentElement(jrxDeclaration.getParentBlock());
            }
        }
        return JrxXmlModelUtil.getElementWithSameNameIndex(jrxParentElement.getChildrenBlock(), jrxDeclaration);
    }

}
