/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import org.jumin.zkx.zkxutils.ComponentUtils;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zul.impl.XulElement;

public class XmlTreeUtil {

    public static void registerControlKeys(XulElement widget) {
		if (widget.getCtrlKeys() == null) {
			widget.setCtrlKeys(XmlTree.MANDATORY_JUMP_CONTROL_KEY);
		} else {
			widget.setCtrlKeys(widget.getCtrlKeys() + XmlTree.MANDATORY_JUMP_CONTROL_KEY);
		}
		widget.addEventListener(Events.ON_CTRL_KEY, new EventListener<KeyEvent>() {
			@Override
			public void onEvent(KeyEvent event) throws Exception {
				if (event.getKeyCode() == (int) XmlTree.MANDATORY_KEY_CODE) {
					XmlTree tree = ComponentUtils.getParentByClass(event.getTarget(), XmlTree.class);
					if (tree == null)
						return;
					if (event.isShiftKey()) {
						// move to previous or 1st mandatory input
						tree.selectPreviousMandatoryElement(event.getTarget());
					} else {
						// move to next or last mandatory input
						tree.selectNextMandatoryElement(event.getTarget());
					}
				}
			}
		});
	}
}
