/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;

public class TreeNavigationPopup extends Menupopup {

	private static final long serialVersionUID = -5413475467073389229L;

    public static final String ON_TREE_EXPAND_ALL = "onTreeExpandAll";

	public static final String ON_TREE_EXPAND = "onTreeExpand";

	public static final String ON_TREE_COLLAPSE_ALL = "onTreeCollapseAll";

	public static final String ON_TREE_COLLAPSE = "onTreeCollapse";

	private Menuitem expandAllLink;
	
	private Menuitem expandLevelLink;

	private Menuitem collapseAllLink;
	
	private Menuitem collapseLevelLink;
	
	public TreeNavigationPopup() {
		super();
		createContents();
		initProperties();
		initEvents();
	}

	private void initProperties() {
	}

	private void initEvents() {
		expandAllLink.addForward(Events.ON_CLICK, this, ON_TREE_EXPAND_ALL);
		expandLevelLink.addForward(Events.ON_CLICK, this, ON_TREE_EXPAND, 1);
		collapseAllLink.addForward(Events.ON_CLICK, this, ON_TREE_COLLAPSE_ALL);
		collapseLevelLink.addForward(Events.ON_CLICK, this, ON_TREE_COLLAPSE, 1);
	}

	private void createContents() {
		expandAllLink = new Menuitem("Expand All");
		expandAllLink.setParent(this);
		
		expandLevelLink = new Menuitem("Expand");
		expandLevelLink.setParent(this);
		
		collapseAllLink = new Menuitem("Collapse All");
		collapseAllLink.setParent(this);
		
		collapseLevelLink = new Menuitem("Collapse");
		collapseLevelLink.setParent(this);
	}
	
	public void setDisabled(boolean expandAll, boolean expand, boolean collapseAll, boolean collapse) {
		expandAllLink.setVisible(expandAll);
		expandLevelLink.setVisible(expand);
		
		collapseAllLink.setVisible(collapseAll);
		collapseLevelLink.setVisible(collapse);
	}
}
