/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;

public class ComponentUtils {

	public static void removeChildren(Component component) {
		while (component.getFirstChild() != null) {
			Component child = component.getFirstChild();
			component.removeChild(child);
		}
	}
	
	public static <T extends Component> T getChildByClass(Component parent, Class<T> clazz) {
		return getChildByClass(parent, clazz, false);
	}

	@SuppressWarnings("unchecked")
    public static <T extends Component> T getChildByClass(Component parent, Class<T> clazz, boolean recursive) {
		if (parent == null || clazz == null) {
			return null;
		}
		
		List<Component> childToExploreList = new ArrayList<Component>();
		for (int i = 0; i < parent.getChildren().size(); i++) {
			Component child = (Component) parent.getChildren().get(i);
			if (clazz.isInstance(child)) {
				return (T) child;
			}
			
			if (child.getChildren().size() > 0 && recursive) {
				childToExploreList.add(child); 
			}
		}
		
		Component result = null;
		if (childToExploreList.size() > 0 && recursive) {
			for (Component child : childToExploreList) {
				result = (Component) getChildByClass(child, clazz, recursive);
				if (result != null) {
					return (T) result;
				}
			}
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
    public static <T extends Component> T getParentByClass(Component component, Class<T> clazz) {
		if (component == null || clazz == null) {
			return null;
		}
		
		Component parent = component.getParent();
		while (parent != null) {
			if (clazz.isInstance(parent)) {
				return (T) parent;
			}
			parent = parent.getParent();
		}
		
		return null;
	}

	public static <T extends Component> List<T> getChildrenByClass(Component parent, Class<T> clazz) {
		return getChildrenByClass(parent, clazz, false);
	}

	@SuppressWarnings("unchecked")
    public static <T extends Component> List<T> getChildrenByClass(Component parent, Class<T> clazz, boolean recursive) {
		if (parent == null || clazz == null) {
			return new ArrayList<T>();
		}
		
		List<T> resultList = new ArrayList<T>();
		
		List<Component> childToExploreList = new ArrayList<Component>();
		for (int i = 0; i < parent.getChildren().size(); i++) {
			Component child = (Component) parent.getChildren().get(i);
			if (clazz.isInstance(child)) {
				resultList.add((T) child);
			}
			
			if (child.getChildren().size() > 0 && recursive) {
				childToExploreList.add(child); 
			}
		}
		
		if (childToExploreList.size() > 0 && recursive) {
			for (Component child : childToExploreList) {
				List<T> subResultList = getChildrenByClass(child, clazz, recursive);
				if (subResultList != null && subResultList.size() > 0) {
					resultList.addAll(subResultList);
				}
			}
		}
		
		return resultList;
	}
	
	public static boolean hasEventListener(Component comp, String eventName) {
		Iterable<?> it = comp.getEventListeners(eventName);
		if (it == null || it.iterator() == null || !it.iterator().hasNext()) {
			return false;
		}
		
		return true;
	}
}
