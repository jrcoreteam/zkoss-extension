/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.lookup;

import java.lang.reflect.InvocationTargetException;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.HtmlBasedComponent;

import com.sun.xml.xsom.XSType;

public class DefaultXmlLookupFactory implements XmlLookupFactory {

    private static final long serialVersionUID = -2847937528241597306L;

    protected static Logger log = LoggerFactory.getLogger(DefaultXmlLookupFactory.class);

    protected EditorMode editorMode = EditorMode.VIEW;

    @Override
    public HtmlBasedComponent getWidget(XSType xsType, ServiceConnector connector, String value, XmlTreerow row) {
        String typeName = xsType.getName();

        if (typeName == null)
            return null;

        return getWidget(typeName, connector, value, row);
    }

    @Override
    public HtmlBasedComponent getWidget(String typeName, ServiceConnector connector, String value, XmlTreerow row) {
        XmlTreeAbstractLookupWidgetCreator widgetCreator = getWidgetCreator(typeName, connector, value, row);

        if (widgetCreator == null) {
            return null;
        }

        return widgetCreator.createWidget();
    }

    protected ITypeNameForLookup[] getTypeNamesForLookup() {
        return TypeNameForLookup.values();
    }

    @Override
    public EditorMode getEditorMode() {
        return editorMode;
    }

    @Override
    public void setEditorMode(EditorMode editorMode) {
        this.editorMode = editorMode;
    }

    protected final ITypeNameForLookup[] mergeTypeNamesForLookup(ITypeNameForLookup[] highPriority,
            ITypeNameForLookup[] lowPriority) {
        if (lowPriority == null || lowPriority.length < 1)
            return highPriority;

        if (highPriority == null || highPriority.length < 1)
            return lowPriority;
        ITypeNameForLookup[] combinedResult = new ITypeNameForLookup[lowPriority.length + highPriority.length];

        for (int i = 0; i < highPriority.length; i++) {
            combinedResult[i] = highPriority[i];
        }

        for (int i = 0; i < lowPriority.length; i++) {
            combinedResult[i + highPriority.length] = lowPriority[i];
        }

        return combinedResult;
    }

    @Override
    public XmlTreeAbstractLookupWidgetCreator getWidgetCreator(XSType xsType, ServiceConnector connector, String value,
            XmlTreerow row) {
        String typeName = xsType.getName();

        if (typeName == null)
            return null;

        return getWidgetCreator(typeName, connector, value, row);
    }

    @Override
    public XmlTreeAbstractLookupWidgetCreator getWidgetCreator(String typeName, ServiceConnector connector,
            String value, XmlTreerow row) {
        if (typeName == null)
            return null;

        for (ITypeNameForLookup lookupDef : getTypeNamesForLookup()) {
            boolean match = false;
            if (TypeNameMatchingMechanism.EQUALS.equals(lookupDef.getMatchingMechanism())
                    && typeName.equals(lookupDef.getTypeName())) {
                match = true;
            } else if (TypeNameMatchingMechanism.CONTAINS.equals(lookupDef.getMatchingMechanism())
                    && typeName.contains(lookupDef.getTypeName())) {
                match = true;
            } else if (TypeNameMatchingMechanism.ENDS_WITH.equals(lookupDef.getMatchingMechanism())
                    && typeName.endsWith(lookupDef.getTypeName())) {
                match = true;
            } else if (TypeNameMatchingMechanism.STARTS_WITH.equals(lookupDef.getMatchingMechanism())
                    && typeName.startsWith(lookupDef.getTypeName())) {
                match = true;
            }
            if (match) {
                try {
                    Class<? extends XmlTreeAbstractLookupWidgetCreator> widgetCreatorClass = lookupDef
                            .getWidgetCreatorClass();
                    if (widgetCreatorClass == null) {
                        log.warn("Cannot find configured widget creator class: '" + lookupDef.getLookupClass() + "' -> "
                                + lookupDef.toString());
                        return null;
                    }

                    return instantiateWidgetCreator(widgetCreatorClass, editorMode, connector, value, row);
                } catch (Exception e) {
                    log.warn("Failure in instantiating widget creator defined in lookup creator: "
                            + lookupDef.toString(), e);
                }
            }
        }

        return null;
    }

    public static final XmlTreeAbstractLookupWidgetCreator instantiateWidgetCreator(
            Class<? extends XmlTreeAbstractLookupWidgetCreator> widgetCreatorClass, EditorMode editorMode,
            ServiceConnector connector, String value, XmlTreerow row)
            throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException {
        XmlTreeAbstractLookupWidgetCreator widgetCreator = widgetCreatorClass
                .getDeclaredConstructor(
                        new Class[] { ServiceConnector.class, XmlTreerow.class, EditorMode.class, String.class })
                .newInstance(connector, row, editorMode, value);

        return widgetCreator;
    }
}
