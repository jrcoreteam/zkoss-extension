/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.translator;

import org.jumin.common.utils.translator.StringTranslator;
import org.jumin.zkx.zkxutils.ZkLabels;

public class ZkUtilsLabelResourceTranslator implements StringTranslator<Object> {

	private static final long serialVersionUID = 1799837029159612194L;

    @Override
	public String translate(Object object) {
		if (object == null) return null;
		
		String key = (String) object;
		
		return ZkLabels.getLabel(key, key);
	}

}
