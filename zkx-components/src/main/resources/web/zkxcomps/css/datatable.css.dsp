<%@ page contentType="text/css;charset=UTF-8" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>

.z-label.zkx-description-text {
    font-size: ${fontSizeS};
}

div.z-listbox.zkx-data-table-list {
    margin-top: 4px;
}

div.z-listbox.zkx-data-table-list div.z-listcell-cnt, 
div.z-listbox.zkx-data-table-list div.z-listcell-cnt span.z-label {
    font-family: ${fontFamilyC};
    color: ${fontColorC};
} 

span.z-label.zkx-data-table-summary-label {
    font-size: ${fontSizeS};
}

span.z-label.zkx-data-table-summary-value {
    font-size: ${fontSizeS};
    font-weight: ${fontWeightT};
}
.zkx-form-grid tr.z-row td.z-cell.zkx-top-left-border {
    border-top: 1px dotted #aaaaaa;
    border-left: 1px dotted #aaaaaa;
}
.zkx-form-grid tr.z-row td.z-cell.zkx-top-right-border {
    border-top: 1px dotted #aaaaaa;
    border-right: 1px dotted #aaaaaa;
}
.zkx-form-grid tr.z-row td.z-cell.zkx-bottom-left-border {
    border-bottom: 1px dotted #aaaaaa;
    border-left: 1px dotted #aaaaaa;
}
.zkx-form-grid tr.z-row td.z-cell.zkx-bottom-right-border {
    border-bottom: 1px dotted #aaaaaa;
    border-right: 1px dotted #aaaaaa;
}
/*
.zkx-button-bar .z-button, 
.zkx-button-bar .z-combobutton .z-combobutton-content {
	height:40px;
}*/
.zkx-button-bar {
	height:44px;
}
