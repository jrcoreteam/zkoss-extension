/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Treerow;

public class XmlChoiceOptionCombobox extends Combobox {

	private static final long serialVersionUID = -6608206078596980424L;

	private XmlTreerow row;

	public Treerow getRow() {
		return row;
	}

	public XmlChoiceOptionCombobox(XmlTreerow row) throws WrongValueException {
		super();
		this.row = row;
		initEvents();
	}

	private void initEvents() {
		addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				((Combobox) event.getTarget()).select();
			}
		});
    }

}
