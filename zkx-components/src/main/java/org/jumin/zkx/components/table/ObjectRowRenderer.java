/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.table;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.jumin.common.preferences.table.TableColumnConfiguration;
import org.jumin.common.utils.ObjectPropertyUtil;
import org.jumin.common.utils.translator.FormatLocaleAndTimeZoneAwareStringTranslator;
import org.jumin.common.utils.translator.FormatLocaleAwareStringTranslator;
import org.jumin.common.utils.translator.ObjectCollectionTranslator;
import org.jumin.common.utils.translator.StringTranslator;
import org.jumin.zkx.components.DynamicAttributesWidget;
import org.jumin.zkx.components.exception.ExceptionLogger;
import org.jumin.zkx.components.translator.ListItemEventTranslator;
import org.jumin.zkx.zkxutils.LabelUtils;
import org.jumin.zkx.zkxutils.MessageDialogUtils;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.jumin.zkx.zkxutils.exception.InvalidSystemRuntimeSettingException;
import org.jumin.zkx.zkxutils.translator.DecimalAlignedAmountWidgetTranslator;
import org.jumin.zkx.zkxutils.translator.ObjectValueListcellLocaleAwareTranslator;
import org.jumin.zkx.zkxutils.translator.WidgetListcellLocaleAwareTranslator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;

public class ObjectRowRenderer implements RowRenderer<Object> {

    private static Logger log = LoggerFactory.getLogger(ObjectRowRenderer.class);

    public static final String KEYWORD_SELF = "${self}";

    class SeleniumWrapper {
        Object value;
        String seleniumLabel;

        SeleniumWrapper(Object value, String seleniumLabel) {
            this.value = value;
            this.seleniumLabel = seleniumLabel;
        }
    }

    private ObjectPropertyUtil propUtil = new ObjectPropertyUtil();

    private WidgetStaticLabelUtils widgetDebugUtil = WidgetStaticLabelUtils.getInstance();

    private DynamicAttributesWidget containerWidget;

    public ObjectRowRenderer(DynamicAttributesWidget containerWidget) {
        super();
        this.containerWidget = containerWidget;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void render(Row row, Object data, int index) throws Exception {
        row.setValue(data);

        List<TableColumnConfiguration> fieldConfigList = containerWidget.getOrderedAttributes();
        Locale l = UserContextUtil.getUserFormatLocale(containerWidget.getDesktop());
        TimeZone tz = UserContextUtil.getUserTimeZone(containerWidget.getDesktop());
        if (fieldConfigList == null) {
            row.appendChild(new Label("" + data));
            return;
        }

        for (int i = 0; i < fieldConfigList.size(); i++) {
            TableColumnConfiguration columnConfig = fieldConfigList.get(i);
            if (columnConfig.isVisible()) {
                String valueText = "";
                Cell cell = new Cell();
                cell.setParent(row);
                cell.setStyle("text-overflow:ellipsis;white-space:nowrap;");

                boolean widgetTranslator = false;
                try {
                    Object objectValue = null;
                    HashMap<String, Object> mapValue = new HashMap<>();

                    if (columnConfig.getName().startsWith(KEYWORD_SELF)) {
                        objectValue = data;
                        if (columnConfig.getName().contains("/")) {
                            mapValue.put("object", objectValue);
                            mapValue.put("extra",
                                    columnConfig.getName().substring(columnConfig.getName().indexOf("/") + 1));
                        }
                    } else {
                        objectValue = propUtil.getPropertyValue(data, columnConfig.getName(),
                                columnConfig.getRetrievingMethod());
                    }

                    boolean tryWithTranslator = false;
                    if (null != objectValue) {
                        if (columnConfig.getTranslator() != null && columnConfig.getTranslator().length() > 0) {
                            tryWithTranslator = true;
                        } else {
                            valueText = "" + objectValue;
                            valueText = LabelUtils.formatNonBreakingSpace(valueText);
                        }
                    } else if (columnConfig.getTranslator() != null && columnConfig.getTranslator().length() > 0) {
                        tryWithTranslator = true;
                    }

                    if (tryWithTranslator) {
                        try {
                            Class<?> translatorClazz = Class.forName(columnConfig.getTranslator());
                            Object translatorInstance = translatorClazz.newInstance();
                            if (translatorInstance instanceof StringTranslator<?>) {
                                StringTranslator<Object> concreteTranslatorInstance = (StringTranslator<Object>) translatorInstance;
                                valueText = concreteTranslatorInstance.translate(objectValue);
                            } else if (translatorInstance instanceof ObjectCollectionTranslator<?>) {
                                ObjectCollectionTranslator<Object> concreteTranslatorInstance = (ObjectCollectionTranslator<Object>) translatorInstance;
                                valueText = concreteTranslatorInstance.translate((Collection<Object>) objectValue);
                            } else if (translatorInstance instanceof FormatLocaleAndTimeZoneAwareStringTranslator<?>) {
                                FormatLocaleAndTimeZoneAwareStringTranslator<Object> concreteTranslatorInstance = (FormatLocaleAndTimeZoneAwareStringTranslator<Object>) translatorInstance;
                                valueText = concreteTranslatorInstance.translate(objectValue, l, tz);
                            } else if (translatorInstance instanceof FormatLocaleAwareStringTranslator<?>) {
                                FormatLocaleAwareStringTranslator<Object> concreteTranslatorInstance = (FormatLocaleAwareStringTranslator<Object>) translatorInstance;
                                valueText = concreteTranslatorInstance.translate(objectValue, l);
                            } else if (translatorInstance instanceof DecimalAlignedAmountWidgetTranslator) {
                                valueText = "";
                                widgetTranslator = true;

                                if (objectValue != null) {
                                    // retrieve max decimal size from listheader
                                    Integer maxFractionDigit = columnConfig.getExtendedValue(
                                            DecimalAlignedAmountWidgetTranslator.AMOUNT_MAX_FRACTION_SIZE);

                                    Integer currencyFractionDigit = 2; 
                                    // Default of 2, since this is mostly used.
                                    try {
                                        // retrieve currency based on the config
                                        Map<String, Integer> currencyFractionDigitTable = columnConfig.getExtendedValue(
                                                DecimalAlignedAmountWidgetTranslator.CURRENCY_FRACTION_DIGIT_TABLE);
                                        List<String> currencyListFromRecord = columnConfig.getExtendedValue(
                                                DecimalAlignedAmountWidgetTranslator.CURRENCY_LIST_FROM_RECORD);

                                        if (currencyListFromRecord != null && currencyListFromRecord.size() > index) {
                                            String currencyCode = currencyListFromRecord.get(index);
                                            if (currencyCode != null && currencyFractionDigitTable != null) {
                                                currencyFractionDigit = currencyFractionDigitTable.get(currencyCode);
                                            }
                                        }
                                    } catch (Exception e) {
                                        log.warn("Fail to retrieved stored fraction digit for column: " + columnConfig,
                                                e);
                                    }

                                    if (maxFractionDigit == null)
                                        maxFractionDigit = currencyFractionDigit;
                                    DecimalAlignedAmountWidgetTranslator concreteTranslatorInstance = (DecimalAlignedAmountWidgetTranslator) translatorInstance;
                                    concreteTranslatorInstance.translate(cell, currencyFractionDigit, maxFractionDigit,
                                            (Double) objectValue, l);
                                }
                            } else if (translatorInstance instanceof WidgetListcellLocaleAwareTranslator<?>) {
                                valueText = "";
                                widgetTranslator = true;
                                WidgetListcellLocaleAwareTranslator<?> concreteTranslatorInstance = (WidgetListcellLocaleAwareTranslator<?>) translatorInstance;

                                if (concreteTranslatorInstance instanceof ObjectValueListcellLocaleAwareTranslator<?>) {
                                    if (concreteTranslatorInstance instanceof ListItemEventTranslator<?>) {
                                        ((ListItemEventTranslator<Object>) concreteTranslatorInstance).setListItem(row);

                                        row.addForward(AbstractDataTable.ON_ITEM_ACTION, row.getGrid(),
                                                AbstractDataTable.ON_ITEM_ACTION);
                                    }
                                    ((ObjectValueListcellLocaleAwareTranslator<?>) concreteTranslatorInstance)
                                            .translate(cell, l, tz, mapValue);
                                } else {
                                    ((WidgetListcellLocaleAwareTranslator<Object>) concreteTranslatorInstance)
                                            .translate(cell, l, tz, objectValue);
                                }
                            } else {
                                // register error
                                String errorMsg = "Translator: '" + columnConfig.getTranslator()
                                        + "' is not a valid registered translator. Fallback to default data formatting.";
                                ExceptionLogger.registerInternalError(errorMsg,
                                        new InvalidSystemRuntimeSettingException(errorMsg));

                                // Fallback to string output
                                valueText = "" + objectValue;
                            }
                        } catch (Exception e) {
                            // register error
                            String errorMsg = "Error using translator: '" + columnConfig.getTranslator()
                                    + "' with data: " + objectValue + "\nFallback to default data formatting.";
                            ExceptionLogger.registerInternalError(errorMsg, e);

                            widgetTranslator = false;

                            // Fallback to string output
                            valueText = "" + objectValue;
                        }
                    }

                } catch (Exception e) {
                    valueText = "";
                    MessageDialogUtils.captureInternalError(e,
                            UserContextUtil.getSessionUserPrincipal(row.getDesktop()), columnConfig);
                    return;
                }
                if (!widgetTranslator) {
                    cell.appendChild(new Label((valueText != null) ? valueText : ""));
                }

                if (null != columnConfig.getSeleniumLabel() && columnConfig.getSeleniumLabel().length() > 0) {
                    widgetDebugUtil.setLabel(cell, columnConfig.getSeleniumLabel() + "_" + index);
                }

                // Behaviors
                row.addForward(Events.ON_DOUBLE_CLICK, row.getGrid(), AbstractDataTable.ON_ITEM_OPEN);
            }
        }
    }
}
