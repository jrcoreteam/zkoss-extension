/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jumin.common.preferences.table.TableColumnConfiguration;
import org.jumin.common.preferences.table.TableColumnSortOrder;
import org.jumin.common.preferences.table.TableConfiguration;
import org.jumin.common.utils.query.AttributeOrder.SortOrder;
import org.jumin.zkx.components.CommonButtonBar;
import org.jumin.zkx.components.model.button.AssignmentItem;
import org.jumin.zkx.components.model.button.FunctionAssignment;
import org.jumin.zkx.components.model.button.UserFunction;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.events.ApplicationEvents;
import org.jumin.zkx.zkxutils.events.EntityCommonFunctionEvents;
import org.jumin.zkx.zkxutils.renderer.ListitemRendererWithHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

public class TablePreferenceDialog extends Window {

    private static final long serialVersionUID = -2538886046130134014L;

    private static final Logger log = LoggerFactory.getLogger(TablePreferenceDialog.class);

    protected Listbox columnList;

    protected Hlayout titleLayout;

    protected A moveUpButton;

    protected A moveDownButton;

    protected A resetSortOrderButton;

    protected CommonButtonBar buttonBar;

    protected TableConfiguration model;

    protected DisplayColumnListitemRenderer renderer = new DisplayColumnListitemRenderer();

    protected static List<AssignmentItem> leftFunctionList;

    protected static List<AssignmentItem> rightFunctionList;

    static {
        leftFunctionList = new ArrayList<AssignmentItem>();

        UserFunction uf;

        uf = new UserFunction();
        uf.setId("resetToDefault");
        uf.setEventName(EntityCommonFunctionEvents.ON_RESET);
        uf.setDefaultLabel("Reset to Default");
        uf.setLabelKey("tableConfigurationDialog.resetToDefault");
        leftFunctionList.add(new FunctionAssignment(uf));

        uf = new UserFunction();
        uf.setId("savePreferences");
        uf.setEventName(EntityCommonFunctionEvents.ON_SAVE);
        uf.setDefaultLabel("Save Preferences");
        uf.setLabelKey("tableConfigurationDialog.savePreferences");
        leftFunctionList.add(new FunctionAssignment(uf));

        rightFunctionList = new ArrayList<AssignmentItem>();

        uf = new UserFunction();
        uf.setId("cancel");
        uf.setEventName(Events.ON_CANCEL);
        uf.setDefaultLabel("Cancel");
        uf.setLabelKey("general.cancel");
        rightFunctionList.add(new FunctionAssignment(uf));

        uf = new UserFunction();
        uf.setId("ok");
        uf.setEventName(Events.ON_OK);
        uf.setDefaultLabel("OK");
        uf.setLabelKey("general.ok");
        rightFunctionList.add(new FunctionAssignment(uf));
    }

    public TablePreferenceDialog() {
        super();
        createContents();
        initEvents();
        initProperties();
    }

    protected void initProperties() {
        log.debug("start");

        this.setTitle(ZkLabels.getLabel("tablePreferenceDialog.title", "Column Settings"));

        setHeight("600px");
        setWidth("800px");
        setBorder(true);
        setSizable(true);

        columnList.setVflex("true");
        columnList.setItemRenderer(renderer);
        columnList.appendChild(renderer.getListhead());

        buttonBar.setModel(null, leftFunctionList, rightFunctionList, null);
        buttonBar.setHflex("true");

        moveDownButton.setDisabled(true);
        moveUpButton.setDisabled(true);

        log.debug("end");
    }

    protected void initEvents() {
        log.debug("start");

        addEventListener(ApplicationEvents.ON_ASYNC_REFRESH, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                refresh();
            }
        });

        moveUpButton.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                doMoveDisplayColumnUp();
            }
        });

        moveDownButton.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                doMoveDisplayColumnDown();
            }
        });

        resetSortOrderButton.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                clearSortOrder();
            }
        });

        buttonBar.addForward(Events.ON_CANCEL, this, Events.ON_CANCEL);
        buttonBar.addForward(Events.ON_OK, this, Events.ON_OK);

        buttonBar.addForward(EntityCommonFunctionEvents.ON_RESET, this, EntityCommonFunctionEvents.ON_RESET);
        buttonBar.addForward(EntityCommonFunctionEvents.ON_SAVE, this, EntityCommonFunctionEvents.ON_SAVE);

        columnList.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                changeColumnSelectionButtonAvailabilities();
            }
        });

        log.debug("end");
    }

    protected void changeColumnSelectionButtonAvailabilities() {
        int selectionIndex = columnList.getSelectedIndex();
        moveUpButton.setDisabled(selectionIndex < 1);
        moveDownButton.setDisabled((selectionIndex > columnList.getItemCount() - 2) || selectionIndex < 0);
    }

    protected void createContents() {
        log.debug("start");

        Vlayout mainLayout = new Vlayout();
        mainLayout.setVflex("true");
        mainLayout.setHflex("true");
        mainLayout.setParent(this);

        titleLayout = new Hlayout();
        titleLayout.setValign("bottom");
        titleLayout.setParent(mainLayout);

        Label label = new Label(ZkLabels.getLabel("dataTablePreferenceDialog.displaySequence", "Display Sequence:"));
        label.setParent(titleLayout);
        label.setHflex("true");
        
        Space space = new Space();
        space.setHflex("true");
        space.setParent(titleLayout);
        
        Hlayout buttonContainer = new Hlayout();
        buttonContainer.setValign("middle");
        buttonContainer.setParent(titleLayout);
        
        moveUpButton = new A();
        moveUpButton.setIconSclass("z-icon-arrow-up z-icon-2x");
        moveUpButton.setTooltiptext(ZkLabels.getLabel("dataTablePreferenceDialog..moveUp", "Move selected column up"));
        moveUpButton.setParent(buttonContainer);

        moveDownButton = new A();
        moveDownButton.setIconSclass("z-icon-arrow-down z-icon-2x");
        moveDownButton.setTooltiptext(ZkLabels.getLabel("dataTablePreferenceDialog.moveDown", "Move selected column down"));
        moveDownButton.setParent(buttonContainer);

        resetSortOrderButton = new A();
        resetSortOrderButton.setIconSclass("z-icon-times z-icon-stack-1x, z-icon-sort-alpha-asc z-icon-stack-1x z-icon-2x");
        resetSortOrderButton.setTooltiptext(ZkLabels.getLabel("dataTablePreferenceDialog.clearSort", "Clear sorting"));
        resetSortOrderButton.setParent(buttonContainer);

        columnList = new Listbox();
        columnList.setParent(mainLayout);

        mainLayout.appendChild(new Separator());

        buttonBar = new CommonButtonBar(true, true, false);
        buttonBar.setParent(mainLayout);
    }

    private void doMoveDisplayColumnDown() {
        if (columnList.getSelectedItem() != null) {
            ListModelList<Object> lm = (ListModelList<Object>) columnList.getListModel();
            int currentIdx = columnList.getSelectedIndex();
            if (currentIdx == columnList.getItemCount() - 1)
                return;

            Object listitem = lm.get(currentIdx);
            Object nextlistitem = lm.get(currentIdx + 1);

            lm.set(currentIdx, nextlistitem);
            lm.set(currentIdx + 1, listitem);
            columnList.setSelectedIndex(currentIdx + 1);
            changeColumnSelectionButtonAvailabilities();
        } else {
        }
    }

    private void doMoveDisplayColumnUp() {
        if (columnList.getSelectedItem() != null) {
            ListModelList<Object> lm = (ListModelList<Object>) columnList.getListModel();
            int currentIdx = columnList.getSelectedIndex();
            if (currentIdx == 0)
                return;

            Object listitem = lm.get(currentIdx);
            Object prevlistitem = lm.get(currentIdx - 1);

            lm.set(currentIdx, prevlistitem);
            lm.set(currentIdx - 1, listitem);
            columnList.setSelectedIndex(currentIdx - 1);
            changeColumnSelectionButtonAvailabilities();
        } else {
        }
    }

    public void setModel(TableConfiguration model) {
        this.model = model;
        synchronizeModelToWidget();
    }

    public TableConfiguration getModel() {
        synchronizeModelFromWidget();
        return model;
    }

    protected void synchronizeModelToWidget() {
        if (model == null)
            return;

        ListModel<TableColumnConfiguration> listModel = new ListModelList<TableColumnConfiguration>(model.getColumns());
        columnList.setModel(listModel);

        Map<TableColumnConfiguration, SortIndexAndOrderDirection> columnSortOrderMap = new HashMap<TableColumnConfiguration, TablePreferenceDialog.SortIndexAndOrderDirection>();
        for (int i = 0; i < model.getColumnSortOrderList().size(); i++) {
            TableColumnSortOrder sortOrder = model.getColumnSortOrderList().get(i);
            columnSortOrderMap.put(sortOrder.getColumn(), new SortIndexAndOrderDirection(i + 1, sortOrder.getOrder()));
        }

        renderer.setColumnSortOrderMap(columnSortOrderMap);
    }

    @SuppressWarnings("unchecked")
    protected void synchronizeModelFromWidget() {
        ListModelList<?> lml = (ListModelList<?>) columnList.getModel();
        model.setColumns((List<TableColumnConfiguration>) lml.getInnerList());
    }

    private void clearSortOrder() {
        model.getColumnSortOrderList().clear();
        synchronizeModelToWidget();
        changeColumnSelectionButtonAvailabilities();
        titleLayout.invalidate();
    }

    public void refresh() {
        titleLayout.getParent().invalidate();
    }

    private static class SortIndexAndOrderDirection implements Serializable {

        private static final long serialVersionUID = 990083008142016834L;

        private int index = -1;

        private SortOrder order = SortOrder.ASCENDING;

        private SortIndexAndOrderDirection(int index, SortOrder order) {
            this.index = index;
            this.order = order;
        }

        @Override
        public String toString() {
            return index + " - " + order.toString().substring(0, 3);
        }
    }

    public static final String createColumnDescriptionKey(TableColumnConfiguration columnConfig) {
        return CommonDataTable.createTableColumnHeaderKey(columnConfig) + ".description";
    }

    private class DisplayColumnListitemRenderer implements ListitemRendererWithHeader<TableColumnConfiguration> {

        private static final long serialVersionUID = 3370620219696620027L;

        private Map<TableColumnConfiguration, SortIndexAndOrderDirection> columnSortOrderMap = new HashMap<TableColumnConfiguration, SortIndexAndOrderDirection>();

        @Override
        public void render(Listitem item, TableColumnConfiguration data, int index) throws Exception {
            final TableColumnConfiguration columnConfig = data;
            Listcell cell = null;

            Checkbox cb = new Checkbox();
            if (columnConfig.isVisible())
                cb.setChecked(true);

            cb.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    CheckEvent checkEvent = (CheckEvent) event;
                    columnConfig.setVisible(checkEvent.isChecked());
                }
            });

            cell = new Listcell();
            cb.setParent(cell);
            cell.setParent(item);

            cell = new Listcell(columnConfig.getWidth());
            cell.setParent(item);

            cell = new Listcell();
            cell.setParent(item);
            if (columnSortOrderMap != null) {
                SortIndexAndOrderDirection sortIndexAndOrder = columnSortOrderMap.get(columnConfig);
                if (sortIndexAndOrder != null) {
                    cell.setLabel(sortIndexAndOrder.toString());
                }
            }

            cell = new Listcell(ZkLabels.getLabel(CommonDataTable.createTableColumnHeaderKey(columnConfig),
                    columnConfig.getLabel()));
            cell.setParent(item);

            cell = new Listcell(ZkLabels.getLabel(createColumnDescriptionKey(columnConfig), columnConfig.getLabel()));
            cell.setParent(item);

            item.setValue(data);
        }

        public void setColumnSortOrderMap(
                Map<TableColumnConfiguration, SortIndexAndOrderDirection> columnSortOrderMap) {
            this.columnSortOrderMap = columnSortOrderMap;
        }

        @Override
        public Listhead getListhead() {
            Listhead head = new Listhead();
            head.setSizable(true);
            Listheader header = null;

            header = new Listheader(ZkLabels.getLabel("general.visible", "Visible"));
            header.setAlign("center");
            header.setWidth("80px");
            header.setParent(head);

            header = new Listheader(ZkLabels.getLabel("general.width", "Width"));
            header.setAlign("right");
            header.setWidth("80px");
            header.setParent(head);

            header = new Listheader(
                    ZkLabels.getLabel("tableConfigurationDialog.columnList.header.sortOrder", "Sort Order"));
            header.setAlign("right");
            header.setWidth("106px");
            header.setParent(head);
            //header.setIconSclass("z-icon-times");

            header = new Listheader(
                    ZkLabels.getLabel("tableConfigurationDialog.columnList.header.columnName", "Column Name"));
            header.setAlign("left");
            header.setParent(head);

            header = new Listheader(ZkLabels.getLabel("tableConfigurationDialog.columnList.header.columnDescription",
                    "Column Description"));
            header.setAlign("left");
            header.setParent(head);

            return head;
        }

    }

}
