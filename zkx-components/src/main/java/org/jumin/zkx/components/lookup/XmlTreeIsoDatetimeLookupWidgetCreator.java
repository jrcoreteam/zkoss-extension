/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.lookup;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.common.utils.DatetimeTextParser;
import org.jumin.common.utils.LocaleUtils;
import org.jumin.zkx.components.IsoDatetimebox;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;

public class XmlTreeIsoDatetimeLookupWidgetCreator extends XmlTreeAbstractLookupWidgetCreator {

	private static final long serialVersionUID = -2918251843511274675L;

	private final DatetimeTextParser dateTimeParser = new DatetimeTextParser();

	public XmlTreeIsoDatetimeLookupWidgetCreator(ServiceConnector connector, XmlTreerow row,
	        EditorMode editorMode, String initialValue) {
		super(connector, row, editorMode, initialValue);
	}

	@Override
	public HtmlBasedComponent createWidget() {
		if (EditorMode.VIEW.equals(editorMode)) {
			return createViewModeWidget();
		}

		IsoDatetimebox dtbox = new IsoDatetimebox();
		dtbox.setTimeZone(LocaleUtils.TIMEZONE_UTC);
		if (value != null && !value.equals("")) {
			Date date = dateTimeParser.getDateFromISODateTime(value);
			dtbox.setValue(date);
			registerValueDescription(value, false);
		}

		dtbox.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				String isoDateTimeValue = "";
				IsoDatetimebox dtbox = (IsoDatetimebox) event.getTarget();

				if (dtbox.getValue() != null) {
					isoDateTimeValue = dateTimeParser.getISODateTimeFromDate(dtbox.getValue());
				}

				registerValueDescription(isoDateTimeValue, true);
			}
		});

		row.setLookupWidget(dtbox);
		return dtbox;
	}

	@Override
	protected void registerValueDescription(String value, boolean triggerEvent) {
		if (row == null || row.getDesktop() == null)
			return;

		String desc = "";

		if (value != null && !value.equals("")) {
			TimeZone currentTz = UserContextUtil.getUserTimeZone(row.getDesktop());
			Locale currentLocale = UserContextUtil.getUserFormatLocale(row.getDesktop());
			desc = dateTimeParser.getStringFromISODateTime(value, currentLocale, currentTz);
		}
		if (desc == null)
			desc = "";

		registerValueDescription(row, desc, triggerEvent);
	}

}
