/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import org.zkoss.zul.AbstractListModel;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

public class ComboboxUtils {
	
	public static <T> T getSelectedValue(Combobox combobox) {
		Comboitem selectedItem = combobox.getSelectedItem();

		if (selectedItem == null)
			return null;

		return selectedItem.getValue();
	}

	public static void initializeValue(Combobox combobox) {
		initializeValue(combobox, 0);
	}

	public static void initializeValue(Combobox combobox, int index) {
		if (combobox.getItemCount() > 0) {
			Comboitem cbItem = null; 
			if (index >= 0 && index < combobox.getItemCount())
				cbItem = combobox.getItemAtIndex(index);
			else
				cbItem = combobox.getItemAtIndex(0);
			
			combobox.setSelectedItem(cbItem);
			combobox.setText(cbItem.getLabel());
		} else {
			combobox.setValue("");
		}
	}

	public static void setSelectionByValueOrEmpty(Combobox combobox, Object value) {
	    if (combobox.getModel() != null && combobox.getModel() instanceof AbstractListModel) {
	        ((AbstractListModel<?>) combobox.getModel()).clearSelection();
	    }
		Comboitem item = getItemByValue(combobox, value);
		if (item != null) {
			combobox.setSelectedItem(item);
			combobox.setText(item.getLabel());
		} else
			combobox.setValue("");
	}

	public static void setSelectionByValue(Combobox combobox, Object value) {
		Comboitem item = getItemByValue(combobox, value);
		if (item != null) {
			combobox.setSelectedItem(item);
			combobox.setText(item.getLabel());
		}
	}

	public static Comboitem getItemByValue(Combobox combobox, Object value) {
		for (int i = 0; i < combobox.getItemCount(); i++) {
			Comboitem item = combobox.getItemAtIndex(i);
			Object itemValue = item.getValue();
			if ((itemValue == null && value == null) || itemValue != null && itemValue.equals(value)) {
				return item;
			}
		}

		return null;
	}
	
}
