/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import java.util.List;
import java.util.Set;

import org.jumin.zkx.components.model.button.AssignmentItem;
import org.jumin.zkx.components.model.button.FunctionAssignment;
import org.jumin.zkx.components.model.button.FunctionSeparator;
import org.jumin.zkx.components.model.button.HorizontalFunctionSeparator;
import org.jumin.zkx.components.model.button.StateFunctionAssignment;
import org.jumin.zkx.components.model.button.UserFunction;
import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.events.SecureComponentEventListener;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobutton;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Menuseparator;
import org.zkoss.zul.Space;
import org.zkoss.zul.impl.LabelImageElement;

public class CommonButtonBar extends Hlayout {

    private static final long serialVersionUID = 6347585669126504550L;

    public static final String DEFAULT_ACTION_EVENT_NAME_VAR = "defaultActionEventName";

    // private Div moreButtonContainer;

    private Button moreButton;

    // private Hlayout detailButtonContainer;
    //
    // private Hlayout specialButtonContainer;
    //
    private Combobutton actionButton;

    private List<AssignmentItem> specialFunctions;

    private List<AssignmentItem> commonActions;

    private List<AssignmentItem> moreFunctions;

    private List<AssignmentItem> detailFunctions;

    private boolean hideActionButton = false;

    private boolean hideMoreButton = false;

    private boolean manualDetailButtonControl = true;

    protected WidgetStaticLabelUtils widgetDebugUtil = null;

    private Component targetComponent = this;

    private Space centerSpace;

    public CommonButtonBar() {
        this(false);
    }

    public CommonButtonBar(boolean hideActionButton) {
        this(hideActionButton, false);
    }

    public CommonButtonBar(boolean hideActionButton, boolean hideMoreButton) {
        this(hideActionButton, hideMoreButton, true);
    }

    public CommonButtonBar(boolean hideActionButton, boolean hideMoreButton, boolean manualDetailButtonControl) {
        super();
        this.hideActionButton = hideActionButton;
        this.hideMoreButton = hideMoreButton;
        this.manualDetailButtonControl = manualDetailButtonControl;

        widgetDebugUtil = WidgetStaticLabelUtils.getInstance();
        createContents();
        initProperties();
        initEvents();
    }

    protected void initProperties() {
        if (!hideMoreButton) {
            // moreButtonContainer.setStyle("min-width: 100px;");

            moreButton.setVisible(false);
            widgetDebugUtil.setLabel(moreButton, "moreButton");
        }

        if (!hideActionButton) {
            actionButton.setVisible(true);
            widgetDebugUtil.setLabel(actionButton, "commonActionButton");
        }

        setSclass("zkx-button-bar");
    }

    protected void initEvents() {
        if (!hideMoreButton) {
            moreButton.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    showMoreFunctions();
                }
            });
        }
        if (!hideActionButton) {
            actionButton.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    raiseFunctionEvent((String) actionButton.getAttribute(DEFAULT_ACTION_EVENT_NAME_VAR));
                }
            });
        }
    }

    private void raiseFunctionEvent(String eventName) {
        if (eventName == null || eventName.equals(""))
            return;
        Events.postEvent(eventName, getTargetComponent(), null);
    }

    protected void createContents() {
        if (!hideMoreButton) {
            // moreButtonContainer = new Div();
            // moreButtonContainer.setParent(this);
            //
            moreButton = new Button(ZkLabels.getLabel("general.more", "More"));
            moreButton.setIconSclass("z-icon-chevron-right");
            moreButton.setDir("reverse");
            moreButton.setParent(this);
        }
        //
        // detailButtonContainer = new Hlayout();
        // detailButtonContainer.setParent(this);

        centerSpace = new Space();
        centerSpace.setOrient("horizontal");
        centerSpace.setHflex("true");
        centerSpace.setParent(this);

        // specialButtonContainer = new Hlayout();
        // specialButtonContainer.setParent(this);

        if (!hideActionButton) {
            appendChild(new Space());

            actionButton = new Combobutton();
            actionButton.setParent(this);
        }
    }

    public void setModel(StateFunctionAssignment sfa) {
        setModel(sfa.getMoreFunctions(), sfa.getSpecialFunctions(), sfa.getCommonActions());
    }

    public void setModel(List<AssignmentItem> moreFunctions, List<AssignmentItem> specialFunctions,
            List<AssignmentItem> commonActions) {
        setModel(moreFunctions, null, specialFunctions, commonActions);
    }

    public void setModel(List<AssignmentItem> moreFunctions, List<AssignmentItem> detailFunctions,
            List<AssignmentItem> specialFunctions, List<AssignmentItem> commonActions) {

        this.detailFunctions = detailFunctions;
        this.moreFunctions = moreFunctions;
        this.specialFunctions = specialFunctions;
        this.commonActions = commonActions;

        if (!hideMoreButton) {
            if (moreFunctions != null && hasVisibleItems(moreFunctions)) {
                moreButton.setVisible(true);
            } else {
                moreButton.setVisible(false);
            }
        }

        renderDetailButtons();

        renderSpecialButtons();

        if (!hideActionButton) {
            if (commonActions != null) {
                if (registerComboButtonFunctions())
                    actionButton.setVisible(true);
                else
                    actionButton.setVisible(false);
            } else {
                actionButton.setVisible(false);
            }
        }

        invalidate();
    }

    private void renderDetailButtons() {
        if (manualDetailButtonControl)
            return; // Skip configuration rendering when manual detail button
                    // control is active

        resetDetailButtonContainer();
        if (detailFunctions == null)
            return;

        renderButtonsFromAssignmentList(detailFunctions, true);
    }

    private boolean hasVisibleItems(List<AssignmentItem> items) {
        if (items.size() == 0)
            return false;

        for (AssignmentItem i : items)
            if (i instanceof FunctionAssignment && ((FunctionAssignment) i).isVisible())
                return true;
        return false;
    }

    private boolean registerComboButtonFunctions() {
        if (hideActionButton)
            return true;

        FunctionAssignment lastAction = null;
        boolean registeredActions = false;

        int lastIndex = commonActions.size() - 1;
        while (lastIndex >= 0) {
            AssignmentItem item = commonActions.get(lastIndex);
            if (item instanceof FunctionAssignment) {
                if (((FunctionAssignment) item).isVisible()) {
                    lastAction = (FunctionAssignment) item;
                    break;
                }
            }
            lastIndex--;
        }

        if (lastAction != null) {
            actionButton.setLabel(ZkLabels.getLabel(lastAction.getFunction().getLabelKey(),
                    lastAction.getFunction().getDefaultLabel()));
            actionButton.setAttribute(DEFAULT_ACTION_EVENT_NAME_VAR, lastAction.getFunction().getEventName());
            registeredActions = true;
        } else {
            actionButton.setLabel("");
        }
        Menupopup comboButtonPopup = (Menupopup) actionButton.getFirstChild();
        if (comboButtonPopup == null) {
            comboButtonPopup = new Menupopup();
            actionButton.appendChild(comboButtonPopup);
        } else {
            while (comboButtonPopup.getFirstChild() != null) {
                comboButtonPopup.removeChild(comboButtonPopup.getFirstChild());
            }
        }

        boolean onlySep = true;
        for (int i = 0; i < lastIndex; i++) {
            if (commonActions.get(i) instanceof FunctionSeparator) {
                if (i < lastIndex - 1 && !onlySep) {
                    comboButtonPopup.appendChild(new Menuseparator());
                    onlySep = true;
                }
            } else {
                FunctionAssignment functionAssignment = (FunctionAssignment) commonActions.get(i);
                UserFunction function = functionAssignment.getFunction();
                if (!functionAssignment.isVisible() || functionAssignment.isDisabled())
                    continue;
                onlySep = false;
                Menuitem comboButtonOtherItem = new Menuitem(
                        ZkLabels.getLabel(function.getLabelKey(), function.getDefaultLabel()));

                setImage(functionAssignment, comboButtonOtherItem);

                comboButtonOtherItem.setParent(comboButtonPopup);
                comboButtonOtherItem.addEventListener(Events.ON_CLICK, new SecureComponentEventListener(
                        getTargetComponent(), function.getEventName(), function.getAction()));
                widgetDebugUtil.setLabel(comboButtonOtherItem, function.getLabelKey());
            }
        }
        // If the actionButton has NO sub-action (only one action), remove the
        // "popup"
        if (comboButtonPopup.getChildren().size() == 0)
            actionButton.removeChild(comboButtonPopup);

        // If the actionButton has ONLY separators, remove them
        if (onlySep) {
            while (comboButtonPopup.getChildren().size() >= 1) {
                comboButtonPopup.removeChild(comboButtonPopup.getFirstChild());
            }
        }

        if (actionButton.getLabel() == null || actionButton.getLabel().equals("")) {
            actionButton.setVisible(false);
        } else {
            actionButton.setVisible(lastAction.isVisible());
        }

        if (lastAction != null) {
            actionButton.setDisabled(lastAction.isDisabled());
        }

        return registeredActions;
    }

    private void showMoreFunctions() {
        if (moreFunctions == null || moreFunctions.size() == 0) {
            moreButton.setVisible(false);
            return;
        }

        moreButton.setVisible(true);
        Menupopup moreActionMenuPopup = new Menupopup();
        for (int i = 0; i < moreFunctions.size(); i++) {
            if (moreFunctions.get(i) instanceof FunctionSeparator) {
                if (i > 0 && i < moreFunctions.size() - 1)
                    moreActionMenuPopup.appendChild(new Menuseparator());
            } else {
                FunctionAssignment functionAssignment = (FunctionAssignment) moreFunctions.get(i);
                UserFunction function = functionAssignment.getFunction();
                if (!functionAssignment.isVisible())
                    continue;

                Menuitem menuItem = new Menuitem(ZkLabels.getLabel(function.getLabelKey(), function.getDefaultLabel()));

                setImage(functionAssignment, menuItem);
                moreActionMenuPopup.appendChild(menuItem);

                if (functionAssignment.isDisabled())
                    menuItem.setDisabled(true);
                else {
                    menuItem.addEventListener(Events.ON_CLICK, new SecureComponentEventListener(getTargetComponent(),
                            function.getEventName(), function.getAction()));
                    widgetDebugUtil.setLabel(menuItem, function.getLabelKey());
                }
            }
        }
        moreActionMenuPopup.setPage(getPage());
        moreActionMenuPopup.open(moreButton, "before_start");
    }

    private void renderSpecialButtons() {
        // Clear detail buttons -> Those buttons or spaces before center space
        // and after More Button
        Component centerSpaceNextSibling = centerSpace.getNextSibling();
        while (centerSpaceNextSibling != null && !centerSpaceNextSibling.equals(actionButton)) {
            removeChild(centerSpaceNextSibling);
            centerSpaceNextSibling = centerSpace.getNextSibling();
        }

        if (specialFunctions == null)
            return;

        renderButtonsFromAssignmentList(specialFunctions, false);
    }

    public void resetDetailButtonContainer() {
        // Clear detail buttons -> Those buttons or spaces before center space
        // and after More Button
        Component centerSpacePreviousSibling = centerSpace.getPreviousSibling();
        while (centerSpacePreviousSibling != null && !centerSpacePreviousSibling.equals(moreButton)) {
            removeChild(centerSpacePreviousSibling);
            centerSpacePreviousSibling = centerSpace.getPreviousSibling();
        }
    }

    public void addDetailButton(Button button) {
        insertBefore(button, centerSpace);
    }

    public void disableButtonByActionSet(Set<String> actionSetToDisable) {
        if (actionSetToDisable == null)
            return;

        if (commonActions != null) {
            // Common Actions
            disableButtonByActionSet(actionSetToDisable, commonActions);
        }

        if (specialFunctions != null) {
            // Special Functions
            disableButtonByActionSet(actionSetToDisable, specialFunctions);
        }

        if (moreFunctions != null) {
            // More Actions
            disableButtonByActionSet(actionSetToDisable, moreFunctions);
        }

        // Feed-in again via setModel
        setModel(moreFunctions, specialFunctions, commonActions);
    }

    private void disableButtonByActionSet(Set<String> actionSetToDisable, List<AssignmentItem> assignmentItemList) {
        for (AssignmentItem ai : assignmentItemList) {
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment fa = (FunctionAssignment) ai;
                if (actionSetToDisable.contains(fa.getFunction().getId())) {
                    fa.setDisabled(true);
                }
            }
        }
    }

    private void setImage(FunctionAssignment functionAssignment, LabelImageElement widget) {
        String buttonImage = functionAssignment.getImage();
        if (buttonImage == null || buttonImage.equals("")) {
            buttonImage = functionAssignment.getFunction().getImage();
        }
        if (buttonImage != null && !buttonImage.equals("")) {
            widget.setImage(buttonImage);
        }

    }

    private void renderButtonsFromAssignmentList(List<AssignmentItem> assignmentItemList, boolean before) {
        for (int i = 0; i < assignmentItemList.size(); i++) {
            AssignmentItem ai = assignmentItemList.get(i);
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment functionAssignment = (FunctionAssignment) ai;
                UserFunction function = functionAssignment.getFunction();
                if (!functionAssignment.isVisible())
                    continue;
                Button functionButton = new Button();
                String labelKey = functionAssignment.getLabelKey();
                String label = ZkLabels.getLabel(labelKey, functionAssignment.getDefaultLabel());
                if (label == null || label.equals("")) {
                    labelKey = function.getLabelKey();
                    label = ZkLabels.getLabel(labelKey, function.getDefaultLabel());
                }
                functionButton.setLabel(label);
                functionButton.addEventListener(Events.ON_CLICK, new SecureComponentEventListener(getTargetComponent(),
                        function.getEventName(), function.getAction()));
                if (before) {
                    insertBefore(functionButton, centerSpace);
                } else {
                    if (actionButton == null) {
                        appendChild(functionButton);
                    } else {
                        insertBefore(functionButton, actionButton);
                    }
                }
                widgetDebugUtil.setLabel(functionButton, labelKey);
                functionButton.setDisabled(functionAssignment.isDisabled());

                // Set Image to button
                setImage(functionAssignment, functionButton);
            } else if (ai instanceof HorizontalFunctionSeparator) {
                HorizontalFunctionSeparator hfs = (HorizontalFunctionSeparator) ai;
                Space space = new Space();
                if (hfs.getSpaceSize().matches("[1-9][0-9]*px")) {
                    space.setSpacing(hfs.getSpaceSize());
                } else if (hfs.getSpaceSize().matches("([Tt][Rr][Uu][Ee])|([Ff][Aa][Ll][Se][Ee])")) {
                    space.setHflex("" + Boolean.parseBoolean(hfs.getSpaceSize()));
                } else if (hfs.getSpaceSize().matches("[1-9][0-9]*")) {
                    space.setHflex(hfs.getSpaceSize());
                }
                if (before) {
                    insertBefore(space, centerSpace);
                } else {
                    if (actionButton == null) {
                        appendChild(space);
                    } else {
                        insertBefore(space, actionButton);
                    }
                }
            } else if (ai instanceof FunctionSeparator) {
                Space separator = new Space();
                if (before) {
                    insertBefore(separator, centerSpace);
                } else {
                    if (actionButton == null) {
                        appendChild(separator);
                    } else {
                        insertBefore(separator, actionButton);
                    }
                }
            }
        }
    }

    public Component getTargetComponent() {
        return targetComponent;
    }

    public void setTargetComponent(Component targetComponent) {
        if (targetComponent == null) {
            this.targetComponent = this;
        } else {
            this.targetComponent = targetComponent;
        }
    }

    public boolean canPerformFunction(String functionId) {
        if (functionId == null)
            return false;

        if (commonActions != null && !hideActionButton && commonActions.size() > 0) {
            Boolean canPerform = canPerformFunction(commonActions, functionId);
            if (canPerform != null)
                return canPerform.booleanValue();
        }

        if (moreFunctions != null && !hideMoreButton && moreFunctions.size() > 0) {
            Boolean canPerform = canPerformFunction(moreFunctions, functionId);
            if (canPerform != null)
                return canPerform.booleanValue();
        }

        if (specialFunctions != null && specialFunctions.size() > 0) {
            Boolean canPerform = canPerformFunction(specialFunctions, functionId);
            if (canPerform != null)
                return canPerform.booleanValue();
        }

        return false;
    }

    private Boolean canPerformFunction(List<AssignmentItem> assignmentItemList, String functionId) {
        for (AssignmentItem ai : assignmentItemList) {
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment fa = (FunctionAssignment) ai;
                if (functionId.equalsIgnoreCase(fa.getFunction().getId())) {
                    if (fa.isDisabled() || !fa.isVisible()) {
                        return false;
                    }
                    return true;
                }
            }
        }

        return null;
    }

    public void setDetailButtonModel(List<AssignmentItem> detailFunctions) {
        this.detailFunctions = detailFunctions;
        renderDetailButtons();
    }
}
