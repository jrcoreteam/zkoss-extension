/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.objectlocking;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.jumin.common.base.services.UserObjectLockingService;
import org.jumin.common.utils.locking.LockItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Session;

public class ObjectLockingUtil {
	
	private static Logger log = LoggerFactory.getLogger(ObjectLockingUtil.class); 
	
	public static final String APPLICATION_LOCK_QUEUE_NAME = "zkx-lock-queue";

	public static final String SEGMENT_DELIMITER = "/";
	public static final String WILDCARD = ".*";

	public static String formulateSourceByHttpSessionId(String sessionId) {
		if (sessionId == null)
			return null;

		return sessionId + SEGMENT_DELIMITER + WILDCARD;
	}
	
	public static String formulateSource(Session session) {
		if (session == null)
			return null;

		HttpSession httpSession = (HttpSession) session.getNativeSession();
		if (httpSession == null) return null;
		
		return formulateSourceByHttpSessionId(httpSession.getId());
	}
	
	public static String formulateSource(Desktop desktop) {
		if (desktop == null)
			return null;

		Session session = desktop.getSession();
		if (session == null)
			return WILDCARD + SEGMENT_DELIMITER + desktop.getId() + SEGMENT_DELIMITER + WILDCARD;

		HttpSession httpSession = (HttpSession) session.getNativeSession();
		if (httpSession == null)
			return null;
		return httpSession.getId() + SEGMENT_DELIMITER + desktop.getId() + SEGMENT_DELIMITER + WILDCARD;
	}

	public static String formulateSource(Component comp) {
		if (comp == null || comp.getDesktop() == null)
			return null;

		Desktop desktop = comp.getDesktop();
		
		Session session = desktop.getSession();
		if (session == null)
			return WILDCARD + SEGMENT_DELIMITER + desktop.getId() + SEGMENT_DELIMITER + comp.getUuid();

		HttpSession httpSession = (HttpSession) session.getNativeSession();
		return httpSession.getId() + SEGMENT_DELIMITER + desktop.getId() + SEGMENT_DELIMITER
		        + comp.getUuid();
	}

	public static FmsLockingSource interpreteSource(String source) {
		if (source == null)
			return null;

		String[] infos = StringUtils.split(source, SEGMENT_DELIMITER);

		if (infos.length < 0)
			return null;

		if (infos.length == 1)
			return new FmsLockingSource(infos[0], "");

		return new FmsLockingSource(infos[0], infos[1]);
	}
	
	public static void clearObjectLockTable(String sourcePattern) {
		log.debug("[LOCK] Clear object lock table by pattern: " + sourcePattern);
		UserObjectLockingService service = new UserObjectLockingService();
		service.setUserId("Agent@System");
		
		List<LockItem> clearedLocks = service.clearLocksBySourcePattern(sourcePattern);
		log.debug("[LOCK] # locks cleared: " + clearedLocks.size());
	}

	public static class FmsLockingSource implements Serializable {

		private static final long serialVersionUID = -5051532199057779359L;

        private String sessionId;

		private String desktopId;

		public FmsLockingSource(String sessionId, String desktopId) {
			this.sessionId = sessionId;
			this.desktopId = desktopId;
		}

		public String getSessionId() {
			return sessionId;
		}

		public String getDesktopId() {
			return desktopId;
		}

	}
}
