/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import java.io.Serializable;

import org.zkoss.zhtml.I;
import org.zkoss.zhtml.Span;
import org.zkoss.zul.A;

public class StackedIcon extends A {

    private static final long serialVersionUID = 6888018251959660979L;
    
    public static final String STYLE_CLASS_STACK_CONTAINER = "z-icon-stack";

    private Span stackContainer;

    private TypeAndColor[] types;
    
    private IconSize size = IconSize.NORMAL;

    public StackedIcon() {
        super();

        createContents();
        initProperties();
    }

    private void initProperties() {
        setZclass("z-toolbarbutton");
        stackContainer.setWidgetClass(STYLE_CLASS_STACK_CONTAINER);
    }

    private void createContents() {
        stackContainer = new Span();
        stackContainer.setParent(this);
    }

    public TypeAndColor[] getTypes() {
        return types;
    }

    public void setTypes(TypeAndColor[] types) {
        this.types = types;

        while (stackContainer.getFirstChild() != null) {
            stackContainer.removeChild(stackContainer.getLastChild());
        }
        
        if (types == null)
            return;

        for (TypeAndColor type : types) {
            I icon = new I();
            icon.setWidgetClass(type.getWidgetClassString());
            if (type.hasColor()) {
                icon.setStyle("color:" + type.color + ";");
            }
            icon.setParent(stackContainer);
        }
    }
    
    public IconSize getSize() {
        return size;
    }
    
    public void setSize(IconSize size) {
        this.size = size;
        stackContainer.setWidgetClass(STYLE_CLASS_STACK_CONTAINER + " " + size.code);
    }
    
    public static enum IconSize {
        NORMAL (""),
        LARGE ("fa-lg"),
        TWICE ("fa-2x"),
        THREE_TIMES ("fa-3x"),
        FOUR_TIMES ("fa-4x"),
        FIVE_TIMES ("fa-5x");
        
        private final String code;
        
        private IconSize(String code) {
            this.code = code;
        }
        
        public String getCode() {
            return code;
        }
    }

    public static class TypeAndColor implements Serializable {

        private static final long serialVersionUID = 8165733408944061641L;

        private String type;

        private String color;
        
        private IconSize size = IconSize.NORMAL;
        
        private String additional;

        public TypeAndColor() {
        }

        public TypeAndColor(String type) {
            this(type, null);
        }

        public TypeAndColor(String type, String color) {
            this.type = type;
            this.color = color;
        }

        public TypeAndColor(String type, String color, IconSize size) {
            this.type = type;
            this.color = color;
            this.size = size;
        }

        public boolean hasColor() {
            return color != null && color.length() > 0;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
        
        public IconSize getSize() {
            return size;
        }
        
        public void setSize(IconSize size) {
            this.size = size;
        }
        
        public String getAdditional() {
            return additional;
        }
        
        public void setAdditional(String additional) {
            this.additional = additional;
        }
        
        public String getWidgetClassString() {
            String widgetClassString = type;
            if (size.compareTo(IconSize.NORMAL) > 0) {
                widgetClassString += " " + size.code;
            }
            
            if (additional != null && additional.trim().length() > 0) {
                widgetClassString += " " + additional.trim();
            }
            
            return widgetClassString;
        }
    }
}
