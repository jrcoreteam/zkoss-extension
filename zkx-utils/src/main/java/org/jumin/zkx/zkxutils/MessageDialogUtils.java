/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.util.Hashtable;
import java.util.Map;

import org.jumin.zkx.zkxutils.MessageDialog.ButtonModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

public class MessageDialogUtils {

	private static Logger log = LoggerFactory.getLogger(MessageDialogUtils.class);

	public static void showDialog(String message, String title, String icon, ButtonModel[] buttonModels,
	        Map<ButtonModel, EventListener<Event>> listenerMap) {
		showDialog(message, title, icon, buttonModels, null, listenerMap);
	}

	public static void showDialog(String message, String title, String icon, ButtonModel[] buttonModels, ButtonModel focus,
	        Map<ButtonModel, EventListener<Event>> listenerMap) {
		Page page = null;
		if (Executions.getCurrent() != null && Executions.getCurrent().getDesktop() != null) {
			page = Executions.getCurrent().getDesktop().getFirstPage();
		}
		if (page == null) return;
		
		MessageDialog dialog = new MessageDialog();
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setIcon(icon);
		dialog.setButtons(null, null, buttonModels, listenerMap);
		dialog.setButtonFocus(focus);
		dialog.setPage(page);
		dialog.doModal();
	}

	public static void showConfirmation(String message, String title, ButtonModel[] buttonModels,
	        Map<ButtonModel, EventListener<Event>> listenerMap) {
		showConfirmation(message, title, buttonModels, null, listenerMap);
	}
	
	public static void showConfirmation(String message, String title, ButtonModel[] buttonModels, ButtonModel focus,
	        Map<ButtonModel, EventListener<Event>> listenerMap) {
		showDialog(message, title, Messagebox.QUESTION, buttonModels, focus, listenerMap);
	}

	public static void showInfo(String message, String title) {
		showDialog(message, title, Messagebox.INFORMATION, new ButtonModel[] { MessageDialog.OK }, null);
	}

	public static void showInfo(String message, String title, EventListener<Event> listener) {
		Map<ButtonModel, EventListener<Event>> listenerMap = new Hashtable<>();
		listenerMap.put(MessageDialog.OK, listener);
		showDialog(message, title, Messagebox.INFORMATION, new ButtonModel[] { MessageDialog.OK }, listenerMap);
	}

	public static void showWarning(String message, String title) {
		showDialog(message, title, Messagebox.EXCLAMATION, new ButtonModel[] { MessageDialog.OK }, null);
	}

	public static void showWarning(String message, String title, EventListener<Event> listener) {
		Map<ButtonModel, EventListener<Event>> listenerMap = new Hashtable<>();
		listenerMap.put(MessageDialog.OK, listener);
		showDialog(message, title, Messagebox.EXCLAMATION, new ButtonModel[] { MessageDialog.OK }, listenerMap);
	}

	public static void showWarning(String message, String title, ButtonModel[] buttonModels, ButtonModel focus, Map<ButtonModel, EventListener<Event>> listenerMap) {
		showDialog(message, title, Messagebox.EXCLAMATION, buttonModels, focus, listenerMap);
	}

	public static void showError(String message, String title) {
		showDialog(message, title, Messagebox.ERROR, new ButtonModel[] { MessageDialog.OK }, null);
	}

	public static void showError(String message, String title, EventListener<Event> listener) {
		Map<ButtonModel, EventListener<Event>> listenerMap = new Hashtable<>();
		listenerMap.put(MessageDialog.OK, listener);
		showDialog(message, title, Messagebox.ERROR, new ButtonModel[] { MessageDialog.OK }, null, listenerMap);
	}

	public static void showError(String message, String title, ButtonModel[] buttonModels, ButtonModel focus, Map<ButtonModel, EventListener<Event>> listenerMap) {
		showDialog(message, title, Messagebox.ERROR, buttonModels, focus, listenerMap);
	}

	public static void captureInternalError(Exception e) {
		log.error("Internal Error!", e);
	}

	public static void captureInternalError(Exception e, Object user, Object data) {
		log.error("Internal Error on user: '" + user + "' on data: '" + data + "'", e);
	}
}
