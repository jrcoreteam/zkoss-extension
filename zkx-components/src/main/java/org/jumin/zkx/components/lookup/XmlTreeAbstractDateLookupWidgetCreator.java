/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.lookup;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.common.utils.DatetimeTextParser;
import org.jumin.common.utils.LocaleUtils;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zul.Label;

public abstract class XmlTreeAbstractDateLookupWidgetCreator extends XmlTreeAbstractLookupWidgetCreator {

    private static final long serialVersionUID = 5350370815404202663L;

    protected final DatetimeTextParser dateTimeParser = new DatetimeTextParser();

    public XmlTreeAbstractDateLookupWidgetCreator(ServiceConnector connector,
            XmlTreerow row, EditorMode editorMode, String initialValue) {
        super(connector, row, editorMode, initialValue);
    }

    @Override
    public Label createViewModeWidget() {
        Label widget = new Label();
        widget.setSclass("jrx-labelvalue");
        widget.setHflex("true");
        if (value != null && !value.equals("")) {
            widget.setValue(value);
            if (!value.startsWith("***")) {
                if (dateTimeParser.analyseDateValueWithKeyword(value) != null) {
                    Locale userFormatLocale = Locale.getDefault();
                    TimeZone userTimeZone = LocaleUtils.TIMEZONE_UTC;
                    if (row != null && row.getDesktop() != null) {
                        userFormatLocale = UserContextUtil.getUserFormatLocale(row.getDesktop());
                        userTimeZone = UserContextUtil.getUserTimeZone(row.getDesktop());
                    }

                    Date date = dateTimeParser.parseKeywordValue(value, userTimeZone);
                    value = convertDateToStringValue(date, userFormatLocale, userTimeZone);
                    widget.setValue(value);

                    if (row.getData() instanceof JrxElement) {
                        // Update the XML
                        JrxElement jrxElement = (JrxElement) row.getData();
                        jrxElement.getXmlElement().setTextContent(value);
                    }
                }
                registerValueDescription(value, false);
            }
        }

        return widget;
    }

    protected abstract Date convertStringToDateObject(String stringValue);
    
    protected abstract String convertDateToStringValue(Date date, Locale formatLocale, TimeZone timezone);

    protected abstract String formatDate(Date dateObject, Locale formatLocale, TimeZone timezone);

}
