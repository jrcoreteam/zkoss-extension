/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treeitem;

public class XmlTreeEvent extends Event{
	
	private static final long serialVersionUID = -1376018834385585642L;
	
    public static final String ON_REPETITIVE_NODE_ADD = "onRepetitiveNodeAdd";
	public static final String ON_REPETITIVE_NODE_REMOVE = "onRepetitiveNodeRemove";
	public static final String ON_SUB_NODE_DEFINED = "onSubNodeDefined";
	
	public XmlTreeEvent(String name, Tree tree, Treeitem item) {
		super(name, tree, item);
	}

	public Treeitem getFromTreeitem() {
		return (Treeitem) getData();
	}
}
