/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Textbox;

public class SimpleStringWidgetCreator extends AbstractWidgetCreator<Textbox, String> {

	private static final long serialVersionUID = 5259437500186953800L;

    private int lineSize = 1;
	
	private boolean multiline = false;
	
	public SimpleStringWidgetCreator() {
	    this(1);
    }
	
	public SimpleStringWidgetCreator(boolean multiline) {
	    super();
	    
	    if (multiline) {
	    	this.lineSize = 3;
	    	this.multiline = multiline;
	    }
    }
	
	public SimpleStringWidgetCreator(int lineSize) {
	    super();
	    
	    if (lineSize > 1) {
	    	this.lineSize = lineSize;
	    	this.multiline = true;
	    }
	}
	
	@Override
    public Textbox createWidget(Component parent) {
	    Textbox tb = new Textbox();
	    tb.setParent(parent);
	    
	    if (lineSize > 1) {
	    	tb.setHeight((lineSize * 15) + "px");
	    }
	    
	    tb.setMultiline(multiline);
	    
	    return tb;
    }

	@Override
    public String getWidgetValue(Textbox widget) {
	    return widget.getValue();
    }

	@Override
    public void setWidgetValue(Textbox widget, String value) {
	    widget.setText(value);   
    }

	@Override
    public void setWidgetValueFromString(Textbox widget, String stringValue) {
	    setWidgetValue(widget, stringValue);
    }

}
