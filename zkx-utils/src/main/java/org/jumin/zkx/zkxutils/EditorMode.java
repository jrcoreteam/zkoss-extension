/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

public enum EditorMode {
	VIEW(0), 
	EDIT(1), 
	NEW(2);

	private final int value;

	EditorMode(int value) {
		this.value = value;
	}

	public static EditorMode fromInt(int value) {
		if (value == 0)
			return VIEW;
		if (value == 1)
			return EDIT;
		if (value == 2)
			return NEW;

		return null;
	}

	public static EditorMode fromString(String value) {
		if (value == null) return null;
		
		if (value.equalsIgnoreCase("View"))
			return VIEW;
		if (value.equalsIgnoreCase("Edit"))
			return EDIT;
		if (value.equalsIgnoreCase("New"))
			return NEW;

		return null;
	}

	public int toInt() {
		return value;
	}

	@Override
	public String toString() {
		if (value == 0)
			return "View";
		if (value == 1)
			return "Edit";
		if (value == 2)
			return "New";

		return "";
	}
}
