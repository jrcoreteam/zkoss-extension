/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.preferences.Preference;
import org.jumin.common.preferences.PreferenceService;
import org.jumin.common.preferences.SystemPreference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.Locales;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;

public class UserContextUtil implements Serializable {

	private static final long serialVersionUID = -157652564969416393L;
	
    private static Logger log = LoggerFactory.getLogger(UserContextUtil.class);

	// User Language Locale
	public static Locale getUserLanguageLocale(Page page) {
		if (page == null) {
			return null;
		}
		return getUserLanguageLocale(page.getDesktop().getSession());
	}

	public static Locale getUserLanguageLocale(Desktop desktop) {
		if (desktop == null) {
			return null;
		}
		return getUserLanguageLocale(desktop.getSession());
	}

	public static Locale getUserLanguageLocale(Session session) {
		if (session == null) {
			return null;
		}

		return (Locale) session.getAttribute(Attributes.PREFERRED_LOCALE);
	}

	public static void setUserLanguageLocale(Page page, Locale languageLocale) {
		if (page == null) {
			return;
		}
		Locales.setThreadLocal(languageLocale);
		setUserLanguageLocale(page.getDesktop().getSession(), languageLocale);
	}

	public static void setUserLanguageLocale(Desktop desktop, Locale languageLocale) {
		if (desktop == null) {
			return;
		}
		Locales.setThreadLocal(languageLocale);
		setUserLanguageLocale(desktop.getSession(), languageLocale);
	}

	public static void setUserLanguageLocale(Session session, Locale languageLocale) {
		if (session == null) {
			return;
		}
		Locales.setThreadLocal(languageLocale);
		session.setAttribute(Attributes.PREFERRED_LOCALE, languageLocale);
	}

	// User Format Locale
	public static Locale getUserFormatLocale(Page page) {
		if (page == null) {
			return null;
		}
		return getUserFormatLocale(page.getDesktop().getSession());
	}

	public static Locale getUserFormatLocale(Desktop desktop) {
		if (desktop == null) {
			return null;
		}
		return getUserFormatLocale(desktop.getSession());
	}

	public static Locale getUserFormatLocale(Session session) {
		if (session == null) {
			return null;
		}

		Locale locale = (Locale) session.getAttribute(ApplicationConstants.SESSION_USER_FORMAT_LOCALE);

		return locale;
	}

	public static void setUserFormatLocale(Page page, Locale userFormatLocale) {
		if (page == null) {
			return;
		}
		setUserFormatLocale(page.getDesktop().getSession(), userFormatLocale);
	}

	public static void setUserFormatLocale(Desktop desktop, Locale userFormatLocale) {
		if (desktop == null) {
			return;
		}
		setUserFormatLocale(desktop.getSession(), userFormatLocale);
	}

	public static void setUserFormatLocale(Session session, Locale userFormatLocale) {
		if (session == null) {
			return;
		}
		session.setAttribute(ApplicationConstants.SESSION_USER_FORMAT_LOCALE, userFormatLocale);
	}

	// User TimeZone
	public static TimeZone getUserTimeZone(Page page) {
		if (page == null) {
			return null;
		}
		return getUserTimeZone(page.getDesktop().getSession());
	}

	public static TimeZone getUserTimeZone(Desktop desktop) {
		if (desktop == null) {
			return null;
		}
		return getUserTimeZone(desktop.getSession());
	}

	public static TimeZone getUserTimeZone(Session session) {
		if (session == null) {
			return null;
		}

		TimeZone tz = (TimeZone) session.getAttribute(Attributes.PREFERRED_TIME_ZONE);

		// if (tz == null)
		// tz = TimeZone.getDefault();
		//
		return tz;
	}

	public static void setUserTimeZone(Page page, TimeZone userTimeZone) {
		if (page == null) {
			return;
		}
		setUserTimeZone(page.getDesktop().getSession(), userTimeZone);
	}

	public static void setUserTimeZone(Desktop desktop, TimeZone userTimeZone) {
		if (desktop == null) {
			return;
		}
		setUserTimeZone(desktop.getSession(), userTimeZone);
	}

	public static void setUserTimeZone(Session session, TimeZone userTimeZone) {
		if (session == null) {
			return;
		}
		session.setAttribute(Attributes.PREFERRED_TIME_ZONE, userTimeZone);
	}

	// User Id
	public static String getSessionUserPrincipal(Page page) {
		if (page == null) {
			return null;
		}
		return getSessionUserPrincipal(page.getDesktop());
	}

	public static String getSessionUserPrincipal(Desktop desktop) {
		if (desktop == null) {
			return null;
		}
		return getSessionUserPrincipal(desktop.getSession());
	}

	public static String getSessionUserPrincipal(Session session) {
		if (session == null) {
			log.warn("SESSION context is null!");
			return null;
		}
		String userPrincipal = (String) session.getAttribute(ApplicationConstants.SESSION_USER_PRINCIPAL);
		log.debug("SESSION NOT NULL, returning USER_PRINCIPAL "
		        + userPrincipal);
		return userPrincipal;
	}
	

	// User preference on the session context
	public static List<Preference> getUserPreferences(Page page, String module) {
		return getUserPreferences(page.getDesktop(), module);
	}

	public static List<Preference> getUserPreferences(Desktop desktop, String module) {
		return getUserPreferences(desktop.getSession(), module);
	}

	@SuppressWarnings("unchecked")
	public static List<Preference> getUserPreferences(Session session, String module) {
		return (List<Preference>) session.getAttribute(ApplicationConstants.SESSION_USER_PREFERENCE
		        + (module == null || module.equals("") || module.equals("*") ? "" : "-" + module));
	}

	public static void setUserPreferences(Page page, String module, List<Preference> preferences) {
		setUserPreferences(page.getDesktop(), module, preferences);
	}

	public static void setUserPreferences(Desktop desktop, String module, List<Preference> preferences) {
		setUserPreferences(desktop.getSession(), module, preferences);
	}

	public static void setUserPreferences(Session session, String module, List<Preference> preferences) {
		session.setAttribute(ApplicationConstants.SESSION_USER_PREFERENCE
		        + (module == null || module.equals("") || module.equals("*") ? "" : "-" + module), preferences);
	}

	public static List<Preference> initializeUserPreferences(Desktop desktop, String module, PreferenceService service) {
		List<Preference> preferenceList;

		if (module == null) {
			preferenceList = service.retrieveUserPreferenceListByType(SystemPreference.class.getSimpleName());
		} else {
			preferenceList = service.retrieveUserPreferenceListByTypeModule(SystemPreference.class.getSimpleName(),
			        module);
		}

		setUserPreferences(desktop, module, preferenceList);

		return preferenceList;
	}

	public static boolean hasUserPrincipal(Session session) {
		return (Boolean) session.getAttribute(ApplicationConstants.SESSION_USER_PRINCIPAL_EXISTANCE);
	}

	public static void setUserPrincipalExistence(Session session, boolean exist) {
		session.setAttribute(ApplicationConstants.SESSION_USER_PRINCIPAL_EXISTANCE, exist);
	}

}
