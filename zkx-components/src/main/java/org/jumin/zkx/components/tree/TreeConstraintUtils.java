/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import java.util.HashMap;
import java.util.Map;

import org.jumin.zkx.components.tree.constraint.AbstractTreeSyntaxValidationConstraint.ValueSyntaxError;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Tree;
import org.zkoss.zul.impl.InputElement;

public class TreeConstraintUtils {
    public static final String VAR_XML2INPUTELEMENT_TABLE = "jrxXml2InputElementTable";
    
    public static final String VAR_MESSAGE_TREE_VALUE_SYNTAX_ERROR = "jrxMessageTreeValueSyntaxError";

    public static void setMessageTreeValueSyntaxErrorTable(Tree tree, Map<Component, ValueSyntaxError> valueSyntaxErrorTable) {
        tree.setAttribute(VAR_MESSAGE_TREE_VALUE_SYNTAX_ERROR, valueSyntaxErrorTable);
    }
    
    public static Map<Component, ValueSyntaxError> getMessageTreeValueSyntaxErrorTable(Tree tree) {
        @SuppressWarnings("unchecked")
        Map<Component, ValueSyntaxError> valueSyntaxErrorTable = (Map<Component, ValueSyntaxError>) tree.getAttribute(VAR_MESSAGE_TREE_VALUE_SYNTAX_ERROR);
        if (valueSyntaxErrorTable == null) {
            valueSyntaxErrorTable = new HashMap<>();
        }

        return valueSyntaxErrorTable;
    }

    public static void setXmlToInputElementTable(Tree tree, Map<Long, InputElement> table) {
        tree.setAttribute(VAR_XML2INPUTELEMENT_TABLE, table);
    }

    public static Map<Long, InputElement> getXmlToInputElementTable(Tree tree) {
        @SuppressWarnings("unchecked")
        Map<Long, InputElement> table = (Map<Long, InputElement>) tree.getAttribute(VAR_XML2INPUTELEMENT_TABLE);
        if (table == null) {
            table = new HashMap<>();
        }
        
        return table;
    }
    
    protected static final void clearValueSyntaxErrorTable(Tree tree) {
        Map<Component, ValueSyntaxError> valueSyntaxErrorTable = getMessageTreeValueSyntaxErrorTable(tree);
        valueSyntaxErrorTable.clear();
        setMessageTreeValueSyntaxErrorTable(tree, valueSyntaxErrorTable);
    }
    
    protected static final void clearXmlToInputElementTable(Tree tree) {
        Map<Long, InputElement> table = getXmlToInputElementTable(tree);
        table.clear();
        setXmlToInputElementTable(tree, table);
    }

}
