/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.sidebar;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.jumin.common.preferences.object.ObjectAttributeConfiguration;
import org.jumin.common.utils.InvalidMethodPathExpression;
import org.jumin.zkx.components.WidgetStyleConstants;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Html;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;

public class ObjectAttributesSidebarBlock<T> extends SidebarBlock {

    private static final long serialVersionUID = 6810290221642039039L;

    public static Logger log = LoggerFactory.getLogger(ObjectAttributesSidebarBlock.class);

    public static final String ERR_FAIL_RETRIEVE_ATTRIBUTE_VALUE = "Fail to retrieve attribute: {0} for ObjectAttributeSidebarBlock from object: {1}";

    private Grid attributeTable;

    private List<ObjectAttributeConfiguration> objectConfig;

    private T model;

    @Override
    protected void createContents() {
        super.createContents();

        attributeTable = new Grid();
        appendContent(attributeTable);
    }

    @Override
    protected void initProperties() {
        super.initProperties();

        setSclass("jrx-objectattribute-sidebarblock");

        setTitle(ZkLabels.getLabel("objectAttributeSidebarBlock.title", "Object Attributes Viewer"));

        attributeTable.setVflex("true");
        attributeTable.setZclass(WidgetStyleConstants.FORM_GRID);
        attributeTable.setRowRenderer(new SingleObjectAttributeRowRenderer());

        if (!widgetDebugUtil.isTestingMode())
            return;

        widgetDebugUtil.setLabel(this, "objectAttributeSidebarBlock");
    }

    public List<ObjectAttributeConfiguration> getObjectConfig() {
        return objectConfig;
    }

    public void setObjectConfig(List<ObjectAttributeConfiguration> objectConfig) {
        this.objectConfig = objectConfig;

        RowRenderer<?> renderer = attributeTable.getRowRenderer();
        if (renderer != null && renderer instanceof ObjectAttributesSidebarBlock.SingleObjectAttributeRowRenderer) {
            @SuppressWarnings("unchecked")
            SingleObjectAttributeRowRenderer concreteRenderer = (SingleObjectAttributeRowRenderer) renderer;
            concreteRenderer.setObjectConfigList(objectConfig);
        }
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
        synchronizeModelToWidgets();
    }

    private void synchronizeModelToWidgets() {
        if (objectConfig == null)
            return;

        Locale locale = UserContextUtil.getUserFormatLocale(getDesktop());
        TimeZone timeZone = UserContextUtil.getUserTimeZone(getDesktop());
        String[][] record = new String[objectConfig.size()][3];
        for (int i = 0; i < objectConfig.size(); i++) {
            ObjectAttributeConfiguration attrConfig = objectConfig.get(i);
            record[i][0] = ZkLabels.getLabel(attrConfig.getLabelKey(), attrConfig.getLabel());
            record[i][2] = attrConfig.getName();
            try {
                record[i][1] = ObjectAttributeUtils.getObjectValue(model, attrConfig, locale, timeZone);
            } catch (InvalidMethodPathExpression e) {
                record[i][1] = ZkLabels.getLabel("general.na", "N/A");
                log.warn(String.format(ERR_FAIL_RETRIEVE_ATTRIBUTE_VALUE, attrConfig.getLabel(), ("" + model)), e);
            }
        }

        attributeTable.setModel(new ListModelArray<String[]>(record));
    }

    public void reset() {
        if (objectConfig == null)
            return;

        String[][] record = new String[objectConfig.size()][3];
        for (int i = 0; i < objectConfig.size(); i++) {
            ObjectAttributeConfiguration attrConfig = objectConfig.get(i);
            record[i][0] = ZkLabels.getLabel(attrConfig.getLabelKey(), attrConfig.getLabel());
            record[i][2] = attrConfig.getName();
            record[i][1] = ZkLabels.getLabel("general.na", "N/A");
        }

        attributeTable.setModel(new ListModelArray<String[]>(record));
    }

    class SingleObjectAttributeRowRenderer implements RowRenderer<String[]> {

        private Map<String, ObjectAttributeConfiguration> objectConfigMap = new HashMap<String, ObjectAttributeConfiguration>();

        public SingleObjectAttributeRowRenderer() {
            super();
        }

        public void setObjectConfigList(List<ObjectAttributeConfiguration> objectConfigList) {
            objectConfigMap.clear();

            if (objectConfigList == null || objectConfigList.size() < 1)
                return;

            for (ObjectAttributeConfiguration objectAttributeConfiguration : objectConfigList) {
                objectConfigMap.put(objectAttributeConfiguration.getName(), objectAttributeConfiguration);
            }
        }

        @Override
        public void render(Row row, String[] data, int index) throws Exception {
            String name = "";
            if (data != null && data.length > 0)
                name = "" + data[0];

            String value = "";
            if (data != null && data.length > 1)
                if (data[1] == null)
                    value = ZkLabels.getLabel("general.na", "N/A");
                else
                    value = "" + data[1];

            row.setSclass("jrx-objectattribute-sidebarblock-item");
            ObjectAttributeConfiguration objectAttributeConfiguration = objectConfigMap.get(data[2]);
            if (objectAttributeConfiguration != null && !objectAttributeConfiguration.isInline()) {
                value = StringUtils.replace(value, "\n", "<br>");
                String htmlContent = "";
                htmlContent = "<div style=\"width:100%;overflow:auto;\"><div class=\"z-label jrx-objectattribute-sidebarblock-itemname\">"
                        + name + ":</div>"
                        + "<div class=\"z-label jrx-objectattribute-sidebarblock-itemvalue\" style=\"margin-left: 26px;\">"
                        + value + "</div></div>";
                Html html = new Html();
                html.setParent(row);
                html.setContent(htmlContent);
            } else {
                Label attrLabel = new Label(name + ":");
                attrLabel.setSclass("jrx-objectattribute-sidebarblock-itemname");
                attrLabel.setStyle("width:100%;overflow:auto;");
                attrLabel.setParent(row);

                Label attrValueLabel = new Label(value);
                attrValueLabel.setSclass("jrx-objectattribute-sidebarblock-itemvalue");
                attrValueLabel.setParent(row);
                attrValueLabel.setPre(true);
            }

            if (widgetDebugUtil.isTestingMode())
                widgetDebugUtil.setLabel(row, name);
        }

    }
}
