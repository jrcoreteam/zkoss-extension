/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.util.Date;

import org.jumin.common.utils.operator.ComparisonOperator;
import org.jumin.common.utils.operator.DatetimeOperator;
import org.jumin.zkx.zkxutils.renderer.AttributeLabelTranslationRenderer;
import org.jumin.zkx.zkxutils.renderer.StringComboitemRenderer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelArray;

public class SearchAttributeDefinitionUtil {

	public static boolean requireOperator(Class<?> dataType) {
		for (Class<?> clazz : dataType.getClasses()) {
			if (isNumberOrDateDataType(clazz)) return true;
        }
		
		return isNumberOrDateDataType(dataType);
	}
	
	public static boolean isNumberOrDateDataType(Class<?> clazz) {
        if (isNumberDataType(clazz)) {
        	return true;
        }
        if (isDateDataType(clazz)) {
        	return true;
        }
        
        return false;
	}

	public static boolean isNumberDataType(Class<?> clazz) {
        if (Number.class.isAssignableFrom(clazz)) {
        	return true;
        }
        
        return false;
	}

	public static boolean isDateDataType(Class<?> clazz) {
        if (Date.class.isAssignableFrom(clazz)) {
        	return true;
        }
        
        return false;
	}
	
	public static Combobox getOperatorWidgetByDataType(Class<?> dataType) {
		if (isDateDataType(dataType)) {
			return createDateOperatorWidget();
		}
		
		if (isNumberDataType(dataType)) {
			return createNumberOperatorWidget();
		}
		
		return null;
	}

	public static Combobox createNumberOperatorWidget() {
		Combobox cb = new Combobox();
		cb.setItemRenderer(new StringComboitemRenderer());
		cb.setModel(new ListModelArray<String>(ComparisonOperator.getValues()));
		
	    return cb;
    }

	public static Combobox createDateOperatorWidget() {
		Combobox cb = new Combobox();
		cb.setItemRenderer(new AttributeLabelTranslationRenderer());
		cb.setModel(new ListModelArray<DatetimeOperator>(DatetimeOperator.values()));
		
	    return cb;
    }
}
