/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import org.jumin.common.xsutils.model.JrxTerm;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treerow;

public class XmlTreerow extends Treerow implements Comparable<XmlTreerow> {

	private static final long serialVersionUID = -7427720920251359673L;

    public static final String ON_DESCRIPTION = "onDescription";
	
	private JrxTerm<?> jrxTerm;
	
	private Component lookupWidget = null;

	public XmlTreerow(JrxTerm<?> jrxTerm) {
		super();
		this.jrxTerm = jrxTerm;
	}

	public void setData(JrxTerm<?> jrxTerm) {
		this.jrxTerm = jrxTerm;
	}

	public JrxTerm<?> getData() {
		return jrxTerm;
	}

	public boolean requiresValueDescription() {
		return lookupWidget != null;
	}

	public void setLookupWidget(Component lookupWidget) {
		this.lookupWidget = lookupWidget;
	}

	public Component getLookupWidget() {
		return lookupWidget;
	}

	@Override
    public int compareTo(XmlTreerow other) {
		if (other == null) return 1;

		int currentObjectId = System.identityHashCode(this);
		int otherObjectId = System.identityHashCode(other);
		
		return Integer.compare(currentObjectId, otherObjectId);
    }

}
