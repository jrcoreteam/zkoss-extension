/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import java.sql.Time;

import org.jumin.common.xmlutils.XmlUtils;
import org.jumin.common.xsutils.model.JrxAttribute;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.impl.InputElement;

/**
 * The class <code>MessageTreeInputEventHandler</code> handles the value transfer from widget to XML model.
 *
 */
public class XmlTreeInputWidgetEventHandler implements EventListener<Event> {

	private static final Logger log = LoggerFactory.getLogger(XmlTreeInputWidgetEventHandler.class);

	private JrxNode jrxNode;

	public XmlTreeInputWidgetEventHandler(JrxNode jrxNode) {
		super();
		this.jrxNode = jrxNode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.zkoss.zk.ui.event.EventListener#onEvent(org.zkoss.zk.ui.event.Event)
	 */
	@Override
	public void onEvent(Event event) throws Exception {
		log.debug("Event name: " + event.getName() + " on jrx element: " + jrxNode.getName());
		if (jrxNode instanceof JrxElement) {
		    JrxElement jrxElement = (JrxElement) jrxNode;
			if (jrxElement.isLeaf() && event.getTarget() instanceof InputElement) {
				String value = getValue((InputElement) event.getTarget());
				Element xmlElement = jrxElement.getXmlElement();
				xmlElement.setTextContent(value);
				log.debug("Setting value: " + jrxElement.getName() + " -> '" + value + "' -> "
				        + XmlUtils.nodeToString(xmlElement));
			}
		} else {
			JrxAttribute jrxAttribute = (JrxAttribute) jrxNode;
			JrxElement jrxElement = jrxAttribute.getOwner();
			if (event.getTarget() instanceof InputElement) {
				String value = getValue((InputElement) event.getTarget());
				Element xmlElement = jrxElement.getXmlElement();
				xmlElement.setAttribute(jrxAttribute.getSimpleName(), value);
				log.debug("Setting value: " + jrxElement.getName() + " -> '" + value + "' -> "
				        + XmlUtils.nodeToString(xmlElement));
			}
		}
	}

	protected String getValue(InputElement widget) {
		if (widget instanceof Longbox && ((Longbox) widget).getValue() != null) {
			return ((Longbox) widget).getValue().toString();
		}
		if (widget instanceof Intbox && ((Intbox) widget).getValue() != null) {
			return ((Intbox) widget).getValue().toString();
		}
		if (widget instanceof Doublebox && ((Doublebox) widget).getValue() != null) {
			return ((Doublebox) widget).getValue().toString();
		}
		if (widget instanceof Decimalbox && ((Decimalbox) widget).getValue() != null) {
			return ((Decimalbox) widget).getValue().toString();
		}
		if (widget instanceof Datebox && ((Datebox) widget).getValue() != null) {
			return XmlUtils.convertDateToXmlValue(((Datebox) widget).getValue());
		}
		if (widget instanceof Timebox && ((Timebox) widget).getValue() != null) {
			return XmlUtils.convertTimeToXmlValue(new Time(((Timebox) widget).getValue().getTime()));
		}
		if (widget instanceof Combobox) {
			if (((Combobox) widget).getSelectedItem() != null) {
				return (String) ((Combobox) widget).getSelectedItem().getValue();
			} else {
				return ((Combobox) widget).getText().trim();
			}
		}
		return widget.getText();
	}

}
