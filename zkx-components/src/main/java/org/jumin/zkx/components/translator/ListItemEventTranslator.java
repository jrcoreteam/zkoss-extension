/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.translator;

import org.jumin.common.utils.exception.BackendRuntimeException;
import org.jumin.zkx.zkxutils.translator.ObjectValueListcellTranslator;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.impl.XulElement;

abstract public class ListItemEventTranslator<T> extends ObjectValueListcellTranslator<T> {

    private static final long serialVersionUID = -1952690610419865463L;
    
    /**
     * Either a ListItem or a Grid Row
     */
    XulElement listItem;

    protected void addForwardEventToListItem(Component originalComp, String originalEvent, String targetEvent, Object eventData) {
        if (getListItem() != null) {
            originalComp.addForward(originalEvent, getListItem(), targetEvent, eventData);
        } else {
            throw new BackendRuntimeException("List item must be setted.", null);
        }
    }

    protected void sendListItemEvent(String targetEvent, Object eventData) {
        if (getListItem() != null) {
            Events.sendEvent(targetEvent, getListItem(), eventData);
        } else {
            throw new BackendRuntimeException("List item must be setted.", null);
        }
    }

    public XulElement getListItem() {
        return listItem;
    }

    public void setListItem(Listitem listItem) {
        this.listItem = listItem;
    }

    public void setListItem(Row listItem) {
        this.listItem = listItem;
    }

}
