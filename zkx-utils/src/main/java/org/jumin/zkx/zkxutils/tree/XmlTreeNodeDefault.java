/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import java.util.Set;

import org.jumin.common.xsutils.model.JrxChoiceGroup;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxTerm;

public class XmlTreeNodeDefault extends AbstractXmlTreeNodeDefinition {

    private static final long serialVersionUID = 5919133342822676691L;
    
    private Boolean open;
    private Boolean visible;

    private Boolean readonly;
    
    // open, visible, and read-only have java.lang.Boolean data type in order to be able
    // to store 3 states: NULL, true, and false

    private static final boolean DEFAULT_OPEN = true;
    private static final boolean DEFAULT_READONLY = false;
    private static final boolean DEFAULT_VISIBLE = true;
    
    private String subscribeEvent = null;
    private int jrxChildNodeIndex = -1;
    private String defaultValue = null;
    
    public XmlTreeNodeDefault() {
        super();
    }
	
    public Boolean isOpen() {
        return open;
    }
	
    public void setOpen(Boolean open) {
        this.open = open;
    }
	
    public Boolean isVisible() {
        return visible;
    }
	
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
	
    @Override
    public int hashCode() {
        final int prime = 31; 
        int totalHashCode = 1;

        totalHashCode = super.hashCode();
        totalHashCode += prime * totalHashCode + (open ? 1231 : 1237);
        totalHashCode += prime * totalHashCode + (visible ? 1231 : 1237);
		
        return totalHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!super.equals(obj)) 
            return false;

		if (!(obj instanceof XmlTreeNodeDefault))
			return false;

		final XmlTreeNodeDefault other = (XmlTreeNodeDefault) obj;
		if (open != other.open)
			return false;
		if (visible != other.visible)
			return false;

		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append(super.toString());
		sb.append("open: ").append(open).append("\n");
		sb.append("visible: ").append(visible).append("\n");

		return sb.toString();
	}

	public Boolean isReadonly() {
		return readonly;
	}
	
	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}
	
	public String getSubscribeEvent() {
		return subscribeEvent;
	}
	
	public void setSubscribeEvent(String subscribeEvent) {
		this.subscribeEvent = subscribeEvent;
	}

	public int getJrxChildNodeIndex() {
	    return jrxChildNodeIndex;
    }
	
	public void initJrxTermSet() {
		getJrxTermSet().clear();
	}

	public void setJrxChildNodeIndex(int jrxChildNodeIndex) {
	    this.jrxChildNodeIndex = jrxChildNodeIndex;
    }
	
	public void addJrxChoiceGroup(JrxChoiceGroup jrxChoiceGroup) {
		getJrxTermSet().add(jrxChoiceGroup);
	}

	public boolean includeJrxTerm(JrxTerm<?> jrxTerm) {
		return getJrxTermSet().contains(jrxTerm);
	}
	
	public Set<JrxElement> getJrxElementSet() {
		java.util.Set<JrxTerm<?>> jrxTermSet = getJrxTermSet();
		
		java.util.Set<JrxElement> jrxElementSet = new java.util.HashSet<>();
		
		if (jrxTermSet.isEmpty()) return jrxElementSet;
		
		for (JrxTerm<?> jrxTerm : jrxTermSet) {
			if (jrxTerm instanceof JrxElement) {
				jrxElementSet.add((JrxElement) jrxTerm);
			}
		}
		
		return jrxElementSet;
	}

	public void addJrxElement(JrxElement jrxElement) {
		getJrxTermSet().add(jrxElement);
	}

	public String getDefaultValue() {
	    return defaultValue;
    }

	public void setDefaultValue(String defaultValue) {
	    this.defaultValue = defaultValue;
    }

	public static boolean isOpen(XmlTreeNodeDefault[] nodeDefaults) {
		if (nodeDefaults == null) return DEFAULT_OPEN;
		
		boolean lastState = DEFAULT_OPEN;
		for (XmlTreeNodeDefault nodeDefault : nodeDefaults) {
	        if (nodeDefault.isOpen() != null) {
	        	lastState = nodeDefault.isOpen();
	        }
        }
		
		return lastState;
	}

	public static boolean isVisible(XmlTreeNodeDefault[] nodeDefaults) {
		if (nodeDefaults == null) return DEFAULT_VISIBLE;
		
		boolean lastState = DEFAULT_VISIBLE;
		for (XmlTreeNodeDefault nodeDefault : nodeDefaults) {
	        if (nodeDefault.isVisible() != null) {
	        	lastState = nodeDefault.isVisible();
	        }
        }
		
		return lastState;
	}
	
	public static boolean isReadonly(XmlTreeNodeDefault[] nodeDefaults) {
		if (nodeDefaults == null) return DEFAULT_READONLY;
		
		// The last provided Read-only state overwrites all previously defined
		boolean lastState = DEFAULT_READONLY;
		for (XmlTreeNodeDefault nodeDefault : nodeDefaults) {
			if (nodeDefault.isReadonly() != null) {
				lastState = nodeDefault.isReadonly(); 
			}
        }
		
		return lastState;
	}
	
	public XmlTreeNodeDefault clone() {
		XmlTreeNodeDefault clone = new XmlTreeNodeDefault();
		
		clone.defaultValue = this.defaultValue;
		clone.jrxChildNodeIndex = this.jrxChildNodeIndex;
		clone.open = this.open;
		clone.readonly = this.readonly;
		clone.subscribeEvent = this.subscribeEvent;
		clone.visible = this.visible;

		clone.setNamespacePrefixes(this.getNamespacePrefixes());
		clone.addXpath(this.getXpaths());
		
		return clone;
	}

}
