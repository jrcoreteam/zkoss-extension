/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import java.util.ArrayList;
import java.util.List;

import org.jumin.common.xsutils.model.JrxTerm;
import org.jumin.zkx.components.tree.renderer.XmlTreeitemRenderer;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.tree.XmlTreeExpansionMode;
import org.jumin.zkx.zkxutils.tree.XmlTreeModel;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Tree;
import org.zkoss.zul.TreeModel;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.impl.InputElement;

public class XmlTree extends Tree {

	private static final long serialVersionUID = 4378586669680221159L;

    public static final String PROTECTED_NODE_LIST = "protectedNodeList";

	public static final String SCLASS = "jrx-msg-tree";
	
	public static final char INFO_KEY_CODE = 'I';
	
	public static final String INFO_CONTROL_KEY = "^" + INFO_KEY_CODE;

	public static final char MANDATORY_KEY_CODE = 'M';
	
	public static final String MANDATORY_JUMP_CONTROL_KEY = "^" + MANDATORY_KEY_CODE;
	
	public static final String ON_JUMP_TO_NEXT_ERROR_ENTRY = "onJumpToNextErrorEntry";

	public static final String ON_JUMP_TO_PREVIOUS_ERROR_ENTRY = "onJumpToPreviousErrorEntry";
	
	private XmlTreeExpansionMode expansionMode = XmlTreeExpansionMode.EXPAND_SINGLE;

	protected XmlTreeitemRenderer renderer;

	public XmlTree() {
		super();

		initProperties();
		initEvents();
	}

	protected void initProperties() {
		setSclass(SCLASS);
		renderer = new XmlTreeitemRenderer();
		renderer.setExpansionMode(getExpansionMode());
		super.setItemRenderer(renderer);
		appendChild(renderer.getTreeColumns());
		setSizedByContent(false);
	}

	protected void initEvents() {
		addEventListener(ON_JUMP_TO_NEXT_ERROR_ENTRY, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				Object obj = event.getData();
				InputElement currentErrorElement = null;
				if (obj instanceof InputElement) {
					currentErrorElement = (InputElement) obj;
				}

				if (currentErrorElement == null) {
					selectFirstErrorElement();
				} else {
					selectNextErrorElement(currentErrorElement);
				}
			}
		});
	}

	public void setExpansionMode(XmlTreeExpansionMode expansionMode) {
		this.expansionMode = expansionMode;
	}

	public XmlTreeExpansionMode getExpansionMode() {
		return expansionMode;
	}

	public void selectFirstMandatoryElement() {
		InputElement firstMandatoryInputElement = null;
		for (Object obj : getItems()) {
			Treeitem treeitem = (Treeitem) obj;
			firstMandatoryInputElement = ComponentUtils.getChildByClass(treeitem.getTreerow(), InputElement.class, true);
			if (firstMandatoryInputElement != null)
				break;
		}

		if (firstMandatoryInputElement != null) {
			Clients.scrollIntoView(firstMandatoryInputElement);
			firstMandatoryInputElement.setFocus(true);
		}
	}

	public void selectNextMandatoryElement(Component currentInputElement) {
		Treeitem currentTreeitem = ComponentUtils.getParentByClass(currentInputElement, Treeitem.class);
		if (currentTreeitem == null) return;

        List<Treeitem> treeItemList = new ArrayList<Treeitem>(getItems());
		int currentIndex = treeItemList.indexOf(currentTreeitem);
		if (currentIndex >= 0 && currentIndex < (treeItemList.size() - 1)) {
			for (int i = currentIndex + 1; i < treeItemList.size(); i++) {
				Treeitem nextTreeitem = treeItemList.get(i);
				XmlTreerow nextRow = (XmlTreerow) nextTreeitem.getTreerow();
				JrxTerm<?> jrxTerm = nextRow.getData();
				if (jrxTerm.isMandatory()) {
					InputElement nextMandatoryInputElement = ComponentUtils.getChildByClass(nextRow, InputElement.class, true);
					if (nextMandatoryInputElement != null) {
						Clients.scrollIntoView(nextMandatoryInputElement);
						nextMandatoryInputElement.focus();
						nextMandatoryInputElement.select();
						return;
					}
				}
	        }
		}
	}

	public void selectPreviousMandatoryElement(Component currentInputElement) {
		Treeitem currentTreeitem = ComponentUtils.getParentByClass(currentInputElement, Treeitem.class);
		if (currentTreeitem == null) return;

        List<Treeitem> treeItemList = new ArrayList<Treeitem>(getItems());
		int currentIndex = treeItemList.indexOf(currentTreeitem);
		if (currentIndex >= 0) {
			for (int i = currentIndex - 1; i >= 0; i--) {
				Treeitem previousTreeitem = treeItemList.get(i);
				XmlTreerow previousRow = (XmlTreerow) previousTreeitem.getTreerow();
				JrxTerm<?> jrxTerm = previousRow.getData();
				if (jrxTerm.isMandatory()) {
					InputElement previousMandatoryInputElement = ComponentUtils.getChildByClass(previousRow, InputElement.class, true);
					if (previousMandatoryInputElement != null) {
						Clients.scrollIntoView(previousMandatoryInputElement);
						previousMandatoryInputElement.focus();
						previousMandatoryInputElement.select();
						return;
					}
				}
	        }
		}
	}

	public void selectFirstErrorElement() {

	}

	public void selectNextErrorElement(InputElement currentErrorElement) {

	}

	public void selectPreviousErrorElement(InputElement currentErrorElement) {

	}
	
	@Override
	public XmlTreeModel getModel() {
	    return (XmlTreeModel) super.getModel();
	}
	
    @SuppressWarnings("unchecked")
    @Override
	public XmlTreeitemRenderer getItemRenderer() {
	    return renderer; 
	}

    @Override
    public void setItemRenderer(TreeitemRenderer<?> renderer) {
        if (!(renderer instanceof XmlTreeitemRenderer)) {
        	throw new RuntimeException("Incorrect type tree renderer. Please assign a tree renderer from type org.jumin.jrx.web.msg.framework.trees.MessageTreeitemRenderer.");
        }
        
        this.renderer = (XmlTreeitemRenderer) renderer;
        
        super.setItemRenderer(renderer);
        reassignTreeitemRenderer();
    }
    
    private void reassignTreeitemRenderer() {
    	// clear tree columns -> do we need to remove existing columns???
    	Treecols oldColumns = ComponentUtils.getChildByClass(this, Treecols.class);
    	if (oldColumns != null) {
    		removeChild(oldColumns);
    	}
    	
    	if (renderer == null) {
    		return;
    	}
    	
    	renderer.setExpansionMode(getExpansionMode());
    	appendChild(renderer.getTreeColumns()); // assign new columns. Do we need to add columns from new renderer???
    }
    
	@Override
	public void setModel(TreeModel<?> model) {
		TreeConstraintUtils.clearXmlToInputElementTable(this);
		TreeConstraintUtils.clearValueSyntaxErrorTable(this);
		
	    super.setModel(model);
	}
}
