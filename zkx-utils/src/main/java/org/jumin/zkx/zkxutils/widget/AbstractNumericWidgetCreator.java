/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.util.Locale;

import org.jumin.zkx.zkxutils.UserContextUtil;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.impl.NumberInputElement;

public abstract class AbstractNumericWidgetCreator<W extends NumberInputElement, T extends Number> extends AbstractWidgetCreator<W, T> {

	private static final long serialVersionUID = 4596471832348677560L;

    @Override
	public W createWidget(Component parent) {
	    Locale formatLocale = Locale.getDefault();
	    
	    if (parent.getDesktop() != null) {
	    	formatLocale = UserContextUtil.getUserFormatLocale(parent.getDesktop());
	    }
		
	    W inputWidget = newNumberInputWidget();
	    inputWidget.setParent(parent);
	    inputWidget.setLocale(formatLocale);
	    
	    return inputWidget;
	}
	
	protected abstract W newNumberInputWidget();

}
