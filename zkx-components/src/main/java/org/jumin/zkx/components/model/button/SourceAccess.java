/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

public enum SourceAccess {
	RESULT_EMPTY("R0"),
	RESULT_NOT_EMPTY("R1"),
	RESULT_SINGLE_SELECTION("RS"), 
	RESULT_MULTI_SELECTION("RM"), 
	DETAIL("DS");

	private final String value;

	SourceAccess(String value) {
		this.value = value;
	}

	public static SourceAccess fromCode(String value) {
		if (value.equals("RS"))
			return RESULT_SINGLE_SELECTION;
		if (value.equals("RM"))
			return RESULT_MULTI_SELECTION;
		if (value.equals("DS"))
			return DETAIL;
		if (value.equals("R0"))
			return RESULT_EMPTY;
		if (value.equals("R1"))
			return RESULT_NOT_EMPTY;

		return null;
	}

	public static SourceAccess fromString(String value) {
		if (value.equalsIgnoreCase("Result Single Selection"))
			return RESULT_SINGLE_SELECTION;
		if (value.equalsIgnoreCase("Result Multi Selection"))
			return RESULT_MULTI_SELECTION;
		if (value.equalsIgnoreCase("Detail"))
			return DETAIL;
		if (value.equalsIgnoreCase("Result Empty"))
			return RESULT_EMPTY;
		if (value.equalsIgnoreCase("Result Not Empty"))
			return RESULT_NOT_EMPTY;

		return null;
	}

	public String toCode() {
		return value;
	}

	@Override
	public String toString() {
		if (value.equals("RS"))
			return "Result Single Selection";
		if (value.equals("RM"))
			return "Result Multi Selection";
		if (value.equals("DS"))
			return "Detail";
		if (value.equals("R0"))
			return "Result Empty";
		if (value.equals("R1"))
			return "Result Not Empty";

		return "";
	}
}
