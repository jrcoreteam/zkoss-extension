/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.renderer;

import java.io.Serializable;
import java.util.Map;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.common.xsutils.model.JrxTerm;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.tree.XmlTreeNodeDefault;
import org.jumin.zkx.zkxutils.tree.XmlTreeRootDefinition;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.impl.InputElement;

public class XmlTemplateTreeitemRenderer extends XmlTreeitemRenderer {

	private static final long serialVersionUID = 6872565976912428409L;

    protected XmlTemplateConfigCellRenderer configCellRenderer = null;
	
	protected Map<JrxTerm<?>, XmlTreeNodeDefault> templateConfigNodeDefaultMap;

	public XmlTemplateTreeitemRenderer() {
		super();
		configCellRenderer = new XmlTemplateConfigCellRenderer();
	}

	@Override
	public void render(Treeitem item, JrxTerm<?> data, int index) throws Exception {
		super.render(item, data, index);

		item.addEventListener(Events.ON_RIGHT_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				showConfigPopup((Treeitem) event.getTarget());
			}
		});

		item.addEventListener(MessageTemplateTreeConfigFunctions.TOGGLE_READONLY.getEventName(),
		        new EventListener<Event>() {
			        @Override
			        public void onEvent(Event event) throws Exception {
				        toggleReadonly((Treeitem) event.getTarget());
			        }
		        });

		item.addEventListener(MessageTemplateTreeConfigFunctions.TOGGLE_EXPAND.getEventName(),
		        new EventListener<Event>() {
			        @Override
			        public void onEvent(Event event) throws Exception {
				        toggleExpand((Treeitem) event.getTarget());
			        }
		        });


		item.addEventListener(MessageTemplateTreeConfigFunctions.SWITCH_VISIBILITY.getEventName(),
		        new EventListener<Event>() {
			        @Override
			        public void onEvent(Event event) throws Exception {
				        switchVisibility((Treeitem) event.getTarget());
			        }
		        });
	}

	protected void showConfigPopup(Treeitem item) {
		Popup popup = new Menupopup();
		popup.setPage(item.getPage());

		MessageTemplateTreeConfig currentConfig = getMessageTreerowConfig(item);

		for (MessageTemplateTreeConfigFunctions function : MessageTemplateTreeConfigFunctions.values()) {
			if (function.getAssociatedConfig().ordinal() < currentConfig.ordinal())
				continue; // skip because of the importance level

			if (MessageTemplateTreeConfig.EXPAND.equals(function.getAssociatedConfig())) {
				if (item.getTreechildren() == null || item.getTreechildren().getItemCount() < 1) {
					continue; // skip because EXPAND is only valid for parent node
				}
			}
			
			Menuitem configItem = new Menuitem(ZkLabels.getLabel(function.labelKey, function.getDefaultLabel()));
			configItem.setParent(popup);
			configItem.addForward(Events.ON_CLICK, item, function.getEventName());
		}

		popup.open(item);
	}

	protected void toggleReadonly(Treeitem item) {
		XmlTreerow row = (XmlTreerow) item.getTreerow();
		if (row == null || row.getFirstChild() == null)
			return;

		Treecell valueCell = (Treecell) row.getFirstChild().getNextSibling();
		if (valueCell == null)
			return;

		InputElement inputWidget = ComponentUtils.getChildByClass(valueCell, InputElement.class, true);
		if (inputWidget != null) {
			Treecell configCell = (Treecell) valueCell.getNextSibling();
			if (configCell == null)
				return;

			Label configSymbolLabel = (Label) configCell.getFirstChild();
			String symbol = MessageTemplateTreeConfig.READ_ONLY.getSymbol();
			if (configSymbolLabel == null) {
				configSymbolLabel = new Label(symbol);
				configCell.appendChild(configSymbolLabel);
			} else {
				if (symbol.equals(configSymbolLabel.getValue())) {
					configSymbolLabel.setValue(MessageTemplateTreeConfig.NONE.getSymbol());
				} else if (MessageTemplateTreeConfig.HIDDEN.getSymbol().equals(configSymbolLabel.getValue())) {
					// Treeitem is hidden, assumption all children are also hidden -> cannot set readonly.
					return;
				} else {
					configSymbolLabel.setValue(symbol);
				}
			}
		} else {
			// for treeitem without input widget -> ignore readonly
		}

		// Recursive to child item(s)
		Treechildren treechildren = item.getTreechildren();
		if (treechildren != null) {
			// Do not use treechildren.getItems() because those that method returns all descendants
			for (Component child : treechildren.getChildren()) {
				if (!(child instanceof Treeitem))
					continue;

				toggleReadonly((Treeitem) child);
			}
		}
	}

	protected void switchVisibility(Treeitem item) {
		XmlTreerow row = (XmlTreerow) item.getTreerow();
		Treecell cell = (Treecell) row.getLastChild().getPreviousSibling();
		Label configSymbolLabel = (Label) cell.getFirstChild();
		boolean visible = true;
		String symbol = MessageTemplateTreeConfig.HIDDEN.getSymbol();
		if (configSymbolLabel == null) {
			configSymbolLabel = new Label(symbol);
			cell.appendChild(configSymbolLabel);
			visible = false;
		} else {
			if (!symbol.equals(configSymbolLabel.getValue())) {
				configSymbolLabel.setValue(symbol);
				visible = false;
			} else {
				configSymbolLabel.setValue(MessageTemplateTreeConfig.NONE.getSymbol());
				visible = true;
			}
		}

		// Recursive to child item(s) with the same status as parent
		Treechildren treechildren = item.getTreechildren();
		if (treechildren != null) {
			// Do not use treechildren.getItems() because those that method returns all descendants
			for (Component child : treechildren.getChildren()) {
				if (!(child instanceof Treeitem))
					continue;

				setVisible((Treeitem) child, visible);
			}
		}
	}

	protected void setVisible(Treeitem item, boolean visible) {
		XmlTreerow row = (XmlTreerow) item.getTreerow();
		Treecell cell = (Treecell) row.getLastChild().getPreviousSibling();
		Label configSymbolLabel = (Label) cell.getFirstChild();
		if (visible) {
			if (configSymbolLabel == null) {
				// Treeitem is visible, assumption all children are also visible -> No need to propagate to children
				return;
			} else {
				configSymbolLabel.setValue(MessageTemplateTreeConfig.NONE.getSymbol());
			}
		} else {
			String symbol = MessageTemplateTreeConfig.HIDDEN.getSymbol();
			if (configSymbolLabel == null) {
				configSymbolLabel = new Label(symbol);
				cell.appendChild(configSymbolLabel);
				cell.setAttribute(MessageTemplateTreeConfigFunctions.class.getSimpleName(), configSymbolLabel);
			} else if (!symbol.equals(configSymbolLabel.getValue())) {
				configSymbolLabel.setValue(symbol);
			} else {
				// Treeitem is hidden, assumption all children are also hidden -> No need to propagate to children
				return;
			}
		}

		// Recursive to child item(s) with the same status as parent
		Treechildren treechildren = item.getTreechildren();
		if (treechildren != null) {
			// Do not use treechildren.getItems() because those that method returns all descendants
			for (Component child : treechildren.getChildren()) {
				if (!(child instanceof Treeitem))
					continue;

				setVisible((Treeitem) child, visible);
			}
		}
	}
	
	protected void toggleExpand(Treeitem item) {
		if (item.getTreechildren() == null || item.getTreechildren().getItemCount() < 1) {
			return;
		}
		
		XmlTreerow row = (XmlTreerow) item.getTreerow();
		Treecell cell = (Treecell) row.getLastChild().getPreviousSibling();
		Label configSymbolLabel = (Label) cell.getFirstChild();
		String symbol = MessageTemplateTreeConfig.EXPAND.getSymbol();
		if (configSymbolLabel == null) {
			configSymbolLabel = new Label(symbol);
			cell.appendChild(configSymbolLabel);
		} else {
			if (symbol.equals(configSymbolLabel.getValue())) {
				configSymbolLabel.setValue(MessageTemplateTreeConfig.NONE.getSymbol());
			} else if (MessageTemplateTreeConfig.HIDDEN.getSymbol().equals(configSymbolLabel.getValue())) {
				// Treeitem is hidden -> cannot set expand.
				return;
			} else {
				configSymbolLabel.setValue(symbol);
			}
		}

	}

	public ServiceConnector getBackendServiceConnector() {
		return valueCellRenderer.getConnector();
	}

	public void setBackendConnector(ServiceConnector connector) {
		valueCellRenderer.setConnector(connector);
	}

	@Override
	public Treecols getTreeColumns() {
		Treecols cols = super.getTreeColumns();

		Treecol col = new Treecol(ZkLabels.getLabel("messageTree.heading.config", "Config"));
		col.setWidth("80px");
		cols.insertBefore(col, cols.getLastChild());

		return cols;
	}

	@Override
	protected void createTreeRowString(Treeitem item, JrxTerm<?> jrxTerm, XmlTreeRootDefinition treeRootDefinition,
	        XmlTreeNodeDefault[] treeNodeDefaults) {
		super.createTreeRowString(item, jrxTerm, treeRootDefinition, treeNodeDefaults);

		// Insert config cell before description cell
		Treecell configCell = configCellRenderer.createConfigCell((XmlTreerow) item.getTreerow());
		if (templateConfigNodeDefaultMap != null) {
			XmlTreeNodeDefault configNodeDefault = templateConfigNodeDefaultMap.get(jrxTerm);
			if (configNodeDefault != null) {
				MessageTemplateTreeConfig templateConfig = null;
				if (configNodeDefault.isVisible() != null && Boolean.FALSE.equals(configNodeDefault.isVisible())) {
					templateConfig = MessageTemplateTreeConfig.HIDDEN;
				} else if (configNodeDefault.isReadonly() != null && Boolean.TRUE.equals(configNodeDefault.isReadonly())) {
					templateConfig = MessageTemplateTreeConfig.READ_ONLY;
				}
				if (templateConfig != null) {
					Label configLabel = (Label) configCell.getFirstChild();
					if (configLabel == null) {
						configLabel = new Label();
						configLabel.setParent(configCell);
					}
					configLabel.setValue(templateConfig.getSymbol());
				}
			}
		}
	}
	
	public void setTemplateConfigNodeDefaultMap(Map<JrxTerm<?>, XmlTreeNodeDefault> templateConfigNodeDefaultMap) {
	    this.templateConfigNodeDefaultMap = templateConfigNodeDefaultMap;
    }

	public static final MessageTemplateTreeConfig getMessageTreerowConfig(Treeitem treeitem) {
		Treerow treerow = treeitem.getTreerow();
		if (treerow != null && treerow instanceof XmlTreerow) {
			return getMessageTreerowConfig((XmlTreerow) treerow);
		}

		return MessageTemplateTreeConfig.NONE;
	}

	public static final MessageTemplateTreeConfig getMessageTreerowConfig(XmlTreerow treerow) {
		if (treerow != null) {

			Label configLabel = null;
			if (treerow.getChildren().size() >= 4) {
				Treecell configCell = (Treecell) treerow.getChildren().get(2);
				if (configCell != null) {
					configLabel = (Label) configCell.getFirstChild();
				}
			}

			if (configLabel != null) {
				return MessageTemplateTreeConfig.fromSymbol(configLabel.getValue());
			}
		}
		return MessageTemplateTreeConfig.NONE;
	}
	
	/**
	 * Please consider the ordinal sequence of this enum's entries properly because it does effect the functions listed
	 * in the popup displayed for configuration.
	 * 
	 */
	public static enum MessageTemplateTreeConfig {

		// @formatter:off
		NONE (""),
		READ_ONLY ("[R/O]"),
		EXPAND ("[EXPAND]"),
		HIDDEN ("[HIDDEN]"),
		// @formatter:on
		;

		private String symbol;

		private MessageTemplateTreeConfig(String symbol) {
			this.symbol = symbol;
		}

		public String getSymbol() {
			return symbol;
		}

		public static final MessageTemplateTreeConfig fromSymbol(String symbol) {
			if (symbol != null) {
				for (MessageTemplateTreeConfig item : values()) {
					if (item.symbol.equals(symbol))
						return item;
				}
			}

			return NONE;
		}
	}

	public static enum MessageTemplateTreeConfigFunctions {
		// @formatter:off
		TOGGLE_READONLY("messageTree.toggleReadonly", "Toggle Readonly", "onToggleReadonly", MessageTemplateTreeConfig.READ_ONLY), 
		TOGGLE_EXPAND("messageTree.toggleExpand", "Toggle Expand (Expand/Collapse)", "onToggleExpand", MessageTemplateTreeConfig.EXPAND),
		SWITCH_VISIBILITY("messageTree.switchVisibility", "Switch Visibility (Show/Hide)", "onSwitchVisibility", MessageTemplateTreeConfig.HIDDEN)
		// @formatter:on
		;

		private String labelKey;

		private String defaultLabel;

		private String eventName;

		private MessageTemplateTreeConfig associatedConfig;

		private MessageTemplateTreeConfigFunctions(String labelKey, String defaultLabel, String eventName,
		        MessageTemplateTreeConfig associatedConfig) {
			this.labelKey = labelKey;
			this.defaultLabel = defaultLabel;
			this.eventName = eventName;
			this.associatedConfig = associatedConfig;
		}

		public String getLabelKey() {
			return labelKey;
		}

		public String getDefaultLabel() {
			return defaultLabel;
		}

		public String getEventName() {
			return eventName;
		}

		public MessageTemplateTreeConfig getAssociatedConfig() {
			return associatedConfig;
		}
	}
	
	static class MessageTemplateConfigNodeDefault implements Serializable {
		private static final long serialVersionUID = 640023749780274246L;

		private JrxTerm<?> jrxTerm;
		
		private XmlTreeNodeDefault nodeDefault;
		
		public MessageTemplateConfigNodeDefault(JrxTerm<?> jrxTerm, XmlTreeNodeDefault nodeDefault) {
			this.jrxTerm = jrxTerm;
			this.nodeDefault = nodeDefault;
		}
		
		public JrxTerm<?> getJrxTerm() {
	        return jrxTerm;
        }
		
		public XmlTreeNodeDefault getNodeDefault() {
	        return nodeDefault;
        }
	}
}
