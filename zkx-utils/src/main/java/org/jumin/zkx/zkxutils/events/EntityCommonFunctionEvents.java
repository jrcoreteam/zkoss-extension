/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.events;

public class EntityCommonFunctionEvents {
	public static final String ON_ADD = "onAdd";
	public static final String ON_ADD_COMMENT = "onAddComment";
	public static final String ON_ALERT = "onAlert";
	public static final String ON_ALERT_ALL = "onAlertAll";
	public static final String ON_BLOCK = "onBlock";
	public static final String ON_CANCEL = "onCancel";
	public static final String ON_COPY = "onCopy";
	public static final String ON_CREATE = "onCreateNew";
	public static final String ON_DELETE = "onDelete";
	public static final String ON_EDIT = "onEdit";
	public static final String ON_EXPORT = "onExport";
	public static final String ON_FORCE_LOGOUT = "onForceLogout";
	public static final String ON_FORCE_LOGOUT_ALL = "onForceLogoutAll";
	public static final String ON_IMPORT = "onImport";
	public static final String ON_MIGRATE = "onMigrate";
	public static final String ON_MOVE_BOTTOM = "onMoveBottom";
	public static final String ON_MOVE_DOWN = "onMoveDown";
	public static final String ON_MOVE_TOP = "onMoveTop";
	public static final String ON_MOVE_UP = "onMoveUp";
	public static final String ON_NEXT = "onNext";
	public static final String ON_PREVIOUS = "onPrevious";
	public static final String ON_PRINT = "onPrint";
	public static final String ON_PRINT_HISTORY = "onPrintHistory";
	public static final String ON_QUICK_VIEW = "onQuickView";
	public static final String ON_REFRESH = "onRefresh";
	public static final String ON_REMOVE = "onRemove";
	public static final String ON_RESET = "onReset";
	public static final String ON_SAVE = "onSave";
	public static final String ON_SAVE_AS = "onSaveAs";
	public static final String ON_SKIP = "onSkip";
	public static final String ON_START = "onStart";
	public static final String ON_STOP = "onStop";
	public static final String ON_STORE_COMMENT = "onStoreComment";
	public static final String ON_TEST = "onTest";
	public static final String ON_USER_COMMENT = "onUserComment";
	public static final String ON_VALIDATE = "onValidate";
	public static final String ON_QUERY = "onQuery";
	public static final String ON_ABORT = "onAbort";
    public static final String ON_SEARCH = "onSearch";
    
	// Approval process
	public static final String ON_ACTIVATE = "onActivate";
	public static final String ON_REJECTION = "onReject";
	public static final String ON_APPROVE = "onApprove";
	public static final String ON_SET_PASSWORD = "onSetPassword";
	public static final String ON_CONFIRM_PASSWORD = "onConfirmPassword";
	public static final String ON_REJECT_PASSWORD = "onRejectPassword";
	public static final String ON_VIEW_PASSWORD = "onViewPassword";
	
	// Entity Locking
	/**
	 * Event when locking an object is requested
	 */
	public static final String ON_LOCK = "onLock";
	
	/**
	 * Event when existing lock got stolen.
	 */
	public static final String ON_LOCK_STOLEN = "onLockStolen";
	
	/**
	 * Event when reserving one or more objects is requested
	 */
	public static final String ON_RESERVE = "onReserve";

	/**
	 * Event when stealing an existing lock is requested 
	 */
	public static final String ON_STEAL_LOCK = "onStealLock";
	
	/**
	 * Event when unlocking an object has been performed
	 */
	public static final String ON_UNLOCK = "onUnlock";
	
	/**
	 * Event when clearing reservation of one or more objects is requested
	 */
	public static final String ON_CLEAR_RESERVATION = "onClearReservation";
	
}
