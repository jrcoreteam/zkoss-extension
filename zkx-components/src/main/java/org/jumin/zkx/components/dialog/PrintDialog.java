/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.dialog;

import java.util.ArrayList;
import java.util.List;

import org.jumin.zkx.components.dialog.PrintDialog.PrintResultType;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.renderer.ListitemRendererWithHeader;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Separator;

public class PrintDialog extends SelectionDialog<PrintResultType> {

	private static final long serialVersionUID = 8729522104146423759L;

    private List<Listitem> selections;
    
    protected DialogButtonBar buttonBar;
    
    protected Listbox selectionList;
    
    public PrintDialog() {
        super();
        
        createContents();
        initEvents();
        initProperties();
    }
    
    @Override
    protected void createHeader(Component parent) {
        Separator sep = new Separator();
        sep.setParent(parent);
    }
    
    @Override
    protected void createBody(Component parent) {
        selectionList = new Listbox();
        selectionList.setParent(parent);
    }
    
    @Override
    protected void createFooter(Component parent) {
        Separator sep = new Separator();
        sep.setParent(this);
        
        buttonBar = new DialogButtonBar();
        buttonBar.setParent(parent);
    }

    @Override
    protected void initEvents() {
        super.initEvents();
        final Component root = this;
        
        selectionList.addEventListener(Events.ON_OK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                if (selectionList.getSelectedCount() > 0) {
                    beforeOK();
                    Events.postEvent(Events.ON_OK, root, null);
                }
            }
        });
        selectionList.addForward(Events.ON_DOUBLE_CLICK, selectionList, Events.ON_OK);
        buttonBar.addEventListener(Events.ON_OK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                beforeOK();
                Events.postEvent(Events.ON_OK, root, null);
            }
        });
        buttonBar.addEventListener(Events.ON_CANCEL, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                beforeCancel();
                Events.postEvent(Events.ON_CANCEL, root, null);
            }
        });
        buttonBar.addEventListener(SingleListSelectionDialogButtonBar.ON_CLEAR, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                clearSelections();
            }
        });
    }
    
    @Override
    protected void initProperties() {
        super.initProperties();
        
        setTitle(ZkLabels.getLabel("printSelection.dialogTitle", "Print Selection"));
        setWidth("200px");
        setHeight("218px");
        
        selections = null;

        buttonBar.setStyle("margin-top:4px");

        // Initialize data
        selectionList.setItemRenderer(new PrintResultTypeListitemRenderer());
        ListModel<PrintResultType> lm = new ListModelArray<PrintResultType>(PrintResultType.values());
        super.setModel(lm);
        selectionList.setModel(lm);
        selectionList.setVflex(true);
        selectionList.setMultiple(true);
        
        
        selectionList.setMultiple(false);
        
        if (!widgetStaticLabelUtil.isTestingMode()) {
            return;
        }
        
        widgetStaticLabelUtil.setLabel(selectionList, "selectionDialog.selectionList");
    }
    
    public void beforeOK() {
        selections = new ArrayList<Listitem>();
        selections.addAll(selectionList.getSelectedItems());
        setVisible(false);
    }

    @Override
    public void beforeCancel() {
        selections = null;
        setVisible(false);
    }

    @Override
    public List<Listitem> getSelections() {
        return selections;
    }

    public Listitem getSelection() {
        if (selections == null || selections.size() < 1) {
            return null;
        }
        return selections.get(0);
    }

    @Override
    public void clearSelections() {
        selectionList.clearSelection();
        selections = new ArrayList<>();
    }

    @Override
    public void selectItems(Object[] objectArray) {
        selectionList.clearSelection();
        
        for (int i = 0; i < objectArray.length; i++) {
            Object key = objectArray[i];
            for (int j = 0; j < selectionList.getItemCount(); j++) {
                Listitem item = selectionList.getItemAtIndex(j);
                if (!item.isLoaded()) selectionList.renderItem(item); // to ensure the rendering of the item.
                if (item.hasAttribute(ListitemRendererWithHeader.LISTITEM_KEY)) {
                    if (key.equals(item.getAttribute(ListitemRendererWithHeader.LISTITEM_KEY))) {
                        item.setSelected(true);
                        if (!selectionList.isMultiple()) {
                            break;
                        }
                    }
                } else {
                    // For those who does not use ITEM_KEY
                    if (key.equals(selectionList.getModel().getElementAt(j))) {
                        if (!selectionList.isMultiple()) {
                            break;
                        }
                    }
                }
            }
        }
    }
    
    private class PrintResultTypeListitemRenderer implements ListitemRendererWithHeader<PrintResultType> {

		private static final long serialVersionUID = -2640537641341469265L;

		@Override
        public void render(Listitem item, PrintResultType data, int index) throws Exception {
	        item.setLabel(ZkLabels.getLabel(data.getLabelKey(), data.getDefaultLabel()));
	        item.setImage(data.getIconSrc());
	        item.setValue(data);
	        item.setAttribute(LISTITEM_KEY, data.getDefaultLabel());
        }

		@Override
        public Listhead getListhead() {
			Listhead head = new Listhead();
			
			Listheader header = new Listheader(ZkLabels.getLabel("printSelection.listheader", "Style / Layout"));
			header.setParent(head);
			
	        return head;
        }
		
	}

	public static enum PrintResultType {
		OVERVIEW ("List", "printSelection.list", "~./zkxcomps/img/dialog/print-overview.png"),
		DETAIL ("Detail", "printSelection.detail", "~./zkxcomps/img/dialog/print-detail.png"),
	    DETAIL_HISTORY ("Detail with History", "printSelection.detailHistory", "~./zkxcomps/img/dialog/print-detail-history.png");
        
		private String defaultLabel;
		
		private String labelKey;
		
		private String iconSrc;
		
		PrintResultType(String defaultLabel, String labelKey, String iconSrc) {
			this.defaultLabel = defaultLabel;
			this.labelKey = labelKey;
			this.iconSrc = iconSrc;
		}

        public String getDefaultLabel() {
	        return defaultLabel;
        }

        public String getLabelKey() {
	        return labelKey;
        }
        
        public String getIconSrc() {
	        return iconSrc;
        }
        
        @Override
        public String toString() {
            return defaultLabel;
        }
        
        public static PrintResultType fromDefaultLabel(String defaultLabel) {
            for (PrintResultType prt : values()) {
                if (prt.defaultLabel.equalsIgnoreCase(defaultLabel)) {
                    return prt;
                }
            }
            return null;
        }
	}
}
