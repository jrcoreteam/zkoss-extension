/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.constraint;

import java.io.Serializable;
import java.util.Map;

import org.jumin.zkx.components.constraint.ContraintValidationMessage;
import org.jumin.zkx.components.constraint.ErrorMessageLabel;
import org.jumin.zkx.components.constraint.WidgetConstraintUtil;
import org.jumin.zkx.components.tree.TreeConstraintUtils;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Constraint;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.impl.InputElement;

public abstract class AbstractTreeSyntaxValidationConstraint implements Constraint, Serializable {

	private static final long serialVersionUID = -7266815487341025779L;
	
    private ErrorMessageLabel errorDescriptionLabel = null;

	protected void showSyntaxValidationError(Component comp, ContraintValidationMessage validationMessage) {
		WidgetConstraintUtil.setErrorOnWidget((InputElement) comp, true);

		// A treerow from message tree consists of 3 cells. The last one is the description cell for syntax error
		// message.
		XmlTreerow treerow = ComponentUtils.getParentByClass(comp, XmlTreerow.class);
		if (treerow == null || treerow.getChildren().size() < 3) {
			return;
		}

		if (errorDescriptionLabel == null) {
			errorDescriptionLabel = new ErrorMessageLabel();
		}
		String errorText = validationMessage.getDefaultMessage();
		errorDescriptionLabel.setValue(errorText);
		if (errorText.length() > 25)
			errorDescriptionLabel.setTooltiptext(errorText);

		Treecell descriptionCell = getDescriptionCell(treerow);
		if (descriptionCell.getChildren().size() > 0) {
			descriptionCell.insertBefore(descriptionCell.getFirstChild(), errorDescriptionLabel);
		} else {
			descriptionCell.appendChild(errorDescriptionLabel);
		}
		
		registerError(treerow.getTree(), comp, validationMessage);
	}

	protected void clearSyntaxValidationError(Component comp) {
		WidgetConstraintUtil.setErrorOnWidget((InputElement) comp, false);

		if (errorDescriptionLabel == null) {
			return;
		}

		// A treerow from message tree consists of 3 cells. The last one is the description cell for syntax error
		// message.
		XmlTreerow treerow = ComponentUtils.getParentByClass(comp, XmlTreerow.class);
		if (treerow == null || treerow.getChildren().size() < 3) {
			return;
		}

		removeError(treerow.getTree(), comp);
		
		Treecell descriptionCell = getDescriptionCell(treerow);
		descriptionCell.removeChild(errorDescriptionLabel);
		errorDescriptionLabel.detach();
		errorDescriptionLabel = null;
	}

	protected void registerError(Tree tree, Component comp, ContraintValidationMessage validationMessage) {
		Map<Component, ValueSyntaxError> valueSyntaxErrorTable = TreeConstraintUtils.getMessageTreeValueSyntaxErrorTable(tree);

		if (valueSyntaxErrorTable.containsKey(comp)) return;
		
		valueSyntaxErrorTable.put(comp, new ValueSyntaxError(comp, validationMessage));
		TreeConstraintUtils.setMessageTreeValueSyntaxErrorTable(tree, valueSyntaxErrorTable);
	}

	protected void removeError(Tree tree, Component component) {
		if (tree == null || component == null)
			return;

		Map<Component, ValueSyntaxError> valueSyntaxErrorTable = TreeConstraintUtils.getMessageTreeValueSyntaxErrorTable(tree);

		valueSyntaxErrorTable.remove(component);
		
		TreeConstraintUtils.setMessageTreeValueSyntaxErrorTable(tree, valueSyntaxErrorTable);
	}
	
	public Treecell getDescriptionCell(Treerow treerow) {
		Component lastCell = treerow.getLastChild();
		while (!(lastCell instanceof Treecell)) {
			lastCell = lastCell.getPreviousSibling();
		}
		return (Treecell) lastCell;
	}
	
	public static class ValueSyntaxError implements Serializable {

		private static final long serialVersionUID = 899382859041432508L;

        private Component widget;
		
		private ContraintValidationMessage validationMessage;
		
		public ValueSyntaxError(Component widget, ContraintValidationMessage validationMessage) {
	        this.widget = widget;
	        this.validationMessage = validationMessage;
        }
		
		public Component getWidget() {
	        return widget;
        }
		
		public ContraintValidationMessage getValidationMessage() {
	        return validationMessage;
        }
	}
}
