/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.lookup;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.zkx.components.TextEditorWindow;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zul.Textbox;

public abstract class XmlTreeMultilineTextWidgetCreator extends XmlTreeAbstractLookupWidgetCreator {

	private static final long serialVersionUID = -528219647501057825L;
	
    public static final String EDITOR_WINDOW = "jrxEditorWindow";
	public static final String SERVICED_TEXTBOX = "jrxServicedTextbox";

	private int maxLine = 2;

	private int maxCharsInLine = 35;

	public XmlTreeMultilineTextWidgetCreator(ServiceConnector connector, XmlTreerow row,
	        EditorMode editorMode, String initialValue, int maxLine, int maxCharsInLine) {
		super(connector, row, editorMode, initialValue);
		this.maxLine = maxLine;
		this.maxCharsInLine = maxCharsInLine;
	}

	@Override
	public HtmlBasedComponent createWidget() {
		if (EditorMode.VIEW.equals(editorMode)) {
			return createViewModeWidget();
		}

		Textbox tb = instantiateNewWidget();
		if (maxCharsInLine > 0) {
			tb.setCols(maxCharsInLine);
		}
		tb.setMultiline(true);
		if (maxCharsInLine > 1) {
			tb.setRows(maxLine);
		}
		tb.setHeight(((maxLine > 4 ? (maxLine > 10 ? (maxLine > 20 ? 12 : 6) : 4) : maxLine) * 14 + 2) + "px");
		tb.setCtrlKeys("^h^H");

		if (value != null && !value.equals("")) {
			tb.setText(value);
		}

		tb.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				Textbox tb = (Textbox) event.getTarget();
				String codeValue = tb.getValue();
				registerValueDescription(codeValue, true);
			}
		});

		tb.addEventListener(Events.ON_CTRL_KEY, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				KeyEvent keyEvent = (KeyEvent) event;
				if ((keyEvent.getKeyCode() == 'h' || keyEvent.getKeyCode() == 'H') && keyEvent.isCtrlKey()) {
					Textbox tb = (Textbox) event.getTarget();
					displayTextEditorWindow(tb);
				}
			}
		});

		row.setLookupWidget(tb);

		return tb;
	}

	protected Textbox instantiateNewWidget() {
		return new Textbox();
	}

	@Override
	protected void registerValueDescription(String value, boolean triggerEvent) {
// @formatter:off
//		if (row == null || EditorMode.VIEW.equals(editorMode)) return;
//		// No need for description when it is readonly.
//		
//		String desc = "";
//		if (value != null && !value.equals("")) {
//	        int length = value.length();
//	        int currentPos = 0;
//	        int nextNewline = value.indexOf('\n');
//	        while (currentPos < length && nextNewline >= 0) {
//	        	if (currentPos > 0) {
//	        		desc += ", ";
//	        		desc += (nextNewline - currentPos - 1);
//	        	} else {
//	        		desc += (nextNewline - currentPos);
//	        	}
//	            currentPos = nextNewline;
//	            nextNewline = value.indexOf('\n', currentPos + 1);
//	        }
//	        if (desc.length() > 0) desc += ", ";
//	        desc += length - (currentPos + 1);
//	        desc = "Length(s): [ " + desc + " ]";
//		}
//		registerValueDescription(row, desc, triggerEvent);
// @formatter:on
	}

	public int getMaxCharsInLine() {
		return maxCharsInLine;
	}

	public int getMaxLine() {
		return maxLine;
	}

	protected void displayTextEditorWindow(Textbox textbox) {
		if (textbox == null || textbox.getPage() == null || textbox.getCols() < 2 || textbox.getRows() < 2)
			return;

		TextEditorWindow editorWindow = (TextEditorWindow) textbox.getAttribute(EDITOR_WINDOW);

		if (editorWindow == null) {
			editorWindow = createTextEditorWindow();
			editorWindow.setColumnSize(textbox.getCols());
			editorWindow.setRowSize(textbox.getRows());
			editorWindow.applyConfig();
			editorWindow.setAttribute(SERVICED_TEXTBOX, textbox);

			editorWindow.addEventListener(Events.ON_OK, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					TextEditorWindow editorWindow = (TextEditorWindow) event.getTarget();
					Textbox textbox = (Textbox) editorWindow.getAttribute(SERVICED_TEXTBOX);
					textbox.setText(editorWindow.getText());
					Events.postEvent(Events.ON_CHANGE, textbox, null);
					editorWindow.setVisible(false);
				}
			});
			editorWindow.addEventListener(Events.ON_CANCEL, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					TextEditorWindow editorWindow = (TextEditorWindow) event.getTarget();
					editorWindow.setVisible(false);
				}
			});

			textbox.setAttribute(EDITOR_WINDOW, editorWindow);
		}

		editorWindow.setText(textbox.getText());
		editorWindow.setPage(textbox.getPage());
		editorWindow.doModal();
		editorWindow.initFocus();
	}

	protected TextEditorWindow createTextEditorWindow() {
		return new TextEditorWindow();
	}
}
