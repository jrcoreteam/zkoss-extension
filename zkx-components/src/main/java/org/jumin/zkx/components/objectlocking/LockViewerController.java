/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.objectlocking;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.jumin.common.base.services.UserObjectLockingService;
import org.jumin.common.utils.locking.LockItem;
import org.jumin.common.utils.locking.ObjectLockingService;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Timer;
import org.zkoss.zul.Window;

public class LockViewerController extends SelectorComposer<Window> {

	private static final long serialVersionUID = 8461070707016997423L;

    public static final int TIMER_DELAY = 3000;
	
	private static Logger log = LoggerFactory.getLogger(LockViewerController.class);
	
	@Wire("listbox")
	private Listbox lockTable;
	
	@Wire("timer")
	private Timer timer;
	
	@Wire("label#timestamp")
	private Label timestamp;
	
	private String userId;
	
	@Override
	public void doAfterCompose(Window comp) throws Exception {
	    super.doAfterCompose(comp);
	    
	    if (getPage().getDesktop().isServerPushEnabled()) {
	    	getPage().getDesktop().enableServerPush(true);
	    }
	    
	    userId = UserContextUtil.getSessionUserPrincipal(getPage());
	    
	    initProperties();
	    initEvents();
	}
	
	private void initEvents() {
	    timer.addEventListener(Events.ON_TIMER, new EventListener<Event>() {
			@Override
            public void onEvent(Event event) throws Exception {
				log.debug("Refresh lock table");
	            timer.stop();
	            refreshLockTable();
	            if (timer.getDelay() < TIMER_DELAY) timer.setDelay(TIMER_DELAY);
	            timer.start();
            }
		});
    }

	private void initProperties() {
		ListitemRenderer<LockItem> renderer = new ListitemRenderer<LockItem>() {
			private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
			@Override
            public void render(Listitem item, LockItem data, int index) throws Exception {
	            Listcell cell = new Listcell(data.getEntityName());
	            cell.setParent(item);
	            cell = new Listcell(data.getEntityId());
	            cell.setParent(item);
	            cell = new Listcell(data.getLockedBy());
	            cell.setParent(item);
	            cell = new Listcell(sdf.format(new Date(data.getLockedUtcTimestamp())));
	            cell.setParent(item);
	            cell = new Listcell(data.getSource());
	            cell.setParent(item);
            }
		};
		
		lockTable.setItemRenderer(renderer);
	}
	
	private void refreshLockTable() {
		ObjectLockingService lockService = new UserObjectLockingService(userId);
		
		// sort lock info
		List<LockItem> lockItemList = lockService.retrieveLockInfo();
		
		SortedMap<String, LockItem> sortedMap = new TreeMap<String, LockItem>();
		for (LockItem lockItem : lockItemList) {
			sortedMap.put(lockItem.getLockedUtcTimestamp() + lockItem.getEntityName() + lockItem.getEntityId(), lockItem);
        }
		
		lockTable.setModel(new ListModelList<LockItem>(sortedMap.values()));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
		timestamp.setValue(sdf.format(new Date(System.currentTimeMillis())));
	}
}
