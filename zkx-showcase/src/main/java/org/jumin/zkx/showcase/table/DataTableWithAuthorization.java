/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.showcase.table;

import java.util.ArrayList;
import java.util.List;

import org.jumin.zkx.components.model.button.ButtonBarModelLoader;
import org.jumin.zkx.components.table.CommonDataTable;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.ListModel;

public class DataTableWithAuthorization<T> extends CommonDataTable<T> {

	private static final long serialVersionUID = -5732147817778607546L;

	private List<String> authorizedActions;

	public DataTableWithAuthorization() {
		super();

		authorizedActions = new ArrayList<>();
	}

	@Override
	protected List<String> getAuthorizedActions() {
		return authorizedActions;
	}

	public void setAuthorizedActions(List<String> authorizedActions) {
		this.authorizedActions = authorizedActions;
	}

	@Override
	protected void initEvents() {
		super.initEvents();

		addEventListener(Events.ON_SELECT, e -> {
			updateButtonBar();
		});
	}

	@Override
	public void clearDataTable() {
		super.clearDataTable();
		updateButtonBar();
	}
	
	@Override
	public void refreshResultTable(boolean refreshHeader) {
		super.refreshResultTable(refreshHeader);
		updateButtonBar();
	}
	
	@Override
	public void setDataTableModel(ListModel<T> model) {
		super.setDataTableModel(model);
		updateButtonBar();
	}
	
	@Override
	protected void updateButtonBar() {
		super.updateButtonStatusBar(new String[] { ButtonBarModelLoader.EMPTY_VALUE });
	}
}
