/**
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang3.StringUtils;
import org.jumin.common.ehcache.CacheManagerFactory;
import org.jumin.common.ehcache.CacheOperationHelper;
import org.jumin.common.utils.ResourceLocatorUtil;
import org.jumin.common.xsutils.JrxXmlModelUtil;
import org.jumin.common.xsutils.XsdToXmlUtil;
import org.jumin.common.xsutils.model.JrxChoiceGroup;
import org.jumin.common.xsutils.model.JrxDeclaration;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxElementGroup;
import org.jumin.common.xsutils.model.JrxGroup;
import org.jumin.common.xsutils.model.JrxTerm;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSTerm;
import com.sun.xml.xsom.XSType;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

public class XmlTreeTagLabelUtil implements Serializable {

    private static final long serialVersionUID = -6936027957901090542L;

    private static Logger log = LoggerFactory.getLogger(XmlTreeTagLabelUtil.class);

    public static final Object[][] SCHEMA_NAMESPACE_PREFIXES = new Object[][] {
// @formatter:off
		{"urn:swift:xsd:", 4}
		// @formatter:on
    };

    public static final String NAMESPACE_LABELS_CACHE_NAME = "jrx-msg-namespace-labels-cache";

    public static final String LABELS_RESOURCE_FILE_SUFFIX = ".properties";

    public static final String ELEMENT_PREFIX = "element.";

    public static final String TYPE_PREFIX = "type.";

    public static final String CHOICE_ITEM_SEPARATOR = "or";

    public static final String LABEL_SEGMENT_SEPARATOR = ".";

    public static final String TAG_LABEL_PREFIX = "tag" + LABEL_SEGMENT_SEPARATOR;

    private Cache namespaceLabelsCache = null;

    private static ConcurrentMap<String, String> labelsResourcePathMap = new ConcurrentHashMap<String, String>();

    /**
     * Get tag label for a term.<br>
     * 
     * @param jrxTerm
     *            term to be resolved.
     * @return string value for tag label.
     */
    public String getTagLabel(JrxTerm<?> jrxTerm) {
        if (jrxTerm instanceof JrxElementGroup) {
            JrxElementGroup jrxElementGroup = (JrxElementGroup) jrxTerm;
            if (jrxElementGroup.getParentBlock() != null) {
                XSParticle[] xsChoiceParticleArray = XsdToXmlUtil.getInstance().getChoiceElementParticle(
                        jrxElementGroup.getXsdDeclaration());

                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < xsChoiceParticleArray.length; i++) {
                    XSParticle xsChoiceParticle = xsChoiceParticleArray[i];
                    if (i > 0) {
                        if (!CHOICE_ITEM_SEPARATOR.startsWith(" "))
                            sb.append(" ");
                        sb.append(CHOICE_ITEM_SEPARATOR.trim());
                        if (!CHOICE_ITEM_SEPARATOR.endsWith(" "))
                            sb.append(" ");
                    }
                    XSTerm xsChoiceTerm = xsChoiceParticle.getTerm();
                    String choiceLabel = xsChoiceTerm.asElementDecl().getName();
                    if (jrxElementGroup instanceof JrxChoiceGroup) {
                        choiceLabel = getChoiceSelectionTagLabel((JrxChoiceGroup) jrxElementGroup,
                                xsChoiceTerm.asElementDecl());
                    }
                    sb.append(choiceLabel);
                }

                return sb.length() > 0 ? sb.toString() : jrxElementGroup.getCompositor().name();
            }
            return getTagLabel(jrxElementGroup.getOwner().getName(), jrxElementGroup.getOwner().getName());
        } else if (jrxTerm instanceof JrxGroup) {
            String groupName = ((JrxGroup) jrxTerm).getName();
            return getTagLabel(groupName, groupName);
        }

        JrxElement jrxElement = (JrxElement) jrxTerm;

        // produce full-scope name from the 0-3 parents
        String fullScopedName = "";
        int i = 0;
        int maxLoop = 2;
        JrxElement jrxCurrentPointer = jrxElement;
        while (true) {
            if (i >= maxLoop || jrxCurrentPointer == null) {
                break;
            }

            if (fullScopedName.equals("")) {
                fullScopedName = jrxCurrentPointer.getSimpleName();
            } else {
                fullScopedName = jrxCurrentPointer.getSimpleName() + LABEL_SEGMENT_SEPARATOR + fullScopedName;
            }

            if (jrxCurrentPointer.getParentBlock() == null) {
                break;
            }

            JrxElement jrxEffectiveParentElement = JrxXmlModelUtil.getParentElement(jrxCurrentPointer.getParentBlock());

            if (jrxEffectiveParentElement.getNamespaceUri() == null || jrxCurrentPointer.getNamespaceUri() == null) {
                break;
            }

            if (!jrxEffectiveParentElement.getNamespaceUri().equals(jrxCurrentPointer.getNamespaceUri())) {
                break;
            }

            jrxCurrentPointer = jrxEffectiveParentElement;

            i++;
        }

        String label = getTagLabel(jrxElement.getNamespaceUri(), ELEMENT_PREFIX + fullScopedName, "");

        if (label.equals("")) {
            // try retrieve from it's complex type if any
            if (jrxElement.getXsdDeclaration() != null) {
                XSType xsType = jrxElement.getXsdDeclaration().getType();
                if (xsType != null) {
                    label = getTagLabel(jrxElement.getNamespaceUri(), TYPE_PREFIX + xsType.getName(), "");
                }
            } else {
                // Try with the XML node name
                String possibleTypeName = jrxElement.getName().substring(0, 1).toUpperCase()
                        + jrxElement.getName().substring(1);
                label = getTagLabel(jrxElement.getNamespaceUri(), TYPE_PREFIX + possibleTypeName, "");
            }
        }

        if (label.equals("")) {
            label = getTagLabel(jrxElement.getNamespaceUri(), ELEMENT_PREFIX + jrxElement.getSimpleName(), "");
        }

        if (label.equals("")) {
            // just use the XML tag name
            label = jrxElement.getSimpleName();
        }

        return label;
    }

    public String getChoiceSelectionTagLabel(JrxChoiceGroup jrxChoiceGroup, XSElementDecl selectionXsElement) {
        String selectionTagName = selectionXsElement.getName();
        String label = null;
        if (jrxChoiceGroup != null && selectionTagName != null) {
            JrxDeclaration<?> jrxParentDeclaration = JrxXmlModelUtil.getParentDeclaration(jrxChoiceGroup);
            String targetNamespaceUri = selectionXsElement.getTargetNamespace();
            if (jrxParentDeclaration != null) {
                // Try with element scoped name
                label = getTagLabel(
                        targetNamespaceUri,
                        ELEMENT_PREFIX
                                + (jrxParentDeclaration instanceof JrxElement ? ((JrxElement) jrxParentDeclaration)
                                        .getSimpleName() : jrxParentDeclaration.getName()) + LABEL_SEGMENT_SEPARATOR
                                + selectionTagName, "");
            }

            if (label.equals("") && selectionXsElement.getType() != null) {
                // Try with data type
                label = getTagLabel(targetNamespaceUri, TYPE_PREFIX + selectionXsElement.getType().getName(), "");
            }

            if (label.equals("")) {
                // Try with simple element name
                label = getTagLabel(targetNamespaceUri, ELEMENT_PREFIX + selectionTagName, "");
            }

            if (label.equals(""))
                label = selectionTagName;
        }

        return label;
    }

    /**
     * Get tag label from the web resource bundle. <br>
     * 
     * @param key
     *            key to be used for searching the resource bundle.
     * @param defaultValue
     *            value to return when there is nothing found.
     * @return string value for tag label.
     */
    public String getTagLabel(String key, String defaultValue) {
        String effectiveLabel = "";
        String searchKey = key;
        while (true) {
            effectiveLabel = ZkLabels.getLabel(TAG_LABEL_PREFIX + searchKey, "");

            if (!effectiveLabel.equals("")) {
                // tab label found in WEB-INF/i3-label[LANG].properties
                break;
            }

            if (searchKey.contains(LABEL_SEGMENT_SEPARATOR)) {
                searchKey = searchKey.substring(searchKey.indexOf(LABEL_SEGMENT_SEPARATOR) + 1);
            } else {
                effectiveLabel = ZkLabels.getLabel(searchKey, defaultValue);
                break;
            }
        }

        return effectiveLabel;
    }

    /**
     * Get tag label of the element within the catalog according to the given namespace URI. <br>
     * 
     * @param namespaceUri
     *            identity of label catalog shall be use for search.
     * @param scopedElementName
     *            scoped name of the tag name.
     * @param defaultLabel
     *            default label to be used when there is nothing found.
     * @return string value for tag label.
     */
    public String getTagLabel(String namespaceUri, String scopedElementName, String defaultLabel) {
        if (namespaceUri == null || namespaceUri.equals("") || scopedElementName == null
                || scopedElementName.equals(""))
            return defaultLabel;

        Map<String, String> labelsCatalog = CacheOperationHelper.retrieveCacheValue(getNamespaceLabelsCache(),
                namespaceUri);

        if (labelsCatalog == null) {
            // load catalog
            labelsCatalog = loadCatalogByNamespaceUri(namespaceUri);
        }

        if (labelsCatalog == null) {
            log.warn("Resource for namespaceUri: '" + namespaceUri + "' is not available or provided.");
            return defaultLabel;
        }

        String foundLabel = labelsCatalog.get(scopedElementName);
        if (foundLabel == null)
            return defaultLabel;

        return foundLabel;
    }

    private Map<String, String> loadCatalogByNamespaceUri(String namespaceUri) {
        String prefix = "";
        int prefixLength = 0;
        for (int i = 0; i < SCHEMA_NAMESPACE_PREFIXES.length; i++) {
            if (namespaceUri.startsWith((String) SCHEMA_NAMESPACE_PREFIXES[i][0])) {
                prefix = (String) SCHEMA_NAMESPACE_PREFIXES[i][0];
                prefixLength = (Integer) SCHEMA_NAMESPACE_PREFIXES[i][1];
                break;
            }
        }

        if (prefix.equals("")) {
            log.warn("Could not match prefix for namespace URI: '" + namespaceUri
                    + "'. Current SCHEMA_NAMESPACE_PREFIX is: " + Arrays.toString(SCHEMA_NAMESPACE_PREFIXES));
            return null;
        }

        String labelsResourceLocation = formulateLabelsResourceLocation(namespaceUri, prefixLength);

        Properties props = new Properties();
        try {
            URL labelsResourceUrl = null;
            if (labelsResourceLocation.startsWith(ResourceLocatorUtil.JAVA_CLASSPATH_PREFIX)) {
                labelsResourceUrl = getClass().getResource(
                        labelsResourceLocation.substring(ResourceLocatorUtil.JAVA_CLASSPATH_PREFIX.length()));
            } else {
                try {
                    labelsResourceUrl = new URL(labelsResourceLocation);
                } catch (Exception e) {
                    labelsResourceUrl = getClass().getResource(labelsResourceLocation);
                }
            }

            InputStream is = null;
            try {
                is = labelsResourceUrl.openStream();
                props.load(is);
            } finally {
                if (is != null) {
                    is.close();
                }
            }

        } catch (Exception e) {
            log.warn("Could not load labels resource for namespace URI: '" + namespaceUri + "' from location: '"
                    + labelsResourceLocation + "'", e);
            return null;
        }

        Map<String, String> labelsCatalog = new Hashtable<String, String>();
        for (Entry<Object, Object> entry : props.entrySet()) {
            labelsCatalog.put((String) entry.getKey(), (String) entry.getValue());
        }
        registerNamespaceLabelCatalog(namespaceUri, labelsCatalog);

        return labelsCatalog;
    }

    private String formulateLabelsResourceLocation(String namespaceUri, int prefixLength) {
        String labelsResourceName = namespaceUri.substring(prefixLength);
        int lastDoubleColonIndex = labelsResourceName.lastIndexOf(":");
        String packageName = labelsResourcePathMap.get(labelsResourceName.substring(0, lastDoubleColonIndex));
        packageName = StringUtils.replace(packageName, "$", "");
        labelsResourceName = StringUtils.replace(
                labelsResourceName.substring(lastDoubleColonIndex + 1).replace(':', '.'), "$", "")
                + LABELS_RESOURCE_FILE_SUFFIX;

        return packageName + "/" + labelsResourceName;
    }

    /**
     * Check the availability of a label catalog for the given namespace URI in the cache. <br>
     * 
     * @param namespaceUri
     * @return <li><b>true</b> if label catalog exists for the provided namespace URI</li><br>
     *         <li><b>false</b> if label catalog does not exist for the provided namespace URI</li>
     */
    public boolean hasNamespaceLabelCatalog(String namespaceUri) {
        return CacheOperationHelper.containsKey(getNamespaceLabelsCache(), namespaceUri);
    }

    /**
     * Register a label catalog to the cache with the given namespace URI as key. <br>
     * 
     * @param namespaceUri
     *            identity of label catalog shall be use for search.
     * @param labelCatalog
     *            label catalog to store.
     */
    public void registerNamespaceLabelCatalog(String namespaceUri, Map<String, String> labelCatalog) {
        CacheOperationHelper.storeToCache(getNamespaceLabelsCache(), namespaceUri, labelCatalog);
    }

    private Cache getNamespaceLabelsCache() {
        if (namespaceLabelsCache != null)
            return namespaceLabelsCache;

        CacheManager defaultCacheManager = CacheManagerFactory.getFmpDefaultCacheManager();
        namespaceLabelsCache = defaultCacheManager.getCache(NAMESPACE_LABELS_CACHE_NAME);

        if (namespaceLabelsCache == null) {
            namespaceLabelsCache = CacheManagerFactory.createSimpleCache(NAMESPACE_LABELS_CACHE_NAME);
            namespaceLabelsCache = (Cache) defaultCacheManager.addCacheIfAbsent(namespaceLabelsCache);
        }

        return namespaceLabelsCache;
    }

    /**
     * @param relevantApplicationNamespaceUri
     * @return
     */
    public static String getLabelsResourcePath(String relevantApplicationNamespaceUri) {
        return labelsResourcePathMap.get(relevantApplicationNamespaceUri);
    }

    /**
     * @param relevantApplicationNamespaceUri
     * @param path
     */
    public static void addLabelsResourcePath(String relevantApplicationNamespaceUri, String path) {
        synchronized (labelsResourcePathMap) {
            labelsResourcePathMap.put(relevantApplicationNamespaceUri, path);
        }
    }

    /**
     * @param relevantApplicationNamespaceUri
     */
    public static void removeLabelsResourcePath(String relevantApplicationNamespaceUri) {
        synchronized (labelsResourcePathMap) {
            labelsResourcePathMap.remove(relevantApplicationNamespaceUri);
        }
    }

}
