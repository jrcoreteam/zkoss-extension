/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;

public class RepetitionRemoveButton extends Button {

	private static final long serialVersionUID = -2272159903605407337L;

    private boolean disabled = false;
	
	private XmlTreerow row;

	public RepetitionRemoveButton(String label, XmlTreerow row) {
		this(label, row, null);
	}
	
	public RepetitionRemoveButton(String label, XmlTreerow row, RepetitionRemoveButtonListener listener) {
		super(label);
		this.row = row;
		initProperties();
		if (listener == null) {
			this.addEventListener(Events.ON_CLICK, new RepetitionRemoveButtonListener(row));
		} else {
			this.addEventListener(Events.ON_CLICK, listener);
		}
	}
	
	private void initProperties() {
		setSclass("jrx-msg-tree-button jrx-msg-tree-remove-button");
	}
	
	public XmlTreerow getRow() {
		return row;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		if (this.disabled) {
			setSclass("jrx-msg-tree-button jrx-msg-tree-remove-button-disabled");
		} else {
			setSclass("jrx-msg-tree-button jrx-msg-tree-remove-button");
		}
	}
}
