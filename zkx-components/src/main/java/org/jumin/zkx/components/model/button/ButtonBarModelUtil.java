/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

import java.util.List;

public class ButtonBarModelUtil {

    public static void setFunctionAssignmentDisabled(StateFunctionAssignment sfa, String functionId, boolean disabled) {
        FunctionAssignment fa = getFunctionAssignment(sfa, functionId);
        if (fa != null) {
            fa.setDisabled(disabled);
        }
    }

    public static void setFunctionAssignmentVisible(StateFunctionAssignment sfa, String functionId, boolean visible) {
        FunctionAssignment fa = getFunctionAssignment(sfa, functionId);
        if (fa != null) {
            fa.setVisible(visible);
        }
    }

    public static FunctionAssignment getFunctionAssignment(StateFunctionAssignment sfa, String functionId) {
        if (sfa == null || functionId == null)
            return null;

        FunctionAssignment fa = null;
        List<AssignmentItem> assignmentItemList = sfa.getCommonActions();
        if (assignmentItemList != null && assignmentItemList.size() > 0) {
            fa = getFunctionAssignment(assignmentItemList, functionId);
            if (fa != null)
                return fa;
        }

        assignmentItemList = sfa.getMoreFunctions();
        if (assignmentItemList != null && assignmentItemList.size() > 0) {
            fa = getFunctionAssignment(assignmentItemList, functionId);
            if (fa != null)
                return fa;
        }

        assignmentItemList = sfa.getSpecialFunctions();
        if (assignmentItemList != null && assignmentItemList.size() > 0) {
            fa = getFunctionAssignment(assignmentItemList, functionId);
            if (fa != null)
                return fa;
        }

        return null;
    }

    private static FunctionAssignment getFunctionAssignment(List<AssignmentItem> assignmentItemList,
            String functionId) {
        for (AssignmentItem ai : assignmentItemList) {
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment fa = (FunctionAssignment) ai;
                if (functionId.equals(fa.getFunction().getId())) {
                    return fa;
                }
            }
        }

        return null;
    }

    public static final String toString(StateFunctionAssignment sfa) {
        StringBuffer sb = new StringBuffer();

        if (sfa == null)
            return null;

        sb.append("Common Actions:\n");
        for (AssignmentItem ai : sfa.getCommonActions()) {
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment fa = (FunctionAssignment) ai;
                sb.append("\t").append(fa.getFunction().getId()).append(" {visible:'").append(fa.isVisible())
                        .append("'; disabled:'").append(fa.isDisabled()).append("'}\n");
            }
        }

        sb.append("Special Functions:\n");
        for (AssignmentItem ai : sfa.getSpecialFunctions()) {
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment fa = (FunctionAssignment) ai;
                sb.append("\t").append(fa.getFunction().getId()).append(" {visible:'").append(fa.isVisible())
                        .append("'; disabled:'").append(fa.isDisabled()).append("'}\n");
            }
        }

        sb.append("More Functions:\n");
        for (AssignmentItem ai : sfa.getMoreFunctions()) {
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment fa = (FunctionAssignment) ai;
                sb.append("\t").append(fa.getFunction().getId()).append(" {visible:'").append(fa.isVisible())
                        .append("'; disabled:'").append(fa.isDisabled()).append("'}\n");
            }
        }

        return sb.toString();
    }
}
