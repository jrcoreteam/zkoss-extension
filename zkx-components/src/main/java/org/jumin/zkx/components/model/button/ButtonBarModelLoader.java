/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jumin.common.xmlutils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ButtonBarModelLoader {

    private static final Logger log = LoggerFactory.getLogger(ButtonBarModelLoader.class);

    public static final String NAMESPACE_FUNCTION_CATALOG = "http://www.jumin.org/common/function/catalog";
    public static final String NAMESPACE_FUNCTION_ASSIGNMENT = "http://www.jumin.org/common/function/assignment";

    public static final String STATE_ACCESS_SEPARATOR = "|";
    public static final String EMPTY_VALUE = "-";
    public static final String WILDCARD_VALUE = "*";

    // Function Catalog
    public static final String TAG_FUNCTION_CATALOG = "functionCatalog";

    public static final String ATTRIBUTE_DEFAULT_LABEL = "defaultLabel";
    public static final String ATTRIBUTE_EVENT_NAME = "eventName";
    public static final String ATTRIBUTE_IMAGE = "image";
    public static final String ATTRIBUTE_LABEL_KEY = "labelKey";

    // User Function Model
    public static final String TAG_BUTTON_STATUS_MODEL = "buttonStatus";
    public static final String TAG_STATE_FUNCTION_ASSIGNMENT = "stateFunctionAssignment";
    public static final String TAG_MORE_FUNCTIONS = "moreFunctions";
    public static final String TAG_SPECIAL_FUNCTIONS = "specialFunctions";
    public static final String TAG_COMMON_ACTIONS = "commonActions";
    public static final String TAG_SEPARATOR = "separator";
    public static final String TAG_STATUS = "status";

    public static final String ATTRIBUTE_ACCESS = "access";
    public static final String ATTRIBUTE_ACTION = "action";
    public static final String ATTRIBUTE_CHECK_LEVEL = "checkLevel";
    public static final String ATTRIBUTE_CONTEXT = "context";
    public static final String ATTRIBUTE_DISABLED = "disabled";
    public static final String ATTRIBUTE_PATTERN = "pattern";
    public static final String ATTRIBUTE_VISIBLE = "visible";

    // Common
    public static final String TAG_FUNCTION = "function";
    public static final String ATTRIBUTE_ID = "id";
    public static final String ATTRIBUTE_IDREF = "idref";

    public Map<String, UserFunction> loadFunctionModel(String xmlString) throws Exception {
        Document xmlDoc = XmlUtils.createDocument(xmlString);
        return loadFunctionModel(xmlDoc);
    }

    public Map<String, UserFunction> loadFunctionModel(InputStream xmlStream) throws Exception {
        Document xmlDoc = XmlUtils.createDocument(xmlStream);
        return loadFunctionModel(xmlDoc);
    }

    public Map<String, UserFunction> loadFunctionModel(Document xmlDoc) throws Exception {
        Map<String, UserFunction> functionCatalog = new HashMap<String, UserFunction>();
        NodeList xmlFunctionNodeList = XmlUtils.getNodeListByXPath(xmlDoc,
                "/" + TAG_FUNCTION_CATALOG + "/" + TAG_FUNCTION);

        for (int i = 0; i < xmlFunctionNodeList.getLength(); i++) {
            UserFunction userFunction = xmlToUserFunction((Element) xmlFunctionNodeList.item(i));
            if (userFunction != null) {
                functionCatalog.put(userFunction.getId(), userFunction);
            }
        }

        return functionCatalog;
    }

    private UserFunction xmlToUserFunction(Element xmlFunctionNode) {
        if (!xmlFunctionNode.getTagName().equals(TAG_FUNCTION)) {
            log.warn("Invalid tag for function definition. Provided tag name is: " + xmlFunctionNode.getTagName());
            return null;
        }

        UserFunction userFunction = new UserFunction();
        userFunction.setId(xmlFunctionNode.getAttribute(ATTRIBUTE_ID));
        userFunction.setAction(xmlFunctionNode.getAttribute(ATTRIBUTE_ACTION));
        userFunction.setLabelKey(xmlFunctionNode.getAttribute(ATTRIBUTE_LABEL_KEY));
        userFunction.setDefaultLabel(xmlFunctionNode.getAttribute(ATTRIBUTE_DEFAULT_LABEL));
        userFunction.setEventName(xmlFunctionNode.getAttribute(ATTRIBUTE_EVENT_NAME));
        userFunction.setCheckLevel(CheckLevel.fromString(xmlFunctionNode.getAttribute(ATTRIBUTE_CHECK_LEVEL)));
        userFunction.setImage(xmlFunctionNode.getAttribute(ATTRIBUTE_IMAGE));

        return userFunction;
    }

    public ButtonBarModel loadButtonModel(Map<String, UserFunction> functionCatalog, InputStream xmlStream)
            throws Exception {
        Document xmlDoc = XmlUtils.createDocument(xmlStream);
        return loadButtonModel(functionCatalog, xmlDoc);
    }

    public ButtonBarModel loadButtonModel(Map<String, UserFunction> functionCatalog, Document xmlDoc) throws Exception {

        ButtonBarModel buttonModel = new ButtonBarModel();
        buttonModel.setFunctionCatalog(functionCatalog);

        Element xmlStateFunctionCatalogNode = xmlDoc.getDocumentElement();
        buttonModel.setContext(xmlStateFunctionCatalogNode.getAttribute(ATTRIBUTE_CONTEXT));

        NodeList xmlStateFunctionAssignmentList = XmlUtils.getNodeListByXPath(xmlDoc,
                "/" + TAG_BUTTON_STATUS_MODEL + "/" + TAG_STATE_FUNCTION_ASSIGNMENT);
        for (int i = 0; i < xmlStateFunctionAssignmentList.getLength(); i++) {
            Element xmlStateFunctionNode = (Element) xmlStateFunctionAssignmentList.item(i);
            // Check if the action is authorized
            StateFunctionAssignment assignment = xmlToStateFunctionAssignment(functionCatalog, xmlStateFunctionNode);
            buttonModel.getStateFunctionCatalog().put(
                    (assignment.getSourceAccess() == null ? EMPTY_VALUE : assignment.getSourceAccess().toCode()),
                    assignment);
        }

        return buttonModel;
    }

    private StateFunctionAssignment xmlToStateFunctionAssignment(Map<String, UserFunction> functionCatalog,
            Element xmlStateFunctionNode) throws Exception {
        if (!xmlStateFunctionNode.getTagName().equals(TAG_STATE_FUNCTION_ASSIGNMENT)) {
            log.warn("Invalid tag for state function assignment definition. Provided tag name is: "
                    + xmlStateFunctionNode.getTagName());
            return null;
        }

        StateFunctionAssignment assignment = new StateFunctionAssignment();
        // assignment.setStatus(xmlStateFunctionNode.getAttribute(ATTRIBUTE_STATE));
        assignment.setSourceAccess(SourceAccess.fromCode(xmlStateFunctionNode.getAttribute(ATTRIBUTE_ACCESS)));

        NodeList xmlItemNodeList = XmlUtils.getNodeListByXPath(xmlStateFunctionNode, "./" + TAG_MORE_FUNCTIONS + "/*");
        assignment.getMoreFunctions().addAll(loadAssignmentItemList(functionCatalog, xmlItemNodeList, assignment));

        xmlItemNodeList = XmlUtils.getNodeListByXPath(xmlStateFunctionNode, "./" + TAG_COMMON_ACTIONS + "/*");
        assignment.getCommonActions().addAll(loadAssignmentItemList(functionCatalog, xmlItemNodeList, assignment));

        xmlItemNodeList = XmlUtils.getNodeListByXPath(xmlStateFunctionNode,
                "./" + TAG_SPECIAL_FUNCTIONS + "/" + TAG_FUNCTION);
        for (int i = 0; i < xmlItemNodeList.getLength(); i++) {
            Element xmlSpecialFunctionNode = (Element) xmlItemNodeList.item(i);
            PatternFunctionAssignment functionAssignment = xmlToPatternFunctionAssignment(functionCatalog,
                    xmlSpecialFunctionNode, assignment);
            if (functionAssignment != null)
                assignment.getSpecialFunctions().add(functionAssignment);
        }

        return assignment;
    }

    private List<AssignmentItem> loadAssignmentItemList(Map<String, UserFunction> functionCatalog,
            NodeList xmlAssignmentItemNodeList, StateFunctionAssignment assignment) throws Exception {
        List<AssignmentItem> result = new ArrayList<AssignmentItem>();
        for (int i = 0; i < xmlAssignmentItemNodeList.getLength(); i++) {
            Element xmlAssignmentItemNode = (Element) xmlAssignmentItemNodeList.item(i);
            if (xmlAssignmentItemNode.getTagName().equals(TAG_FUNCTION)) {
                PatternFunctionAssignment functionAssignment = xmlToPatternFunctionAssignment(functionCatalog,
                        xmlAssignmentItemNode, assignment);
                if (functionAssignment != null) {
                    result.add(functionAssignment);
                }
            } else if (xmlAssignmentItemNode.getTagName().equals(TAG_SEPARATOR)) {
                result.add(new FunctionSeparator());
            } else {
                log.warn("Invalid tag for assignment item definition. Provided tag name is: "
                        + xmlAssignmentItemNode.getTagName());
            }
        }
        return result;
    }

    private String[] xmlPatternToString(Element functionNode) throws Exception {

        NodeList xmlStatusNodeList = XmlUtils.getNodeListByXPath(functionNode, "./" + TAG_STATUS);
        ArrayList<String> al = new ArrayList<String>();
        for (int i = 0; i < xmlStatusNodeList.getLength(); i++) {
            Element xmlStatusNode = (Element) xmlStatusNodeList.item(i);
            al.add(xmlStatusNode.getAttribute(ATTRIBUTE_PATTERN));
        }
        return al.toArray(new String[0]);
    }

    private boolean getBooleanAttributeValue(Element xmlElement, String attributeName, boolean defaultValue) {
        String booleanString = xmlElement.getAttribute(attributeName);
        return booleanString.equals("") ? defaultValue : Boolean.parseBoolean(booleanString);
    }

    public StateFunctionAssignment findMatchingStateFunctionAssignment(ButtonBarModel model, String state,
            SourceAccess sourceAccess) {
        if (sourceAccess == null)
            return null;

        Map<String, StateFunctionAssignment> subCatalog = model.getAccessIndexedCatalog().get(sourceAccess.toCode());
        if (subCatalog == null)
            return null;
        String assignmentKey = state;

        if (assignmentKey == null)
            assignmentKey = ButtonBarModelLoader.EMPTY_VALUE;

        StateFunctionAssignment sfa = subCatalog.get(assignmentKey);

        if (sfa == null) {
            assignmentKey = state + "/" + ButtonBarModelLoader.EMPTY_VALUE;
            sfa = subCatalog.get(assignmentKey);
        }

        if (sfa == null) {
            assignmentKey = state + "/" + ButtonBarModelLoader.WILDCARD_VALUE;
            sfa = subCatalog.get(assignmentKey);
        }

        if (sfa == null) {
            assignmentKey = ButtonBarModelLoader.WILDCARD_VALUE + "/"
                    + ButtonBarModelLoader.WILDCARD_VALUE;
            sfa = subCatalog.get(assignmentKey);
        }

        if (sfa == null) {
            assignmentKey = ButtonBarModelLoader.WILDCARD_VALUE;
            sfa = subCatalog.get(assignmentKey);
        }

        return sfa;
    }

    private void processCheckLevelAssignment(Element xmlAssignmentItemNode,
            PatternFunctionAssignment functionAssignment, StateFunctionAssignment stateFunctionAssignment) {
        String checkLevelString = xmlAssignmentItemNode.getAttribute(ATTRIBUTE_CHECK_LEVEL);
        if (!checkLevelString.equals("")) {
            functionAssignment.setCheckLevel(CheckLevel.fromString(checkLevelString));
        } else {
            functionAssignment.setCheckLevel(functionAssignment.getFunction().getCheckLevel());
        }
        if (!stateFunctionAssignment.getCheckLevelAggregation().contains(functionAssignment.getCheckLevel())) {
            stateFunctionAssignment.getCheckLevelAggregation().add(functionAssignment.getCheckLevel());
        }

    }

    private PatternFunctionAssignment xmlToPatternFunctionAssignment(Map<String, UserFunction> functionCatalog,
            Element xmlAssignmentItemNode, StateFunctionAssignment assignment) throws Exception {
        if (!xmlAssignmentItemNode.getTagName().equals(TAG_FUNCTION))
            return null;

        String functionId = xmlAssignmentItemNode.getAttribute(ATTRIBUTE_IDREF);
        UserFunction function = functionCatalog.get(functionId);
        if (function != null) {
            PatternFunctionAssignment functionAssignment = new PatternFunctionAssignment(function);
            functionAssignment.setPatterns(xmlPatternToString(xmlAssignmentItemNode));
            functionAssignment.setVisible(getBooleanAttributeValue(xmlAssignmentItemNode, ATTRIBUTE_VISIBLE, true));
            functionAssignment.setDisabled(getBooleanAttributeValue(xmlAssignmentItemNode, ATTRIBUTE_DISABLED, false));
            functionAssignment.setLabelKey(xmlAssignmentItemNode.getAttribute(ATTRIBUTE_LABEL_KEY));
            functionAssignment.setDefaultLabel(xmlAssignmentItemNode.getAttribute(ATTRIBUTE_DEFAULT_LABEL));
            functionAssignment.setImage(xmlAssignmentItemNode.getAttribute(ATTRIBUTE_IMAGE));
            processCheckLevelAssignment(xmlAssignmentItemNode, functionAssignment, assignment);
            return functionAssignment;
        } else {
            log.warn("Invalid function id: '" + functionId + "'");
        }
        return null;
    }
}
