/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.constraint;

import java.util.regex.Pattern;

import org.jumin.common.xsutils.XmlValidationMessage;
import org.jumin.zkx.components.constraint.ContraintValidationMessage;
import org.jumin.zkx.zkxutils.tree.DefaultInputFunction;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;

public class TreeSyntaxPatternConstraint extends AbstractTreeSyntaxValidationConstraint {

	private static final long serialVersionUID = -8904097843087696111L;
	
    private String regex;
	private Pattern pattern;

	public TreeSyntaxPatternConstraint(String regex) {
		super();
		this.regex = regex;
		this.pattern = Pattern.compile(regex);
	}

	@Override
	public void validate(Component comp, Object value) throws WrongValueException {
		String strValue = (String) value;

		if (strValue != null && strValue.length() > 0) {
			if (!DefaultInputFunction.isFunction(strValue)) {
				if (pattern != null) {
					if (!pattern.matcher(strValue).matches()) {
						showSyntaxValidationError(comp, new ContraintValidationMessage(
						        XmlValidationMessage.ERROR_INVALID_CONTENT_PATTERN, new Object[] { value, regex }));
						return;
					}
				}
			}
		}

		clearSyntaxValidationError(comp);
	}
}
