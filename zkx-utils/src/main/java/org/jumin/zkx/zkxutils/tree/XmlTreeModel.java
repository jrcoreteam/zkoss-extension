/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import java.util.ArrayList;
import java.util.List;

import org.jumin.common.xsutils.model.JrxChoiceGroup;
import org.jumin.common.xsutils.model.JrxDocument;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxElementGroup;
import org.jumin.common.xsutils.model.JrxGroup;
import org.jumin.common.xsutils.model.JrxTerm;
import org.zkoss.zul.AbstractTreeModel;

import com.sun.xml.xsom.XSModelGroup;

public class XmlTreeModel extends AbstractTreeModel<Object> {

    private static final long serialVersionUID = 2086727717531160264L;

    private List<JrxElement> jrxTreeRootElementList = null;

    public static final String DEFAULT_DOCUMENT_TAG = "Document";

    public XmlTreeModel(JrxDocument JrxDocument) {
        super(JrxDocument);
    }

    public XmlTreeModel(JrxDocument JrxDocument, List<JrxElement> jrxTreeRootElementList) {
        super(JrxDocument);
        this.jrxTreeRootElementList = jrxTreeRootElementList;
    }

    public JrxDocument getJrxDocument() {
        return (JrxDocument) getRoot();
    }

    @Override
    public boolean isLeaf(Object node) {
        if (node == null)
            return true;

        if (node instanceof JrxDocument)
            return false;

        if (node instanceof JrxGroup)
            return false;

        if (node instanceof JrxChoiceGroup) {
            JrxChoiceGroup jrxChoiceGroup = (JrxChoiceGroup) node;
            return getChildCount(jrxChoiceGroup) < 1;
        }

        if (node instanceof JrxElementGroup) {
            JrxElementGroup jrxElementGroup = (JrxElementGroup) node;
            return jrxElementGroup.getElements().size() == 0;
        }

        JrxElement jrxElement = (JrxElement) node;
        if (jrxElement.getChildrenBlock() == null) {
            return true;
        }

        if (jrxElement.getScopedSingleChoiceGroup() != null) {
            // Choice Element -> DIRECT / INDIRECT
            if (jrxElement.getScopedSingleChoiceGroup().getSelection() == null) {
                return true; // Let selection take place before creating sub
            } else {
                return false;
            }
        }

        if (jrxElement.getChildrenBlock().getElements().size() == 1) {
            // Node with single child

            // Check the child count from schema
            if (jrxElement.getChildrenBlock().getXsdDeclaration() != null) {
                XSModelGroup xsModelGroup = jrxElement.getChildrenBlock().getXsdDeclaration();
                if (xsModelGroup.getChildren().length > 1
                        || (xsModelGroup.getChildren().length == 1 && xsModelGroup.getChildren()[0].isRepeated()))
                    return false;
            }

            JrxTerm<?> jrxTerm = jrxElement.getChildrenBlock().getElements().get(0);
            if (jrxTerm instanceof JrxElementGroup && ((JrxElementGroup) jrxTerm).getElements().size() == 0) {
                // It happens to be without sub-element(s) or the sub-element(s) is still pending because of choice
                // group defined.
                return true;
            }

            if (jrxTerm instanceof JrxElement && ((JrxElement) jrxTerm).isLeaf() && !jrxTerm.isRepetitive()) {
                return true;
            }
        }

        return jrxElement.getChildrenBlock().getElements() == null ? true : jrxElement.getChildrenBlock().getElements()
                .size() < 1;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof JrxDocument) {
            JrxDocument jrxDocument = (JrxDocument) parent;
            if (jrxTreeRootElementList == null || jrxTreeRootElementList.size() == 0) {
                return jrxDocument.getRootElement();
            }

            return jrxTreeRootElementList.get(index);
        }

        if (parent instanceof JrxChoiceGroup) {
            return getChild((JrxChoiceGroup) parent, index);
        }

        if (parent instanceof JrxGroup) {
            return getChild((JrxGroup) parent, index);
        }

        if (parent instanceof JrxElementGroup) {
            return ((JrxElementGroup) parent).getElements().get(index);
        }

        if (parent instanceof JrxElement) {
            JrxElement jrxElement = (JrxElement) parent;
            if (jrxElement.getChildrenBlock() == null) {
                return null;
            }

            JrxChoiceGroup scopedSingleChoiceGroup = jrxElement.getScopedSingleChoiceGroup();
            if (scopedSingleChoiceGroup != null) {
                // Choice Element -> DIRECT / INDIRECT
                return getChild(scopedSingleChoiceGroup, index);
            }

            if (jrxElement.getChildrenBlock().getElements().size() == 1) {
                JrxTerm<?> jrxTerm = jrxElement.getChildrenBlock().getElements().get(0);
                if (jrxTerm instanceof JrxChoiceGroup) {
                    return getChild((JrxChoiceGroup) jrxTerm, index);
                }
            }

            // Simplified Document element
            if (DEFAULT_DOCUMENT_TAG.equals(jrxElement.getSimpleName())) {
                JrxElement jrxDocumentRootElement = (JrxElement) jrxElement.getChildrenBlock().getElements().get(0);
                if (jrxDocumentRootElement != null && jrxDocumentRootElement.getChildrenBlock() != null) {
                    return jrxDocumentRootElement.getChildrenBlock().getElements().get(index);
                }
            }

            int actualIndex = index;

            List<JrxTerm<?>> jrxEffectiveChildren = jrxElement.getChildrenBlock().getElements();

            // Let see how much node we need to skip/ignore
            int ignoreChild = 0;
            int realIndex = -1;
            for (int i = 0; i < jrxEffectiveChildren.size(); i++) {
                if (jrxEffectiveChildren.get(i).getMaxOccurs() == 0) {
                    ignoreChild++;
                } else {
                    realIndex++;
                    if (realIndex == actualIndex) {
                        // we have found the node
                        break;
                    }
                }
            }

            actualIndex += ignoreChild;

            // Resolve nested element group (non-choice only).
            jrxEffectiveChildren = resolveNestedElementGroup(jrxEffectiveChildren);

            JrxTerm<?> jrxChildTerm = jrxEffectiveChildren.get(actualIndex);
            if (jrxTreeRootElementList != null) {
                while (jrxTreeRootElementList.contains(jrxChildTerm)) {
                    jrxChildTerm = jrxEffectiveChildren.get(actualIndex++);
                }
            }
            return jrxChildTerm;
        }

        return null;
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent instanceof JrxDocument) {
            if (jrxTreeRootElementList == null || jrxTreeRootElementList.size() == 0) {
                return 1;
            }
            // Return #root according to the configuration
            return jrxTreeRootElementList.size();
        }

        if (parent instanceof JrxElement) {
            JrxElement jrxElement = (JrxElement) parent;
            if (jrxElement.getChildrenBlock() == null) {
                return 0;
            }

            JrxChoiceGroup scopedSingleChoiceGroup = jrxElement.getScopedSingleChoiceGroup();
            if (scopedSingleChoiceGroup != null) {
                // Choice Element -> DIRECT / INDIRECT
                return getChildCount(scopedSingleChoiceGroup);
            }

            if (jrxElement.getChildrenBlock().getElements().size() == 1) {
                JrxTerm<?> jrxTerm = jrxElement.getChildrenBlock().getElements().get(0);
                if (jrxTerm instanceof JrxChoiceGroup) {
                    return getChildCount((JrxChoiceGroup) jrxTerm);
                }

                if (jrxTerm instanceof JrxElement && ((JrxElement) jrxTerm).isLeaf()) {
                    // Check the child count from schema
                    if (jrxElement.getChildrenBlock().getXsdDeclaration() != null) {
                        XSModelGroup xsModelGroup = jrxElement.getChildrenBlock().getXsdDeclaration();
                        if (xsModelGroup.getChildren().length > 1
                                || (xsModelGroup.getChildren().length == 1 && xsModelGroup.getChildren()[0]
                                        .isRepeated()))
                            return 1;
                    }

                    return 0;
                }
            }

            // Simplified Document element
            if (jrxElement.getSimpleName().equals(DEFAULT_DOCUMENT_TAG)) {
                JrxElement jrxDocumentRootElement = (JrxElement) jrxElement.getChildrenBlock().getElements().get(0);
                if (jrxDocumentRootElement != null && jrxDocumentRootElement.getChildrenBlock() != null) {
                    return jrxDocumentRootElement.getChildrenBlock().getElements().size();
                }
            }

            // Reduce the count with the number of root element found in the
            // children!
            List<JrxTerm<?>> children = jrxElement.getChildrenBlock().getElements();

            // Resolve nested element group (non-choice only).
            children = resolveNestedElementGroup(children);

            int containedRoot = 0;
            if (jrxTreeRootElementList != null) {
                for (JrxElement jrxRootElement : jrxTreeRootElementList) {
                    if (children.contains(jrxRootElement)) {
                        containedRoot++;
                    }
                }
            }
            int ignoreChild = 0;
            for (JrxTerm<?> t : children) {
                if (t.getMaxOccurs() == 0) {
                    ignoreChild++;
                }
            }

            return children.size() - containedRoot - ignoreChild;
        }

        if (parent instanceof JrxChoiceGroup) {
            return getChildCount((JrxChoiceGroup) parent);
        }

        if (parent instanceof JrxGroup) {
            return ((JrxGroup) parent).getChildrenBlock().getElements().size();
        }

        if (parent instanceof JrxElementGroup) {
            return ((JrxElementGroup) parent).getElements().size();
        }

        return 0;
    }

    private Object getChild(JrxChoiceGroup jrxChoiceGroup, int index) {
        JrxTerm<?> jrxChoiceTerm = jrxChoiceGroup.getSelection();
        if (jrxChoiceTerm == null) {
            return null;
        }

        if (isLeaf(jrxChoiceTerm)) {
            // Selection is a leaf
            return jrxChoiceTerm;
        }

        // Return child from selection
        return getChild(jrxChoiceTerm, index);
    }

    private Object getChild(JrxGroup jrxGroup, int index) {
        return jrxGroup.getChildrenBlock().getElements().get(index);
    }

    private int getChildCount(JrxChoiceGroup jrxChoiceGroup) {
        JrxTerm<?> jrxChoiceTerm = jrxChoiceGroup.getSelection();
        if (jrxChoiceTerm == null) {
            return 0;
        }

        if (isLeaf(jrxChoiceTerm)) {
            // Selection is a leaf
            return 1;
        }

        // Selection has more children, return the children size of the selection
        return getChildCount(jrxChoiceTerm);
    }

    public void insert(JrxElement parent, int index, JrxTerm<?> newTerm) throws IndexOutOfBoundsException {
        JrxElement stn = parent;
        try {
            stn.getChildrenBlock().getElements().add(index, newTerm);
        } catch (Exception exp) {
            throw new IndexOutOfBoundsException("Out of bound: " + index + " while size="
                    + stn.getChildrenBlock().getElements().size());
        }
    }

    protected List<JrxTerm<?>> resolveNestedElementGroup(List<JrxTerm<?>> jrxTermList) {
        if (jrxTermList == null || jrxTermList.isEmpty()) {
            return new ArrayList<JrxTerm<?>>();
        }

        List<JrxTerm<?>> resolvedJrxTermList = new ArrayList<JrxTerm<?>>();
        for (JrxTerm<?> jrxChildTerm : jrxTermList) {
            if (jrxChildTerm instanceof JrxElementGroup) {
                JrxElementGroup jrxChildElementGroup = (JrxElementGroup) jrxChildTerm;
                if (jrxChildElementGroup.isChoiceGroup()) {
                    resolvedJrxTermList.add(jrxChildTerm);
                    continue;
                }

                // Nested element group
                resolvedJrxTermList.addAll(resolveNestedElementGroup(jrxChildElementGroup.getElements()));
            } else {
                resolvedJrxTermList.add(jrxChildTerm);
            }
        }
        return resolvedJrxTermList;
    }

}
