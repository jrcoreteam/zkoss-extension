/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.constraint;

import org.jumin.common.xsutils.XmlValidationMessage;
import org.jumin.zkx.components.constraint.ContraintValidationMessage;
import org.jumin.zkx.zkxutils.tree.DefaultInputFunction;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;

public class TreeSyntaxTextLengthConstraint extends AbstractTreeSyntaxValidationConstraint {

	private static final long serialVersionUID = 298737109607700070L;

    private int minLength;

	private int maxLength;

	public TreeSyntaxTextLengthConstraint(int minLength, int maxLength) {
		super();
		this.minLength = minLength;
		this.maxLength = maxLength;
	}

	@Override
	public void validate(Component comp, Object value) throws WrongValueException {
		String strValue = (String) value;

		if (strValue != null && strValue.length() > 0) {
			if (!DefaultInputFunction.isFunction(strValue)) {
				int valueLength = strValue.length();
				boolean minLengthFailure = false;
				boolean maxLengthFailure = false;
				if (valueLength < minLength) {
					minLengthFailure = true;
				}

				if (valueLength > maxLength) {
					maxLengthFailure = true;
				}

				if (minLengthFailure && maxLengthFailure) {
					showSyntaxValidationError(comp, new ContraintValidationMessage(
					        XmlValidationMessage.ERROR_INVALID_CONTENT_MIN_MAX_LENGTH, value, minLength, maxLength));
					return;
				}
				if (minLengthFailure) {
					showSyntaxValidationError(comp, new ContraintValidationMessage(
					        XmlValidationMessage.ERROR_INVALID_CONTENT_MIN_LENGTH, value, minLength));
					return;
				}
				if (maxLengthFailure) {
					showSyntaxValidationError(comp, new ContraintValidationMessage(
					        XmlValidationMessage.ERROR_INVALID_CONTENT_MAX_LENGTH, value, maxLength));
					return;
				}
			}
		}

		clearSyntaxValidationError(comp);
	}

}
