/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import org.jumin.common.utils.InvalidMethodPathExpression;
import org.jumin.common.utils.ObjectPropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArrayResultObjectIndexComparator implements Comparator<Object>, Serializable {

    private static final long serialVersionUID = 827565084926922489L;

    private static Logger log = LoggerFactory.getLogger(ArrayResultObjectIndexComparator.class);

    protected ObjectPropertyUtil objectUtil;
    private boolean asc = false;
    private String field = "";
    private int v;
    private String retrievingMethod;

    public ArrayResultObjectIndexComparator(boolean ascending, String field) {
        this(ascending, field, ObjectPropertyUtil.RETRIEVING_METHOD_DEFAULT);
    }

    public ArrayResultObjectIndexComparator(boolean ascending, String field, String retrievingMethod) {
        this.objectUtil = new ObjectPropertyUtil();
        asc = ascending;
        this.field = field;
        this.retrievingMethod = retrievingMethod;
    }

    public int compare(Object o1, Object o2) {
        Object temp1 = getPropertyValue(o1, field);
        Object temp2 = getPropertyValue(o2, field);

        // NULL value comparison (any of them)
        if (temp1 == null && temp2 == null) {
            v = 0;
        } else if (temp1 == null) {
            v = -1;
        } else if (temp2 == null) {
            v = 1;
        } else if (temp1 instanceof String) {
            String s1 = (String) temp1;
            String s2 = (String) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Integer) {
            Integer s1 = (Integer) temp1;
            Integer s2 = (Integer) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Long) {
            Long s1 = (Long) temp1;
            Long s2 = (Long) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Float) {
            Float s1 = (Float) temp1;
            Float s2 = (Float) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Double) {
            Double s1 = (Double) temp1;
            Double s2 = (Double) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof BigInteger) {
            BigInteger s1 = (BigInteger) temp1;
            BigInteger s2 = (BigInteger) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof BigDecimal) {
            BigDecimal s1 = (BigDecimal) temp1;
            BigDecimal s2 = (BigDecimal) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Time) {
            Time s1 = (Time) temp1;
            Time s2 = (Time) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Timestamp) {
            Timestamp s1 = (Timestamp) temp1;
            Timestamp s2 = (Timestamp) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Calendar) {
            Calendar s1 = (Calendar) temp1;
            Calendar s2 = (Calendar) temp2;
            v = s1.compareTo(s2);
        } else if (temp1 instanceof Date) {
            Date s1 = (Date) temp1;
            Date s2 = (Date) temp2;
            v = s1.compareTo(s2);
        } else {
            String s1 = "" + temp1;
            String s2 = "" + temp2;
            v = s1.compareTo(s2);
        }
        log.debug("Compare " + temp1 + " and " + temp2 + " = " + v);
        return asc ? v : -v;
    }

    protected Object getPropertyValue(Object object, String field) {
        try {
            return ((ObjectPropertyUtil) objectUtil).getPropertyValue(object, field, retrievingMethod);
        } catch (InvalidMethodPathExpression e) {
            log.error("Invalid method path: '" + field + "' with retrieving method: '" + retrievingMethod + "' for object: " + object, e);
        }
        return null;
    }
}
