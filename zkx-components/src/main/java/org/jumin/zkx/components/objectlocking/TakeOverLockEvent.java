/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.objectlocking;

import org.jumin.common.utils.locking.LockItem;
import org.jumin.zkx.zkxutils.events.EntityCommonFunctionEvents;
import org.zkoss.zk.ui.event.Event;

public class TakeOverLockEvent extends Event {

	private static final long serialVersionUID = -423416239061035056L;

    private LockItem existingLock;
	
	private LockItem newLock;
	
	public TakeOverLockEvent(LockItem existingLock, LockItem newLock) {
		super(EntityCommonFunctionEvents.ON_STEAL_LOCK);
		this.existingLock = existingLock;
		this.newLock = newLock;
	}

	public LockItem getExistingLock() {
	    return existingLock;
    }

	public LockItem getNewLock() {
	    return newLock;
    }

}
