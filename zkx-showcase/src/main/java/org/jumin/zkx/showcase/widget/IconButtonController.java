/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.showcase.widget;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;

public class IconButtonController extends SelectorComposer<Vlayout> {

    private static final long serialVersionUID = -5111152103869364778L;

    @Wire
    private Vlayout buttonContainer;
    
    @Wire
    private Textbox iconSclassValue;
    
    @Listen("onClick = button#showButton; onOK = textbox#iconSclassValue")
    public void showButton(Event event) {
        if (iconSclassValue.getText().trim().length() < 1) return;
        
        Button b = new Button(iconSclassValue.getText());
        b.setSclass("jrx-naked-button");
        b.setIconSclass(b.getLabel());
        b.setParent(buttonContainer);
        b.setAttribute("onClick", "org.zkoss.zk.ui.util.Clients.showNotification(\"hello\")");
    }
    
    @Listen("onClick = button#clearButton")
    public void clearButtons(Event event) {
        while (buttonContainer.getLastChild() != null) {
            buttonContainer.removeChild(buttonContainer.getLastChild());
        }
    }
    
    public void showInfo(Event event) {
        Clients.showNotification("Hello...");
    }
}
