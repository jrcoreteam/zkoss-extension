/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.constraint;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Locale;

import org.jumin.common.utils.validation.ValidationMessage;

public class ContraintValidationMessage implements Serializable {

	private static final long serialVersionUID = 772158677204021136L;

	private ValidationMessage message;

	// Parameters to bind while building the localized error description
	private Object[] parameters;

	public ContraintValidationMessage(ValidationMessage message) {
		this(message, (Object[]) null);
	}
	
	public ContraintValidationMessage(ValidationMessage message, Object... parameters) {
		super();
		this.message = message;
		this.parameters = parameters;
	}
	
	public ValidationMessage getMessage() {
		return message;
	}

	public Object[] getParameters() {
		return parameters;
	}

	@Override
	public String toString() {
	    return message + " (" + parameters + ")";
	}

	public String getDefaultMessage() {
		return MessageFormat.format(message.getDefaultDescription(), parameters);
	}

	public String getDefaultMessage(Locale locale) {
		MessageFormat mf = new MessageFormat(message.getDefaultDescription(), locale);
		return mf.format(parameters, new StringBuffer(), null).toString();
	}
}
