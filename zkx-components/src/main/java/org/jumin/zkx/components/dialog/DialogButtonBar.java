/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.dialog;

import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Space;

public class DialogButtonBar extends Hlayout {

	private static final long serialVersionUID = 6104476076797681735L;

	private Button okButton;
	private Button cancelButton;

	protected WidgetStaticLabelUtils widgetDebugUtil = null;

	public DialogButtonBar() {
		this(ZkLabels.getLabel("general.ok", "OK"), ZkLabels.getLabel("general.cancel", "Cancel"));
	}
	
	public DialogButtonBar(String okButtonLabel, String cancelButtonLabel) {
		super();
		widgetDebugUtil = WidgetStaticLabelUtils.getInstance();
		createContents();
		initProperties();
		initEvents();
		okButton.setLabel(okButtonLabel);
		cancelButton.setLabel(cancelButtonLabel);
	}

	protected void initEvents() {
		setSpacing("10px");
		okButton.addForward(Events.ON_CLICK, this, Events.ON_OK);
		cancelButton.addForward(Events.ON_CLICK, this, Events.ON_CANCEL);
    }

	protected void initProperties() {
		setSclass("zkx-dialog-buttonbar");
		
		if (!widgetDebugUtil.isTestingMode()) return;
		
		widgetDebugUtil.setLabel(okButton, "selectionDialog.ok");
		widgetDebugUtil.setLabel(cancelButton, "selectionDialog.cancel");
	}

	protected void createContents() {
		Space space = new Space();
		space.setHflex("true");
		space.setParent(this);
		
		okButton = new Button();
		okButton.setParent(this);
		
		cancelButton = new Button();
		cancelButton.setParent(this);
	}

	public String getOkButtonLabel() {
		return okButton.getLabel();
	}

	public void setOkButtonLabel(String okButtonLabel) {
		okButton.setLabel(okButtonLabel);
	}

	public String getCancelButtonLabel() {
		return cancelButton.getLabel();
	}

	public void setCancelButtonLabel(String cancelButtonLabel) {
		cancelButton.setLabel(cancelButtonLabel);
	}
	
  public void setOkButtonDisabled(boolean disabled) {
	  setOkButtonDisabled(disabled, true);
  }
  
  public void setOkButtonDisabled(boolean disabled, boolean focus) {
    okButton.setDisabled(disabled);
    
    if(!okButton.isDisabled() && focus) okButton.setFocus(focus);
  }
}
