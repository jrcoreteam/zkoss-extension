/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.translator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.jumin.zkx.zkxutils.ExcelUtils;
import org.zkoss.zk.ui.HtmlNativeComponent;
import org.zkoss.zul.impl.XulElement;

public class DecimalAlignedAmountWidgetTranslator implements Serializable {

    private static final long serialVersionUID = 1564612378667979215L;

    public static final int DEFAULT_CHARACTER_PIXEL_WIDTH = 9;

    public static final String AMOUNT_MAX_FRACTION_SIZE = "zkUtilsAmountMaxFractionSize";

    public static final String CURRENCY_FRACTION_DIGIT_TABLE = "zkUtilsCurrencyFractionDigitTable";

    public static final String CURRENCY_LIST_FROM_RECORD = "zkUtilsCurrencyListFromRecord";

    public void translate(XulElement container, int currencyFractionDigit, int maxFractionDigitSize, Double amount,
            Locale locale) {
        container.setAttribute(ExcelUtils.VAR_ZKUTILS_ORIGINAL_VALUE, amount);

        BigDecimal bdec = BigDecimal.valueOf(amount);
        BigDecimal fracBd = bdec.subtract(new BigDecimal(bdec.toBigInteger())).abs();

        int decimalCellWidth = maxFractionDigitSize * DEFAULT_CHARACTER_PIXEL_WIDTH;
        String fractionCellStyle = "text-align:left;width:" + decimalCellWidth + "px;";

        HtmlNativeComponent table = new HtmlNativeComponent("table");
        table.setDynamicProperty("border", "0");
        table.setDynamicProperty("width", "100%");
        table.setDynamicProperty("cellspacing", "0");
        table.setDynamicProperty("cellpadding", "0");
        table.setPrologContent("<tr>");
        table.setEpilogContent("</tr>");
        table.setParent(container);

        DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);

        String formattedIntegerValue = NumberFormat.getIntegerInstance(locale).format(bdec.longValue());
        HtmlNativeComponent integerCell = new HtmlNativeComponent("td");
        integerCell.setDynamicProperty("style", "text-align:right");
        integerCell.setPrologContent("<span>" + formattedIntegerValue);
        integerCell.setEpilogContent("</span>");
        integerCell.setParent(table);

        HtmlNativeComponent fractionCell = new HtmlNativeComponent("td");
        fractionCell.setDynamicProperty("style", fractionCellStyle);
        fractionCell.setEpilogContent("</span>");
        fractionCell.setParent(table);

        String fractionStringValue = "";
        if (currencyFractionDigit > 0) {
            fractionStringValue = String.format(locale, "%1$." + currencyFractionDigit + "f", fracBd);
            if (fractionStringValue.length() > 1)
                fractionStringValue = fractionStringValue.substring(1);
        } else {
            if (fracBd.compareTo(BigDecimal.ZERO) == 0) {
                fractionStringValue = "" + dfs.getDecimalSeparator();
            } else {
                fractionStringValue = String.format(locale, "%1$." + fracBd.scale() + "f", fracBd);
                if (fractionStringValue.length() > 1)
                    fractionStringValue = fractionStringValue.substring(1);
            }
        }
        fractionCell.setPrologContent("<span>" + fractionStringValue);
        container.setAttribute(ExcelUtils.VAR_ZKUTILS_STRING_FORMATTED_VALUE,
                formattedIntegerValue + fractionStringValue);
    }
}
