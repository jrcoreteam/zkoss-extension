/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.constraint;

import org.apache.commons.lang3.StringUtils;
import org.zkoss.zul.impl.InputElement;

public class WidgetConstraintUtil {

	protected static final String INPUT_ERROR_CLASS = "zkx-input-error";
	
	public static void setErrorOnWidget(InputElement inputWidget, boolean showError) {
		String existingSclass = inputWidget.getSclass();
		if (showError) {
			// Show error
			if (existingSclass == null) {
				inputWidget.setSclass(INPUT_ERROR_CLASS);
			} else {
				if (!existingSclass.contains(INPUT_ERROR_CLASS)) {
					if (existingSclass.equals("")) {
						inputWidget.setSclass(INPUT_ERROR_CLASS);
					} else {
						inputWidget.setSclass(existingSclass + " " + INPUT_ERROR_CLASS);
					}
				}
			}
		} else {
			// clear error
			if (existingSclass != null && existingSclass.length() > 0) {
				inputWidget.setSclass(StringUtils.replace(existingSclass, INPUT_ERROR_CLASS, "").trim());
			} else {
				inputWidget.setSclass(null);
			}
		}
	}

}
