/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.jumin.common.utils.DatetimeTextParser;

public class DefaultInputFunction {
	public static final String FUNCTION_REGEX = "\\$\\{[a-zA-Z]+(([\\-]|[_])[a-zA-Z]*)*\\}";
	
	public static final Pattern FUNCTION_PATTERN = Pattern.compile(FUNCTION_REGEX);

	public static boolean isFunction(String value) {
		return FUNCTION_PATTERN.matcher(value).matches();
	}
	
	public static String resolveFunction(String functionName, TimeZone userTimeZone) {
		if (functionName == null || !functionName.startsWith("${") || !functionName.endsWith("}")) {
			return null;
		}
		
		String value = null;
		
		if (functionName.equalsIgnoreCase("${now}")) {
			if (userTimeZone == null) userTimeZone = TimeZone.getDefault();
			DatetimeTextParser dtp = new DatetimeTextParser();
			return dtp.getISODateTimeFromDate(new Date(System.currentTimeMillis()), userTimeZone);
		}

		if (functionName.equalsIgnoreCase("${n}")) {
			if (userTimeZone == null) userTimeZone = TimeZone.getDefault();
			DatetimeTextParser dtp = new DatetimeTextParser();
			return dtp.getISODateTimeFromDate(new Date(System.currentTimeMillis()), userTimeZone);
		}

		if (functionName.equalsIgnoreCase("${today}")) {
			if (userTimeZone == null) userTimeZone = TimeZone.getDefault();
			DatetimeTextParser dtp = new DatetimeTextParser();
			return dtp.getISODateFromDate(new Date(System.currentTimeMillis()), userTimeZone);
		}

		if (functionName.equalsIgnoreCase("${t}")) {
			if (userTimeZone == null) userTimeZone = TimeZone.getDefault();
			DatetimeTextParser dtp = new DatetimeTextParser();
			return dtp.getISODateFromDate(new Date(System.currentTimeMillis()), userTimeZone);
		}

		return value; 
	}

	public static String resolveFunction(String functionName) {
		return resolveFunction(functionName, TimeZone.getDefault()); 
	}
}
