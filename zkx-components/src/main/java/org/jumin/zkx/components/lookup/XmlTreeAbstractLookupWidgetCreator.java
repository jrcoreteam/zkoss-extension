/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.lookup;

import java.io.Serializable;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.zkx.zkxutils.ApplicationConstants;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;

public abstract class XmlTreeAbstractLookupWidgetCreator implements Serializable {

	private static final long serialVersionUID = -6994313223502139192L;

    protected ServiceConnector connector;
	
	protected EditorMode editorMode;
	
	protected XmlTreerow row;

	protected String value;

	public XmlTreeAbstractLookupWidgetCreator(ServiceConnector connector, XmlTreerow row,
	        EditorMode editorMode, String initialValue) {
		super();
		this.connector = connector;
		this.row = row;
		this.value = initialValue;
		this.editorMode = editorMode;
	}

	public Label createViewModeWidget() {
		Label widget = new Label();
		widget.setSclass("jrx-labelvalue");
		widget.setHflex("true");
		if (value != null && !value.equals("")) {
			widget.setValue(value);
			if  (!value.startsWith("***"))
				registerValueDescription(value, false);
		}
		
		return widget;
	}
	
	public abstract HtmlBasedComponent createWidget();

	protected void registerValueDescription(XmlTreerow row, String descValue) {
		registerValueDescription(row, descValue, true);
	}
	
	public static void registerValueDescription(XmlTreerow row, String descValue, boolean triggerEvent) {
		if (row == null)
			return;

		row.setAttribute(ApplicationConstants.COMPONENT_LOOKUP_DESCRIPTION, descValue);
		if (triggerEvent)
			Events.postEvent(XmlTreerow.ON_DESCRIPTION, row, null);
	}

	protected abstract void registerValueDescription(String value, boolean triggerEvent);
}
