<%@ page contentType="text/css;charset=UTF-8" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>

/* Buttons */
.zkx-button-naked-icon-s {
    padding: 0;
}

.zkx-button-naked-icon {
	padding: 8px;
}

.zkx-button-naked-icon-l {
    padding: 14px;
}

.z-button.zkx-button-naked-icon .z-icon-stack {
	height: 19px;
	line-height: 19px;
	width: 18px;
}
