/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.showcase.table;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jumin.common.preferences.Preference;
import org.jumin.common.preferences.table.TableConfiguration;
import org.jumin.common.preferences.table.TableConfigurationLoader;
import org.jumin.common.utils.DatetimeTextParser;
import org.jumin.zkx.components.model.button.ButtonBarModel;
import org.jumin.zkx.components.model.button.ButtonBarModelLoader;
import org.jumin.zkx.components.model.button.UserFunction;
import org.jumin.zkx.showcase.table.model.Message;
import org.jumin.zkx.showcase.table.model.MessageType;
import org.jumin.zkx.zkxutils.UserContextUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.ListModelList;

public class DataTableButtonAuthorizationController extends SelectorComposer<DataTableWithAuthorization<Object>> {

    private static final long serialVersionUID = 2675012012451340249L;

    private static Logger logger = LoggerFactory.getLogger(DataTableButtonAuthorizationController.class);
    
    public static final String SESSION_ATTR_AUTHORIZED_ACTIONS = "AppAuthorizedActions";

    private DataTableWithAuthorization<Object> overviewTable;

    protected ButtonBarModel buttonBarModel = null;
    
    private String[] authorizedActions = null;
    
    @Override
    public void doAfterCompose(DataTableWithAuthorization<Object> comp) throws Exception {
        super.doAfterCompose(comp);

        initProperties();

        overviewTable = comp;
        initAuthorization();
        initDataTableConfig();
        initButtonBarModel();

        initData();
    }

    private void initAuthorization() {
    	try {
    		String[] authActions = (String[]) getPage().getDesktop().getSession().getAttribute(SESSION_ATTR_AUTHORIZED_ACTIONS);
    		this.authorizedActions = authActions;
    		overviewTable.setAuthorizedActions(Arrays.asList(authorizedActions));
    	} catch (ClassCastException e) {
    		logger.warn("Invalid authorized actions set in the session!", e);
    	} catch (Exception e) {
    		logger.error("Failure in retrieving authorized actions", e);
    	}
	}

	private void initDataTableConfig() {
        logger.debug("Start...");
        String sysConfig = "";
        String orgConfig = "";
        String userConfig = "";

        InputStream configStream = getPage().getDesktop().getWebApp()
                .getResourceAsStream("/table/system-table-column-config.xml");
        TableConfigurationLoader loader = new TableConfigurationLoader();
        try {
            List<Preference> prefList = loader.load(configStream);
            if (prefList.isEmpty())
                return;

            TableConfiguration tableConfig = loader.convert(prefList.get(0));
            overviewTable.setSystemResultConfig(tableConfig.getDefinitionValue());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                configStream.close();
            } catch (IOException e) {
            }
        }

        logger.debug("End...");
    }

    private void initProperties() {
        if (UserContextUtil.getUserFormatLocale(getPage()) == null)
            UserContextUtil.setUserFormatLocale(getPage(), Locale.getDefault());
        
        if (UserContextUtil.getUserTimeZone(getPage()) == null)
            UserContextUtil.setUserTimeZone(getPage(), TimeZone.getDefault());
    }

    private void initButtonBarModel() {
        try {
            if (buttonBarModel == null) {
                ButtonBarModelLoader loader = new ButtonBarModelLoader();
                Map<String, UserFunction> functionMap = loader.loadFunctionModel(getFunctionCatalogStream());
                buttonBarModel = loader.loadButtonModel(functionMap, getButtonBarModelStream());
            }
            overviewTable.setButtonBarModel(buttonBarModel);
        } catch (Exception e) {
            logger.error("Error in initializing button bar model!", e);
        }
    }

    private InputStream getButtonBarModelStream() {
        return getPage().getDesktop().getWebApp().getResourceAsStream("/table/msg-button-model.xml");
    }

    private InputStream getFunctionCatalogStream() {
        return getClass().getResourceAsStream("/config/function-catalog.xml");
    }

    private void initData() {
        MessageType msgType = new MessageType();
        msgType.setId("swift.mt.103");
        msgType.setName("Customer Payment");
        msgType.setDescription("Standard customer payment message.");

        Random r = new Random();
        List<Message> data = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            int randomSize = r.nextInt(1000) + 1;

            Message msg = new Message();
            msg.setMessageId(StringUtils.remove(UUID.randomUUID().toString(), "-"));
            msg.setType(msgType);
            msg.setCreatedBy("SystemAgent@System");
            Date date = new Date(System.currentTimeMillis());
            date = DateUtils.addSeconds(date, randomSize);
            msg.setCreationTimetamp(date);
            msg.setModifiedBy("SystemAgent@System");
            date = DateUtils.addSeconds(date, randomSize);
            msg.setModificationTimestamp(date);
            msg.setTransactionReference("TEST01234567");
            msg.setCounterpartyCode("UBSWCHZH80A");
            msg.setOwnpartyCode("CSFBCHZH10F");
            msg.setExtendedPropertyValue("status", "Sent");
            msg.setExtendedPropertyValue("isin", "ISIN12345678");
            msg.setExtendedPropertyValue("unitNumber",  randomSize * 10);
            msg.setExtendedPropertyValue("currency", r.nextBoolean() ? "EUR" : (r.nextBoolean() ? "CHF" : "USD"));
            msg.setExtendedPropertyValue("amount", new Double(randomSize * 1000));
            msg.setExtendedPropertyValue("accountNo", "AC1234567890");
            msg.setExtendedPropertyValue("rate", new Double(1));
            data.add(msg);
        }

        overviewTable.setDataTableModel(new ListModelList<Object>(data));
        
        Locale formatLocale = UserContextUtil.getUserFormatLocale(getPage());
        TimeZone tz = UserContextUtil.getUserTimeZone(getPage());
        
        NumberFormat nf = NumberFormat.getIntegerInstance(formatLocale);
        DatetimeTextParser dtp = new DatetimeTextParser();
        overviewTable.setEntriesFoundNumber(nf.format(data.size()));
        overviewTable.setEntriesFoundTimestamp(dtp.getStringFromDateTime(Calendar.getInstance().getTime(), formatLocale, tz));
    }
}
