/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.renderer;

import java.util.Hashtable;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jumin.common.base.services.ServiceConnector;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxTerm;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.EditorMode;
import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.renderer.TreeitemRendererWithColumn;
import org.jumin.zkx.zkxutils.tree.XmlTreeDefinition;
import org.jumin.zkx.zkxutils.tree.XmlTreeExpansionMode;
import org.jumin.zkx.zkxutils.tree.XmlTreeNodeDefault;
import org.jumin.zkx.zkxutils.tree.XmlTreeRootDefinition;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.AbstractTreeModel;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.impl.InputElement;

public class XmlTreeitemRenderer implements TreeitemRendererWithColumn<JrxTerm<?>> {

    private static final long serialVersionUID = -2526002135661772963L;

    private static Logger log = LoggerFactory.getLogger(XmlTreeitemRenderer.class);

    protected XmlTreeExpansionMode expansionMode = XmlTreeExpansionMode.EXPAND_SINGLE;

    protected EditorMode editorMode = EditorMode.VIEW;

    protected XmlTreeLabelCellRenderer labelCellRenderer = null;

    protected AbstractXmlTreeValueCellRenderer valueCellRenderer = null;

    protected XmlTreeDescriptionCellRenderer descriptionCellRenderer = null;

    private XmlTreeDefinition treeDefinition = null;

    private Hashtable<Long, SortedSet<XmlTreerow>> termLocations;

    //private XmlEditor editor = null;

    protected WidgetStaticLabelUtils widgetDebugUtil = WidgetStaticLabelUtils.getInstance();

    public XmlTreeitemRenderer() {
        super();
        this.termLocations = new Hashtable<Long, SortedSet<XmlTreerow>>();
        this.labelCellRenderer = new SimpleXmlTreeLabelCellRenderer();
        this.valueCellRenderer = new XmlTreeEntryValueCellRenderer();
        this.descriptionCellRenderer = new XmlTreeDescriptionCellRenderer();
    }

    @Override
    public void render(Treeitem item, JrxTerm<?> data, int index) throws Exception {
        if (data == null)
            return;

        render(item, (JrxTerm<?>) data);

        if (item.getTree() == null)
            return;

        AbstractTreeModel<?> model = (AbstractTreeModel<?>) item.getTree().getModel();
        try {
            if (!item.isOpen() && model.isObjectOpened(data)) {
                item.setOpen(true);
            }
        } catch (Exception e) {
            // ignore for the time being
            log.warn("Please check the XML tree handling isOpen()!", e);
        }
    }

    public void render(Treeitem item, JrxTerm<?> jrxTerm) throws Exception {
        try {
            item.setValue(jrxTerm);

            XmlTreeRootDefinition treeRootDefinition = null;
            XmlTreeNodeDefault[] treeNodeDefaults = null;
            if (treeDefinition != null && jrxTerm instanceof JrxElement) {
                JrxElement jrxElement = (JrxElement) jrxTerm;
                treeRootDefinition = treeDefinition.getRootDefinition(jrxElement);
                treeNodeDefaults = treeDefinition.getNodeDefaultDefinitions(jrxElement);
            }

            boolean open = XmlTreeExpansionMode.EXPAND_ALL.equals(expansionMode);
            createTreeRowString(item, jrxTerm, treeRootDefinition, treeNodeDefaults);

            // if (jrxTerm instanceof JrxChoiceGroup && ((JrxChoiceGroup)
            // jrxTerm).getSelection() != null) {
            // open = true;
            // }
            if (treeNodeDefaults == null || treeNodeDefaults.length < 1) {
                open = open || (jrxTerm.isMandatory());

                if (!open && jrxTerm instanceof JrxElement) {
                    JrxElement jrxElement = (JrxElement) jrxTerm;
                    if (!isJrxElementEmpty(jrxElement)) {
                        open = true; // open non-mandatory element that is not
                                     // empty
                    }
                }

                item.setOpen(open);
            } else {
                // Override standard behavior!
                item.setOpen(XmlTreeNodeDefault.isOpen(treeNodeDefaults));
                item.setVisible(XmlTreeNodeDefault.isVisible(treeNodeDefaults));
            }

            addTermLocation(jrxTerm, item);
        } catch (Exception e) {
            log.error("Error in rendering tree.", e);
        }
    }

    protected boolean isJrxElementEmpty(JrxElement jrxElement) {
        return jrxElement.isEmpty();
    }

    protected void createTreeRowString(Treeitem item, JrxTerm<?> jrxTerm, XmlTreeRootDefinition treeRootDefinition,
            XmlTreeNodeDefault[] treeNodeDefaults) {
        XmlTreerow row = new XmlTreerow(jrxTerm);
        row.setParent(item);

        Treecell labelCell = null;
        if (treeRootDefinition == null) {
            labelCell = labelCellRenderer.createFirstCell(jrxTerm);
        } else {
            labelCell = labelCellRenderer.createFirstCell(treeRootDefinition.getLabelKey(), jrxTerm.isMandatory(),
                    jrxTerm.isRepetitive());
        }
        labelCell.setParent(row);

        if (widgetDebugUtil.isTestingMode()) {
            String rowLabel = ComponentUtils.getChildByClass(labelCell, Label.class).getValue();
            rowLabel = generateUniqueInputStaticLabel(item, rowLabel);
            widgetDebugUtil.setLabel(row, rowLabel);
        }

        Treecell valueCell = valueCellRenderer.createSecondCell(row, treeNodeDefaults);

        InputElement ie = ComponentUtils.getChildByClass(valueCell, InputElement.class, true);
        if (ie != null) {
// @formatter:off
// TODO: Shall be fixed when editor is ready
//            XmlEditor editor = getEditor(item);
//            if (editor != null) {
//                ie.addForward(Events.ON_CHANGE, getEditor(item), Events.ON_CHANGE);
//
//                // Purpose: for validation result identification
//                ie.setAttribute(ApplicationConstants.WIDGET_VAR_ATTRIBUTE_NAME,
//                        ((Label) labelCell.getFirstChild()).getValue());
//                ie.setAttribute(ApplicationConstants.WIDGET_VAR_ATTRIBUTE_LABEL,
//                        ((Label) labelCell.getFirstChild()).getValue());
//
//                // Handle default ENUM (1st entry) from XSD
//                if (!EditorMode.VIEW.equals(getEditorMode()) && jrxTerm instanceof JrxElement && ie instanceof Combobox
//                        && !(ie instanceof XmlChoiceOptionCombobox)) {
//                    Combobox cb = (Combobox) ie;
//                    if (cb.getSelectedItem() != null && cb.getSelectedItem().getValue() != null) {
//                        Events.postEvent(Events.ON_CHANGE, ie, null);
//                    }
//                }
//            }
// @formatter:on
        }

        Treecell descriptionCell = descriptionCellRenderer.createThirdCell(row);
        descriptionCell.setParent(row);
    }

    public Treecols getTreeColumns() {
        Treecols cols = new Treecols();
        cols.setSizable(true);

        Treecol col = new Treecol(ZkLabels.getLabel("messageTree.heading.tag", "Tag / Field"));
        col.setParent(cols);

        col = new Treecol(ZkLabels.getLabel("messageTree.heading.value", "Value"));

        col.setParent(cols);

        col = new Treecol(ZkLabels.getLabel("messageTree.heading.description", "Description"));
        col.setParent(cols);

        return cols;
    }

    public void setExpansionMode(XmlTreeExpansionMode expansionMode) {
        this.expansionMode = expansionMode;
    }

    public XmlTreeExpansionMode getExpansionMode() {
        return expansionMode;
    }

    public void setEditorMode(EditorMode editorMode) {
        this.editorMode = editorMode;
        valueCellRenderer.setEditorMode(editorMode);
        descriptionCellRenderer.setEditorMode(editorMode);
    }

    public EditorMode getEditorMode() {
        return editorMode;
    }

    public void setMessageTreeDefinition(XmlTreeDefinition treeDefinition) {
        this.treeDefinition = treeDefinition;
    }

    public XmlTreeDefinition getMessageTreeDefinition() {
        return treeDefinition;
    }

    public ServiceConnector getBackendServiceConnector() {
        return valueCellRenderer.getConnector();
    }

    public void setBackendConnector(ServiceConnector connector) {
        valueCellRenderer.setConnector(connector);
    }

    public Hashtable<Long, SortedSet<XmlTreerow>> getTermLocations() {
        return termLocations;
    }
// @formatter:off
// TODO: Shall be fixed when editor is ready
//    protected XmlEditor getEditor(Treeitem item) {
//        if (editor == null) {
//            editor = ComponentUtils.getParentByClass(item, XmlEditor.class);
//        }
//
//        return editor;
//    }
// @formatter:on
    
    protected String generateUniqueInputStaticLabel(Treeitem item, String currentLabel) {
        if (item == null || item.getParentItem() == null) {
            return currentLabel;
        }
        String scopedLabel = currentLabel;
        Treeitem parentItem = item.getParentItem();
        while (parentItem != null) {
            Label parentLabel = ComponentUtils.getChildByClass(parentItem, Label.class, true);
            if (parentLabel == null)
                break;

            scopedLabel = parentLabel.getValue() + "." + scopedLabel;
            parentItem = parentItem.getParentItem();
        }

        return scopedLabel;
    }

    protected void addTermLocation(JrxTerm<?> jrxTerm, Treeitem item) {
        SortedSet<XmlTreerow> treeRowList = termLocations.get(jrxTerm.getScopedNameHashCode());
        if (treeRowList == null)
            treeRowList = new TreeSet<XmlTreerow>();
        treeRowList.add((XmlTreerow) item.getTreerow());
        termLocations.put(jrxTerm.getScopedNameHashCode(), treeRowList);

    }
}
