/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.jumin.common.authorization.BaseSecurityConstants;
import org.jumin.common.preferences.GlobalPreferenceCatalog;
import org.jumin.common.preferences.table.TableColumnConfiguration;
import org.jumin.common.preferences.table.TableColumnSortOrder;
import org.jumin.common.preferences.table.TableConfiguration;
import org.jumin.common.preferences.table.TableConfigurationLoader;
import org.jumin.common.preferences.table.TableConfigurationUtil;
import org.jumin.common.utils.InvalidMethodPathExpression;
import org.jumin.common.utils.ObjectPropertyUtil;
import org.jumin.common.utils.query.AttributeOrder.SortOrder;
import org.jumin.zkx.components.CommonButtonBar;
import org.jumin.zkx.components.exception.ExceptionLogger;
import org.jumin.zkx.components.model.button.AssignmentItem;
import org.jumin.zkx.components.model.button.ButtonBarModel;
import org.jumin.zkx.components.model.button.ButtonBarModelLoader;
import org.jumin.zkx.components.model.button.FunctionAssignment;
import org.jumin.zkx.components.model.button.FunctionSeparator;
import org.jumin.zkx.components.model.button.PatternFunctionAssignment;
import org.jumin.zkx.components.model.button.SourceAccess;
import org.jumin.zkx.components.model.button.StateFunctionAssignment;
import org.jumin.zkx.zkxutils.ArrayResultObjectIndexComparator;
import org.jumin.zkx.zkxutils.ExcelUtils;
import org.jumin.zkx.zkxutils.MessageDialogUtils;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.events.ApplicationEvents;
import org.jumin.zkx.zkxutils.events.EntityCommonFunctionEvents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.AbstractListModel;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Menuseparator;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Space;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.ColSizeEvent;
import org.zkoss.zul.event.PageSizeEvent;
import org.zkoss.zul.event.ZulEvents;

public class CommonDataTable<T> extends AbstractDataTable<T> {

	private static final long serialVersionUID = -6678712763095125427L;

	private static Logger log = LoggerFactory.getLogger(CommonDataTable.class);

	public static final String ON_SAVE_TABLE_CONFIG = "onSaveTableConfig";

	public static final String ON_TABLE_ROW_SIZE_CHANGE = "onTableRowSizeChange";

	protected static final ConcurrentMap<String, Pattern> REGEX_PATTERN_CATALOG = new ConcurrentSkipListMap<>();

	private String userConfigXmlString;

	private boolean useConfiguration;

	protected TableConfiguration config;

	private String systemConfigXmlString;

	private String orgUnitConfigXmlString;

	protected Listbox listbox;

	private Paging paging;

	private ObjectItemRenderer renderer;

	protected Label entriesFoundNumber;

	protected Label entriesFoundNumberLabel;

	protected Label entriesFoundTimestamp;

	protected Label entriesFoundTimestampLabel;

	protected A configureIcon;

	protected A export2ExcelIcon;

	private Hlayout headerInfoContainer;

	private CommonButtonBar buttonBar;

	private ButtonBarModel buttonBarModel;

	private TablePreferenceDialog prefDialog;

	protected TableConfigurationLoader configLoader = new TableConfigurationLoader();

	private boolean largeDataSupport = false;

	private String ownerColumnName;

	private String lastUserColumnName;

	private Label pageSizeLabel;

	private Combobox pageSizeCombobox;

	protected ButtonBarModelLoader buttonModelLoader;

	private List<Listitem> selectionListCache = null;

	protected ObjectPropertyUtil propUtil = new ObjectPropertyUtil();

	private TableControlLocation tableControlLocation = TableControlLocation.TOP;

	public CommonDataTable() {
		super();
		this.useConfiguration = false;
		buttonModelLoader = new ButtonBarModelLoader();
	}

	public List<TableColumnSortOrder> getSortOrder() {
		if (config != null) {
			return config.getColumnSortOrderList();
		}
		return null;
	}

	@Override
	protected void initProperties() {
		configureIcon.setIconSclass("z-icon-wrench z-icon-lg");
		configureIcon.setTooltiptext(ZkLabels.getLabel("listbox.configure", "Configure columns and sort order"));

		export2ExcelIcon.setIconSclass("z-icon-file-excel-o z-icon-lg");
		export2ExcelIcon.setTooltiptext(ZkLabels.getLabel("listbox.exportToExcel.icon.tooltip", "Export to Excel"));

		listbox.setSclass("zkx-data-table-list");
		listbox.setCheckmark(false);
		listbox.setMultiple(true);
		listbox.setSizedByContent(false);
		listbox.setMold("paging");
		// dataTableList.setAutopaging(true);
		listbox.setVflex("true");
		listbox.setWidth("100%");
		widgetDebugUtil.setLabel(listbox, "dataTableList");

		refreshPagingMechanism();

		paging.setDetailed(true);

		buttonBar.setTargetComponent(this);

		headerInfoContainer.setValign("middle");

		entriesFoundNumberLabel.setSclass("zkx-data-table-summary-label");
		entriesFoundNumber.setSclass("zkx-data-table-summary-value");
		entriesFoundTimestampLabel.setSclass("zkx-data-table-summary-label");
		entriesFoundTimestamp.setSclass("zkx-data-table-summary-value");

		pageSizeLabel.setSclass("zkx-data-table-summary-label");
		pageSizeCombobox.setSclass("zkx-combobox-small");
		pageSizeCombobox.setReadonly(true);
		pageSizeCombobox.setModel(new ListModelList<Integer>(new Integer[] { 10, 20, 50, 100, 500 }));
		pageSizeCombobox.setWidth("85px");
		pageSizeCombobox.setValue("50");

		paging.setPageSize(Integer.parseInt(pageSizeCombobox.getValue()));
		listbox.setPageSize(paging.getPageSize());
	}

	@Override
	protected void initEvents() {
		log.debug("start");
		final Component root = this;
		listbox.addForward(AbstractDataTable.ON_ITEM_OPEN, this, AbstractDataTable.ON_ITEM_OPEN,
				listbox.getSelectedItem());
		listbox.addForward(AbstractDataTable.ON_ITEM_ACTION, this, AbstractDataTable.ON_ITEM_ACTION);

		listbox.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				selectionListCache = null;
				Events.sendEvent(Events.ON_SELECT, root, null);
			}
		});
		listbox.addEventListener(ZulEvents.ON_PAGE_SIZE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				PageSizeEvent pageSizeEvent = (PageSizeEvent) event;
				if (largeDataSupport) {
					paging.setPageSize(pageSizeEvent.getPageSize());
					Events.postEvent(root, event);
				}
			}
		});

		paging.addForward(ZulEvents.ON_PAGING, this, ZulEvents.ON_PAGING);

		configureIcon.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				showConfigDialog();
			}
		});

		export2ExcelIcon.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				exportToExcel();
			}
		});

		pageSizeCombobox.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				onPageSizeComboChange();
				Events.sendEvent(ON_TABLE_ROW_SIZE_CHANGE, root, null);
			}
		});

		log.debug("end");
	}

	protected void onPageSizeComboChange() {
		paging.setPageSize(Integer.parseInt(pageSizeCombobox.getValue()));
		listbox.setPageSize(paging.getPageSize());
	}

	// this is not nice, but we need an index for the selenium-attributes
	// the value -1 signifies that no such entry is found in the list
	@SuppressWarnings("unchecked")
	public int getIndexOf(T data) {
		ListModelList<T> model = (ListModelList<T>) listbox.getModel();
		if (null != model) {
			return model.indexOf(data);
		}
		return -1;
	}

	@Override
	protected void createContents() {
		log.debug("start");

		createHeaderInfoLabel();
		createDataTable();
		createButtonBar();

		log.debug("end");
	}

	protected void createButtonBar() {
		buttonBar = createDefaultButtonBar();
		buttonBar.setParent(this);
	}

	protected CommonButtonBar createDefaultButtonBar() {
		return new CommonButtonBar();
	}

	protected void createHeaderInfoLabel() {
		log.debug("start");
		headerInfoContainer = new Hlayout();
		headerInfoContainer.setParent(this);

		pageSizeLabel = new Label(ZkLabels.getLabel("commonDataTable.pageSize", "Page Size") + ":");
		pageSizeLabel.setParent(headerInfoContainer);

		pageSizeCombobox = new Combobox();
		pageSizeCombobox.setParent(headerInfoContainer);

		Div longSpace = new Div();
		longSpace.setHflex("true");
		longSpace.setParent(headerInfoContainer);

		entriesFoundNumberLabel = new Label();
		entriesFoundNumberLabel.setParent(headerInfoContainer);
		entriesFoundNumberLabel.setValue(ZkLabels.getLabel("commonDataTable.totalFoundLabel", "Total") + ": ");

		entriesFoundNumber = new Label();
		entriesFoundNumber.setParent(headerInfoContainer);
		entriesFoundNumber.setValue("-");
		widgetDebugUtil.setLabel(entriesFoundNumber, "datatable_num_of_results");

		entriesFoundTimestampLabel = new Label();
		entriesFoundTimestampLabel.setParent(headerInfoContainer);
		entriesFoundTimestampLabel
				.setValue(" " + ZkLabels.getLabel("commonDataTable.entriesFoundAtLabel", "entries found at") + ": ");

		entriesFoundTimestamp = new Label();
		entriesFoundTimestamp.setParent(headerInfoContainer);
		entriesFoundTimestamp.setValue("-");
		widgetDebugUtil.setLabel(entriesFoundTimestamp, "datatable_timestamp");

		headerInfoContainer.appendChild(new Space());

		configureIcon = new A();
		configureIcon.setParent(headerInfoContainer);

		export2ExcelIcon = new A();
		export2ExcelIcon.setParent(headerInfoContainer);

		log.debug("end");
	}

	@SuppressWarnings("unchecked")
	private void createDataTable() {
		log.debug("start");
		renderer = new ObjectItemRenderer((AbstractDataTable<Object>) this);

		listbox = new Listbox();
		listbox.setParent(this);
		listbox.setItemRenderer(renderer);

		initPaging();
		log.debug("end");
	}

	protected void initPaging() {
		paging = new Paging();
		paging.setParent(this);
	}

	protected Listhead refreshListhead() {
		log.debug("Start...");

		final AbstractDataTable<T> thisDataTableComponent = this;
		final Listhead listhead = new Listhead();
		listhead.setSizable(true);
		if (listbox.getListhead() != null) {
			listbox.removeChild(listbox.getListhead());
		}
		listhead.setParent(listbox);

		if (config == null)
			return listhead;

		// retrieved defined Sort Order
		TableColumnSortOrder existingColumnSortOrder = null;

		if (config.getColumnSortOrderList() != null && !config.getColumnSortOrderList().isEmpty()) {
			existingColumnSortOrder = config.getColumnSortOrderList().get(0);
		}

		HashMap<TableColumnSortOrder, Listheader> colSort = new HashMap<TableColumnSortOrder, Listheader>();

		for (int i = 0; i < config.getColumns().size(); i++) {
			TableColumnConfiguration columnConfig = config.getColumns().get(i);

			if (!columnConfig.isVisible()) {
				continue;
			}

			Listheader listheader = new Listheader();

			listheader.setValue(columnConfig);
			listheader.setParent(listhead);
			listheader.setDraggable("true");
			listheader.setDroppable("true");
			listheader.setLabel(ZkLabels.getLabel(createTableColumnHeaderKey(columnConfig), columnConfig.getLabel()));
			if (columnConfig.isSortable()) {
				if (existingColumnSortOrder != null
						&& columnConfig.getName().equals(existingColumnSortOrder.getColumn().getName())) {
					SortOrder sortOrder = existingColumnSortOrder.getOrder();
					if (sortOrder != null) {
						listheader.setSortDirection(sortOrder.name().toLowerCase());
					}
				}

				// this is the column based approach, otherwise, do sorting in
				// the model
				listheader.setSortAscending(new ArrayResultObjectIndexComparator(true, columnConfig.getName(),
						columnConfig.getRetrievingMethod()));
				listheader.setSortDescending(new ArrayResultObjectIndexComparator(false, columnConfig.getName(),
						columnConfig.getRetrievingMethod()));
			}
			if (!largeDataSupport) {
				// apply sorting
				TableColumnSortOrder sortOrder = config.getColumnSortOrder(columnConfig);
				if (sortOrder != null) {
					SortOrder order = sortOrder.getOrder();
					if (order != null) {
						colSort.put(sortOrder, listheader);
					}
				}
			}

			// Set the width if it was already set...
			String width = columnConfig.getWidth();
			if (width != null && !width.equals("") && width.endsWith("px")) {
				listheader.setWidth(width);
			}

			// Set text alignment
			if (!TableColumnConfiguration.Alignment.LEFT.equals(columnConfig.getAlign())) {
				listheader.setAlign(columnConfig.getAlign().name().toLowerCase());
			}

			if (columnConfig.getTooltip() != null && !columnConfig.getTooltip().equals("")) {
				// listheader.setTooltip(ZkLabels.getLabel(columnConfig.getTooltip()));
				listheader.setTooltiptext(ZkLabels.getLabel(columnConfig.getTooltip()));
			}

			listheader.addEventListener(Events.ON_DROP, new EventListener<DropEvent>() {
				@Override
				public void onEvent(DropEvent event) throws Exception {
					onDrop(event);
				}
			});

			listheader.addEventListener(Events.ON_SIZE, new EventListener<Event>() {
				public void onEvent(Event event) throws Exception {
					Listheader header = (Listheader) event.getTarget();
					header.getListbox().invalidate();
				}
			});

			listheader.addEventListener(Events.ON_SORT, new EventListener<Event>() {
				public void onEvent(Event event) throws Exception {
					onListHeaderSort(event, thisDataTableComponent);
				}
			});

			listheader.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
				public void onEvent(Event event) throws Exception {
					Listheader header = (Listheader) event.getTarget();
					header.getListbox().invalidate();
				}
			});

			listheader.addEventListener(Events.ON_RIGHT_CLICK, new EventListener<Event>() {
				public void onEvent(Event event) throws Exception {
					Listheader header = (Listheader) event.getTarget();
					TableColumnConfiguration columnConfig = (TableColumnConfiguration) header.getValue();

					if (!columnConfig.isSortable())
						return;

					Menupopup popup = new Menupopup();

					Menu sort = new Menu(ZkLabels.getLabel("general.sort"));
					Menupopup sortPU = new Menupopup();
					Menuitem asc = new Menuitem(ZkLabels.getLabel("general.ascending"));
					asc.setAttribute("ColHeader", header);
					asc.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							onClickHeaderSort(event, true);
						}
					});
					Menuitem desc = new Menuitem(ZkLabels.getLabel("general.descending"));
					desc.setAttribute("ColHeader", header);
					desc.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							onClickHeaderSort(event, false);
						}
					});
					Menuitem resetSort = new Menuitem(ZkLabels.getLabel("general.reset"));
					resetSort.setAttribute("ColHeader", header);
					resetSort.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							config.resetSortOrder();
						}
					});
					asc.setParent(sortPU);
					desc.setParent(sortPU);
					sortPU.appendChild(new Menuseparator());
					resetSort.setParent(sortPU);
					sortPU.setParent(sort);

					Menuitem find = new Menuitem(ZkLabels.getLabel("general.find"));
					find.setAttribute("ColConfig", ((Listheader) event.getTarget()).getValue());
					find.setAttribute("ListHeader", (Listheader) event.getTarget());

					find.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							TableColumnConfiguration colConfig = (TableColumnConfiguration) event.getTarget()
									.getAttribute("ColConfig");
							Listheader listHeader = (Listheader) event.getTarget().getAttribute("ListHeader");
							FindPopup fp = new FindPopup(colConfig, listbox, listHeader);
							fp.initEvents();
							fp.setTitle(ZkLabels.getLabel("general.search"));
							fp.show();
						}
					});

					sort.setParent(popup);
					find.setParent(popup);
					popup.setPage(event.getPage());
					popup.open(event.getTarget());

				}
			});
		}
		if (!largeDataSupport) {
			List<TableColumnSortOrder> sortColumns = config.getColumnSortOrderList();
			for (int i = sortColumns.size() - 1; i >= 0; i--) {
				TableColumnSortOrder sortColumn = sortColumns.get(i);
				SortOrder order = sortColumn.getOrder();
				colSort.get(sortColumn).sort(SortOrder.ASCENDING.equals(order), true);
			}
		}
		listhead.addEventListener(ZulEvents.ON_COL_SIZE, new EventListener<Event>() {
			public void onEvent(Event event) throws Exception {
				changeHeaderWidth(event);
			}
		});

		log.debug("End...");

		return listhead;
	}

	protected void onListHeaderSort(Event event, AbstractDataTable<T> dataTable) {
		Listheader header = (Listheader) event.getTarget();
		TableColumnConfiguration columnConfig = (TableColumnConfiguration) header.getValue();
		if (columnConfig.isSortable()) {
			config.addColumnSortOrder(columnConfig);
			if (largeDataSupport) {
				Events.postEvent(Events.ON_SORT, dataTable, config);
			}
		}
	}

	protected void onClickHeaderSort(Event event, Boolean isAsc) {
		Listheader header = (Listheader) event.getTarget().getAttribute("ColHeader");
		header.sort(isAsc);
	}

	protected void changeHeaderWidth(Event event) {
		if (!(event instanceof ColSizeEvent))
			return;

		ColSizeEvent colSizeEvent = (ColSizeEvent) event;
		TableColumnConfiguration columnConfig = (TableColumnConfiguration) ((Listheader) listbox.getListhead()
				.getChildren().get(colSizeEvent.getColIndex())).getValue();
		columnConfig.setWidth(colSizeEvent.getWidth());

		Events.postEvent(ZulEvents.ON_COL_SIZE, this, null);
	}

	public int getColSize(int colIdx) {
		TableColumnConfiguration columnConfig = (TableColumnConfiguration) ((Listheader) listbox.getListhead()
				.getChildren().get(colIdx)).getValue();
		String width = columnConfig.getWidth();
		if (width != null && !width.equals("") && width.endsWith("px")) {
			int widthValue = Integer.parseInt(width.substring(0, width.length() - 2));
			return widthValue;
		}

		return -1;
	}

	public void onDrop(DropEvent evt) {
		// check if that's a column
		if (!(evt.getDragged() instanceof Listheader) || !(evt.getTarget() instanceof Listheader))
			return;

		Listheader selected = (Listheader) evt.getDragged();
		Listheader target = (Listheader) evt.getTarget();

		int targetIdx = config.getColumns().indexOf(target.getValue());

		// Re-position the item to the correct index
		config.getColumns().remove(selected.getValue());
		config.getColumns().add(targetIdx, (TableColumnConfiguration) selected.getValue());

		// Update header display
		refreshListhead();

		// Force re-rendering of the model
		listbox.setModel(listbox.getModel());
	}

	protected TablePreferenceDialog getConfigDialog() {
		if (prefDialog != null) {
			prefDialog.setPage(getPage());
			// Column sequence, width, or visibility has changed!
			prefDialog.setModel(config);
			return prefDialog;
		}

		prefDialog = new TablePreferenceDialog();
		prefDialog.setPage(getPage());
		prefDialog.setModel(config);

		final CommonDataTable<T> dataTableComponent = this;

		prefDialog.addEventListener(Events.ON_OK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				// Apply the settings to the GUI
				setOrderedAttributes(prefDialog.getModel());

				// Close the window!
				prefDialog.setVisible(false);
			}
		});

		prefDialog.addEventListener(Events.ON_CANCEL, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				// Close the window!
				prefDialog.setVisible(false);
			}
		});

		prefDialog.addEventListener(EntityCommonFunctionEvents.ON_RESET, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				config = getDefaultConfiguration();
				prefDialog.setModel(config);
			}
		});

		prefDialog.addEventListener(EntityCommonFunctionEvents.ON_SAVE, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				// Apply the settings to the GUI
				setOrderedAttributes(prefDialog.getModel());
				Events.sendEvent(ON_SAVE_TABLE_CONFIG, dataTableComponent, null);
			}
		});

		return prefDialog;
	}

	public void refreshResultTable() {
		refreshResultTable(true);
	}

	public void refreshResultTable(boolean refreshHeader) {
		if (refreshHeader)
			refreshListhead();
		listbox.renderAll();
	}

	@Override
	public void setDataTableModel(ListModel<T> model) {
		selectionListCache = null;
		boolean isMultiple = listbox.isMultiple();
		if (model instanceof AbstractListModel) {
			AbstractListModel<T> modelImpl = (AbstractListModel<T>) model;
			modelImpl.setMultiple(isMultiple);
		}

		listbox.setModel(model);
		// The list model my reset the multi-selection value
		listbox.clearSelection();
		listbox.setMultiple(isMultiple);
		if (largeDataSupport && listbox.getPaginal() != null) {
			paging.setPageSize(listbox.getPaginal().getPageSize());
		}

		// apply sorting
		refreshResultTable();
		updateButtonBar();
	}

	public void setOrderedAttributes(TableConfiguration config) {
		config.setColumns(config.getColumns());

		// Update header display
		refreshListhead();

		// Force re-rendering of the model
		// dataTableList.renderAll();
		listbox.setModel(listbox.getModel());

		// Update minimum column width!
		// ensureColumnWidth(newLH);
	}

	public List<TableColumnConfiguration> getOrderedAttributes() {
		if (config == null)
			return null;

		return config.getColumns();
	}

	protected TableConfiguration getDefaultConfiguration() throws Exception {
		TableConfiguration defaultConfig = getSystemPreferences();
		defaultConfig = applyOrganizationUnitPreferences(defaultConfig);

		return defaultConfig;
	}

	protected TableConfiguration getSystemPreferences() throws Exception {
		String xmlContent = getSystemResultConfig();
		if (xmlContent == null || xmlContent.equals("")) {
			return null;
		}

		TableConfiguration config = configLoader.loadSingleConfig(xmlContent);

		return config;
	}

	private TableConfiguration applyOrganizationUnitPreferences(TableConfiguration systemConfig) throws Exception {
		String xmlContent = getOrgUnitResultConfig();
		if (xmlContent == null || xmlContent.equals("")) {
			return systemConfig;
		}

		TableConfiguration orgUnitConfig = configLoader.loadSingleConfig(xmlContent);
		if (orgUnitConfig == null) {
			return systemConfig;
		}

		return mergeTableConfiguration(systemConfig, orgUnitConfig);
	}

	public TableColumnConfiguration findColumnConfigByLabel(List<TableColumnConfiguration> columnConfigList,
			String label) {

		for (int i = 0; i < columnConfigList.size(); i++) {
			TableColumnConfiguration config = columnConfigList.get(i);
			if (config.getLabel().equals(label)) {
				return config;
			}
		}

		return null;
	}

	public TableColumnConfiguration findColumnConfigByName(List<TableColumnConfiguration> columnConfigList,
			String name) {

		for (int i = 0; i < columnConfigList.size(); i++) {
			TableColumnConfiguration config = columnConfigList.get(i);
			if (config.getName().equals(name)) {
				return config;
			}
		}

		return null;
	}

	private TableConfiguration applyUserPreferences(TableConfiguration organizationUnitConfig) throws Exception {
		String xmlContent = "";
		if (useConfiguration) {
			xmlContent = getUserResultConfig();
		}
		if (xmlContent.equals("")) {
			return organizationUnitConfig;
		}

		TableConfiguration userConfig = configLoader.loadSingleConfig(xmlContent);
		if (userConfig == null) {
			return organizationUnitConfig;
		}

		return mergeUserTableConfiguration(organizationUnitConfig, userConfig);
	}

	public Listitem getSelectedItem() {
		return listbox.getSelectedItem();
	}

	public List<Listitem> getSelectedItems() {
		return getSelectedItems(false);
	}

	public List<Listitem> getSelectedItems(boolean forceRefresh) {
		if (selectionListCache == null || forceRefresh) {
			selectionListCache = new ArrayList<>();
			int selectedCount = listbox.getSelectedCount();
			for (Listitem item : listbox.getItems()) {
				if (item.isSelected())
					selectionListCache.add(item);
				if (selectionListCache.size() >= selectedCount)
					break;
			}
		}
		return selectionListCache;
	}

	public ListModel<T> getResultModel() {
		return listbox.getModel();
	}

	public void setEntriesFoundNumber(String value) {
		entriesFoundNumber.setValue(value);
	}

	public void setEntriesFoundTimestamp(String value) {
		entriesFoundTimestamp.setValue(value);
	}

	public void setHeaderInfoVisible(boolean visible) {
		headerInfoContainer.setVisible(visible);
	}

	public boolean getHeaderInfoVisible() {
		return headerInfoContainer.isVisible();
	}

	public String getUserResultConfig() {
		return userConfigXmlString;
	}

	public void setUserResultConfig(String resultConfiguration) {
		this.userConfigXmlString = resultConfiguration;
		this.useConfiguration = true;
		try {
			config = applyUserPreferences(config);
		} catch (Exception e) {
			log.error("Failed to apply user preference", e);
		}
		refreshListhead();
	}

	public void setSystemResultConfig(String systemConfigXmlString) {
		this.systemConfigXmlString = systemConfigXmlString;
		try {
			config = getSystemPreferences();
		} catch (Exception e) {
			log.error("Failed to fetch system preferences", e);
		}
		refreshListhead();
	}

	public String getSystemResultConfig() {
		return systemConfigXmlString;
	}

	public void setOrgUnitResultConfig(String orgUnitConfigXmlString) {
		this.orgUnitConfigXmlString = orgUnitConfigXmlString;
		try {
			config = applyOrganizationUnitPreferences(config);
		} catch (Exception e) {
			log.error("Failed to apply organization unit preferences", e);
		}
		refreshListhead();
	}

	public String getOrgUnitResultConfig() {
		return orgUnitConfigXmlString;
	}

	public TableConfiguration getUserResultConfigModel() {
		return config;
	}

	class FindPopup extends Window {

		private static final long serialVersionUID = 3971792077217680736L;

		private Listheader ll;
		private Textbox tb;
		private Button bCancel;
		private Button bFind;
		private Listbox csr;
		private int currentSelected;
		private Radio ra1;
		private Radio ra2;
		private Checkbox cscb;
		private Checkbox emcb;
		private int colIdx;

		public FindPopup(TableColumnConfiguration columnConfig, Listbox dataTable, Listheader listHeader) {
			this.csr = dataTable;
			colIdx = config.getColumns().indexOf(columnConfig);
			ll = listHeader;
			currentSelected = -1;
			Table g = new Table();

			// TR
			Tr tr1 = new Tr();
			// TD1
			Td td1 = new Td();
			Label colName = new Label(ll.getLabel());
			colName.setParent(td1);
			td1.setParent(tr1);
			// TD2
			Td td2 = new Td();
			tb = new Textbox();
			tb.setParent(td2);
			tb.setHflex("true");
			td2.setParent(tr1);
			// /TR
			tr1.setParent(g);

			// TR
			Tr tr2 = new Tr();
			// TD1
			Td td3 = new Td();
			Label sortName = new Label(ZkLabels.getLabel("general.direction"));
			sortName.setParent(td3);
			td3.setParent(tr2);

			Td td4 = new Td();
			Radiogroup rg = new Radiogroup();
			rg.setHflex("true");
			rg.setParent(td4);

			Table _g = new Table();
			Tr _tr1 = new Tr();
			Tr _tr2 = new Tr();
			Td _td1 = new Td();
			Td _td2 = new Td();

			ra1 = new Radio(ZkLabels.getLabel("general.direction.forward"));
			ra1.setParent(_td1);

			ra2 = new Radio(ZkLabels.getLabel("general.direction.backward"));
			ra2.setParent(_td2);

			_td1.setParent(_tr1);
			_td2.setParent(_tr2);

			_tr1.setParent(_g);
			_tr2.setParent(_g);

			_g.setParent(rg);
			// /TR
			td4.setParent(tr2);
			tr2.setParent(g);

			// TR
			Tr tr4 = new Tr();
			Td td41 = new Td();
			Label optionsName = new Label(ZkLabels.getLabel("general.options"));
			optionsName.setParent(td41);
			td41.setParent(tr4);

			Td td42 = new Td();
			Table _g2 = new Table();
			Tr _tr12 = new Tr();
			Td tdaa = new Td();
			// Exact
			emcb = new Checkbox(ZkLabels.getLabel("general.search.exactMatch"));
			emcb.setParent(tdaa);
			tdaa.setParent(_tr12);
			_tr12.setParent(_g2);

			Tr _tr13 = new Tr();
			Td tdaa2 = new Td();
			// Case
			tdaa2.setParent(_tr13);
			cscb = new Checkbox(ZkLabels.getLabel("general.search.caseSensitive"));
			cscb.setParent(tdaa2);
			_tr13.setParent(_g2);

			_g2.setParent(td42);
			td42.setParent(tr4);

			tr4.setParent(g);

			// Buttons
			Tr tr3 = new Tr();
			Td td5 = new Td();
			td5.setParent(tr3);

			Hbox td6 = new Hbox();
			td6.setPack("end");
			td6.setWidth("100%");

			// Button Find
			bFind = new Button(ZkLabels.getLabel("general.find"));
			bFind.setParent(td6);

			// Button Cancel
			bCancel = new Button(ZkLabels.getLabel("general.cancel"));
			bCancel.setParent(td6);
			td6.setParent(tr3);
			// /TR
			tr3.setParent(g);

			g.setParent(this);
		}

		public void show() {
			this.setPage(ll.getPage());
			this.setLeft("40%");
			this.setTop("40%");
			this.doOverlapped();
			tb.setFocus(true);
			ra1.setSelected(true);

		}

		public void initEvents() {
			final Component root = this;

			this.addEventListener(Events.ON_OPEN, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					tb.setFocus(true);
					ra1.setSelected(true);
				}
			});

			this.addEventListener(Events.ON_OK, new EventListener<Event>() {

				@Override
				public void onEvent(Event event) throws Exception {
					Events.sendEvent(bFind, new Event(Events.ON_CLICK));
				}
			});
			this.addEventListener(Events.ON_CANCEL, new EventListener<Event>() {

				@Override
				public void onEvent(Event event) throws Exception {
					Events.sendEvent(new Event(Events.ON_CLICK, bCancel, null));
				}
			});
			bCancel.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					root.setVisible(false);

				}
			});

			bFind.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					String searchText = tb.getText();
					ListModel<Object> lm = csr.getModel();
					int idx = getNextMatchIndex(lm, searchText);
					if (idx != -1) {
						csr.setSelectedIndex(idx);
						currentSelected = idx;
					}
				}

				private int getNextMatchIndex(ListModel<Object> lm, String txt) throws InvalidMethodPathExpression {
					int idx = -1;
					// Get current selection...
					currentSelected = csr.getSelectedIndex();

					if (ra1.isSelected()) {
						if (currentSelected == -1)
							idx = 0;
						else
							idx = currentSelected + 1;

						for (int i = idx; i < lm.getSize(); i++) {
							Object o = lm.getElementAt(i);
							// Get the field value
							String rmMethod = config.getColumns().get(colIdx).getRetrievingMethod();
							String attributeName = config.getColumns().get(colIdx).getName();
							String v = ""
									+ ObjectPropertyUtil.getInstance().getPropertyValue(o, attributeName, rmMethod);
							if (matchHasBeenFound(v, txt, cscb.isChecked(), emcb.isChecked()))
								return i;
						}
					} else {
						if (currentSelected == -1)
							idx = lm.getSize() - 1;
						else
							idx = currentSelected - 1;
						for (int i = idx; i >= 0; i--) {
							Object o = lm.getElementAt(i);
							// Get the field value
							String rmMethod = config.getColumns().get(colIdx).getRetrievingMethod();
							String attributeName = config.getColumns().get(colIdx).getName();
							String v = ""
									+ ObjectPropertyUtil.getInstance().getPropertyValue(o, attributeName, rmMethod);
							if (matchHasBeenFound(v, txt, cscb.isChecked(), emcb.isChecked()))
								return i;
						}
					}
					return -1;
				}

				protected boolean matchHasBeenFound(String sCurCellValue, String sOrigFindWhat, boolean bMatchCase,
						boolean bExactMatch) {
					// STAGE 1: make sure current column not empty.
					if ((sCurCellValue == null) || (sCurCellValue.trim().equals("")) || (sOrigFindWhat == null)
							|| (sOrigFindWhat.trim().equals(""))) {
						return (false); // current column empty -> no match
					}

					String sFindWhat = new String(sOrigFindWhat);

					// STAGE 2: treat the match-case flag
					if (!(bMatchCase)) {
						// user has asked to IGNORE case-sensitive. now, since
						// the loop below
						// used the Java-metod 'indexOf', and since this
						// Java-metod 'indexOf'
						// DOES pay attention to case-sensitive (hard-coded,
						// without any
						// parameter to change it) - translate both string to
						// lower case in
						// order to ignore any possible case-sensitive
						// differences
						sFindWhat = sFindWhat.toLowerCase();
						sCurCellValue = sCurCellValue.toLowerCase();
					}

					sFindWhat = sFindWhat.trim();
					sCurCellValue = sCurCellValue.trim();

					// STAGE 3: look in current column for all 'sFindWhat'
					// sub-string
					// occurrences and search for a fully-match
					for (int iSubStringIndex = 0; (iSubStringIndex = sCurCellValue.indexOf(sFindWhat,
							iSubStringIndex)) != -1; iSubStringIndex += sFindWhat.length()) {
						// check all match-sub-strings (if there are any) until
						// a fully match
						// is found (fully match means against the case-sesitive
						// & exact-match)

						// STAGE 4: treat the exact-match flag
						if (bExactMatch) {
							// user has asked for an exact-match.
							// see if current result
							// fits the exact-match condition:
							// STAGE 4a: check the char before the sub-string
							if ((iSubStringIndex > 0)
									&& (!Character.isWhitespace(sCurCellValue.charAt(iSubStringIndex - 1)))) {
								// exact-match check failed -> no match -> look
								// for another sub-string
								continue;
							}

							// STAGE 4b: check the char after the sub-string
							if ((iSubStringIndex + sFindWhat.length() < sCurCellValue.length()) && (!Character
									.isWhitespace(sCurCellValue.charAt(iSubStringIndex + sFindWhat.length())))) {
								// exact-match check failed -> no match -> look
								// for another sub-string
								continue;
							}

						}
						return (true);
						// a fully-match (whichever it is) has been found
					}
					// all sub-strings sFindWhat which had been found in the
					// current-checked-column have not matched to some of the
					// terms.
					return (false);
				}

			});
		}
	}

	protected StateFunctionAssignment processCollectiveStateFunctionAssignments(StateFunctionAssignment sfa,
			StateFunctionAssignment nextSfa) {

		if (sfa == null || nextSfa == null)
			return sfa;

		sfa.setCommonActions(filterAssignmentItemList(sfa.getCommonActions(), nextSfa.getAllFunctions()));
		sfa.setMoreFunctions(filterAssignmentItemList(sfa.getMoreFunctions(), nextSfa.getAllFunctions()));
		sfa.setSpecialFunctions(filterAssignmentItemList(sfa.getSpecialFunctions(), nextSfa.getAllFunctions()));

		return sfa;
	}

	@Override
	public int getDataTablePageSize() {
		int pageSize = listbox.getPaginal().getPageSize();
		return pageSize;
	}

	@Override
	public int getDataTableActivePage() {
		if (largeDataSupport)
			return paging.getActivePage();
		else {
			if (listbox.getPaginal() != null) {
				return listbox.getPaginal().getActivePage();
			}
		}

		return 0;
	}

	@Override
	public void setDataTableActivePage(int activePageIndex) {
		if (largeDataSupport) {
			if (activePageIndex < paging.getPageCount() && activePageIndex >= 0) {
				paging.setActivePage(activePageIndex);
			} else {
				paging.setActivePage(paging.getPageCount() - 1);
			}
		} else {
			if (listbox.getPaginal() != null) {
				if (activePageIndex < listbox.getPaginal().getPageCount()) {
					listbox.getPaginal().setActivePage(activePageIndex);
				} else {
					listbox.getPaginal().setActivePage(listbox.getPaginal().getPageCount() - 1);
				}
			}
		}
	}

	public void setResultTotal(int totalSize) {
		paging.setTotalSize(totalSize);
	}

	public void setButtonBarModel(ButtonBarModel buttonBarModel) {
		// ButtonBarModel oldModel = this.buttonBarModel;
		this.buttonBarModel = buttonBarModel;
		// reRegisterEventForwarding(oldModel, this.buttonBarModel);
	}

	protected ButtonBarModel getButtonBarModel() {
		return buttonBarModel;
	}

	protected boolean hasSelection() {
		return listbox.getSelectedCount() > 0;
	}

	protected boolean isSingleSelected() {
		return listbox.getSelectedCount() == 1;
	}

	protected boolean isMultipleSelected() {
		return listbox.getSelectedCount() > 1;
	}

	protected CommonButtonBar getButtonBar() {
		return buttonBar;
	}

	public boolean isLargeDataSupport() {
		return largeDataSupport;
	}

	public void setLargeDataSupport(boolean largeDataSupport) {
		this.largeDataSupport = largeDataSupport;
		refreshPagingMechanism();
	}

	private void refreshPagingMechanism() {
		if (listbox.getPaginal() != null) {
			((Component) listbox.getPaginal()).setVisible(!largeDataSupport);
		}
		paging.setVisible(largeDataSupport);
	}

	public void setButtonBarModel(StateFunctionAssignment sfa, String[] objectStates) {
		StateFunctionAssignment copiedSfa = sfa.copy();
		copiedSfa.refresh();

		List<String> tobeAuthOperations = new ArrayList<>(copiedSfa.getTobeAuthorizedFunctionSet());

		if (tobeAuthOperations == null || tobeAuthOperations.isEmpty()) {
			buttonBar.setVisible(true);
			buttonBar.setModel(copiedSfa);
			return;
		}

		List<String> authorizedActions = getAuthorizedActions();

		buttonBar.setVisible(true);
		// Apply authorization to buttons
		copiedSfa.applyAuthorizedActions(authorizedActions);

		buttonBar.setModel(copiedSfa);
	}

	protected List<String> getAuthorizedActions() {
		return null;
	}

	protected void updateButtonStatusBar(String[] objectStates) {
		if (getButtonBarModel() == null)
			return;

		StateFunctionAssignment sfa = null;

		String assignmentKey = ButtonBarModelLoader.EMPTY_VALUE;
		SourceAccess access = SourceAccess.RESULT_EMPTY;
		if (hasSelection()) {
			access = SourceAccess.RESULT_SINGLE_SELECTION;
			if (isMultipleSelected()) {
				access = SourceAccess.RESULT_MULTI_SELECTION;
			}
		} else {
			if (listbox.getItemCount() > 0) {
				access = SourceAccess.RESULT_NOT_EMPTY;
			}
		}
		sfa = getButtonBarModel().getStateFunctionCatalog().get(access.toCode());

		if (sfa == null) {
			sfa = getDefaultStatusFunctionAssignment((ButtonBarModel) buttonBarModel);
		}

		if (sfa == null) {
			assignmentKey = ButtonBarModelLoader.EMPTY_VALUE;
			sfa = getButtonBarModel().getStateFunctionCatalog().get(assignmentKey);
		}

		// further modification/filtering on copy to avoid changes on the basis
		// model and thus
		// avoiding reloading from XML file.
		sfa = sfa.copy();

		// fill up common, more and special based on statusCommon, statusMore
		// and statusCommon
		sfa.setCommonActions(filterAssignmentItemList(sfa.getCommonActions(), objectStates));
		sfa.setMoreFunctions(filterAssignmentItemList(sfa.getMoreFunctions(), objectStates));
		sfa.setSpecialFunctions(filterAssignmentItemList(sfa.getSpecialFunctions(), objectStates));

		if (sfa != null)
			setButtonBarModel(sfa, objectStates);
	}

	/**
	 * Filter assignment item list according to the object states.
	 * 
	 * @param assignmentItemList
	 * @param objectStates
	 * @return
	 */
	private List<AssignmentItem> filterAssignmentItemList(List<AssignmentItem> assignmentItemList,
			String[] objectStates) {
		List<AssignmentItem> filteredList = new ArrayList<AssignmentItem>();
		for (AssignmentItem item : assignmentItemList) {
			if (item instanceof PatternFunctionAssignment) {
				String[] pattern = ((PatternFunctionAssignment) item).getPatterns();
				// IF MATCH...
				boolean addIt = true;
				if (pattern != null && pattern.length > 0) {
					// No pattern means no condition.
					for (int j = 0; j < objectStates.length; j++) {
						boolean onePatternMatch = false;
						for (int i = 0; i < pattern.length; i++) {
							String regexString = pattern[i];
							Pattern regexPatternObject = REGEX_PATTERN_CATALOG.get(regexString);
							if (regexPatternObject == null) {
								regexPatternObject = Pattern.compile(regexString);
							}
							Matcher matcher = regexPatternObject.matcher(objectStates[j]);
							if (matcher.matches()) {
								onePatternMatch = true;
								break;
							}
						}
						if (!onePatternMatch) {
							addIt = false;
							break;
						}
					}
				}
				if (addIt) {
					filteredList.add(((PatternFunctionAssignment) item).copy());
				}
			} else if (item instanceof FunctionSeparator) {
				if (filteredList.size() > 0
						&& !(filteredList.get(filteredList.size() - 1) instanceof FunctionSeparator)) {
					filteredList.add(item);
				}
			}
		}

		return filteredList;
	}

	/**
	 * Filter Assignment List against allowed action set.
	 * 
	 * @param assignmentItemList
	 *            assignment item list
	 * @param filterActionSet
	 *            set of action as constraint
	 * @return
	 */
	private List<AssignmentItem> filterAssignmentItemList(List<AssignmentItem> assignmentItemList,
			Set<String> filterActionSet) {
		if (filterActionSet == null)
			return assignmentItemList;

		List<AssignmentItem> filteredList = new ArrayList<AssignmentItem>();
		for (AssignmentItem item : assignmentItemList) {
			if (item instanceof FunctionAssignment) {
				String actionId = ((FunctionAssignment) item).getFunction().getId();
				if (filterActionSet.contains(actionId)) {
					filteredList.add(((FunctionAssignment) item).copy());
				}
			} else if (item instanceof FunctionSeparator) {
				if (filteredList.size() > 0
						&& !(filteredList.get(filteredList.size() - 1) instanceof FunctionSeparator)) {
					filteredList.add(item);
				}
			}
		}

		return filteredList;
	}

	protected void updateButtonBar(String[] objectStates) {
		try {
			if (getButtonBarModel() == null)
				return;

			StateFunctionAssignment sfa = null;

			SourceAccess access = SourceAccess.RESULT_EMPTY;
			String assignmentKey = "";
			if (hasSelection()) {
				String modelState = ButtonBarModelLoader.EMPTY_VALUE;
				access = SourceAccess.RESULT_SINGLE_SELECTION;
				if (isSingleSelected()) {
					modelState = objectStates[0];
					sfa = buttonModelLoader.findMatchingStateFunctionAssignment(getButtonBarModel(), modelState,
							access);
				} else {
					access = SourceAccess.RESULT_MULTI_SELECTION;
					if (objectStates.length == 1) {
						assignmentKey = modelState + ButtonBarModelLoader.STATE_ACCESS_SEPARATOR + access.toCode();
						sfa = getButtonBarModel().getStateFunctionCatalog().get(assignmentKey);
					} else {
						modelState = objectStates[0];
						sfa = buttonModelLoader.findMatchingStateFunctionAssignment(getButtonBarModel(), modelState,
								access);
						for (int i = 1; i < objectStates.length; i++) {
							modelState = objectStates[i];
							StateFunctionAssignment nextSfa = buttonModelLoader
									.findMatchingStateFunctionAssignment(getButtonBarModel(), modelState, access);
							sfa = processCollectiveStateFunctionAssignments(sfa, nextSfa);
						}
					}
				}
			} else {
				if (listbox.getItemCount() > 0) {
					access = SourceAccess.RESULT_NOT_EMPTY;
				}
				sfa = buttonModelLoader.findMatchingStateFunctionAssignment(getButtonBarModel(),
						ButtonBarModelLoader.EMPTY_VALUE, access);
			}

			if (sfa == null) {
				sfa = getDefaultStateFunctionAssignment();
			}

			if (sfa == null) {
				assignmentKey = ButtonBarModelLoader.EMPTY_VALUE;
				sfa = getButtonBarModel().getStateFunctionCatalog().get(assignmentKey);
			}

			if (sfa != null)
				setButtonBarModel(sfa, objectStates);
		} catch (RuntimeException e) {
			ExceptionLogger.registerInternalError(e.getMessage(), e);
			ExceptionLogger.showInternalError(getPage(), e.getMessage(), e);
		}
	}

	private StateFunctionAssignment getDefaultStateFunctionAssignment() {
		String assignmentKey = ButtonBarModelLoader.EMPTY_VALUE + "/" + ButtonBarModelLoader.EMPTY_VALUE
				+ ButtonBarModelLoader.STATE_ACCESS_SEPARATOR + ButtonBarModelLoader.EMPTY_VALUE;

		StateFunctionAssignment sfa = getButtonBarModel().getStateFunctionCatalog().get(assignmentKey);

		if (sfa == null) {
			assignmentKey = ButtonBarModelLoader.EMPTY_VALUE + ButtonBarModelLoader.STATE_ACCESS_SEPARATOR
					+ ButtonBarModelLoader.EMPTY_VALUE;
			sfa = getButtonBarModel().getStateFunctionCatalog().get(assignmentKey);
		}

		return sfa;
	}

	private StateFunctionAssignment getDefaultStatusFunctionAssignment(ButtonBarModel buttonBarModel) {
		String assignmentKey = ButtonBarModelLoader.EMPTY_VALUE;
		StateFunctionAssignment sfa = buttonBarModel.getStateFunctionCatalog().get(assignmentKey);
		return sfa;
	}

	@Override
	public void clearDataTable() {
		setDataTableModel(new ListModelList<T>(new ArrayList<T>()));
		selectionListCache = null;
		entriesFoundNumber.setValue("");
		entriesFoundTimestamp.setValue("");
		updateButtonBar();
	}

	@Override
	public void clearAttributeSortOrder() {
		config.resetSortOrder();
	}

	public void setPaging(boolean paging) {
		pageSizeLabel.setVisible(paging);
		pageSizeCombobox.setVisible(paging);

		if (paging) {
			listbox.setMold("paging");
			// dataTableList.setAutopaging(true);

			return;
		}

		listbox.setMold("default");
	}

	protected void updateButtonBar() {
		updateButtonBar(new String[] { ButtonBarModelLoader.EMPTY_VALUE });
	}

	protected Hlayout getHeaderInfoContainer() {
		return headerInfoContainer;
	}

	@Override
	public void clearPagingInfo() {
		if (paging.isVisible()) {
			paging.setPageSize(Integer.parseInt(pageSizeCombobox.getValue()));
			paging.setActivePage(0);
			paging.setTotalSize(0);
		}
	}

	public void clearSelection() {
		listbox.clearSelection();
	}

	public boolean isResultModelAssigned() {
		return listbox.getListModel() != null;
	}

	public boolean isEmpty() {
		return listbox.getListModel() == null || listbox.getListModel().getSize() == 0;
	}

	public List<Listitem> getAllItems() {
		return listbox.getItems();
	}

	public Listitem getItemAtIndex(int index) {
		return listbox.getItemAtIndex(index);
	}

	public void setEmptyMessage(String emptyMessage) {
		listbox.setEmptyMessage(emptyMessage);
	}

	public void setMultipleSelection(boolean multiSelection) {
		listbox.setMultiple(multiSelection);
	}

	private TableConfiguration mergeTableConfiguration(TableConfiguration highPriorityConfig,
			TableConfiguration lowPriorityConfig) {
		List<Integer> invalidOrgColumnConfigIndexList = new ArrayList<Integer>();
		for (int i = 0; i < lowPriorityConfig.getColumns().size(); i++) {
			TableColumnConfiguration lowPriorityColumnConfig = lowPriorityConfig.getColumns().get(i);
			TableColumnConfiguration highPriorityColumnConfig = findColumnConfigByName(highPriorityConfig.getColumns(),
					lowPriorityColumnConfig.getName());

			if (highPriorityColumnConfig == null) {
				invalidOrgColumnConfigIndexList.add(i);
				continue;
			}

			if (!lowPriorityColumnConfig.isAllowed()) {
				invalidOrgColumnConfigIndexList.add(i); // Not allowed here
			}

			if (lowPriorityColumnConfig.getAlign() == null
					|| !lowPriorityColumnConfig.getAlign().equals(highPriorityColumnConfig.getAlign())) {
				lowPriorityColumnConfig.setAlign(highPriorityColumnConfig.getAlign());
			}

			if (lowPriorityColumnConfig.getColumnFormat() == null
					|| !lowPriorityColumnConfig.getColumnFormat().equals(highPriorityColumnConfig.getColumnFormat())) {
				lowPriorityColumnConfig.setColumnFormat(highPriorityColumnConfig.getColumnFormat());
			}

			if (lowPriorityColumnConfig.getRelatedColumn() == null || !lowPriorityColumnConfig.getRelatedColumn()
					.equals(highPriorityColumnConfig.getRelatedColumn())) {
				lowPriorityColumnConfig.setRelatedColumn(highPriorityColumnConfig.getRelatedColumn());
			}

			if (lowPriorityColumnConfig.getRetrievingMethod() == null || !lowPriorityColumnConfig.getRetrievingMethod()
					.equals(highPriorityColumnConfig.getRetrievingMethod())) {
				lowPriorityColumnConfig.setRetrievingMethod(highPriorityColumnConfig.getRetrievingMethod());
			}

			if (lowPriorityColumnConfig.getSeleniumLabel() == null || !lowPriorityColumnConfig.getSeleniumLabel()
					.equals(highPriorityColumnConfig.getSeleniumLabel())) {
				lowPriorityColumnConfig.setSeleniumLabel(highPriorityColumnConfig.getSeleniumLabel());
			}

			if (lowPriorityColumnConfig.getSortColumnName() == null || !lowPriorityColumnConfig.getSortColumnName()
					.equals(highPriorityColumnConfig.getSortColumnName())) {
				lowPriorityColumnConfig.setSortColumnName(highPriorityColumnConfig.getSortColumnName());
			}

			if (lowPriorityColumnConfig.getTranslator() == null
					|| !lowPriorityColumnConfig.getTranslator().equals(highPriorityColumnConfig.getTranslator())) {
				lowPriorityColumnConfig.setTranslator(highPriorityColumnConfig.getTranslator());
			}
		}

		for (int i = invalidOrgColumnConfigIndexList.size() - 1; i >= 0; i--) {
			// Backward to avoid index shifting
			Integer invalidColumnIndex = invalidOrgColumnConfigIndexList.get(i);
			if (invalidColumnIndex != null)
				lowPriorityConfig.getColumns().remove(invalidColumnIndex.intValue());
		}

		return lowPriorityConfig;
	}

	private TableConfiguration mergeUserTableConfiguration(TableConfiguration highPriorityConfig,
			TableConfiguration lowPriorityConfig) {
		List<TableColumnConfiguration> newColumnConfigList = new ArrayList<TableColumnConfiguration>();
		TreeMap<Integer, TableColumnConfiguration> matchedColumnConfigMap = new TreeMap<Integer, TableColumnConfiguration>();
		for (int i = 0; i < highPriorityConfig.getColumns().size(); i++) {
			TableColumnConfiguration workingConfig = highPriorityConfig.getColumns().get(i);
			workingConfig = workingConfig.copy();

			TableColumnConfiguration lowPriorityColumnConfig = findColumnConfigByName(lowPriorityConfig.getColumns(),
					workingConfig.getName());

			if (lowPriorityColumnConfig == null) {
				newColumnConfigList.add(workingConfig);
				continue;
			}

			if (!workingConfig.isAllowed()) {
				continue; // skip
			}

			if (lowPriorityColumnConfig.getWidth() == null
					|| !lowPriorityColumnConfig.getWidth().equals(workingConfig.getWidth())) {
				workingConfig.setWidth(lowPriorityColumnConfig.getWidth());
			}

			if (lowPriorityColumnConfig.isVisible() != workingConfig.isVisible()) {
				workingConfig.setVisible(lowPriorityColumnConfig.isVisible());
			}

			int userColumnConfigIndex = lowPriorityConfig.getColumns().indexOf(lowPriorityColumnConfig);
			matchedColumnConfigMap.put(userColumnConfigIndex, workingConfig);
		}

		List<TableColumnConfiguration> mergedColumnConfigList = new ArrayList<TableColumnConfiguration>(
				matchedColumnConfigMap.values());
		mergedColumnConfigList.addAll(newColumnConfigList);

		lowPriorityConfig.setColumns(mergedColumnConfigList);

		return lowPriorityConfig;
	}

	protected void exportToExcel() {
		if (listbox.getItemCount() < 1) {
			MessageDialogUtils.showInfo(
					ZkLabels.getLabel("listbox.exportToExcel.empty.message",
							"Export cannot be started because there is no data in the list to export."),
					ZkLabels.getLabel("listbox.exportToExcel.empty.title", "Export to Excel"));
			return;
		}

		ExcelUtils util = new ExcelUtils();
		util.exportListbox(listbox, "Data Table", "Data Table");
	}

	protected String getObjectOwner(T object) {
		if (ownerColumnName == null) {
			try {
				TableConfiguration defaultConfig = GlobalPreferenceCatalog.retrieveGlobalPreference(
						TableConfiguration.class.getSimpleName(), getModule(), getEntity(),
						TableConfigurationUtil.DEFAULT_NAME);
				ownerColumnName = defaultConfig.getOwnerColumnName();
			} catch (Exception e1) {
				log.warn("No ownerColumnName defined for " + getEntity() + " in " + getModule());
			}
		}

		if (ownerColumnName == null) {
			return null;
		}
		TableColumnConfiguration ownerColumn = config.getColumnByName(ownerColumnName);
		if (ownerColumn != null) {
			try {
				return (String) propUtil.getPropertyValue(object, ownerColumn.getName(),
						ownerColumn.getRetrievingMethod());
			} catch (InvalidMethodPathExpression e) {
				log.warn("Fail retrieving ownerColumnName", e);
			}
		}

		return null;
	}

	protected String getLastModifiedUser(T object) {
		if (lastUserColumnName == null) {
			try {
				TableConfiguration defaultConfig = GlobalPreferenceCatalog.retrieveGlobalPreference(
						TableConfiguration.class.getSimpleName(), getModule(), getEntity(),
						TableConfigurationUtil.DEFAULT_NAME);
				lastUserColumnName = defaultConfig.getLastUserColumnName();
			} catch (Exception e1) {
				log.warn("No lastUserColumnName defined for " + getEntity() + " in " + getModule());
			}
		}

		if (lastUserColumnName != null) {
			TableColumnConfiguration lastUserColumn = config.getColumnByName(lastUserColumnName);
			if (lastUserColumn != null) {
				try {
					return "" + propUtil.getPropertyValue(object, lastUserColumn.getName(),
							lastUserColumn.getRetrievingMethod());
				} catch (InvalidMethodPathExpression e1) {
					log.warn("Fail retrieving lastUserColumnName.", e1);
				}
			}
		}

		return null;
	}

	public static final String createTableColumnHeaderKey(TableColumnConfiguration columnConfig) {
		return ZkLabels.createLabelKey(
				"listbox.column." + ZkLabels
						.convertToKeySegmentNaming(StringUtils.remove(columnConfig.getOwner().getEntity(), ' ')),
				columnConfig.getLabel());
	}

	public boolean isAllowToOpen() {
		return buttonBar.canPerformFunction(BaseSecurityConstants.ACT_VIEW.getName());
	}

	public void setButtonEventTargetComponent(Component targetComponent) {
		buttonBar.setTargetComponent(targetComponent);
	}

	public void resetSelectionCache() {
		selectionListCache = null;
	}

	public void showConfigDialog() {
		if (getPage() == null) {
			return;
		}

		TablePreferenceDialog dialog = getConfigDialog();
		dialog.setPage(getPage());
		dialog.doModal();
		Events.echoEvent(ApplicationEvents.ON_ASYNC_REFRESH, dialog, null);
	}

	public void setSelectedItems(List<Listitem> list) {
		for (Listitem item : list) {
			listbox.selectItem(item);
		}
	}

	public void setSelectedAll() {
		listbox.selectAll();
	}

	public void setInitialPageSize(int pageSize) {
		String effectiveStringValue = "50";
		if (pageSize <= 15) {
			effectiveStringValue = "10";
		} else if (pageSize <= 35) {
			effectiveStringValue = "20";
		} else if (pageSize <= 75) {
			effectiveStringValue = "50";
		} else if (pageSize <= 300) {
			effectiveStringValue = "100";
		} else {
			effectiveStringValue = "500";
		}
		pageSizeCombobox.setValue(effectiveStringValue);
		onPageSizeComboChange();
	}

	public TableControlLocation getTableControlLocation() {
		return tableControlLocation;
	}

	public void setTableControlLocation(String tableControlLocation) {
		if (tableControlLocation == null)
			return; // ignore
		try {
			setTableControlLocation(TableControlLocation.valueOf(tableControlLocation.toUpperCase()));
		} catch (Exception e) {
			// ignore -> keep the previous state
		}
	}

	public void setTableControlLocation(TableControlLocation tableControlLocation) {
		if (this.tableControlLocation.equals(tableControlLocation)) {
			return; // ignore
		}

		this.tableControlLocation = tableControlLocation;
		refreshTableControlLocation();
	}

	protected void refreshTableControlLocation() {
		if (headerInfoContainer == null || buttonBar == null || listbox == null) {
			return;
		}

		if (TableControlLocation.BOTTOM.equals(tableControlLocation)) {
			insertBefore(headerInfoContainer, buttonBar);
		} else {
			insertBefore(headerInfoContainer, listbox);
		}
		invalidate();
	}

	public static enum TableControlLocation {
		TOP, BOTTOM
	}
}
