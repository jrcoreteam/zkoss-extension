/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import org.zkoss.zul.Intbox;

public class IntegerWidgetCreator extends AbstractNumericWidgetCreator<Intbox, Integer> {

	private static final long serialVersionUID = 630340623904700321L;

    @Override
    protected Intbox newNumberInputWidget() {
	    Intbox ibox = new Intbox();
        ibox.setFormat("#,##0");
        return ibox;
    }

	@Override
    public Integer getWidgetValue(Intbox widget) {
	    return widget.getValue();
    }

	@Override
    public void setWidgetValue(Intbox widget, Integer value) {
	    widget.setValue(value);
    }

	@Override
    public void setWidgetValueFromString(Intbox widget, String stringValue) {
		Integer objectValue = null;
		try {
			objectValue = Integer.valueOf(stringValue);
		} catch (Exception e) {
			// do nothing
		}
		widget.setValue(objectValue);
    }

}
