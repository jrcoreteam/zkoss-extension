/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.constraint;

import java.io.Serializable;
import java.util.regex.Pattern;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Constraint;

public class SimpleInputElementConstraint implements Constraint, Serializable {

	private static final long serialVersionUID = 5956820394708774784L;

	private String regex;
	private String errorMessage;
	private Pattern pattern;

	public SimpleInputElementConstraint(String regex, String errorMessage) {
		super();
		this.regex = regex;
		this.errorMessage = errorMessage;
		this.pattern = Pattern.compile(regex);
	}

	@Override
	public void validate(Component comp, Object value) throws WrongValueException {
		String strValue = (String) value;

		if (strValue != null && strValue.length() > 0) {
			if (pattern != null) {
				if (!pattern.matcher(strValue).matches()) {
					throw new WrongValueException(comp, errorMessage == null ? "Illegal char '"
					        + strValue.charAt(strValue.length() - 1)
					        + "' found. Valid pattern expression for the field is " + regex : errorMessage);
				}
			}
		}
	}

}
