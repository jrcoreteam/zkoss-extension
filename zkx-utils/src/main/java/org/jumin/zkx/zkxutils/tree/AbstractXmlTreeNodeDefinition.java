/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jumin.common.xsutils.model.JrxTerm;

public abstract class AbstractXmlTreeNodeDefinition implements Serializable {

    private static final long serialVersionUID = 884058696014477869L;
    
    private List<String> xpathList;
    private Set<JrxTerm<?>> jrxTermSet;

    private String[][] namespacePrefixes;

    public AbstractXmlTreeNodeDefinition() {
        super();
        xpathList = new ArrayList<String>();
        jrxTermSet = new HashSet<JrxTerm<?>>();
    }
	
    protected Set<JrxTerm<?>> getJrxTermSet() {
        return jrxTermSet;
    }
	
    protected void setJrxTermSet(Set<JrxTerm<?>> jrxTermSet) {
        this.jrxTermSet = jrxTermSet;
    }
	
    public String[][] getNamespacePrefixes() {
        return namespacePrefixes;
    }
	
    public void setNamespacePrefixes(String[][] namespacePrefixes) {
        this.namespacePrefixes = namespacePrefixes;
    }
	
    @Override
    public int hashCode() {
        final int prime = 31; 
        int totalHashCode = 1;

        totalHashCode = prime * totalHashCode + ((xpathList == null) ? 0 : xpathList.hashCode());
        totalHashCode = prime * totalHashCode + ((jrxTermSet == null) ? 0 : jrxTermSet.hashCode());
        totalHashCode = prime * totalHashCode + ((namespacePrefixes == null) ? 0 : namespacePrefixes.hashCode());
		
        return totalHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

		if (!(obj instanceof AbstractXmlTreeNodeDefinition))
			return false;

		final AbstractXmlTreeNodeDefinition other = (AbstractXmlTreeNodeDefinition) obj;
		if (xpathList == null) {
			if (other.xpathList != null)
				return false;
		} else if (!xpathList.equals(other.xpathList)) {
			return false;
		}
		if (jrxTermSet == null) {
			if (other.jrxTermSet != null)
				return false;
		} else if (!jrxTermSet.equals(other.jrxTermSet)) {
			return false;
		}
		if (namespacePrefixes == null) {
			if (other.namespacePrefixes != null)
				return false;
		} else if (!namespacePrefixes.equals(other.namespacePrefixes)) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("xpathList: ").append(xpathList).append("\n");
		sb.append("jrxTermSet: ").append(jrxTermSet).append("\n");
		sb.append("namespacePrefixes: ").append(namespacePrefixes).append("\n");

		return sb.toString();
	}
	
    public String getXpath() {
        return xpathList.size() > 0 ? xpathList.get(0) : null;
    }
    
    public String[] getXpaths() {
        return xpathList.toArray(new String[] {});
    }
    
    public void addXpath(String xpath) {
        if (xpath == null) return;
        
        if (!xpathList.contains(xpath)) {
            xpathList.add(xpath);
        }
    }
    
    public void addXpath(String[] xpathArray) {
        if (xpathArray == null) return;
        
        for (int i = 0; i < xpathArray.length; i++) {
            String xpath = xpathArray[i];
            if (!xpathList.contains(xpath)) {
                xpathList.add(xpath);
            }
        }
    }

}
