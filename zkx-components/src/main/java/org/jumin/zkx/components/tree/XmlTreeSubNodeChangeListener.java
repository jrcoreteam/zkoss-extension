/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree;

import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.tree.XmlTreeEvent;
import org.jumin.zkx.zkxutils.tree.XmlTreerow;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Treeitem;

public class XmlTreeSubNodeChangeListener implements EventListener<Event> {

	public XmlTreeSubNodeChangeListener() {
		super();
	}

	@Override
	public void onEvent(Event event) throws Exception {
		XmlTreerow row = (XmlTreerow) ComponentUtils.getParentByClass(event.getTarget(), XmlTreerow.class);
		Events.postEvent(new XmlTreeEvent(XmlTreeEvent.ON_SUB_NODE_DEFINED, row.getTree(), (Treeitem) row.getParent()));
	}

}
