/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

public class WidgetStyleConstants {
	
	public static final String CSS_STYLE_CODE_UTILITY = "zkx-code-utility";
	
	public static final String CSS_STYLE_XML_TAG = "zkx-xml-tag";
	
	public static final String CSS_STYLE_XML_ATTR = "zkx-xml-attr";
	
	public static final String CSS_STYLE_XML_ATTR_VALUE = "zkx-xml-attr-value";

	public static final String EDIT_ICON = "zkx-edit-icon";
	
	public static final String FORM_GRID = "zkx-form-grid";
	
	public static final String TEXT_BUTTON = "zkx-text-button";

	public static final String SMALL_BUTTON = "zkx-small-button";

   public static final String BUTTON_NAKED_ICON_SMALL = "zkx-button-naked-icon-s";

	public static final String BUTTON_NAKED_ICON = "zkx-button-naked-icon";
	
	public static final String BUTTON_NAKED_ICON_LARGE = "zkx-button-naked-icon-l";
}
