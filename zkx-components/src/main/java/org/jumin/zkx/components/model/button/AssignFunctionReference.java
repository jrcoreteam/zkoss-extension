/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

import java.util.HashMap;
import java.util.Map;

import org.jumin.common.xmlutils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class AssignFunctionReference {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssignFunctionReference.class);

    public void assignFunctionReference(Document xmlButtonPositioningModel, Document xmlFunctionCatalog)
            throws Exception {
        NodeList xmlFunctionRefNodeList = XmlUtils.getNodeListByXPath(xmlButtonPositioningModel,
                "//" + ButtonBarModelLoader.TAG_FUNCTION);

        Map<String, String> lookupCache = new HashMap<String, String>();
        String functionXpath = "/" + ButtonBarModelLoader.TAG_FUNCTION_CATALOG + "/"
                + ButtonBarModelLoader.TAG_FUNCTION;
        for (int i = 0; i < xmlFunctionRefNodeList.getLength(); i++) {
            Element xmlFunctionRefNode = (Element) xmlFunctionRefNodeList.item(i);
            String functionLabel = xmlFunctionRefNode.getAttribute(ButtonBarModelLoader.ATTRIBUTE_IDREF);
            if (lookupCache.containsKey(functionLabel)) {
                xmlFunctionRefNode.setAttribute(ButtonBarModelLoader.ATTRIBUTE_IDREF,
                        lookupCache.get(functionLabel));
            } else {
                Element xmlFunctionDefNode = (Element) XmlUtils.getNodeByXPath(xmlFunctionCatalog,
                        functionXpath + "[@defaultLabel='" + functionLabel + "']");
                if (xmlFunctionDefNode != null) {
                    String functionId = xmlFunctionDefNode.getAttribute(ButtonBarModelLoader.ATTRIBUTE_ID);
                    lookupCache.put(functionLabel, functionId);
                    xmlFunctionRefNode.setAttribute(ButtonBarModelLoader.ATTRIBUTE_IDREF, functionId);
                } else {
                    xmlFunctionDefNode = (Element) XmlUtils.getNodeByXPath(xmlFunctionCatalog,
                            functionXpath + "[@id='" + functionLabel + "']");
                    if (xmlFunctionDefNode == null) {
                        LOGGER.info("No function label found: " + functionLabel);
                    }
                }
            }
        }
    }
}
