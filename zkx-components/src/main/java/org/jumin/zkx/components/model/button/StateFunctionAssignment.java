/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class StateFunctionAssignment implements Serializable, Cloneable {

    private static final long serialVersionUID = -3708919655581640500L;
    
    private String status;
    private SourceAccess sourceAccess;
    private List<AssignmentItem> specialFunctions;
    private List<AssignmentItem> commonActions;
    private List<AssignmentItem> moreFunctions;

    private SortedSet<CheckLevel> checkLevelAggregation;

    private Set<String> allFunctionSet;
    private Set<String> tobeAuthorizedFunctionSet;
    private List<String> workflowFunctionList;

    private final static String ACTION_SEPARATOR = ",";

    public StateFunctionAssignment() {
        super();
        specialFunctions = new ArrayList<AssignmentItem>();
        commonActions = new ArrayList<AssignmentItem>();
        moreFunctions = new ArrayList<AssignmentItem>();
        checkLevelAggregation = new TreeSet<CheckLevel>();
        workflowFunctionList = null;
        allFunctionSet = null;
        tobeAuthorizedFunctionSet = null;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SourceAccess getSourceAccess() {
        return sourceAccess;
    }

    public void setSourceAccess(SourceAccess sourceAccess) {
        this.sourceAccess = sourceAccess;
    }

    public List<AssignmentItem> getSpecialFunctions() {
        return specialFunctions;
    }

    public void setSpecialFunctions(List<AssignmentItem> specialFunctions) {
        this.specialFunctions = specialFunctions;
        workflowFunctionList = null;
        allFunctionSet = null;
        tobeAuthorizedFunctionSet = null;
    }

    public List<AssignmentItem> getCommonActions() {
        return commonActions;
    }

    public void setCommonActions(List<AssignmentItem> commonActions) {
        this.commonActions = commonActions;
        workflowFunctionList = null;
        allFunctionSet = null;
        tobeAuthorizedFunctionSet = null;
    }

    public List<AssignmentItem> getMoreFunctions() {
        return moreFunctions;
    }

    public void setMoreFunctions(List<AssignmentItem> moreFunctions) {
        this.moreFunctions = moreFunctions;
        workflowFunctionList = null;
        allFunctionSet = null;
        tobeAuthorizedFunctionSet = null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int totalHashCode = 1;

        totalHashCode = prime * totalHashCode + ((status == null) ? 0 : status.hashCode());
        totalHashCode = prime * totalHashCode + ((sourceAccess == null) ? 0 : sourceAccess.hashCode());

        return totalHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof StateFunctionAssignment))
            return false;

        final StateFunctionAssignment other = (StateFunctionAssignment) obj;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status)) {
            return false;
        }
        if (sourceAccess == null) {
            if (other.sourceAccess != null)
                return false;
        } else if (!sourceAccess.equals(other.sourceAccess)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("status: ").append(status).append("\n");
        sb.append("sourceAccess: ").append(sourceAccess).append("\n");

        return sb.toString();
    }

    public SortedSet<CheckLevel> getCheckLevelAggregation() {
        return checkLevelAggregation;
    }

    public StateFunctionAssignment copy() {
        StateFunctionAssignment clone = new StateFunctionAssignment();

        clone.status = status;
        clone.sourceAccess = sourceAccess;
        clone.checkLevelAggregation.addAll(checkLevelAggregation);

        clone.commonActions.addAll(copyAssignmentItem(commonActions));
        clone.moreFunctions.addAll(copyAssignmentItem(moreFunctions));
        clone.specialFunctions.addAll(copyAssignmentItem(specialFunctions));

        return clone;
    }

    protected List<FunctionAssignment> copyFunctionAssignment(List<FunctionAssignment> faList) {
        List<FunctionAssignment> copy = new ArrayList<FunctionAssignment>();

        for (FunctionAssignment fa : faList) {
            copy.add(fa.copy());
        }

        return copy;
    }

    protected List<AssignmentItem> copyAssignmentItem(List<AssignmentItem> assignmentList) {
        List<AssignmentItem> copy = new ArrayList<AssignmentItem>();

        for (AssignmentItem assignment : assignmentList) {
            if (assignment instanceof PatternFunctionAssignment) {
                copy.add(((PatternFunctionAssignment) assignment).copy());
            } else if (assignment instanceof FunctionAssignment) {
                copy.add(((FunctionAssignment) assignment).copy());
            } else {
                copy.add(assignment);
            }
        }

        return copy;
    }

    public List<String> getWorkflowFunctions() {
        if (workflowFunctionList == null) {
            refresh();
        }

        return workflowFunctionList;
    }

    public Set<String> getAllFunctions() {
        if (allFunctionSet == null) {
            refresh();
        }
        return allFunctionSet;
    }

    public void refresh() {
        checkLevelAggregation = new TreeSet<CheckLevel>();
        workflowFunctionList = new ArrayList<String>();
        allFunctionSet = new HashSet<String>();
        tobeAuthorizedFunctionSet = new HashSet<String>();

        refresh(commonActions);
        refresh(moreFunctions);
        refresh(specialFunctions);
    }

    private void refresh(List<AssignmentItem> assignmentItemList) {
        for (AssignmentItem ai : assignmentItemList) {
            if (ai instanceof FunctionAssignment) {
                FunctionAssignment functionAssignment = (FunctionAssignment) ai;
                if (!checkLevelAggregation.contains(functionAssignment.getCheckLevel())) {
                    checkLevelAggregation.add(functionAssignment.getCheckLevel());
                }

                String[] actionNames = functionAssignment.getFunction().getAction().split(ACTION_SEPARATOR);
                for (String actionName : actionNames) {
                    if (CheckLevel.WORKFLOW.equals(functionAssignment.getCheckLevel())
                            && !workflowFunctionList.contains(actionName)) {
                        workflowFunctionList.add(actionName);
                        tobeAuthorizedFunctionSet.add(actionName);
                    }

                    if (functionAssignment.getCheckLevel() != null
                            && functionAssignment.getCheckLevel().compareTo(CheckLevel.PERMISSION) >= 0
                            && !tobeAuthorizedFunctionSet.contains(actionName)) {
                        tobeAuthorizedFunctionSet.add(actionName);
                    }

                    if (!allFunctionSet.contains(actionName)) {
                        allFunctionSet.add(actionName);
                    }
                }
            }
        }
    }

    public Set<String> getTobeAuthorizedFunctionSet() {
        if (tobeAuthorizedFunctionSet == null) {
            refresh();
        }
        return tobeAuthorizedFunctionSet;
    }

    public void applyAuthorizedActions(List<String> authorizedActions) {
        for (AssignmentItem ai : specialFunctions) {
            if (ai instanceof FunctionAssignment) {
                checkFunctionAssignment((FunctionAssignment) ai, authorizedActions);
            }
        }
        for (AssignmentItem ai : commonActions) {
            if (ai instanceof FunctionAssignment) {
                checkFunctionAssignment((FunctionAssignment) ai, authorizedActions);
            }
        }

        for (AssignmentItem ai : moreFunctions) {
            if (ai instanceof FunctionAssignment) {
                checkFunctionAssignment((FunctionAssignment) ai, authorizedActions);
            }
        }
        return;
    }

    private void checkFunctionAssignment(FunctionAssignment fa, List<String> allowed) {
        if (CheckLevel.NONE.equals(fa.getCheckLevel()))
            return;

        // We check if at least one of the action is allowed
        String[] actions = fa.getFunction().getAction().split(ACTION_SEPARATOR);
        boolean atLeastOneActionAllowed = false;
        for (String action : actions) {
            if (allowed != null && allowed.contains(action)) {
                atLeastOneActionAllowed = true;
                break;
            }
        }
        if (!atLeastOneActionAllowed) {
            fa.setVisible(false);
        }
    }

}
