/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.jumin.common.utils.LocaleUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;

public class IsoDatetimeConstraint extends DatetimeConstraint {

	private static final long serialVersionUID = -2739882012419946433L;

    public IsoDatetimeConstraint() {
		this(LocaleUtils.TIMEZONE_UTC, Locale.getDefault());
	}
	
	public IsoDatetimeConstraint(TimeZone timezone, Locale locale) {
		super(timezone, locale);
	}

	@Override
	public void validate(Component comp, Object value) throws WrongValueException {
		if (isSkip()) {
			// skip validation
			return;
		}
		String textValue = (String) value;
		
		String[] dateStringValueArray = null;
		int dateRangeSeparatorPos = textValue.indexOf("..");
		if (dateRangeSeparatorPos >= 0) {
			dateStringValueArray = new String[] { textValue.substring(0, dateRangeSeparatorPos).trim(),
			        textValue.substring(dateRangeSeparatorPos + 2, textValue.length()).trim() };
		} else {
			dateStringValueArray = new String[] { textValue };
		}
		
		Date[] dates = null;
		List<Date> dateList = new ArrayList<Date>();
		for (String dateStringValue : dateStringValueArray) {
			if (parser.analyseDateValueWithKeyword(dateStringValue) != null) {
				Date parsedDate = parser.parseKeywordValue(dateStringValue, getTimezone());
				dateList.add(parsedDate);
			} else {
				dateList.add(parser.getDateFromISODateTime(dateStringValue));
			}
		}
		
		dates = dateList.toArray(new Date[0]);
		if (dates.length > 1 && dates[0] != null && dates[1] != null) {
			if (!dates[0].before(dates[1])) {
				throw new WrongValueException(comp, "Invalid date range specified. End date/time must be after start date/time.");
			}
		}
	}

}
