/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

public enum XmlTreeExpansionMode {
    EXPAND_SINGLE(0),
    EXPAND_MANDATORY(1),
    EXPAND_ALL(2);
    
    private final int value;

    XmlTreeExpansionMode(int value) {
    	this.value = value;
    }
    
	public static XmlTreeExpansionMode fromInt(int value) {
		if (value == 0)
			return EXPAND_SINGLE;
		if (value == 1)
			return EXPAND_MANDATORY;
		if (value == 2)
			return EXPAND_ALL;

		return null;
	}

	public static XmlTreeExpansionMode fromString(String value) {
		if (value.equalsIgnoreCase("Single"))
			return EXPAND_SINGLE;
		if (value.equalsIgnoreCase("Mandatory"))
			return EXPAND_MANDATORY;
		if (value.equalsIgnoreCase("All"))
			return EXPAND_ALL;

		return null;
	}

	public int toInt() {
		return value;
	}

	@Override
	public String toString() {
		if (value == 0)
			return "Single";
		if (value == 1)
			return "Mandatory";
		if (value == 2)
			return "All";

		return "";
	}

}
