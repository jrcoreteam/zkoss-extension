/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.showcase.widget;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jumin.common.utils.model.LabelDefinition;
import org.jumin.zkx.zkxutils.widget.DateWidgetCreator;
import org.jumin.zkx.zkxutils.widget.SimpleStringWidgetCreator;
import org.jumin.zkx.zkxutils.widget.WidgetAttributeDefinition;
import org.jumin.zkx.zkxutils.widget.WidgetGridLayoutCreator;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Spinner;
import org.zkoss.zul.Vlayout;

public class WidgetCreatorController extends SelectorComposer<Vlayout> {

	private static final long serialVersionUID = 7803069226874316985L;

	@Wire("div")
	private Div container;
	
	@Wire("spinner#columnCount")
	private Spinner columnCount;
	
	private WidgetGridLayoutCreator layoutCreator = new WidgetGridLayoutCreator();
	
	private List<WidgetAttributeDefinition<?>> wadList = new ArrayList<>();
	
	public void doAfterCompose(Vlayout comp) throws Exception {
		super.doAfterCompose(comp);
		initModel();
	}

	private void initModel() {
		wadList.add(new WidgetAttributeDefinition<String>("ID", new LabelDefinition("general.id", "ID"), String.class));
		wadList.add(new WidgetAttributeDefinition<String>("FIRST_NAME", new LabelDefinition("general.person.firstName", "First Name"), String.class));
		wadList.add(new WidgetAttributeDefinition<String>("LAST_NAME", new LabelDefinition("general.person.lastName", "Last Name"), String.class));
		//wadList.add(new WidgetAttributeDefinition<String>("TYPE", new LabelDefinition("general.type", "Type"), String.class, Lookupbox.class));
		wadList.add(new WidgetAttributeDefinition<String>("PLACE_OF_BIRTH", new LabelDefinition("general.person.placeOfBirth", "Place of Birth"), String.class));
		wadList.add(new WidgetAttributeDefinition<Date>("DATE_OF_BIRTH", new LabelDefinition("general.person.dateOfBirth", "Date of Birth"), Date.class, new DateWidgetCreator(), "120px"));
		wadList.add(new WidgetAttributeDefinition<Double>("SALARY", new LabelDefinition("general.person.salary", "Salary"), Double.class, "120px"));
		wadList.add(new WidgetAttributeDefinition<String>("REMARKS", new LabelDefinition("general.person.remarks", "Remarks"), String.class, new SimpleStringWidgetCreator(5)));
    };
	
    @Listen("onClick = button#renderButton")
    public void render() {
    	clearContainer();
    	
    	Grid grid = layoutCreator.createLayout(container, wadList, columnCount.getValue());
    	grid.setParent(container);
    }
	
    @Listen("onClick = button#rearrangeButton")
    public void rearrange() {
    	clearContainer();
    	
    	Grid grid = layoutCreator.arrangeLayout(container, wadList, columnCount.getValue());
    	grid.setParent(container);
    }
	
    private void clearContainer() {
    	while (container.getFirstChild() != null) {
    		container.removeChild(container.getFirstChild());
    	}
    }
}
