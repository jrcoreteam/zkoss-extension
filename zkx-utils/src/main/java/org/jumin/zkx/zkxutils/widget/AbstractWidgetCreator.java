/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.io.Serializable;

import org.zkoss.zk.ui.Component;

public abstract class AbstractWidgetCreator<W extends Component, T> implements Serializable {
	
	private static final long serialVersionUID = 1356879618198834719L;

    public abstract W createWidget(Component parent);
	
	public abstract T getWidgetValue(W widget);
	
	public abstract void setWidgetValue(W widget, T value);
	
	public abstract void setWidgetValueFromString(W widget, String stringValue);
}
