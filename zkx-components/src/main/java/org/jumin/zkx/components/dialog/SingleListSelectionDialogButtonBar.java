/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.dialog;

import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;

public class SingleListSelectionDialogButtonBar extends DialogButtonBar {
	
	private static final long serialVersionUID = 4873947436516447126L;

    public static final String ON_CLEAR = "onClear";
	
	protected Button clearButton;

	public SingleListSelectionDialogButtonBar() {
		super();
		clearButton.setLabel(ZkLabels.getLabel("general.clear", "Clear"));
	}
	
	public SingleListSelectionDialogButtonBar(String okButtonLabel, String cancelButtonLabel, String clearButtonLabel) {
		super(okButtonLabel, cancelButtonLabel);
		clearButton.setLabel(clearButtonLabel);
	}
	
	@Override
	protected void initProperties() {
	    super.initProperties();
	    
		if (!widgetDebugUtil.isTestingMode()) return;
		
		widgetDebugUtil.setLabel(clearButton, "selectionDialog.clear");
	}
	
	@Override
	protected void initEvents() {
	    super.initEvents();
	    
	    clearButton.addForward(Events.ON_CLICK, this, ON_CLEAR);
	}
	
	@Override
	protected void createContents() {
		clearButton = new Button();
		clearButton.setParent(this);
		
	    super.createContents();
	}
	
	public void setShowClearButton(boolean show) {
	    clearButton.setVisible(show);
	}
}
