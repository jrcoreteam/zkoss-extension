/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.renderer;

import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

public class StringListitemRenderer implements ListitemRendererWithHeader<String>{

	private static final long serialVersionUID = 6207303646633265697L;

    @Override
	public void render(Listitem item, String data, int index) throws Exception {
		item.setLabel("" + data);
		item.addForward(Events.ON_DOUBLE_CLICK, item.getListbox(), Events.ON_OK);
		item.setValue(data);
	}
	
	@Override
	public Listhead getListhead() {
		Listhead head = new Listhead();
		
		Listheader header = new Listheader();
		header.setParent(head);
		
		return head;
	}

}
