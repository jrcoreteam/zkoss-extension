/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

import java.io.Serializable;

public class UserFunction implements Serializable {
    private static final long serialVersionUID = -2751611487706462762L;

    private String id;
    private String action;
    private String labelKey;
    private String defaultLabel;
    private String eventName;
    private String image;
    private boolean visible = true;
    private boolean disabled = false;
    private CheckLevel checkLevel;

    public UserFunction() {
        super();
    }
	
    public String getId() {
        return id;
    }
	
    public void setId(String id) {
        this.id = id;
    }
	
    public String getLabelKey() {
        return labelKey;
    }
	
    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }
	
    public String getDefaultLabel() {
        return defaultLabel;
    }
	
    public void setDefaultLabel(String defaultLabel) {
        this.defaultLabel = defaultLabel;
    }
	
    public String getEventName() {
        return eventName;
    }
	
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
	
    public String getImage() {
        return image;
    }
	
    public void setImage(String image) {
        this.image = image;
    }
	
    public boolean isVisible() {
        return visible;
    }
	
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
	
    public boolean isDisabled() {
        return disabled;
    }
	
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
	
    public CheckLevel getCheckLevel() {
        return checkLevel;
    }
	
    public void setCheckLevel(CheckLevel checkLevel) {
        this.checkLevel = checkLevel;
    }

	public String getAction() {
	    return (action == null || "".equals(action)) ? id : action;
    }

	public void setAction(String action) {
	    this.action = action;
    }
	
    @Override
    public int hashCode() {
        final int prime = 31; 
        int totalHashCode = 1;

        totalHashCode = prime * totalHashCode + ((id == null) ? 0 : id.hashCode());
        totalHashCode = prime * totalHashCode + ((action == null) ? 0 : action.hashCode());
        totalHashCode = prime * totalHashCode + ((labelKey == null) ? 0 : labelKey.hashCode());
        totalHashCode = prime * totalHashCode + ((defaultLabel == null) ? 0 : defaultLabel.hashCode());
        totalHashCode = prime * totalHashCode + ((eventName == null) ? 0 : eventName.hashCode());
        totalHashCode = prime * totalHashCode + ((image == null) ? 0 : image.hashCode());
        totalHashCode += prime * totalHashCode + (visible ? 1231 : 1237);
        totalHashCode += prime * totalHashCode + (disabled ? 1231 : 1237);
        totalHashCode = prime * totalHashCode + ((checkLevel == null) ? 0 : checkLevel.hashCode());
		
        return totalHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

		if (!(obj instanceof UserFunction))
			return false;

		final UserFunction other = (UserFunction) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action)) {
			return false;
		}
		if (labelKey == null) {
			if (other.labelKey != null)
				return false;
		} else if (!labelKey.equals(other.labelKey)) {
			return false;
		}
		if (defaultLabel == null) {
			if (other.defaultLabel != null)
				return false;
		} else if (!defaultLabel.equals(other.defaultLabel)) {
			return false;
		}
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName)) {
			return false;
		}
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image)) {
			return false;
		}
		if (visible != other.visible)
			return false;
		if (disabled != other.disabled)
			return false;
		if (checkLevel == null) {
			if (other.checkLevel != null)
				return false;
		} else if (!checkLevel.equals(other.checkLevel)) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		sb.append("id: ").append(id).append("\n");
		sb.append("action: ").append(action).append("\n");
		sb.append("labelKey: ").append(labelKey).append("\n");
		sb.append("defaultLabel: ").append(defaultLabel).append("\n");
		sb.append("eventName: ").append(eventName).append("\n");
		sb.append("image: ").append(image).append("\n");
		sb.append("visible: ").append(visible).append("\n");
		sb.append("disabled: ").append(disabled).append("\n");
		sb.append("checkLevel: ").append(checkLevel).append("\n");

		return sb.toString();
	}
	
}
