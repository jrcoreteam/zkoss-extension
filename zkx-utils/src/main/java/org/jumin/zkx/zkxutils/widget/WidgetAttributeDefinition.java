/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.widget;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jumin.common.utils.model.AttributeDefinition;
import org.jumin.common.utils.model.LabelDefinition;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Label;

public class WidgetAttributeDefinition<T> extends AttributeDefinition<T> {

    private static final long serialVersionUID = 6580345131557263384L;

    private LabelDefinition labelDefinition;

    private Class<? extends Component> widgetClass;

    private Component widgetObject;

    private Label labelWidgetObject;

    private AbstractWidgetCreator<? extends Component, T> widgetCreator;

    private static final Map<Class<?>, Class<? extends AbstractWidgetCreator<?, ?>>> DEFAULT_WIDGET_CREATOR = new HashMap<>();

    private String width;

    private boolean showLabel = true;

    static {
        DEFAULT_WIDGET_CREATOR.put(Date.class, DateWidgetCreator.class);
        DEFAULT_WIDGET_CREATOR.put(String.class, SimpleStringWidgetCreator.class);
    }

    public WidgetAttributeDefinition(String name, Class<T> dataType) {
        this(name, dataType, (Class<? extends Component>) null);
    }

    public WidgetAttributeDefinition(String name, Class<T> dataType, String width) {
        this(name, dataType, (Class<? extends Component>) null, width);
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType) {
        this(name, labelDefinition, dataType, (Class<? extends Component>) null);
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType, String width) {
        this(name, labelDefinition, dataType, (Class<? extends Component>) null, width, true);
    }

    public WidgetAttributeDefinition(String name, Class<T> dataType, Class<? extends Component> widgetClass) {
        this(name, dataType, widgetClass, "true");
    }

    public WidgetAttributeDefinition(String name, Class<T> dataType, Class<? extends Component> widgetClass,
            String width) {
        this(name, new LabelDefinition(name, ""), dataType, widgetClass, width, true);
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType,
            Class<? extends Component> widgetClass) {
        this(name, labelDefinition, dataType, widgetClass, "true");
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType,
            Class<? extends Component> widgetClass, String width) {
        this(name, labelDefinition, dataType, widgetClass, width, true);
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType,
            Class<? extends Component> widgetClass, String width, boolean showLabel) {
        super(name, dataType);
        this.labelDefinition = labelDefinition;
        this.widgetClass = widgetClass;
        this.width = width;
        this.showLabel = showLabel;
    }

    public WidgetAttributeDefinition(String name, Class<T> dataType, Component widgetObject) {
        this(name, dataType, widgetObject, "true");
    }

    public WidgetAttributeDefinition(String name, Class<T> dataType, Component widgetObject, String width) {
        this(name, new LabelDefinition(name, ""), dataType, widgetObject, width, true);
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType,
            Component widgetObject, String width, boolean showLabel) {
        super(name, dataType);
        this.labelDefinition = labelDefinition;
        this.widgetObject = widgetObject;
        this.width = width;
        this.showLabel = showLabel;
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType,
            AbstractWidgetCreator<? extends Component, T> widgetCreator) {
        this(name, labelDefinition, dataType, widgetCreator, "true");
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType,
            AbstractWidgetCreator<? extends Component, T> widgetCreator, String width) {
        this(name, labelDefinition, dataType, widgetCreator, width, true);
    }

    public WidgetAttributeDefinition(String name, LabelDefinition labelDefinition, Class<T> dataType,
            AbstractWidgetCreator<? extends Component, T> widgetCreator, String width, boolean showLabel) {
        super(name, dataType);
        this.labelDefinition = labelDefinition;
        this.widgetCreator = widgetCreator;
        this.width = width;
        this.showLabel = showLabel;
    }

    public AbstractWidgetCreator<? extends Component, T> getWidgetCreator() {
        return widgetCreator;
    }

    public Class<? extends Component> getWidgetClass() {
        return widgetClass;
    }

    public Component getWidgetObject() {
        return widgetObject;
    }

    public void setWidgetObject(Component widgetObject) {
        this.widgetObject = widgetObject;
    }

    public LabelDefinition getLabelDefinition() {
        return labelDefinition;
    }

    public void setLabelDefinition(LabelDefinition labelDefinition) {
        this.labelDefinition = labelDefinition;
    }

    public String getDisplayLabel() {
        if ((labelDefinition.getLabelKey() == null || "".equals(labelDefinition.getLabelKey()))
                && (labelDefinition.getDefaultLabel() == null || "".equals(labelDefinition.getDefaultLabel()))) {
            return getName();
        }

        if (labelDefinition.getLabelKey() == null || "".equals(labelDefinition.getLabelKey())) {
            return labelDefinition.getDefaultLabel();
        }

        String displayLabel = ZkLabels.getLabel(labelDefinition.getLabelKey(), labelDefinition.getDefaultLabel());
        if ("".equals(displayLabel) && labelDefinition.getLabelKey() != null
                && !"".equals(labelDefinition.getLabelKey())) {
            return labelDefinition.getLabelKey();
        }

        return displayLabel;
    }

    public void setShowLabel(boolean showLabel) {
        this.showLabel = showLabel;
    }

    public boolean isShowLabel() {
        return showLabel;
    }

    public Label getLabelWidgetObject() {
        return labelWidgetObject;
    }

    public void setLabelWidgetObject(Label labelWidgetObject) {
        this.labelWidgetObject = labelWidgetObject;
    }

    /**
     * @param width
     *            can be in the form of pixel size, hflex number, or hflex status (true or false).
     */
    public void setWidth(String width) {
        this.width = width;
    }

    public String getWidth() {
        return width;
    }

    public WidgetAttributeDefinition<T> clone() {
        @SuppressWarnings("unchecked")
        WidgetAttributeDefinition<T> clone = new WidgetAttributeDefinition<T>(getName(), this.labelDefinition,
                (Class<T>) getDataType(), this.widgetClass, this.width, this.showLabel);
        clone.widgetCreator = this.widgetCreator;
        clone.setValue(null);

        return clone;
    }
}
