/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.exception;

import org.jumin.common.utils.exception.BusinessServiceRuntimeException;
import org.jumin.zkx.components.dialog.SystemErrorDialog;
import org.jumin.zkx.zkxutils.ApplicationConstants;
import org.jumin.zkx.zkxutils.events.ApplicationEvents;
import org.jumin.zkx.zkxutils.exception.InternalSystemError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueues;

public class ExceptionLogger {
	private static Logger log = LoggerFactory.getLogger(ExceptionLogger.class);
	
	public static final String ERROR_DIALOG_ID = "zkx.errorDialog";
	
	public static void registerInternalError(String message, Throwable throwable) {
		log.error(message, throwable);
		InternalSystemError ise = new InternalSystemError(message, throwable);
		EventQueues.lookup(ApplicationConstants.QUEUE_SESSION_INTERNAL_ERROR, EventQueues.SESSION, true)
		  .publish(new Event(ApplicationEvents.ON_INTERNAL_ERROR, null, ise));
	}

	public static void showInternalError(Page page, String message, Throwable throwable) {
		SystemErrorDialog errorDialog = (SystemErrorDialog) page.getFellowIfAny(ERROR_DIALOG_ID);
		
		if (errorDialog == null) {
			// avoid multi error message display
			errorDialog = new SystemErrorDialog();
			errorDialog.setId(ERROR_DIALOG_ID);
			errorDialog.setPage(page);
		}
		
		errorDialog.setErrorMessage(message);
		errorDialog.setException(throwable);
        errorDialog.doModal();
	}

    public static void showApplicationError(Page page, BusinessServiceRuntimeException applicationException) {
        SystemErrorDialog errorDialog = (SystemErrorDialog) page.getFellowIfAny(ERROR_DIALOG_ID);
        if (errorDialog == null) {
            // avoid multi error message display
            errorDialog = new SystemErrorDialog();
            errorDialog.setId(ERROR_DIALOG_ID);
            errorDialog.setPage(page);
        }
        errorDialog.setErrorMessage(applicationException.getMessage());
        errorDialog.setException(applicationException.getCause());
        errorDialog.setTitle(applicationException.getTitle());

        errorDialog.doModal();
    }
    
}
