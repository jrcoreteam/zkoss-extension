/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.tree.renderer;

import org.jumin.common.xsutils.JrxXmlModelUtil;
import org.jumin.common.xsutils.labels.XmlTagLabelUtil;
import org.jumin.common.xsutils.model.JrxElement;
import org.jumin.common.xsutils.model.JrxTerm;
import org.jumin.zkx.zkxutils.ComponentUtils;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;

public abstract class XmlTreeLabelCellRenderer {

	protected XmlTagLabelUtil tagLabelUtil = null;
	
	protected JrxXmlModelUtil jrxXmlUtil = null;
		
	public XmlTreeLabelCellRenderer(XmlTagLabelUtil tagLabelUtil) {
		this.tagLabelUtil = tagLabelUtil;
		jrxXmlUtil = new JrxXmlModelUtil();
    }

	public XmlTreeLabelCellRenderer() {
		jrxXmlUtil = new JrxXmlModelUtil();
	}

	public Treecell createFirstCell(String name, boolean isMandatory, boolean isRepetive) {
		String nodeName = getTagLabel(name, name);
		Label label = new Label(nodeName);
		label.setHflex("true");
		if (isMandatory == true) {
			label.setSclass("jrx-msg-tree-label-mandatory");
		}
		Treecell treecell = new Treecell();
		label.setParent(treecell);
		if (isRepetive == true) {
			treecell.setSclass("jrx-msg-tree-repetitive");
		}

		treecell.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				Treecell cell = (Treecell) event.getTarget();
				Treeitem item = ComponentUtils.getParentByClass(cell, Treeitem.class);
				JrxTerm<?> jrxTerm = item.getValue();
				if (jrxTerm instanceof JrxElement && ((JrxElement) jrxTerm).isLeaf())
					return; // Skip

				// Toggle collapse/expand
				boolean previousState = item.isOpen();
				item.setOpen(!previousState);
			}
		});

		return treecell;
	}

	public Treecell createFirstCell(JrxTerm<?> jrxTerm) {
		String nodeName = tagLabelUtil.getTagLabel(jrxTerm);
		return createFirstCell(nodeName, jrxTerm instanceof JrxElement ? jrxXmlUtil.isElementMandatory((JrxElement) jrxTerm) : jrxTerm
		                .isMandatory(), jrxTerm.isRepetitive());
	}

	/**
	 * Get tag label from the web resource bundle. <br>
	 * 
	 * @param key
	 *            key to be used for searching the resource bundle.
	 * @param defaultValue
	 *            value to return when there is nothing found.
	 * @return string value for tag label.
	 */
	public String getTagLabel(String key, String defaultValue) {
		String effectiveLabel = "";
		String searchKey = key;
		while (true) {
			effectiveLabel = ZkLabels.getLabel(XmlTagLabelUtil.TAG_LABEL_PREFIX + searchKey, "");

			if (!effectiveLabel.equals("")) {
				// tab label found in WEB-INF/i3-label[LANG].properties
				break;
			}

			if (searchKey.contains(XmlTagLabelUtil.LABEL_SEGMENT_SEPARATOR)) {
				searchKey = searchKey.substring(searchKey.indexOf(XmlTagLabelUtil.LABEL_SEGMENT_SEPARATOR) + 1);
			} else {
				effectiveLabel = ZkLabels.getLabel(searchKey, defaultValue);
				break;
			}
		}

		return effectiveLabel;
	}
}
