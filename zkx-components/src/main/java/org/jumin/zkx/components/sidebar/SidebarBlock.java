/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.sidebar;

import org.jumin.zkx.components.Toggle;
import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlNativeComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;

public class SidebarBlock extends Div {

	private static final long serialVersionUID = -4292698962007212561L;

	private Label titleLabel;

	private Toggle arrowIcon;

	private Div closeIcon;
	
	protected Div content;

	private boolean fillHeight = false;

	private boolean contentAlreadyCreated = false;

	protected HtmlNativeComponent headerTable;
	
	protected WidgetStaticLabelUtils widgetDebugUtil = null;

	public SidebarBlock() {
		super();
		widgetDebugUtil = WidgetStaticLabelUtils.getInstance();
		createContents();
		contentAlreadyCreated = true;
		initProperties();
		initEvents();
	}

	protected void initProperties() {
		setSclass("zkx-sidebar-navi-container");
		setHflex("true");

		closeIcon.setSclass("zkx-close-icon"); 
		closeIcon.setVisible(false);
		arrowIcon.setOpen(true);
	}

	protected void createContents() {
		createHeaderBlock();
		createContentBlock();
	}

	private void createHeaderBlock() {
		headerTable = new HtmlNativeComponent("table");
		headerTable.setDynamicProperty("class", "zkx-sidebar-group-header");
		headerTable.setPrologContent("<tbody><tr valign=\"middle\" style=\"height:20px;\">");
		headerTable.setEpilogContent("</tr></tbody>");
		headerTable.setParent(this);

		HtmlNativeComponent cell = new HtmlNativeComponent("td");
		cell.setDynamicProperty("class", "zkx-sidebar-group-header-cell-arrow");
		cell.setParent(headerTable);

		arrowIcon = new Toggle();
		arrowIcon.setParent(cell);

		cell = new HtmlNativeComponent("td");
		cell.setDynamicProperty("class", "zkx-sidebar-group-header-cell-title");
		cell.setParent(headerTable);

		titleLabel = new Label();
		titleLabel.setSclass("zkx-sidebar-group-header-text");
		titleLabel.setParent(cell);
		
		cell = new HtmlNativeComponent("td");
		cell.setParent(headerTable);
		
		closeIcon = new Div();
		closeIcon.setParent(cell);
	}

	private void createContentBlock() {
		content = new Div();
		content.setVflex("true");
		content.setSclass("zkx-sidebar-content-block");
		content.setParent(this);
	}

	protected void initEvents() {
		titleLabel.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				arrowIcon.setOpen(!arrowIcon.isOpen());
			}
		});

		arrowIcon.addEventListener(Events.ON_OPEN, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				toggleContentVisibility();
			}
		});
		
		closeIcon.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
			    hide();
			}
		});
	}

	private void toggleContentVisibility() {
		if (arrowIcon.isOpen()) {
			if (fillHeight) {
				setVflex("true");
			}
		} else {
			if (fillHeight) {
				setVflex("false");
			}
		}
		content.setVisible(arrowIcon.isOpen());
		if (getParent() != null)
			getParent().invalidate();
	}

	public void switchContentDisplay() {
		arrowIcon.setOpen(!arrowIcon.isOpen());
	}

	public void setTitle(String title) {
		titleLabel.setValue(title);
	}

	public String getTitle() {
		return titleLabel.getValue();
	}

    public void appendToolbarbutton(Div toolbarbutton) {
        HtmlNativeComponent cell = new HtmlNativeComponent("td");
        cell.setDynamicProperty("align", "right");
        cell.setParent(headerTable);

        cell.appendChild(toolbarbutton);
    }

    public void appendToolbarbutton(Button toolbarbutton) {
        HtmlNativeComponent cell = new HtmlNativeComponent("td");
        cell.setDynamicProperty("align", "right");
        cell.setParent(headerTable);

        cell.appendChild(toolbarbutton);
    }

	public void appendContent(Component component) {
		content.appendChild(component);
	}

	public void removeContent(Component component) {
		content.removeChild(component);
	}

	@Override
	public void onChildAdded(Component child) {
		if (contentAlreadyCreated) {
			child.setParent(content);
		} else {
			super.onChildAdded(child);
		}
	}

	public boolean isContentVisible() {
		return content.isVisible();
	}

	public void setFillHeight(boolean fillHeight) {
		this.fillHeight = fillHeight;
		setVflex("" + fillHeight);
	}

	public boolean isFillHeight() {
		return fillHeight;
	}
	
	public void setOpen(boolean open) {
		arrowIcon.setOpen(open);
		toggleContentVisibility();
	}
	
	public void hide() {
		setVisible(false);
	}
	
	public void setClosable(boolean closable) {
		closeIcon.setVisible(closable);
	}
	
	public boolean isClosable() {
		return closeIcon.isVisible();
	}
}
