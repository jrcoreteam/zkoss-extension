/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.IOException;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Column;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

public class ExcelUtils {
	public static final String JAVA_SYSTEM_VARIABLE_HEADLESS_MODE = "java.awt.headless";

	// For complex translated value as display -> the original value shall be available as simple value in this widget
	// attribute
	public static final String VAR_ZKUTILS_ORIGINAL_VALUE = "zkUtilsOriginalValue";
	public static final String VAR_ZKUTILS_STRING_FORMATTED_VALUE = "zkUtilsStringFormattedValue";

	public void exportGrid(Grid grid) {
		exportGrid(grid, "ZkUtilsExport");
	}

	public void exportGrid(Grid grid, String fileName) {
		exportGrid(grid, fileName, "Sheet1");
	}

	@SuppressWarnings("unused")
	public void exportGrid(Grid grid, String fileName, String sheetName) {
		String originalHeadlessMode = System.getProperty(JAVA_SYSTEM_VARIABLE_HEADLESS_MODE);
		if (originalHeadlessMode == null)
			originalHeadlessMode = "true";
		System.setProperty(JAVA_SYSTEM_VARIABLE_HEADLESS_MODE, "true");

		// create a new workbook
		Workbook excelWorkbook = new HSSFWorkbook();
		// create a new sheet
		Sheet excelSheet = excelWorkbook.createSheet();

		// create 3 cell styles
		CellStyle headerCellStyle = excelWorkbook.createCellStyle();
		headerCellStyle.setFillBackgroundColor((short) 0xddd);

		CellStyle valueCellStyle = excelWorkbook.createCellStyle();
		valueCellStyle.setWrapText(true);

		// create 2 fonts objects
		Font headerFont = excelWorkbook.createFont();
		Font valueFont = excelWorkbook.createFont();

		// set font 1 to 12 point type
		headerFont.setFontHeightInPoints((short) 12);
		// make it blue
		headerFont.setColor((short) 0xc);
		// make it bold
		// arial is the default font
		headerFont.setBold(true);

		headerCellStyle.setFont(headerFont);
		valueFont.setFontHeightInPoints((short) 10);
		valueFont.setColor((short) Font.COLOR_NORMAL);

		valueCellStyle.setFont(valueFont);

		// set the sheet name in Unicode
		excelWorkbook.setSheetName(0, sheetName);

		// declare a row object reference
		Row excelRow = null;
		// declare a cell object reference
		Cell excelCell = null;

		boolean hasColumn = false;
		// Create table header
		if (grid.getColumns() != null) {
			for (short colIndex = (short) 0; colIndex < grid.getColumns().getChildren().size(); colIndex++) {
				if (excelRow == null) {
					excelRow = excelSheet.createRow(0);
				}
				// create a numeric cell
				excelCell = excelRow.createCell(colIndex);

				Column zkColumn = (Column) grid.getColumns().getChildren().get(colIndex);
				String cellValue = zkColumn.getLabel();

				excelCell.setCellStyle(headerCellStyle);

				excelCell.setCellValue(cellValue);
				hasColumn = true;
			}
		}

		// Create values
		int rowOffset = hasColumn ? 1 : 0;
		int rownum;
		for (rownum = (short) 0; rownum < grid.getRows().getChildren().size(); rownum++) {
			// create a row
			excelRow = excelSheet.createRow(rownum + rowOffset);

			org.zkoss.zul.Row zkRow = (org.zkoss.zul.Row) grid.getRows().getChildren().get(rownum);

			for (short cellnum = (short) 0; cellnum < zkRow.getChildren().size(); cellnum++) {
				// create a numeric cell
				excelCell = excelRow.createCell(cellnum);

				Component cellComp = zkRow.getChildren().get(cellnum);
				String cellValue = "";
				if (cellComp instanceof Label) {
					cellValue = ((Label) cellComp).getValue();
				} else if (cellComp instanceof org.zkoss.zul.Cell) {
					Object complexTranslatedValue = cellComp.getAttribute(VAR_ZKUTILS_STRING_FORMATTED_VALUE);
					if (complexTranslatedValue != null) {
						cellValue = "" + complexTranslatedValue;
					} else {
						Label labelComp = ComponentUtils.getChildByClass(cellComp, Label.class, true);
						if (labelComp != null) {
							cellValue = labelComp.getValue();
						}
					}
				}

				excelCell.setCellStyle(valueCellStyle);
				excelCell.setCellValue(cellValue);
			}
		}

		if (excelSheet.getLastRowNum() > 0) {
			// auto-adjust width
			Row excelFirstRow = excelSheet.getRow(0);
			int columnIndex = 0;
			for (Cell cell : excelFirstRow) {
				excelSheet.autoSizeColumn(columnIndex);
				columnIndex++;
			}
		}

		System.setProperty(JAVA_SYSTEM_VARIABLE_HEADLESS_MODE, originalHeadlessMode);
		
		// write the workbook to the output stream
		// close our file (don't blow out our file handles
		// create a new stream
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			excelWorkbook.write(out);
			AMedia amedia = new AMedia(fileName, "xls", "application/file", out.toByteArray());
			Filedownload.save(amedia);
		} catch (IOException e) {
			throw new RuntimeException("Failure in providing download to Excel.", e);
		} finally {
		    if (excelWorkbook != null) {
		        try {
                    excelWorkbook.close();
                } catch (IOException e) {
                }
		    }
		    if (out != null) {
		        try {
                    out.close();
                } catch (IOException e) {
                }
		    }
		}
	}

	public void exportListbox(Listbox listbox) {
		exportListbox(listbox, "ZkUtilsExport");
	}

	public void exportListbox(Listbox listbox, String fileName) {
		exportListbox(listbox, fileName, "Sheet1");
	}

	@SuppressWarnings("unused")
	public void exportListbox(Listbox listbox, String fileName, String sheetName) {
		String originalHeadlessMode = System.getProperty(JAVA_SYSTEM_VARIABLE_HEADLESS_MODE);
		if (originalHeadlessMode == null)
			originalHeadlessMode = "true";
		System.setProperty(JAVA_SYSTEM_VARIABLE_HEADLESS_MODE, "true");

		// create a new workbook
		Workbook excelWorkbook = new HSSFWorkbook();
		// create a new sheet
		Sheet excelSheet = excelWorkbook.createSheet();

		// create 3 cell styles
		CellStyle headerCellStyle = excelWorkbook.createCellStyle();
		headerCellStyle.setFillBackgroundColor((short) 0xddd);

		CellStyle valueCellStyle = excelWorkbook.createCellStyle();
		valueCellStyle.setWrapText(true);

		// create 2 fonts objects
		Font headerFont = excelWorkbook.createFont();
		Font valueFont = excelWorkbook.createFont();

		// set font 1 to 12 point type
		headerFont.setFontHeightInPoints((short) 12);
		// make it blue
		headerFont.setColor((short) 0xc);
		// make it bold
		// arial is the default font
		headerFont.setBold(true);

		headerCellStyle.setFont(headerFont);
		valueFont.setFontHeightInPoints((short) 10);
		valueFont.setColor((short) Font.COLOR_NORMAL);

		valueCellStyle.setFont(valueFont);

		// set the sheet name in Unicode
		excelWorkbook.setSheetName(0, sheetName);

		// declare a row object reference
		Row excelRow = null;
		// declare a cell object reference
		Cell excelCell = null;

		boolean hasColumn = false;
		// Create table header
		if (listbox.getListhead() != null) {
			for (short colIndex = (short) 0; colIndex < listbox.getListhead().getChildren().size(); colIndex++) {
				if (excelRow == null) {
					excelRow = excelSheet.createRow(0);
				}
				// create a numeric cell
				excelCell = excelRow.createCell(colIndex);

				Listheader zkHeader = (Listheader) listbox.getListhead().getChildren().get(colIndex);
				String cellValue = zkHeader.getLabel();

				excelCell.setCellStyle(headerCellStyle);

				excelCell.setCellValue(cellValue);
				hasColumn = true;
			}
		}

		// Create values
		int rowOffset = hasColumn ? 1 : 0;
		int rownum;
		for (rownum = (short) 0; rownum < listbox.getItemCount(); rownum++) {
			// create a row
			excelRow = excelSheet.createRow(rownum + rowOffset);

			Listitem zkListitem = listbox.getItemAtIndex(rownum);

			for (short cellnum = (short) 0; cellnum < zkListitem.getChildren().size(); cellnum++) {
				// create a numeric cell
				excelCell = excelRow.createCell(cellnum);

				String cellValue = "";
				if (zkListitem.getChildren().size() == 1) {
					cellValue = zkListitem.getLabel();
				} else {
					Component cellComp = zkListitem.getChildren().get(cellnum);
					if (cellComp instanceof Label) {
						cellValue = ((Label) cellComp).getValue();
					} else if (cellComp instanceof Listcell) {
						Listcell zkCell = (Listcell) cellComp;
						cellValue = zkCell.getLabel();
						Object complexTranslatedValue = zkCell.getAttribute(VAR_ZKUTILS_STRING_FORMATTED_VALUE);
						if (cellValue == null || (cellValue.equals("") && (zkCell.getChildren().size() > 0 || complexTranslatedValue != null))) {
							if (complexTranslatedValue != null) {
								cellValue = "" + complexTranslatedValue;
							} else {
								Label labelComp = ComponentUtils.getChildByClass(cellComp, Label.class, true);
								if (labelComp != null) {
									cellValue = labelComp.getValue();
								}
							}
						}
					}
				}

				excelCell.setCellStyle(valueCellStyle);
				excelCell.setCellValue(cellValue);
			}
		}

		if (excelSheet.getLastRowNum() > 0) {
			// auto-adjust width
			Row excelFirstRow = excelSheet.getRow(0);
			int columnIndex = 0;
			for (Cell cell : excelFirstRow) {
				excelSheet.autoSizeColumn(columnIndex);
				columnIndex++;
			}
		}

		System.setProperty(JAVA_SYSTEM_VARIABLE_HEADLESS_MODE, originalHeadlessMode);

		// write the workbook to the output stream
		// close our file (don't blow out our file handles
		// create a new stream
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			excelWorkbook.write(out);
			AMedia amedia = new AMedia(fileName, "xls", "application/file", out.toByteArray());
			Filedownload.save(amedia);
			out.close();
		} catch (IOException e) {
            throw new RuntimeException("Failure in providing download to Excel.", e);
        } finally {
            if (excelWorkbook != null) {
                try {
                    excelWorkbook.close();
                } catch (IOException e) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
		}
	}
}
