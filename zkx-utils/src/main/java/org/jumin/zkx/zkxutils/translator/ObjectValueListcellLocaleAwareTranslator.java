/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.translator;

import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import org.zkoss.zul.impl.XulElement;

public abstract class ObjectValueListcellLocaleAwareTranslator<T> implements WidgetListcellLocaleAwareTranslator<HashMap<String, Object>> {

    private static final long serialVersionUID = 1227346580980119261L;

    @SuppressWarnings("unchecked")
    @Override
    public void translate(XulElement cell, Locale locale, TimeZone timeZone, HashMap<String, Object> value) {
        translate(cell, locale, timeZone, (T) value.get("object"), (String) value.get("extra"));
    }

    public abstract void translate(XulElement cell, Locale locale, TimeZone timeZone, T object, String extra);
}
