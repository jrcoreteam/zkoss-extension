/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.model.button;

public enum CheckLevel {
	NONE,
	PERMISSION,
	RESTRICTION,
	WORKFLOW;
	
	public String toString() {
		String lowerValue = name().toLowerCase();
		return lowerValue.substring(0,1).toUpperCase() + lowerValue.substring(1);
	}

	public static CheckLevel fromString(String value) {
		return fromString(value, NONE);
	}

	public static CheckLevel fromString(String value, CheckLevel defaultCheckLevel) {
		if (value == null) return defaultCheckLevel;
		
		String lowerValue = value.toLowerCase();
		if (lowerValue.startsWith("p")) {
			return PERMISSION;
		} else if (lowerValue.startsWith("r")) {
			return RESTRICTION;
		} else if (lowerValue.startsWith("w")) {
			return WORKFLOW;
		}
		
		return defaultCheckLevel;
	}
}
