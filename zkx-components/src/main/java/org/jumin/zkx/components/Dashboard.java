/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.East;
import org.zkoss.zul.Vlayout;

public class Dashboard extends East {

    private static final long serialVersionUID = 4327536040667196796L;

    private Vlayout layout;

    private boolean isInitialized = false;

    public Dashboard() {
        super();
        isInitialized = false;
        createContents();
        initProperties();
        isInitialized = true;
    }

    private void initProperties() {
        layout.setHflex("true");
        layout.setVflex("true");
        layout.setSpacing("0px");
    }

    private void createContents() {
        layout = new Vlayout();
        layout.setParent(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.zkoss.zk.ui.AbstractComponent#appendChild(org.zkoss.zk.ui.Component)
     */
    @Override
    public boolean appendChild(Component child) {
        if (isInitialized) {
            return layout.appendChild(child);
        }
        return super.appendChild(child);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.zkoss.zk.ui.AbstractComponent#removeChild(org.zkoss.zk.ui.Component)
     */
    @Override
    public boolean removeChild(Component child) {
        if (isInitialized) {
            return layout.removeChild(child);
        }
        return super.removeChild(child);
    }

    public Component getChild(int index) {
        if (isInitialized) {
            return (Component) layout.getChildren().get(index);
        }
        return (Component) super.getChildren().get(index);
    }

    public int getChildSize() {
        if (isInitialized) {
            return layout.getChildren().size();
        }
        return super.getChildren().size();
    }
}
