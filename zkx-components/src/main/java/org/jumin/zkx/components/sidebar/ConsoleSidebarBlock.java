/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.sidebar;

import org.jumin.zkx.zkxutils.ZkLabels;
import org.zkoss.zul.Textbox;

public class ConsoleSidebarBlock extends SidebarBlock {

	private static final long serialVersionUID = -563229037386551045L;

    public static final String DEFAULT_EXPANDED_HEIGHT = "120px";

	private Textbox consoleText;

	@Override
	protected void createContents() {
		super.createContents();

		consoleText = new Textbox();
		appendContent(consoleText);
	}
	
	@Override
	protected void initProperties() {
	    super.initProperties();
	    
	    setSclass("zkx-console-sidebarblock");

		setTitle(ZkLabels.getLabel("consoleSidebarBlock.title", "Console"));
		
		consoleText.setSclass("zkx-code-textbox");
		consoleText.setReadonly(true);
	    consoleText.setMultiline(true);
	    consoleText.setHflex("true");
	    consoleText.setVflex("true");

	    setHeight(DEFAULT_EXPANDED_HEIGHT);
	    if (!widgetDebugUtil.isTestingMode())
			return;

		widgetDebugUtil.setLabel(this, getClass().getSimpleName());
	}
	
	public void setText(String text) {
		consoleText.setText(text);
	}
}
