/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components.exception;

import org.jumin.zkx.zkxutils.WidgetStaticLabelUtils;
import org.jumin.zkx.zkxutils.ZkLabels;
import org.jumin.zkx.zkxutils.events.EntityCommonFunctionEvents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.HtmlNativeComponent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Window;

public class ObjectLockWarningDialog extends Window {

    private static final long serialVersionUID = 3749622383133084276L;

    public static Logger log = LoggerFactory.getLogger(ObjectLockWarningDialog.class);
    
    private Div icon;
    
    private Label messageLabel;
    
    private Button continueButton;
    
    private Button stealLockButton;
    
    private WidgetStaticLabelUtils widgetLabelUtil = WidgetStaticLabelUtils.getInstance();
    
    public ObjectLockWarningDialog() {
	    super();
	    
	    createContents();
	    initProperties();
	    initEvents();
    }
    
    private void initProperties() {
	    setTitle(ZkLabels.getLabel("objectLock.warningDialog.title", "Object Lock Warning"));
	    setBorder(true);
	    setWidth("500px");
	    setSizable(true);
	    
	    icon.setClass("zkx-system-warning-image");
	    continueButton.setLabel(ZkLabels.getLabel("general.continue", "Continue"));
	    
	    stealLockButton.setLabel(ZkLabels.getLabel("objectLock.takeOverLock", "Take-over lock"));
	    stealLockButton.setVisible(false);
	    
	    if (!widgetLabelUtil.isTestingMode()) {
	    	return;
	    }
	    
	    widgetLabelUtil.setLabel(this, "objectLockWarningDialog");
	    widgetLabelUtil.setLabel(continueButton, "continueButton");
	    widgetLabelUtil.setLabel(stealLockButton, "takeOverLockButton");
    }

	private void initEvents() {
	    continueButton.addForward(Events.ON_CLICK, this, Events.ON_OK);
	    stealLockButton.addForward(Events.ON_CLICK, this, EntityCommonFunctionEvents.ON_STEAL_LOCK);
    }

	private void createContents() {
		HtmlNativeComponent table = new HtmlNativeComponent("table");
		table.setStubonly("true");
		table.setDynamicProperty("style", "padding: 5px 5px 2px 5px; width: 100%;");
	    table.setParent(this);
	    
	    HtmlNativeComponent tbody = new HtmlNativeComponent("tbody");
	    tbody.setStubonly("true");
	    tbody.setParent(table);
	    
		HtmlNativeComponent row = new HtmlNativeComponent("tr");
		row.setStubonly("true");
		row.setDynamicProperty("style", "vertical-align: top;");
		row.setParent(tbody);
	    
		HtmlNativeComponent cell = new HtmlNativeComponent("td");
		cell.setStubonly("true");
		cell.setDynamicProperty("style", "width: 40px;");
		cell.setParent(row);
		
	    icon = new Div();
	    icon.setParent(cell);
	    
	    cell = new HtmlNativeComponent("td");
		cell.setStubonly("true");
		cell.setParent(row);
		
	    messageLabel = new Label(ZkLabels.getLabel("login.description", "Error message line 1\nError message line 2\nError message line 3"));
	    messageLabel.setParent(cell);
	    
	    Separator separator = new Separator();
	    separator.setBar(true);
	    separator.setStubonly("true");
	    separator.setParent(this);
	    
	    table = new HtmlNativeComponent("table");
		table.setStubonly("true");
		table.setDynamicProperty("style", "margin-top: 5px; width: 100%;");
		table.setDynamicProperty("border", "0");
		table.setDynamicProperty("cellpadding", "0");
		table.setDynamicProperty("cellspacing", "0");
	    table.setParent(this);
	    
	    tbody = new HtmlNativeComponent("tbody");
	    tbody.setStubonly("true");
	    tbody.setParent(table);
	    
		row = new HtmlNativeComponent("tr");
		row.setStubonly("true");
		row.setParent(tbody);
	    
		cell = new HtmlNativeComponent("td");
		cell.setStubonly("true");
		cell.setParent(row);
		
		cell = new HtmlNativeComponent("td");
		cell.setDynamicProperty("align", "right");
		cell.setStubonly("true");
		cell.setParent(row);
		
		stealLockButton = new Button();
		stealLockButton.setParent(cell);
		
	    continueButton = new Button();
	    continueButton.setParent(cell);
    }
	
	public void setStealLock(boolean stealLock) {
		stealLockButton.setVisible(stealLock);
	}
	
	public void setMessage(String message) {
		messageLabel.setValue(message);
	}
	
}
