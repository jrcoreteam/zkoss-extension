/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.components;

import org.apache.commons.lang3.StringUtils;
import org.jumin.zkx.components.dialog.DialogButtonBar;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

public class TextEditorWindow extends Window {

	private static final long serialVersionUID = -3455703613799482597L;

    public static final String TICKER = "1234567890";
	
	public static final int CONTENT_HEIGHT_OFFSET_CHROME = 20;
	
	public static final int CONTENT_HEIGHT_OFFSET_FF = 20;
	
	public static final int CONTENT_HEIGHT_OFFSET_IE = 0;
	
	public static final int CONTENT_WIDTH_OFFSET_CHROME = 8;
	
	public static final int CONTENT_WIDTH_OFFSET_FF = 8;
	
	public static final int CONTENT_WIDTH_OFFSET_IE = 28;
	
	public static final float TEXT_HEIGHT_CHROME = 14.5f;
	
	public static final float TEXT_HEIGHT_FF = 14.5f;
	
	public static final float TEXT_HEIGHT_IE = 14.5f;
	
	public static final float TEXT_WIDTH_CHROME = 6.5f;
	
	public static final float TEXT_WIDTH_FF = 6.5f;
	
	public static final float TEXT_WIDTH_IE = 6.5f;
	
	public static final int ROW_SIZE_TO_SCROLL = 10;
	
	private int columnSize = 35;

	private int rowSize = 4;

	private Label majorTickerLabel;

	private Label minorTickerLabel;

	protected Textbox contentBox;

	private Textbox lineNumberBox;

	private DialogButtonBar buttonBar;
	
	private Hlayout contentContainer;
	
	private Vlayout tickerContainer;

	private String browser = "ff";
	
	private int effectiveWidthOffset = CONTENT_WIDTH_OFFSET_FF;
	
	private int effectiveHeightOffset = CONTENT_HEIGHT_OFFSET_FF;
	
	private float effectiveTextHeight = TEXT_HEIGHT_FF;
	
	private float effectiveTextWidth = TEXT_WIDTH_FF;
	
	public TextEditorWindow() {
		super();

		createContents();
		initEvents();
		initProperties();

		applyConfig();
	}

	private void initProperties() {
		setTitle("Text Editor");
		setBorder(true);

		setSclass("zkx-text-editor");

		tickerContainer.setSclass("zkx-editor-ticker-container");
		tickerContainer.setSpacing("0");

		contentContainer.setSclass("zkx-editor-content-container");
		contentContainer.setSpacing("1px");
		contentContainer.setValign("top");
		contentContainer.setVflex("true");

		contentBox.setMultiline(true);
		contentBox.setRows(rowSize);
		contentBox.setCols(columnSize);
		contentBox.setHflex("true");
		contentBox.setSclass("zkx-contentbox");
		contentBox.setWidgetAttribute("wrap", "off");
		contentBox.setStyle("overflow-x:hidden;");

		lineNumberBox.setMultiline(true);
		lineNumberBox.setReadonly(true);
		lineNumberBox.setWidgetAttribute("tabindex", "-1");
		lineNumberBox.setSclass("zkx-line-number");

		majorTickerLabel.setPre(true);
	}

	private void initEvents() {
		buttonBar.addForward(Events.ON_OK, this, Events.ON_OK);
		buttonBar.addForward(Events.ON_CANCEL, this, Events.ON_CANCEL);
	}

	private void createContents() {
		Div mainLayout = new Div();
		mainLayout.setParent(this);
		mainLayout.setVflex("true");
		mainLayout.setSclass("zkx-main-layout");

		tickerContainer = new Vlayout();
		tickerContainer.setParent(mainLayout);

		majorTickerLabel = new Label();
		majorTickerLabel.setParent(tickerContainer);

		minorTickerLabel = new Label();
		minorTickerLabel.setParent(tickerContainer);

		contentContainer = new Hlayout();
		contentContainer.setParent(mainLayout);

		lineNumberBox = new Textbox();
		lineNumberBox.setParent(contentContainer);

		contentBox = createContentBox();
		contentBox.setParent(contentContainer);

		buttonBar = new DialogButtonBar();
		buttonBar.setParent(mainLayout);
	}

	public int getColumnSize() {
		return columnSize;
	}

	public void setColumnSize(int columnSize) {
		this.columnSize = columnSize;
	}

	public int getRowSize() {
		return rowSize;
	}

	public void setRowSize(int rowSize) {
		this.rowSize = rowSize;
	}

	public void applyConfig() {
		String majorTickerText = "";
		int effectiveColumnSize = columnSize;
		if (effectiveColumnSize < 1) {
			effectiveColumnSize = 200;
		}

		int effectiveRowSize = rowSize;
		if (effectiveRowSize < 1) {
			effectiveRowSize = 1000;
		}

		int majorTickerSize = effectiveColumnSize / 10;
		if (majorTickerSize < 1)
			majorTickerSize = 1;
		for (int i = 1; i <= majorTickerSize; i++) {
			majorTickerText += StringUtils.leftPad("" + i, 10);
		}
		majorTickerLabel.setValue(majorTickerText);
		minorTickerLabel.setValue(StringUtils.leftPad("", effectiveColumnSize + 5, TICKER));

		if (columnSize > 0) {
			int editorWidth = (int) (columnSize * effectiveTextWidth) + effectiveWidthOffset;
			contentBox.setCols(columnSize);
			contentBox.setWidth(editorWidth + "px");
			
			tickerContainer.setHflex(null);
			
			setWidth((editorWidth + 120) + "px");
			setSizable(false);
			
		} else {
			setWidth("480px");
			setSizable(true);
			
			tickerContainer.setHflex("true");
		}

		String lineNumberContent = "";
		for (int i = 1; i <= effectiveRowSize; i++) {
			String lineNoString = i + "\n";
			lineNumberContent += lineNoString;
		}
		if (rowSize > 1) {
			if (rowSize > ROW_SIZE_TO_SCROLL) {
				contentContainer.setVflex("true");
				setHeight("600px");
				setSizable(true);
			} else {
				contentContainer.setVflex(null);
				setHeight(null);
				setSizable(false);
			}

			int editorHeight = (int) (rowSize * effectiveTextHeight) + effectiveHeightOffset;
			
			contentBox.setRows(rowSize);
			contentBox.setHeight(editorHeight + "px");

			lineNumberBox.setHeight(editorHeight + "px");
		} else {
			contentContainer.setVflex("true");
			setHeight("600px");
			setSizable(true);
		}

		lineNumberBox.setText(lineNumberContent);
		invalidate();
	}

	@Override
	public void onPageAttached(Page newpage, Page oldpage) {
	    super.onPageAttached(newpage, oldpage);
	    
	    browser = newpage.getDesktop().getExecution().getBrowser();
	    
	    if ("ie".equalsIgnoreCase(browser)) {
	    	effectiveHeightOffset = CONTENT_HEIGHT_OFFSET_IE;
	    	effectiveWidthOffset = CONTENT_WIDTH_OFFSET_IE;
	    	effectiveTextHeight = TEXT_HEIGHT_IE;
	    	effectiveTextWidth = TEXT_WIDTH_IE;
	    } else if ("ff".equalsIgnoreCase(browser) || "gecko".equalsIgnoreCase(browser)) {
	    	effectiveHeightOffset = CONTENT_HEIGHT_OFFSET_FF;
	    	effectiveWidthOffset = CONTENT_WIDTH_OFFSET_FF;
	    	effectiveTextHeight = TEXT_HEIGHT_FF;
	    	effectiveTextWidth = TEXT_WIDTH_FF;
	    } else if ("webkit".equalsIgnoreCase(browser) || "safari".equalsIgnoreCase(browser)) {
	    	effectiveHeightOffset = CONTENT_HEIGHT_OFFSET_CHROME;
	    	effectiveWidthOffset = CONTENT_WIDTH_OFFSET_CHROME;
	    	effectiveTextHeight = TEXT_HEIGHT_CHROME;
	    	effectiveTextWidth = TEXT_WIDTH_CHROME;
	    }
	}
	
	public String getText() {
		return contentBox.getText();
	}
	
	public void setText(String text) {
		contentBox.setText(text);
	}
	
	public void initFocus() {
		contentBox.setFocus(true);
	}
	
	protected Textbox createContentBox() {
	    return new Textbox();
    }

}
