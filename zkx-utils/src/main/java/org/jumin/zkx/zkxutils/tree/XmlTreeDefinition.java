/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils.tree;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jumin.common.xmlutils.XmlUtils;
import org.jumin.common.xsutils.model.JrxElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * The class <code>XmlTreeDefinition</code> defines the message tree definition.
 * 
 */
public class XmlTreeDefinition implements Serializable {

    private static final long serialVersionUID = 8263556743665324701L;

    private static Logger log = LoggerFactory.getLogger(XmlTreeDefinition.class);

    public static final String TAG_ALTERNATIVE_LOCATION = "alternativeLocation";
    public static final String TAG_NAMESPACE = "namespace";
    public static final String TAG_NODE_DEFAULT = "nodeDefault";
    public static final String TAG_ROOT_DEFINITION = "rootDefinition";
    public static final String TAG_STRUCTURE_DEFINITION = "structureDefinition";

    public static final String ATTRIBUTE_CONTEXT = "context";
    public static final String ATTRIBUTE_DEFAULT_VALUE = "defaultValue";
    public static final String ATTRIBUTE_JRX_CHILD_NODE_INDEX = "jrxChildNodeIndex";
    public static final String ATTRIBUTE_LABEL_KEY = "labelKey";
    public static final String ATTRIBUTE_NAMESPACE_PREFIX = "namespacePrefix";
    public static final String ATTRIBUTE_NAMESPACE_URI = "namespaceUri";
    public static final String ATTRIBUTE_OPEN = "open";
    public static final String ATTRIBUTE_OPTIONAL = "optional";
    public static final String ATTRIBUTE_READONLY = "readonly";
    public static final String ATTRIBUTE_SUBCRIBE_EVENT = "subscribeEvent";
    public static final String ATTRIBUTE_VISIBLE = "visible";
    public static final String ATTRIBUTE_XPATH = "xpath";
    
    private List<XmlTreeRootDefinition> rootDefinitionList;

    private List<XmlTreeNodeDefault> nodeDefaultDefinitionList;
    
    private Map<String, String> namespaceMap = new HashMap<String, String>();

    private Map<String, String> namespaceMapIndexedByUri = new HashMap<String, String>();

    public XmlTreeDefinition() {
        super();
        rootDefinitionList = new ArrayList<XmlTreeRootDefinition>();
        nodeDefaultDefinitionList = new ArrayList<XmlTreeNodeDefault>();
    }

    public List<JrxElement> getJrxRootElementList() {
        List<JrxElement> jrxElementList = new ArrayList<JrxElement>();

        for (int i = 0; i < rootDefinitionList.size(); i++) {
        	if (rootDefinitionList.get(i).getJrxElement() == null) 
        		continue;
        	
            jrxElementList.add(rootDefinitionList.get(i).getJrxElement());
        }

        return jrxElementList;
    }

    public List<XmlTreeRootDefinition> getRootDefinitionList() {
        return rootDefinitionList;
    }

    public void setNodeDefaultDefinitionList(List<XmlTreeNodeDefault> nodeDefaultDefinitionSet) {
        this.nodeDefaultDefinitionList = nodeDefaultDefinitionSet;
    }

    public List<XmlTreeNodeDefault> getNodeDefaultDefinitionList() {
        return nodeDefaultDefinitionList;
    }

    public boolean isRootElementValid() {
        for (XmlTreeRootDefinition rootDef : rootDefinitionList) {
            if (rootDef.getJrxElement() == null && !rootDef.isOptional()) {
        		return false;
            }
        }
        return true;
    }

    public void cleanInvalidRootElement() {
        int i = 0;
        while (i < rootDefinitionList.size()) {
            XmlTreeRootDefinition rootDef = rootDefinitionList.get(i);
            if (rootDef.getJrxElement() == null) {
                rootDefinitionList.remove(rootDef);
            } else {
                i++;
            }
        }
    }

    public XmlTreeRootDefinition getRootDefinition(JrxElement jrxElement) {
        if (jrxElement == null) {
            return null;
        }
        for (int i = 0; i < rootDefinitionList.size(); i++) {
            XmlTreeRootDefinition rootDefinition = rootDefinitionList.get(i);
            if (rootDefinition.getJrxElement() != null && jrxElement.equals(rootDefinition.getJrxElement())) {
                return rootDefinition;
            }
        }

        return null;
    }

    public XmlTreeNodeDefault[] getNodeDefaultDefinitions(JrxElement jrxElement) {
        if (jrxElement == null) {
            return null;
        }
        List<XmlTreeNodeDefault> nodeDefaultList = new ArrayList<XmlTreeNodeDefault>();
        
        for (Iterator<XmlTreeNodeDefault> it = nodeDefaultDefinitionList.iterator(); it.hasNext();) {
            XmlTreeNodeDefault nodeDefaultDefinition = it.next();
            if (nodeDefaultDefinition.includeJrxTerm(jrxElement)) {
            	nodeDefaultList.add(nodeDefaultDefinition);
            }
        }

        return nodeDefaultList.toArray(new XmlTreeNodeDefault[] {});
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int totalHashCode = 1;

        totalHashCode = prime * totalHashCode + ((rootDefinitionList == null) ? 0 : rootDefinitionList.hashCode());
        totalHashCode = prime * totalHashCode + ((nodeDefaultDefinitionList == null) ? 0 : nodeDefaultDefinitionList.hashCode());

        return totalHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof XmlTreeDefinition))
            return false;

        final XmlTreeDefinition other = (XmlTreeDefinition) obj;

        if (rootDefinitionList == null) {
            if (other.rootDefinitionList != null)
                return false;
        } else if (!rootDefinitionList.equals(other.rootDefinitionList)) {
            return false;
        }

        if (nodeDefaultDefinitionList == null) {
            if (other.nodeDefaultDefinitionList != null)
                return false;
        } else if (!nodeDefaultDefinitionList.equals(other.nodeDefaultDefinitionList)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append(rootDefinitionList);
        sb.append(nodeDefaultDefinitionList);

        return sb.toString();
    }

    public static XmlTreeDefinition load(InputStream xmlStream, String context) throws Exception {
        return load(XmlUtils.createDocument(xmlStream), context);
    }

    public static XmlTreeDefinition load(String xmlString, String context) throws Exception {
        return load(XmlUtils.createDocument(xmlString), context);
    }

    public static XmlTreeDefinition load(Document xmlDoc, String context) throws Exception {
        XmlTreeDefinition ins = new XmlTreeDefinition();

        ins.namespaceMap = loadNamespaces(xmlDoc, context);
        for (Entry<String, String> entry : ins.namespaceMap.entrySet()) {
        	ins.namespaceMapIndexedByUri.put(entry.getValue(), entry.getKey());
        }
        
        ins.rootDefinitionList = loadRootDefinition(xmlDoc, context, ins.namespaceMap);
        ins.nodeDefaultDefinitionList = loadNodeDefaultDefinition(xmlDoc, context, ins.namespaceMap);

        return ins;
    }

    private static List<XmlTreeRootDefinition> loadRootDefinition(Document xmlDoc, String context, Map<String, String> namespaceMap)
                    throws Exception {
        List<XmlTreeRootDefinition> rootDefList = new ArrayList<XmlTreeRootDefinition>();

        NodeList xmlNodeList = XmlUtils.getNodeListByXPath(xmlDoc, "//" + TAG_STRUCTURE_DEFINITION + "[@context='" + context + "']/"
                        + TAG_ROOT_DEFINITION);
        for (int i = 0; i < xmlNodeList.getLength(); i++) {
            Element xmlNode = (Element) xmlNodeList.item(i);

            XmlTreeRootDefinition rootDef = new XmlTreeRootDefinition();
            rootDef.setLabelKey(xmlNode.getAttribute(ATTRIBUTE_LABEL_KEY));
            rootDef.setOptional(getBooleanAttribute(xmlNode, ATTRIBUTE_OPTIONAL, new Boolean(false)));
            rootDef.addXpath(xmlNode.getAttribute(ATTRIBUTE_XPATH));
            rootDef.addXpath(retrieveAlternativeLocation(xmlNode));

            rootDef.setNamespacePrefixes(loadElementNamespace(xmlNode, rootDef.getXpath(), namespaceMap));

            rootDefList.add(rootDef);
        }

        return rootDefList;
    }

    private static String[] retrieveAlternativeLocation(Element xmlNode) {
        List<String> alternativeLocation = new ArrayList<String>();

        NodeList xmlAlternativeLocationNodeList;
        try {
            xmlAlternativeLocationNodeList = XmlUtils.getNodeListByXPath(xmlNode, "./" + TAG_ALTERNATIVE_LOCATION);
            for (int i = 0; i < xmlAlternativeLocationNodeList.getLength(); i++) {
                Element xmlAlternativeLocationNode = (Element) xmlAlternativeLocationNodeList.item(i);
                String xpath = xmlAlternativeLocationNode.getAttribute(ATTRIBUTE_XPATH);
                if (!alternativeLocation.contains(xpath)) {
                    alternativeLocation.add(xpath);
                }
            }
        } catch (Exception e) {
            log.error("Failure in retrieving alternative location", e);
        }

        return alternativeLocation.toArray(new String[] {});
    }

    private static List<XmlTreeNodeDefault> loadNodeDefaultDefinition(Document xmlDoc, String context, Map<String, String> namespaceMap)
                    throws Exception {
        List<XmlTreeNodeDefault> nodeDefaultList = new ArrayList<XmlTreeNodeDefault>();

        NodeList xmlNodeList = XmlUtils.getNodeListByXPath(xmlDoc, "//" + TAG_STRUCTURE_DEFINITION + "[@context='" + context + "']/"
                        + TAG_NODE_DEFAULT);
        for (int i = 0; i < xmlNodeList.getLength(); i++) {
            Element xmlNode = (Element) xmlNodeList.item(i);

            XmlTreeNodeDefault nodeDefaultDef = new XmlTreeNodeDefault();
            nodeDefaultDef.setDefaultValue(xmlNode.getAttribute(ATTRIBUTE_DEFAULT_VALUE));
            nodeDefaultDef.setOpen(getBooleanAttribute(xmlNode, ATTRIBUTE_OPEN, null));
            nodeDefaultDef.setVisible(getBooleanAttribute(xmlNode, ATTRIBUTE_VISIBLE, null));
            nodeDefaultDef.setReadonly(getBooleanAttribute(xmlNode, ATTRIBUTE_READONLY, null));
            
            nodeDefaultDef.setSubscribeEvent(xmlNode.getAttribute(ATTRIBUTE_SUBCRIBE_EVENT));

            int intValue = -1;
            String stringValue = xmlNode.getAttribute(ATTRIBUTE_JRX_CHILD_NODE_INDEX);
            if (!stringValue.equals("")) {
            	try {
            		intValue = Integer.parseInt(stringValue);
            	} catch (NumberFormatException e) {
					intValue = -1;
				}
            }
            nodeDefaultDef.setJrxChildNodeIndex(intValue);

            nodeDefaultDef.addXpath(xmlNode.getAttribute(ATTRIBUTE_XPATH));
            nodeDefaultDef.addXpath(retrieveAlternativeLocation(xmlNode));

            nodeDefaultDef.setNamespacePrefixes(loadElementNamespace(xmlNode, nodeDefaultDef.getXpath(), namespaceMap));

            nodeDefaultList.add(nodeDefaultDef);
        }

        return nodeDefaultList;
    }
    
    public static XmlTreeNodeDefault createNodeDefaultFromXml(Element xmlElement, Map<String, String> namespaceMap) {
    	XmlTreeNodeDefault nodeDefaultDef = createNodeDefaultFromXml(xmlElement);
    	
    	return setNamespacePrefix(nodeDefaultDef, xmlElement, namespaceMap);
    }
    
    public static XmlTreeNodeDefault createNodeDefaultFromXml(Element xmlElement) {
    	XmlTreeNodeDefault nodeDefaultDef = new XmlTreeNodeDefault();
        nodeDefaultDef.setDefaultValue(xmlElement.getAttribute(ATTRIBUTE_DEFAULT_VALUE));
        nodeDefaultDef.setOpen(getBooleanAttribute(xmlElement, ATTRIBUTE_OPEN, null));
        nodeDefaultDef.setVisible(getBooleanAttribute(xmlElement, ATTRIBUTE_VISIBLE, null));
        nodeDefaultDef.setReadonly(getBooleanAttribute(xmlElement, ATTRIBUTE_READONLY, null));
        
        nodeDefaultDef.setSubscribeEvent(xmlElement.getAttribute(ATTRIBUTE_SUBCRIBE_EVENT));

        int intValue = -1;
        String stringValue = xmlElement.getAttribute(ATTRIBUTE_JRX_CHILD_NODE_INDEX);
        if (!stringValue.equals("")) {
        	try {
        		intValue = Integer.parseInt(stringValue);
        	} catch (NumberFormatException e) {
				intValue = -1;
			}
        }
        nodeDefaultDef.setJrxChildNodeIndex(intValue);

        nodeDefaultDef.addXpath(xmlElement.getAttribute(ATTRIBUTE_XPATH));
        nodeDefaultDef.addXpath(retrieveAlternativeLocation(xmlElement));

        return nodeDefaultDef;
    }
    
    public static Element createXmlFromNodeDefault(XmlTreeNodeDefault nodeDefaultDef, Element xmlParentElement) {
    	Element xmlElement = XmlUtils.createSubElement(xmlParentElement, TAG_NODE_DEFAULT);
    	if (nodeDefaultDef.getDefaultValue() != null)
    		xmlElement.setAttribute(ATTRIBUTE_DEFAULT_VALUE, nodeDefaultDef.getDefaultValue());
    	
    	if (nodeDefaultDef.isOpen() != null) 
    		xmlElement.setAttribute(ATTRIBUTE_OPEN, "" + nodeDefaultDef.isOpen());
    	
    	if (nodeDefaultDef.isVisible() != null) 
    		xmlElement.setAttribute(ATTRIBUTE_VISIBLE, "" + nodeDefaultDef.isVisible());
    	
    	if (nodeDefaultDef.isReadonly() != null) 
    		xmlElement.setAttribute(ATTRIBUTE_READONLY, "" + nodeDefaultDef.isReadonly());
    	
    	if (nodeDefaultDef.getSubscribeEvent() != null) 
    		xmlElement.setAttribute(ATTRIBUTE_SUBCRIBE_EVENT, nodeDefaultDef.getSubscribeEvent());

    	if (nodeDefaultDef.getJrxChildNodeIndex() >= 0)
    		xmlElement.setAttribute(ATTRIBUTE_JRX_CHILD_NODE_INDEX, Integer.toString(nodeDefaultDef.getJrxChildNodeIndex()));

    	String[] xpaths = nodeDefaultDef.getXpaths();
    	if (xpaths != null && xpaths.length > 0) {
    		xmlElement.setAttribute(ATTRIBUTE_XPATH, xpaths[0]);
    		if (xpaths.length > 1) {
    			for (int i = 1; i < xpaths.length; i++) {
    				Element xmlAlternativeLocationNode = XmlUtils.createSubElement(xmlElement, TAG_ALTERNATIVE_LOCATION);
    				xmlAlternativeLocationNode.setAttribute(ATTRIBUTE_XPATH, xpaths[i]);
    			}
    		}
    	}

        return xmlElement;
    }
    
    public static XmlTreeNodeDefault setNamespacePrefix(XmlTreeNodeDefault nodeDefaultDef, Element xmlElement, Map<String, String> namespaceMap) {
    	nodeDefaultDef.setNamespacePrefixes(loadElementNamespace(xmlElement, nodeDefaultDef.getXpath(), namespaceMap));
    	
    	return nodeDefaultDef;
    }

    private static String[][] loadElementNamespace(Element xmlElement, String xpath, Map<String, String> namespaceMap) {
        String namespacePrefixDef = xmlElement.getAttribute(ATTRIBUTE_NAMESPACE_PREFIX);
        String[][] namespaceDefArray = buildNamespaceArray(namespacePrefixDef, namespaceMap);

        String[][] xpathNamespaceDefArray = buildNamespaceArrayFromXpath(xpath, namespaceMap);

        if (namespacePrefixDef.equals("")) {
            // Just take the namespace prefix from xpath
            namespaceDefArray = xpathNamespaceDefArray;
        } else {
            // Additionally perform merging from XPath definition
            namespaceDefArray = mergeNamespaceDefinitionArray(namespaceDefArray, xpathNamespaceDefArray);
        }
        return namespaceDefArray;
    }

    protected static String[][] buildNamespaceArray(String namespacePrefixes, Map<String, String> namespaceMap) {
        List<String[]> namespaceDefList = new ArrayList<String[]>();
        Set<String> definedPrefixSet = new HashSet<String>();
        if (!namespacePrefixes.equals("")) {
            String[] namespacePrefixArray = namespacePrefixes.split(",");
            for (int j = 0; j < namespacePrefixArray.length; j++) {
                String namespacePrefix = namespacePrefixArray[j].trim();
                String namespaceUri = namespaceMap.get(namespacePrefix);
                if (namespaceUri != null) {
                    String[] namespaceDef = new String[] { namespacePrefix, namespaceUri };
                    if (namespaceDef.length > 0 && !definedPrefixSet.contains(namespaceDef[0])) {
                        definedPrefixSet.add(namespaceDef[0]);
                        namespaceDefList.add(namespaceDef);
                    }
                }
            }
        }

        return namespaceDefList.toArray(new String[][] { {} });
    }

    protected static String[][] buildNamespaceArrayFromXpath(String xpath, Map<String, String> namespaceMap) {
        List<String> namespacePrefixList = collectPrefixFromXpath(xpath);

        String[][] namespaceDefArray = new String[namespacePrefixList.size()][2];

        for (int i = 0; i < namespacePrefixList.size(); i++) {
            String namespacePrefix = namespacePrefixList.get(i);
            String[] namespaceDef = new String[] { namespacePrefix, namespaceMap.get(namespacePrefix) };
            namespaceDefArray[i] = namespaceDef;
        }

        return namespaceDefArray;
    }

    public static List<String> collectPrefixFromXpath(String xpath) {
        List<String> namespacePrefixList = new ArrayList<String>();
        String[] elementDefArray = StringUtils.split(xpath, "/");
        for (int i = 0; i < elementDefArray.length; i++) {
            String elementDef = elementDefArray[i];
            if (elementDef.equals("") || !elementDef.contains(":"))
                continue;

            if (elementDef.contains("["))
                elementDef = elementDef.substring(0, elementDef.indexOf('[')); // cut before xpath condition

            if (elementDef.contains("@")) {
                // Attribute name is involved
                elementDef = elementDef.substring(elementDef.indexOf('@') + 1);
            }
            String prefix = elementDef.substring(0, elementDef.indexOf(':'));
            if (!namespacePrefixList.contains(prefix)) {
                namespacePrefixList.add(prefix);
            }
        }

        return namespacePrefixList;
    }

    private static String[][] mergeNamespaceDefinitionArray(String[][] array1, String[][] array2) {
        List<String[]> result = new ArrayList<String[]>();
        for (int i = 0; i < array1.length; i++) {
            result.add(array1[i]);
        }

        for (int i = 0; i < array2.length; i++) {
            String prefix = array2[i][0];
            if (!isNamespaceDefined(prefix, array1)) {
                result.add(array2[i]);
            }
        }

        return result.toArray(new String[][] { {} });
    }

    private static boolean isNamespaceDefined(String prefix, String[][] namespaceDefArray) {
        for (int i = 0; i < namespaceDefArray.length; i++) {
            if (namespaceDefArray[i].length > 0 && prefix.equals(namespaceDefArray[i][0])) {
                return true;
            }
        }
        return false;
    }

    private static Map<String, String> loadNamespaces(Document xmlDoc, String context) throws Exception {
        Map<String, String> namespaceMap = new HashMap<String, String>();

        NodeList xmlNodeList = XmlUtils.getNodeListByXPath(xmlDoc, "//" + TAG_STRUCTURE_DEFINITION + "[@context='" + context + "']/" + TAG_NAMESPACE);
        for (int i = 0; i < xmlNodeList.getLength(); i++) {
            Element xmlElement = (Element) xmlNodeList.item(i);
            String prefix = xmlElement.getAttribute(ATTRIBUTE_NAMESPACE_PREFIX);
            String uri = xmlElement.getAttribute(ATTRIBUTE_NAMESPACE_URI);
            if (!prefix.equals("")) {
                namespaceMap.put(prefix, uri);
            }
        }

        return namespaceMap;
    }

	public Map<String, String> getNamespaceMap() {
	    return namespaceMap;
    }

	public Map<String, String> getNamespaceMapIndexedByUri() {
	    return namespaceMapIndexedByUri;
    }

	public XmlTreeDefinition clone() {
		XmlTreeDefinition clone = new XmlTreeDefinition();
		
		// Clone Namespace
		clone.namespaceMap = this.namespaceMap;
		
		// Clone Root Node Definition
		for (XmlTreeRootDefinition rootDef : rootDefinitionList) {
			XmlTreeRootDefinition rootDefClone = rootDef.clone();
			clone.getRootDefinitionList().add(rootDefClone);
		}
		
		// Clone Node Default Definition
		for (XmlTreeNodeDefault nodeDefaultDef : nodeDefaultDefinitionList) {
			XmlTreeNodeDefault nodeDefaultDefClone = nodeDefaultDef.clone();
			clone.getNodeDefaultDefinitionList().add(nodeDefaultDefClone);
		}
		
		return clone;
	}
	
	protected static Boolean getBooleanAttribute(Element xmlElement, String attributeName, Boolean defaultValue) {
		String stringValue = xmlElement.getAttribute(attributeName);

		if (stringValue == null || stringValue.equals(""))
			return defaultValue;

		try {
			return Boolean.valueOf(stringValue);
		} catch (Exception e) {
			// do nothing, let the default value returns.
		}

		return defaultValue;
	}

}
