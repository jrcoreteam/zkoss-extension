/*
 * Copyright (c) 2016-2018 Jumin Rubin
 * LinkedIn: https://www.linkedin.com/in/juminrubin/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jumin.zkx.zkxutils;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;

import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Separator;
import org.zkoss.zul.Space;
import org.zkoss.zul.Window;

public class MessageDialog extends Window {

    private static final long serialVersionUID = 3445730103658859228L;

    public static final String ON_ABORT = "onAbort";

    public static final String ON_NO = "onNo";

    public static final String ON_RETRY = "onRetry";

    public static final String ON_YES = "onYes";

    public static final ButtonModel OK = new ButtonModel("general.ok", "OK", Events.ON_OK);

    public static final ButtonModel CANCEL = new ButtonModel("general.cancel", "Cancel", Events.ON_CANCEL);

    public static final ButtonModel ABORT = new ButtonModel("general.abort", "Abort", ON_ABORT);

    public static final ButtonModel YES = new ButtonModel("general.yes", "Yes", ON_YES);

    public static final ButtonModel NO = new ButtonModel("general.no", "No", ON_NO);

    // Typical Composites
    public static final ButtonModel[] YES_NO = new ButtonModel[] { YES, NO };

    public static final ButtonModel[] OK_CANCEL = new ButtonModel[] { OK, CANCEL };

    public final static String ON_BUTTON_CLICKED = "onButtonClicked";

    private Label messageLabel;

    private Caption titleCaption;

    private Table buttonContainer;

    private Div image;

    private Map<ButtonModel, Button> buttonMap = new Hashtable<ButtonModel, Button>();

    public MessageDialog() {
        super();
        createContents();
        initProperties();
    }

    private void clearButtons() {
        while (buttonContainer.getFirstChild() != null) {
            buttonContainer.removeChild(buttonContainer.getFirstChild());
        }
    }

    protected void createContents() {
        titleCaption = new Caption();
        titleCaption.setParent(this);

        Hlayout layout = new Hlayout();
        layout.setStyle("margin: 10px 4px 4px;");
        layout.setSpacing("10px");
        layout.setParent(this);

        image = new Div();
        image.setWidth("36px");
        image.setClass(Messagebox.INFORMATION);
        image.setParent(layout);

        Div textContainer = new Div();
        textContainer.setParent(layout);
        textContainer.setHflex("true");
        textContainer.setStyle("margin-top:4px");

        messageLabel = new Label();
        messageLabel.setParent(textContainer);

        Separator separator = new Separator();
        separator.setBar(true);
        separator.setSpacing("8px");
        appendChild(separator);

        buttonContainer = new Table();
        buttonContainer.setParent(this);
    }

    protected void initProperties() {
        setSclass("zkx-message-dialog");
        setBorder("normal");
        setClosable(true);

        messageLabel.setMultiline(true);
        messageLabel.setStyle("word-wrap: word-break;");
        messageLabel.setHflex("true");

        buttonContainer.setDynamicProperty("style", "width: 100%;");

        setButtons(null, null, new ButtonModel[] { OK });
    }

    private void renderButtons(Component parent, ButtonModel[] buttonModels, String style,
            Map<ButtonModel, EventListener<Event>> eventListenerMap) {
        for (ButtonModel buttonModel : buttonModels) {
            if (buttonModel != null) {
                Button button = new Button(
                        ZkLabels.getLabel(buttonModel.getLabelKey(), buttonModel.getDefaultLabel()));
                button.setStyle(style);
//                button.setSclass(AdditionalStyleConstants.TEXT_BUTTON);
                button.setParent(parent);
                button.setAttribute("model", buttonModel);

                EventListener<Event> eventListener = null;
                if (eventListenerMap != null) {
                    eventListener = eventListenerMap.get(buttonModel);
                }
                if (eventListener == null) {
                    button.addForward(Events.ON_CLICK, this, ON_BUTTON_CLICKED, buttonModel.getEventName());
                } else {
                    button.addEventListener(Events.ON_CLICK, eventListener);
                }
                if (buttonModel.isCloseDialogOnClick()) {
                    button.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            setVisible(false);
                        }
                    });
                }

                buttonMap.put(buttonModel, button);
            } else {
                Space space = new Space();
                space.setSpacing("70px");
                space.setParent(parent);
            }
        }
    }

    public void setButtons(ButtonModel[] leftSideButtons, ButtonModel[] middlePartButtons,
            ButtonModel[] rightSideButtons) {
        setButtons(leftSideButtons, middlePartButtons, rightSideButtons, null);
    }

    public void setButtons(ButtonModel[] leftSideButtons, ButtonModel[] middlePartButtons,
            ButtonModel[] rightSideButtons, Map<ButtonModel, EventListener<Event>> eventListenerMap) {
        clearButtons();

        Tr row = new Tr();
        row.setParent(buttonContainer);

        if (leftSideButtons != null && leftSideButtons.length > 0) {
            Td leftCell = new Td();
            leftCell.setDynamicProperty("align", "left");
            leftCell.setParent(row);

            renderButtons(leftCell, leftSideButtons, "margin-right: 10px;", eventListenerMap);
        }

        if (middlePartButtons != null && middlePartButtons.length > 0) {
            Td middleCell = new Td();
            middleCell.setDynamicProperty("align", "center");
            middleCell.setParent(row);

            renderButtons(middleCell, middlePartButtons, "margin-right: 10px;", eventListenerMap);
        }

        if (rightSideButtons != null && rightSideButtons.length > 0) {
            Td rightCell = new Td();
            rightCell.setDynamicProperty("align", "right");
            rightCell.setParent(row);

            renderButtons(rightCell, rightSideButtons, "margin-left: 10px;", eventListenerMap);
        }
    }

    public void setButtonFocus(ButtonModel buttonModel) {
        if (buttonModel == null)
            return;

        Button button = buttonMap.get(buttonModel);
        if (button != null)
            button.setFocus(true);
    }

    /**
     * Set INFO, WARNING, or ERROR icons.
     * 
     * @param icon
     *            Messagebox.INFORMATION, Messagebox.EXCLAMATION, etc
     */
    public void setIcon(String icon) {
        image.setSclass(icon);
    }

    public void setDialogTitleIcon(String src) {
        titleCaption.setImage(src);
        titleCaption.setTop("5px");
    }

    public void setMessage(String msg) {
        messageLabel.setValue(msg);
    }

    protected Button getButton(ButtonModel buttonModel) {
        if (buttonModel == null)
            return null;

        return buttonMap.get(buttonModel);
    }

    public static class ButtonModel implements Serializable {

        private static final long serialVersionUID = -8296944735247206916L;

        private String labelKey;

        private String defaultLabel;

        private String eventName;

        private boolean closeDialogOnClick = true;

        public ButtonModel(String labelKey, String defaultLabel, String eventName) {
            this(labelKey, defaultLabel, eventName, true);
        }

        public ButtonModel(String labelKey, String defaultLabel, String eventName, boolean closeDialogOnClick) {
            super();
            this.labelKey = labelKey;
            this.defaultLabel = defaultLabel;
            this.eventName = eventName;
            this.closeDialogOnClick = closeDialogOnClick;
        }

        public String getLabelKey() {
            return labelKey;
        }

        public String getDefaultLabel() {
            return defaultLabel;
        }

        public String getEventName() {
            return eventName;
        }

        public boolean isCloseDialogOnClick() {
            return closeDialogOnClick;
        }
    }

}
